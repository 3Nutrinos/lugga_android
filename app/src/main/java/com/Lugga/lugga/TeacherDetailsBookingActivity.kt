package com.Lugga.lugga

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.adapters.AdapterTeacherDetailProfile
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.sendRequest.SendRequestResponse
import com.Lugga.lugga.model.teacherdetails.TeacherDetailsBodyItem
import com.Lugga.lugga.model.teacherdetails.TeacherDetailsResponse
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.SocketNetworking
import com.Lugga.lugga.utils.ApiClient
import com.bumptech.glide.Glide
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import io.socket.client.Ack
import io.socket.client.Socket
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class TeacherDetailsBookingActivity : AppCompatActivity() {
    var recyclerView: RecyclerView? = null
    var adapterTeacherDetailProfile: AdapterTeacherDetailProfile? = null
    var imageView: CircleImageView? = null
    var circularimageview: CircleImageView? = null
    var layoutTeacherDetailDuringBooking: RelativeLayout? = null
    var teacherDetailBackPress: ImageView? = null
    var teacherDetailsBodyItemArrayList: ArrayList<TeacherDetailsBodyItem>? = null
    var view: View? = null
    var luggaAPI: LuggaAPI? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var NameteacherDetail: TextView? = null
    var levelTeacherDetail: TextView? = null
    var completedjob: TextView? = null
    var uncompletedjob: TextView? = null
    var rating: TextView? = null
    var teacherDetailRequestSend: TextView? = null
    var teacherId = 0
    var tv_requestSend: TextView? = null
    var socket: Socket? = null
    private var isTeacherRequestSend = false
    private var bookingID = 0
    var feedbackLayout: RelativeLayout? = null
    var rlRequestBooking: RelativeLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.teacher_detail_during_booking)
        initSocket()
        feedbackLayout = findViewById(R.id.feedbackLayout)
        init()
        onClick()
        teacherDetails
        val linearLayoutManager = LinearLayoutManager(BaseApplication.getContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.hasFixedSize()
    }

    private fun init() {
        intent = getIntent()
        bookingID = intent.getIntExtra("bookingID", 0)
        teacherId = intent.getIntExtra("teacherId", 0)
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        recyclerView = findViewById(R.id.teacherRecyclerview)
        imageView = findViewById(R.id.circularImage)
        circularimageview = findViewById(R.id.circularImage)
        NameteacherDetail = findViewById(R.id.nameTeacherDetail)
        levelTeacherDetail = findViewById(R.id.levelTeacherDetail)
        completedjob = findViewById(R.id.teacherDetailCompletedJob)
        uncompletedjob = findViewById(R.id.teacherDetailUncompletedJob)
        rating = findViewById(R.id.teacherDetailRating)
        teacherDetailRequestSend = findViewById(R.id.teacherDetailRequestSend)
        rlRequestBooking = findViewById(R.id.rlRequestBooking)
        teacherDetailBackPress = findViewById(R.id.teacherDetailBackPress)
        layoutTeacherDetailDuringBooking = findViewById(R.id.layoutTeacherDetailDuringBooking)
        teacherDetailBackPress!!.setOnClickListener(View.OnClickListener { onBackPressed() })
    }

    private fun onClick() {
        rlRequestBooking!!.setOnClickListener {
            isTeacherRequestSend = true
            connectSocket()
        }
    }

    private fun initSocket() {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun connectSocket() {
        if (socket!!.connected()) {
            sendRequest()
        }
    }

    private fun sendRequest() {
        val obj = JSONObject()
        try {
            obj.put("bookingId", bookingID)
            obj.put("teacherId", teacherId)
            if (socket!!.connected()) {
                //socket!!.send(obj)
                socket!!.emit("requestTeacher", obj, Ack { })
                requestTeacherResponse
            } else {
                socket!!.connect()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    private val requestTeacherResponse: Unit
        private get() {
            if (socket!!.connected()) {
                socket!!.on("returnRequestLearner$bookingID") { args ->
                    val msgresponse = args[0].toString()
                    val gson = Gson()
                    val chatListResponse = gson.fromJson(msgresponse, SendRequestResponse::class.java)
                    runOnUiThread {
                        startActivity(Intent(BaseApplication.getContext(), HomeActivity::class.java))

                        val jsonObject=JSONObject()
                        jsonObject.put("id", teacherId.toString())
                        jsonObject.put("user_type", "1")
                        socket?.emit("HomepageSocket", jsonObject)
                    }
                }
            } else {
                socket!!.connect()
            }
        }

    private val teacherDetails: Unit
        private get() {
            teacherDetailsBodyItemArrayList = ArrayList()
            val user_id: RequestBody =  RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherId.toString())
            luggaAPI!!.getteacherdetails(user_id).enqueue(object : Callback<TeacherDetailsResponse?> {
                override fun onResponse(call: Call<TeacherDetailsResponse?>, response: Response<TeacherDetailsResponse?>) {
                    if (response.body() != null) {
                        if (response.isSuccessful && response.body()!!.success == 200) {
                            teacherDetailsBodyItemArrayList = response.body()!!.feedback
                            if (teacherDetailsBodyItemArrayList!!.size == 0) {
                                feedbackLayout!!.visibility = View.INVISIBLE
                            } else {
                                feedbackLayout!!.visibility = View.VISIBLE
                            }
                            adapterTeacherDetailProfile = AdapterTeacherDetailProfile(this@TeacherDetailsBookingActivity, teacherDetailsBodyItemArrayList!!)
                            recyclerView!!.adapter = adapterTeacherDetailProfile
                            usersharedPrefernce!!.setlevel(response.body()!!.body!!.get(0).level!!)
                            usersharedPrefernce!!.setcompletedjob(response.body()!!.body!!.get(0).completeBooking!!)
                            usersharedPrefernce!!.setuncompletedjob(response.body()!!.body!!.get(0).experience!!)
                           /// usersharedPrefernce!!.setrating(response.body()!!.body!!.get(0).other_language!!)
                            NameteacherDetail!!.text = response.body()!!.body!!.get(0).name
                            when (response.body()!!.body!!.get(0).level) {
                                1 -> levelTeacherDetail!!.text = "Beginner"
                                2 -> levelTeacherDetail!!.text = "Intermediate"
                                3 -> levelTeacherDetail!!.text = "Advacned"
                                else -> {
                                }
                            }
                            completedjob!!.text = response.body()!!.body!!.get(0).completeBooking.toString()
                            uncompletedjob!!.text =response.body()!!.body!!.get(0).experience.toString()
                            rating!!.text = "" + response.body()!!.body!!.get(0).other_language
                            if (response.body()!!.body!!.get(0).profileImage != null) {
                                Glide.with(this@TeacherDetailsBookingActivity).load(ApiClient.IMAGE_URL +response.body()!!.body!!.get(0).profileImage)
                                        .placeholder(R.drawable.person_girl)
                                        .centerCrop().into(imageView!!)
                            } else {
                                imageView!!.setImageResource(R.drawable.person_girl)
                            }

                        }
                    } else {
                        Toast.makeText(BaseApplication.getContext(), "" + response.message(), Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<TeacherDetailsResponse?>, t: Throwable) {
                    Toast.makeText(BaseApplication.getContext(), "" + t.message, Toast.LENGTH_SHORT).show()
                }
            })
        }

    override fun onDestroy() {
        super.onDestroy()
        if (isTeacherRequestSend) {
            if (socket != null && socket!!.connected()) socket!!.disconnect()
        }
    }
}