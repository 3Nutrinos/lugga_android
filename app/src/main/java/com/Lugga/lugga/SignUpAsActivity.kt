package com.Lugga.lugga

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_sign_up_as.*
import kotlinx.android.synthetic.main.activity_sign_up_sign_in.*

class SignUpAsActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_as)

        tvLearnerAs.setOnClickListener(this)
        tvTeacherAs.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v){
            tvLearnerAs->startActivity(Intent(this,LearnerNewSignupActivity::class.java).putExtra("from","learner"))

            tvTeacherAs->startActivity(Intent(this,TeacherNewSignUpActivity::class.java).putExtra("from","teacher"))
        }
    }
}