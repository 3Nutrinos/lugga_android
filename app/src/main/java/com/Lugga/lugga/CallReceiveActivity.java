package com.Lugga.lugga;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

public class CallReceiveActivity extends FragmentActivity {
//
//    ImageView ivRejectCall, ivAcceptCall;
//    String  callerId, receiverId, callerName, userType, callType, senderName, bookingId;
//    URL serverURL;
//    Socket socket;
//    MediaPlayer mMediaPlayer;
//    TextView tvCallerName, tvCallType;
//
//    CountDownTimer countDownTimer;
//    JitsiMeetView view;
//    String channelId="";
//    @Override
//    public void onRequestPermissionsResult(
//            final int requestCode,
//            final String[] permissions,
//            final int[] grantResults) {
//        JitsiMeetActivityDelegate.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_receive);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
//        ivRejectCall = findViewById(R.id.ivRejectCall);
//        tvCallerName = findViewById(R.id.tvCallerName);
//        ivAcceptCall = findViewById(R.id.ivAcceptCall);
//        tvCallType = findViewById(R.id.tvCallType);


//        mMediaPlayer = MediaPlayer.create(this, R.raw.incoming_tone);
//        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//        mMediaPlayer.setLooping(true);
//        mMediaPlayer.start();
//
//
//        try {
//            socket = SocketNetworking.getSocket();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            serverURL = new URL("https://meet.jit.si");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//            throw new RuntimeException("Invalid server URL!");
//        }

//        JitsiMeetConferenceOptions defaultOptions
//                = new JitsiMeetConferenceOptions.Builder()
//                .setServerURL(serverURL)
//                .setWelcomePageEnabled(false)
//                .build();
//        JitsiMeet.setDefaultConferenceOptions(defaultOptions);      // for calling


//        Intent intent = getIntent();
//        if (intent == null) {
//            return;
//        }
//        if (intent.getAction() != null) {
//            intent.getAction().equalsIgnoreCase("calls");
//        }
//        if (intent.getStringExtra("channel_id") != null) {
//            channelId = intent.getStringExtra("channel_id");
//            callerId = intent.getStringExtra("caller_id");
//            receiverId = intent.getStringExtra("receiver_id");
//            callerName = intent.getStringExtra("caller_name");
//            userType = intent.getStringExtra("user_type");
//            callType = intent.getStringExtra("call_type");
//            senderName = intent.getStringExtra("sender_name");
//            bookingId = intent.getStringExtra("booking_id");
//        }
//        notifyToUserByCallStatus();
//        if (callerName != null) {
//            tvCallerName.setText(callerName);
//        }
//        if (callType != null) {
//            if (callType.equalsIgnoreCase("1")) {
//                tvCallType.setText("Audio Call");
//            } else {
//                tvCallType.setText("Vedio Call");
//            }
//        }
//
//        ivAcceptCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mMediaPlayer != null) {
//                    mMediaPlayer.stop();
//                }
//                socket.connect();
//                if (socket.connected()) {
//                    soceketRetuenCallResponseNegative("1");
//                } else {
//                    socket.connect();
//                }
//                if (callType != null) {
//                    if (callType.equalsIgnoreCase("1")) {
//                        tvCallType.setText("Audio Call");
//                        //setRoomForReceiveCallAudio(channelId);
//                    } else {
//                        tvCallType.setText("Video Call");
//                        //setRoomForReceiveCall(channelId);
//                    }
//                }
//                //accept the call
//                cancelNotification(CallReceiveActivity.this, 12);
//
//            }
//        });
//
//        ivRejectCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cancelNotification(CallReceiveActivity.this, 12);
//                if (mMediaPlayer != null) {
//                    mMediaPlayer.stop();
//                }
//                try {
//                    socket = SocketNetworking.getSocket();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                socket.connect();
//                if (socket.connected()) {
//                    soceketRetuenCallResponseNegative("3");
//                } else {
//                    socket.connect();
//                }
//
//            }
//        });
//
//    }
//    @Override
//    public void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        JitsiMeetActivityDelegate.onNewIntent(intent);
//    }
//    @Override
//    protected void onStop() {
//        super.onStop();
//
//        JitsiMeetActivityDelegate.onHostPause(this);
//    }
//    private void setRoomForReceiveCallAudio(String channelId) {
//        JitsiMeetUserInfo info = new JitsiMeetUserInfo();
//        info.setDisplayName(callerName);
//        JitsiMeetConferenceOptions options
//                = new JitsiMeetConferenceOptions.Builder()
//                .setRoom(channelId)
//                .setAudioOnly(true)
//                .setUserInfo(info)
//                .setSubject(callerName)
//                .build();
//
//        //  JitsiMeetActivity.launch(this, options);
//        view = new JitsiMeetView(CallReceiveActivity.this);
//
//        view.setListener(new JitsiMeetViewListener() {
//            @Override
//            public void onConferenceJoined(Map<String, Object> map) {
//                Log.e("yes", "joined");
//                if (countDownTimer != null) {
//                    countDownTimer.cancel();
//                }
//            }
//
//            @Override
//            public void onConferenceTerminated(Map<String, Object> map) {
//                soceketRetuenCallResponseNegative("2");
//
//
//            }
//
//            @Override
//            public void onConferenceWillJoin(Map<String, Object> map) {
//                Log.e("yes", "willjoin");
//            }
//        });
//
//        view.join(options);
//        setContentView(view);
//
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        view = new JitsiMeetView(CallReceiveActivity.this);
//        JitsiMeetActivityDelegate.onHostResume(this);
//
//    }
//
//    private void soceketRetuenCallResponseNegative(String status) {
//        JSONObject obj = new JSONObject();
//
//        try {
//            obj.put("user_type", userType);
//            obj.put("call_type", callType);
//            obj.put("caller_id", callerId);
//            obj.put("receiever_id", receiverId);
//            obj.put("booking_id", bookingId);
//            obj.put("time_count", "0");
//            obj.put("status", status);
//            obj.put("endCall", "receiver");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        socket.send(obj);
//        Log.e("objSend", String.valueOf(obj));
//        socket.emit("callStatusRequest", obj, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//            }
//        });
//        // finish();
//        socketCutCallResponse();
//
//
//    }
//
//    private void socketCutCallResponse() {
//        if (socket.connected()) {
//            socket.on("returncallStatusRequest" + bookingId, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Message msg = new Message();
//                    JSONObject jobj = (JSONObject) args[0];
//                    msg.obj = jobj;
//                    try {
//                        if (jobj.getString("status") != null) {
//                            if (jobj.getString("status").equalsIgnoreCase("1")) {
//                                if (callType != null) {
//                                    if (callType.equalsIgnoreCase("1")) {
//                                        callAcceptHandler.sendMessage(msg);
//
//                                    } else {
//                                        callAcceptVedioHandler.sendMessage(msg);
//
//                                    }
//                                }
//
//                            } else if (jobj.getString("status").equalsIgnoreCase("2")) {
//                                handler_getdta.sendMessage(msg);
//
//                            } else if (jobj.getString("status").equalsIgnoreCase("3")) {
//                                finish();
//                            }
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            });
//        } else {
//            //Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show();
//            socket.connect();
//        }
//    }
//
//
//    private void setRoomForReceiveCall(String channel_id) {
//        JitsiMeetUserInfo info = new JitsiMeetUserInfo();
//        info.setDisplayName(callerName);
//        JitsiMeetConferenceOptions options
//                = new JitsiMeetConferenceOptions.Builder()
//                .setRoom(channel_id)
//                .setUserInfo(info)
//                .setAudioOnly(false)
//                .setSubject(callerName)
//                .build();
//
//        //  JitsiMeetActivity.launch(this, options);
//        view = new JitsiMeetView(CallReceiveActivity.this);
//        view.setListener(new JitsiMeetViewListener() {
//            @Override
//            public void onConferenceJoined(Map<String, Object> map) {
//                Log.e("yes", "joined");
//                if (countDownTimer != null) {
//                    countDownTimer.cancel();
//                }
//            }
//
//            @Override
//            public void onConferenceTerminated(Map<String, Object> map) {
//                soceketRetuenCallResponseNegative("2");
//
//
//            }
//
//            @Override
//            public void onConferenceWillJoin(Map<String, Object> map) {
//
//                Log.e("yes", "willjoin");
//            }
//        });
//
//        view.join(options);
//        setContentView(view);
//
//    }
//
//
//    public void OnClickAccept(View view) {
//
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (mMediaPlayer != null) {
//            mMediaPlayer.stop();
//        }
//        if (view != null) {
//            view.leave();
//            view.dispose();
//            view = null;
//        }
//
////
////        if (AudioModeModule.useConnectionService()) {
////            ConnectionService.abortConnections();
////        }
////        JitsiMeetOngoingConferenceService.abort(this);
//        JitsiMeetActivityDelegate.onHostDestroy(CallReceiveActivity.this);
//        socket.disconnect();
//    }
//
//    public static void cancelNotification(Context ctx, int notifyId) {
//        String ns = Context.NOTIFICATION_SERVICE;
//        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
//        nMgr.cancel(notifyId);
//    }
//
//
//    public void notifyToUserByCallStatus() {
//
//        if (socket.connected()) {
//            Log.e("book", bookingId);
//            socket.on("returncallStatusRequest" + bookingId, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Message msg = new Message();
//                    JSONObject jobj = (JSONObject) args[0];
//                    msg.obj = jobj;
//                    try {
//                        if (jobj.getString("status") != null) {
//                            if (jobj.getString("status").equalsIgnoreCase("2")) {
//                                handler_getdta.sendMessage(msg);
//                            }
//
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    {
//
//                    }
//
//
//                }
//            });
//
//        } else {
//            socket.connect();
//            //   Toast.makeText(this, "Socket was not connected down", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    @SuppressLint("HandlerLeak")
//    private Handler handler_getdta = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    if (view != null) {
//                        Toast.makeText(CallReceiveActivity.this, "Call Ended", Toast.LENGTH_SHORT).show();
//                        view.leave();
//                        view.dispose();
//                        view = null;
//                        startActivity(new Intent(CallReceiveActivity.this, HomeActivity.class));
//                        finish();
//
//                    }
//
//
//                }
//            });
//
//        }
//    };
//
//    @SuppressLint("HandlerLeak")
//    private Handler callAcceptHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    setRoomForReceiveCallAudio(channelId);
//                }
//            });
//
//        }
//    };
//
//    @SuppressLint("HandlerLeak")
//    private Handler callAcceptVedioHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    setRoomForReceiveCall(channelId);
//
//                }
//            });
//
//        }
//    };
//
//
//
    }
}
