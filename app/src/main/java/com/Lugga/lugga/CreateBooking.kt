package com.Lugga.lugga

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.adapters.AllLanguageAdapter
import com.Lugga.lugga.adapters.CreateLanguageAdapter
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne
import com.Lugga.lugga.model.searchteacher.SearchTeacherBodyItem
import com.Lugga.lugga.model.searchteacher.SearchTeacherResponse
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.SocketNetworking
import com.Lugga.lugga.utils.ApiClient
import com.google.gson.Gson
import io.socket.client.Ack
import io.socket.client.Socket
import kotlinx.android.synthetic.main.create_booking.*
import kotlinx.android.synthetic.main.language_item.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CreateBooking : AppCompatActivity() {
    private var selectedOtherLanguage:String?=""
    private var ratingFilter: String? = null
    private var pricePerHr: String? = null
    var sessonlist: ArrayList<String>? = null
    var levellist: ArrayList<String>? = null
    var adapter: ArrayAdapter<String>? = null

    // var spinnerLearnLanguage: Spinner? = null
    var spinnerSession: Spinner? = null
    var spinnerLevel: Spinner? = null
    var currentBooking: RadioButton? = null
    var scheduleBooking: RadioButton? = null
    var radioGroupBooking: RadioGroup? = null
    var searchTeacher: TextView? = null
    var createBookingBackPress: ImageView? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var luggaAPI: LuggaAPI? = null
    var countradio = 0
    var sessionStartTime: String? = null
    var sessionEndTime: String? = null
    var startBookingSession: String? = null
    var EndBookingsSession: String? = null
    var postime = 0
    var poslevel = 0
    var socket: Socket? = null
    var searchTeacherBodyItemArrayList: ArrayList<SearchTeacherBodyItem>? = null
    var level: String? = null
    var time: String? = null
    var isSocketConnected = false
    var day: String? = null
    var month: String? = null
    var yearr: String? = null
    private var monthss: String? = null
    private var sendScheduleDate: String? = null
    private var dayss: String? = null
    private val value: String? = null
    private var allLanguageAdapter: CreateLanguageAdapter? = null
    private var selectedLanguage: String? = null
    var tvScheduleBooking: TextView? = null
    var tvCurrentBooking: TextView? = null
    var tvTimeSchedule: TextView? = null
    private var millisToAdd: Long = 0
    private val timeZone: String? = null
    private var second: String? = null
    private var minutes: String? = null
    private var endsecond: String? = null
    private var Endminutes: String? = null
    private var finalHour: String? = null
    private var finalMin: String? = null
    var progressBar: ProgressBar? = null
    private var endhour: String? = null
    private var starthour: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_booking)

        tvCurrentBooking = findViewById(R.id.tvCurrentBooking)
        tvScheduleBooking = findViewById(R.id.tvScheduleBooking)
        tvTimeSchedule = findViewById(R.id.tvTimeSchedule)
        progressBar = findViewById(R.id.progressBar)
        searchTeacher = findViewById(R.id.searchTeacher)
        initView()
        spinnerItems()
        radioBooking()
        onClick()
        searchTeacher!!.setOnClickListener(View.OnClickListener {
            if (selectedLanguage == null || selectedLanguage!!.isEmpty()) {
                Toast.makeText(this, "Choose Language to Learn", Toast.LENGTH_SHORT).show()
            }
            else if (postime == 0) {

                Toast.makeText(this@CreateBooking, "Please select a session Time", Toast.LENGTH_SHORT).show()
            } else if (poslevel == 0) {
                Toast.makeText(this@CreateBooking, "Please select a level", Toast.LENGTH_SHORT).show()
            } else {
                progressBar!!.setVisibility(View.VISIBLE)
                startSocket()

            }
        })

        iniSpinner()
        tvChooseLanguage.setOnClickListener {
            val intent = Intent(this, LanguagesActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        tvChooseOtherLanguage.setOnClickListener {
            val intent = Intent(this, LanguagesActivity::class.java)
            startActivityForResult(intent, 1002)
        }
    }

    override fun onResume() {
        super.onResume()
        iniSpinner()
    }

    private fun iniSpinner() {
        val price = resources.getStringArray(R.array.price)                 //Price Adapter
        val adapterPets = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_dropdown_item, price
        )
        spinnerPricePerHr?.adapter = adapterPets

        spinnerPricePerHr?.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View?,
                            position: Int,
                            id: Long
                    ) {
                        if (position == 0) {
                            pricePerHr = ""
                        } else if (position == 1) {
                            pricePerHr = "1"

                        } else if (position == 2) {
                            pricePerHr == "2"
                        } else {
                            pricePerHr = "3"
                        }

                    }

                }


        val rating = resources.getStringArray(R.array.rating)                 //Rating Adapter
        val ratingAdapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_dropdown_item, rating
        )
        spinnerRatingFilter?.adapter = ratingAdapter

        spinnerRatingFilter?.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View?,
                            position: Int,
                            id: Long
                    ) {

                        if (position == 0) {
                            ratingFilter = ""

                        } else if (position == 1) {
                            ratingFilter = "1"
                        } else {
                            ratingFilter = "2"
                        }

                    }

                }

    }

    private fun initView() {
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        //spinnerLearnLanguage = findViewById(R.id.spinnerLearnLanguage)
        spinnerSession = findViewById(R.id.spinnerSessionTime)
        spinnerLevel = findViewById(R.id.spinnerLevel)
        currentBooking = findViewById(R.id.currentBookingButton)
        scheduleBooking = findViewById(R.id.scheduleBookingButton)
        radioGroupBooking = findViewById(R.id.radioBooking)
        createBookingBackPress = findViewById(R.id.createBookingBackPress)
    }

    private fun getlanguages() {

        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages()?.enqueue(object : Callback<AllLanguageResposne> {
            override fun onResponse(call: Call<AllLanguageResposne>, response: Response<AllLanguageResposne>) {
                if (response.isSuccessful && response.body() != null) {

                } else {
                }
            }

            override fun onFailure(call: Call<AllLanguageResposne>, t: Throwable) {}
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1001 && resultCode == Activity.RESULT_OK) {
            selectedLanguage = data?.getStringExtra("lang")
            tvChooseLanguage.text=selectedLanguage
        }

        if (requestCode == 1002 && resultCode == Activity.RESULT_OK) {
            selectedOtherLanguage = data?.getStringExtra("lang")
            tvChooseOtherLanguage.text=selectedOtherLanguage
        }

    }
//    private fun getlanguages() {
//
//        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages()?.enqueue(object : Callback<AllLanguageResposne> {
//            override fun onResponse(call: Call<AllLanguageResposne>, response: Response<AllLanguageResposne>) {
//                if (response.isSuccessful) {
//                    response.body()?.body?.let {
//                        allLanguageAdapter = CreateLanguageAdapter(this@CreateBooking, it)
//                        spinnerLearnLanguage!!.adapter = allLanguageAdapter
//                    }
//
//                } else {
//                }
//            }
//
//            override fun onFailure(call: Call<AllLanguageResposne>, t: Throwable) {}
//        })
//
//        spinnerLearnLanguage!!.onItemSelectedListener = object : OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
//                selectedLanguage = (allLanguageAdapter!!.getItem(position) as AllLanguagModel).language
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {}
//        }
//    }

    private fun spinnerItems() {
        // getlanguages()
        sessonlist = ArrayList()
        sessonlist!!.add("Please select session time")
        sessonlist!!.add("30 min")
        sessonlist!!.add("60 min")
        sessonlist!!.add("90 min")
        sessonlist!!.add("120 min")
        val dataAdapter1 = ArrayAdapter(this@CreateBooking,
                android.R.layout.simple_spinner_item, sessonlist!!)
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerSession!!.adapter = dataAdapter1
        spinnerSession!!.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                if (position == 1) {
                    millisToAdd = 1800000
                } else if (position == 2) {
                    millisToAdd = 3600000
                } else if (position == 3) {
                    millisToAdd = 5400000
                } else if (position == 4) {
                    millisToAdd = 7200000
                }
                postime = position
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        levellist = ArrayList()
        levellist!!.add("Please select level")
        levellist!!.add("Beginner")
        levellist!!.add("Intermediate")
        levellist!!.add("Advanced")
        val dataAdapter2 = ArrayAdapter(this@CreateBooking,
                android.R.layout.simple_spinner_item, levellist!!)
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerLevel!!.adapter = dataAdapter2
        spinnerLevel!!.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                poslevel = position
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun startSocket() {
        setSessionTime()
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        connectWithChatSocket()
    }

    private fun connectWithChatSocket() {
        isSocketConnected = true
        if (socket!!.connected()) {
            //   getChatList();
            searchTeacher()
        } else {
            socket!!.on(Socket.EVENT_CONNECT) {
                if (socket!!.connected()) {
                    searchTeacher()
                } else {
                }
            }
            socket!!.connect()
        }
    }

    fun searchTeacher() {
        val c = Calendar.getInstance().time
        val df = SimpleDateFormat("yyyy-MM-dd")
        val formattedDate = df.format(c)

        val obj = JSONObject()
        try {
            val createBookingId = obj.put("learnerId", usersharedPrefernce!!.getlearnerid()).toString()
            obj.put("learnerLanguage", selectedLanguage)
            obj.put("other_language", selectedOtherLanguage)
            obj.put("sessionTime", time)
            obj.put("level", poslevel)

            obj.put("amount_range", pricePerHr)
            obj.put("rating_range", ratingFilter)

            if (sendScheduleDate == null) {
                obj.put("booking_date", formattedDate)
                obj.put("bookingType", "0")
                obj.put("startTime", sessionStartTime)
                obj.put("endTime", sessionEndTime)
            } else {
                obj.put("booking_date", sendScheduleDate)
                obj.put("bookingType", "1")
                obj.put("startTime", startBookingSession)
                obj.put("endTime", EndBookingsSession)
            }
            val timeZone = TimeZone.getDefault()
            obj.put("timezone", timeZone.id)

            if (socket!!.connected()) {
                socket!!.send(obj)
                socket!!.emit("searchTeacher", obj, Ack { args ->

                })
                getReturnSearchTeacher(createBookingId)
            } else {
                socket!!.connect()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getReturnSearchTeacher(bookingId: String?) {
        progressBar!!.visibility = View.GONE
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            searchTeacherBodyItemArrayList = ArrayList()
            socket!!.on("returnSearchTeacher" + usersharedPrefernce!!.getlearnerid()) { args ->
                val msgresponse = args[0].toString()
                val gson = Gson()
                val chatListResponse = gson.fromJson(msgresponse, SearchTeacherResponse::class.java)
                println("bookingId" + chatListResponse.bookingID)
                //  usersharedPrefernce.setBOOKINGID(chatListResponse.getBookingID());
                searchTeacherBodyItemArrayList = chatListResponse.body
                if (searchTeacherBodyItemArrayList!!.size > 0) {
                    val dvsd = Gson()
                    if (application != null) {

                        //TODO: Check if it is fine to turn off socket here...
                        socket!!.close()
                        socket!!.off()
                        val intent = Intent(this@CreateBooking, AvailableTeacherActivity::class.java)
                        intent.putExtra("teacherlist", searchTeacherBodyItemArrayList)
                        intent.putExtra("bookingId", chatListResponse.bookingID)
                        startActivity(intent)
                    }
                } else {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    handleData.sendMessage(msg)
                }
            }
        } else {
            socket!!.connect()
        }
    }

    private val handleData: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            runOnUiThread { Toast.makeText(this@CreateBooking, "No available Teacher", Toast.LENGTH_SHORT).show() }
        }
    }

    private fun onClick() {
        currentBooking!!.setOnClickListener(View.OnClickListener {
            tvTimeSchedule!!.visibility = View.GONE
            if (postime == 0) {
                Toast.makeText(this@CreateBooking, "Please select session time", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            sendScheduleDate = null
        })
        tvCurrentBooking!!.setOnClickListener(View.OnClickListener {
            tvTimeSchedule!!.visibility = View.GONE
            if (postime == 0) {
                Toast.makeText(this@CreateBooking, "Please select session time", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            sendScheduleDate = null
        })
        scheduleBooking!!.setOnClickListener(View.OnClickListener {
            if (postime == 0) {
                Toast.makeText(this@CreateBooking, "Please select session time", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR]
            val mMonth = c[Calendar.MONTH]
            val mDay = c[Calendar.DAY_OF_MONTH]
            c.add(Calendar.DAY_OF_MONTH, 30)
            val datePickerDialog = DatePickerDialog(this@CreateBooking,
                    OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        day = dayOfMonth.toString()
                        month = (monthOfYear + 1).toString()
                        yearr = year.toString()
                        if (month!!.length == 2) {
                            monthss = month
                        } else if (month!!.length < 2) {
                            monthss = "0$month"
                        }
                        if (day!!.length == 2) {
                            dayss = day
                        } else if (day!!.length <= 2) {
                            dayss = "0$day"
                        }
                        sendScheduleDate = "$yearr-$monthss-$dayss"
                       // Log.e("dateeeeee", sendScheduleDate)

                        onTime()
                    }, mYear, mMonth, mDay)
            datePickerDialog.datePicker.maxDate = c.timeInMillis
            datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()

        })
        tvScheduleBooking!!.setOnClickListener(View.OnClickListener {
            tvTimeSchedule!!.visibility = View.VISIBLE
            if (postime == 0) {
                Toast.makeText(this@CreateBooking, "Please select session time", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR]
            val mMonth = c[Calendar.MONTH]
            val mDay = c[Calendar.DAY_OF_MONTH]
            c.add(Calendar.DAY_OF_MONTH, 30)
            val datePickerDialog = DatePickerDialog(this@CreateBooking,
                    OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        day = dayOfMonth.toString()
                        month = (monthOfYear + 1).toString()
                        yearr = year.toString()
                        if (month!!.length == 2) {
                            monthss = month
                        } else if (month!!.length < 2) {
                            monthss = "0$month"
                        }
                        if (day!!.length == 2) {
                            dayss = day
                        } else if (day!!.length <= 2) {
                            dayss = "0$day"
                        }
                        sendScheduleDate = "$yearr-$monthss-$dayss"
                      //  Log.e("dateeeeee", sendScheduleDate)
                        onTime()
                    }, mYear, mMonth, mDay)
            datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()

        })
        createBookingBackPress!!.setOnClickListener {
            val i = Intent(this@CreateBooking, HomeActivity::class.java)
            startActivity(i)
        }
    }

    private fun onTime() {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(this@CreateBooking, OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
            tvTimeSchedule!!.visibility = View.VISIBLE
            val dateString = "$selectedHour:$selectedMinute:00"
            val hour = selectedHour.toString()
            finalHour = if (hour.length < 2) {
                "0$hour"
            } else {
                hour
            }
            val minute = selectedMinute.toString()
            finalMin = if (minute.length < 2) {
                "0$minute"
            } else {
                minute
            }
            startBookingSession = "$finalHour:$finalMin:00"
            tvTimeSchedule!!.text = startBookingSession
            val format: DateFormat = SimpleDateFormat("HH:mm:ss")
            var d: Date? = null
            try {
                d = format.parse(dateString)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            d!!.time = d.time + millisToAdd
            val s = d.toString()
            val timee = s.substring(11, 19)
            EndBookingsSession = timee
            println("New value: $timee")
        }, hour, minute, true) //Yes 24 hour time
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }

    private fun setSessionTime() {
        val sessiontime = spinnerSession!!.selectedItem.toString()
        val currenttimeinmilliseconds = System.currentTimeMillis()
        val timeinmilliseconds: Long
        val extenedetimemilliseconds: Long
        if (sessiontime == "30 min") {
            time = "30"
            timeinmilliseconds = 1800000
            extenedetimemilliseconds = currenttimeinmilliseconds + timeinmilliseconds
        } else if (sessiontime == "60 min") {
            time = "60"
            timeinmilliseconds = 3600000
            extenedetimemilliseconds = currenttimeinmilliseconds + timeinmilliseconds
        } else if (sessiontime == "90 min") {
            time = "90"
            timeinmilliseconds = 5400000
            extenedetimemilliseconds = currenttimeinmilliseconds + timeinmilliseconds
        } else {
            time = "120"
            timeinmilliseconds = 7200000
            extenedetimemilliseconds = currenttimeinmilliseconds + timeinmilliseconds
        }
        val cl = Calendar.getInstance()
        val c2 = Calendar.getInstance()
        cl.timeInMillis = currenttimeinmilliseconds
        c2.timeInMillis = extenedetimemilliseconds
        val hour = cl[Calendar.HOUR_OF_DAY].toString()
        starthour = if (hour.length < 2) {
            "0$hour"
        } else {
            hour
        }
        val sec = cl[Calendar.SECOND].toString()
        second = if (sec.length < 2) {
            "0$sec"
        } else {
            sec
        }
        val min = cl[Calendar.MINUTE].toString()
        minutes = if (min.length < 2) {
            "0$min"
        } else {
            min
        }
        sessionStartTime = "$starthour:$minutes:$second"
        val Endhour = c2[Calendar.HOUR_OF_DAY].toString()
        endhour = if (Endhour.length < 2) {
            "0$Endhour"
        } else {
            Endhour
        }
        val Endsec = c2[Calendar.SECOND].toString()
        endsecond = if (Endsec.length < 2) {
            "0$Endsec"
        } else {
            Endsec
        }
        val Endmin = c2[Calendar.MINUTE].toString()
        Endminutes = if (Endmin.length < 2) {
            "0$Endmin"
        } else {
            Endmin
        }
        sessionEndTime = "$endhour:$Endminutes:$endsecond"
        println("c   $currenttimeinmilliseconds    $extenedetimemilliseconds")
        println("sfjskdfjksjgksdg    $sessionStartTime        $sessionEndTime")
    }

    private fun radioBooking() {
        currentBooking!!.isChecked = true
        radioGroupBooking!!.setOnCheckedChangeListener { radioGroup, i ->
            val rb = radioGroupBooking!!.findViewById<RadioButton>(i)
            if (null != rb && i > -1) {
                if (rb == currentBooking) {
                    countradio = 0
                } else if (rb == scheduleBooking) {
                    countradio = 1
                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (isSocketConnected) {
            if (socket != null && socket!!.connected()) socket!!.disconnect()
        }
    }
}