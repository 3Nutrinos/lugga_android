package com.Lugga.lugga

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.adapters.AllLanguageAdapter
import com.Lugga.lugga.adapters.DaysAdapter
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.DaysList
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import kotlinx.android.synthetic.main.activity_new_log_in.*
import kotlinx.android.synthetic.main.select_days_dialog.*
import kotlinx.android.synthetic.main.sign_up.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat
import java.util.regex.Pattern


class TeacherNewSignUpActivity : AppCompatActivity(), View.OnClickListener {
    private var commaSeperatedDays: String?=""
    private var daysAdapter: DaysAdapter? = null
    private var daysModel: DaysList?=null
    private var daysList: ArrayList<DaysList>? = null

    private var selectedLanguage2: String?=""
    private var usersharedPrefernce: UsersharedPrefernce? = null
    var luggaAPI: LuggaAPI? = null

    private var selectedLanguage1: String? = ""
    private var languageList: ArrayList<AllLanguagModel>? = null
    private var selectedExperienceValue: String? = ""
    private var allLanguageAdapter: AllLanguageAdapter? = null
    var stripe_publishable_key: String? = null
    var stripe_user_id: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_log_in)

        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        getLanguage()
        tvExperience.setOnClickListener(this)
        iniSpinner()

        iniPriceCalculation()
        iniDays()

        tvTeacherSignUp.setOnClickListener(this)
        tvTeacherSignUpLinkk.setOnClickListener(this)
        tvSignUpWithStripe.setOnClickListener(this)

        val animation: Animation
        animation = AnimationUtils.loadAnimation(applicationContext,
                R.anim.blink)
        tvBlinkk.animation = animation

    }

    private fun iniDays() {


        tvDays.setOnClickListener {
            daysList = ArrayList()
            daysList?.clear()

            for (i in 0..6) {
                daysModel = DaysList()
                when (i) {
                    0 ->daysModel!!.day="Monday"
                    1 ->daysModel!!.day="Tuesday"
                    2 ->daysModel!!.day="Wednesday"
                    3 ->daysModel!!.day="Thursday"
                    4 ->daysModel!!.day="Friday"
                    5 ->daysModel!!.day="Saturday"
                    6 ->daysModel!!.day="Sunday"
                }
                daysList!!.add(daysModel!!)

            }
            val dialog = Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(R.layout.select_days_dialog)

             daysAdapter = DaysAdapter(this,daysList)
            dialog.rvDays.adapter=daysAdapter


            dialog.show()

            dialog.tvDone.setOnClickListener {
                if(daysAdapter!!.getSelected()!!.size<=0){
                    Toast.makeText(this, "Select days", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                for (i in daysAdapter!!.getSelected()!!.indices){
                     commaSeperatedDays = daysAdapter!!.getSelected()!!.joinToString { it -> it.day.toString() }
                }

                tvDays.text=commaSeperatedDays?.replace(" ","")

               // Log.d("commaSeperatedDays",commaSeperatedDays)
                dialog.dismiss()
            }
        }


    }
    private fun iniPriceCalculation() {
        etPrice.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s?.length != 0) {
                    tvCalculatedPrice.visibility = View.VISIBLE
                    val amount = s.toString().toDouble()
                    val teacherAmount = 30 / 100.0f * amount
                    val df = DecimalFormat("###.#")


                    tvCalculatedPrice.text = "(Less admin fee $" + df.format(teacherAmount).toString() + ")"
                } else {
                    tvCalculatedPrice.visibility = View.GONE
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
    }


    private fun iniSpinner() {
        spinnerLanguage1!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedLanguage1 = (allLanguageAdapter!!.getItem(position) as AllLanguagModel).language
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinnerLanguage2!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedLanguage2 = (allLanguageAdapter!!.getItem(position) as AllLanguagModel).language
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            tvExperience -> {
                val dialog = Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true)
                dialog.setContentView(R.layout.dialog_experience);


                val done = dialog.findViewById<TextView>(R.id.tvDoneExperience)

                val radioGroup = dialog.findViewById(R.id.radioGroupExperience) as RadioGroup

                done.setOnClickListener {
                    val intSelectButton: Int = radioGroup!!.checkedRadioButtonId
                    val radioButton = dialog.findViewById(intSelectButton) as RadioButton
                    selectedExperienceValue = radioButton.text.toString()
                    tvExperience.text = selectedExperienceValue
                    dialog.dismiss()

                }

                dialog.show()


                //Splash
            }

            tvSignUpWithStripe -> {

                if (etName.text.toString().isEmpty()) {
                    Toast.makeText(this, "Enter Name", Toast.LENGTH_SHORT).show()
                    return
                }
                if (etEmail.text.toString().isEmpty()) {
                    Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show()
                    return
                }

                if (!isValidEmailId(etEmail.getText().toString().trim())) {
                    Toast.makeText(this, "Enter valid Email", Toast.LENGTH_SHORT).show()
                    return
                }

                if (etNumber.text.toString().isEmpty()) {
                    Toast.makeText(this, "Enter Number", Toast.LENGTH_SHORT).show()
                    return
                }

                if (etPassword.text.toString().isEmpty()) {
                    Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show()
                    return
                }

                if (tvExperience.text.toString().isEmpty()) {
                    Toast.makeText(this, "Choose Experience", Toast.LENGTH_SHORT).show()
                    return
                }

                if (etPrice.text.toString().isEmpty()) {
                    Toast.makeText(this, "Enter Price", Toast.LENGTH_SHORT).show()
                    return
                }

                val intent = Intent(this, StripeWebViewActivity::class.java)
                startActivityForResult(intent, STRIPE_ACCOUNT)
            }

            tvTeacherSignUpLinkk->{
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/Gd_f8F2-fd4"))
                startActivity(browserIntent)
            }

        }
    }

    private fun callTeacherSignUpApi() {
        progressBar.visibility = View.VISIBLE
        val days = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),
            commaSeperatedDays?.replace(" ","").toString()
        )
        val name = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etName!!.text.toString())
        val email: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etEmail!!.text.toString())
        val mobile_number: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etNumber!!.text.toString())
        val password: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etPassword!!.text.toString())
        val user_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "1")
        val device_token: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.devicetoken)
        val language: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),
            selectedLanguage1.toString()
        )
        val otherLanguage: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),
            selectedLanguage2.toString()
        )
        val device_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "Android")
        val experience: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),
            selectedExperienceValue.toString()
        )
        val finalAmount = etPrice.text.toString().trim()
        val amount: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), finalAmount)
        val time: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "30")
        val Stripe_publishable_key: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), stripe_publishable_key!!)
        val Stripe_user_id: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), stripe_user_id!!)
        //val level: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), levelcount.toString())
        luggaAPI!!.signup(otherLanguage,days,name, email, mobile_number, password, user_type, language, device_type, device_token, experience, amount, time, Stripe_publishable_key, Stripe_user_id).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                if (response.body() != null) {
                    if (response.isSuccessful) {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this@TeacherNewSignUpActivity, "Registered Successfully", Toast.LENGTH_SHORT).show()
                      //  Toast.makeText(this@TeacherNewSignUpActivity, response.body(), Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this@TeacherNewSignUpActivity, LoginActivity::class.java))
                        finish()
                    } else {
                        progressBar.visibility = View.GONE

                        Toast.makeText(this@TeacherNewSignUpActivity, "Something went wrong !!", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                progressBar.visibility = View.GONE

                Toast.makeText(this@TeacherNewSignUpActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun isValidEmailId(email: String): Boolean {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == STRIPE_ACCOUNT) {
            if (resultCode == Activity.RESULT_OK) {
                stripe_publishable_key = data!!.extras!!.getString("stripe_publishable_key")
                stripe_user_id = data.extras!!.getString("stripe_user_id")

                if (stripe_publishable_key == null && stripe_user_id == null) {
                    Toast.makeText(this, "Please register your stripe account", Toast.LENGTH_SHORT).show()
                    return
                }

                callTeacherSignUpApi()

            }
        }
    }

    private fun getLanguage() {
        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages().enqueue(object : Callback<AllLanguageResposne> {
            override fun onResponse(call: Call<AllLanguageResposne>, response: Response<AllLanguageResposne>) {
                if (response.isSuccessful && response.body() != null) {
                    languageList = response.body()!!.body
                    allLanguageAdapter = AllLanguageAdapter(this@TeacherNewSignUpActivity, languageList!!)
                    spinnerLanguage1!!.adapter = allLanguageAdapter
                    spinnerLanguage2!!.adapter = allLanguageAdapter
                } else {
                    Toast.makeText(this@TeacherNewSignUpActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<AllLanguageResposne>, t: Throwable) {
                Toast.makeText(this@TeacherNewSignUpActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()

            }
        })
    }

    companion object {
        private const val GALLERY = 7
        private const val STRIPE_ACCOUNT = 333
    }
}