package com.Lugga.lugga

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.views.StripeModelClass
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

class StripeWebViewActivity : AppCompatActivity(), View.OnClickListener {
    var luggaAPI: LuggaAPI? = null
    var progressBar: ProgressBar? = null
    private var countDownTimer: CountDownTimer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stripe_web_view)
        progressBar = findViewById(R.id.progressBar)
        progressBar!!.setVisibility(View.VISIBLE)
        val webView = findViewById<WebView>(R.id.webView)
        val ivBack = findViewById<ImageView>(R.id.ivBack)
        setTimer()
        luggaAPI = retrofit.create(LuggaAPI::class.java)
        webView.settings.loadsImagesAutomatically = true
        webView.settings.javaScriptEnabled = true
        webView.loadUrl("https://connect.stripe.com/express/oauth/authorize?redirect_uri=http://www.luggalanguages.com&client_id=ca_HATBlcNsvjXfvPxsjZHQSr8mfHtqUdul&state=initial") //LiveKey
       // webView.loadUrl("https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://connect.stripe.com/connect/default/oauth/test&client_id=ca_HATBFUcEsh9rtPKlUzSoBAXVyWvjXmlX&state=initial");   //Testing URL
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                if (request.url.getQueryParameter("code") != null) {
                    hitStripeApi(request.url.getQueryParameter("code"))
                }
                return false
            }
        }
        ivBack.setOnClickListener(this)
    }

    private fun setTimer() {
        countDownTimer = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                progressBar!!.visibility = View.GONE
            }
        }
        countDownTimer!!.start()
    }

    private fun hitStripeApi(code: String?) {
        val onCallBackStripe = luggaAPI!!.getStripeResponse(code, "authorization_code")
        onCallBackStripe.enqueue(object : Callback<StripeModelClass?> {
            override fun onResponse(call: Call<StripeModelClass?>, response: Response<StripeModelClass?>) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.stripe_publishable_key != null && response.body()!!.stripe_user_id != null) {
                        val intent = Intent()
                        intent.putExtra("stripe_publishable_key", response.body()!!.stripe_publishable_key)
                        intent.putExtra("stripe_user_id", response.body()!!.stripe_user_id)
                        Toast.makeText(this@StripeWebViewActivity, "Added Successfully", Toast.LENGTH_SHORT).show()
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }
                }
            }

            override fun onFailure(call: Call<StripeModelClass?>, t: Throwable) {
                if (t is UnknownHostException) {
                    Toast.makeText(this@StripeWebViewActivity, "No Internet Connection", Toast.LENGTH_SHORT).show()
                } else if (t is SocketTimeoutException) {
                    Toast.makeText(this@StripeWebViewActivity, "Server is not responding. Please try again", Toast.LENGTH_SHORT).show()
                } else if (t is ConnectException) {
                    Toast.makeText(this@StripeWebViewActivity, "Failed to connect server", Toast.LENGTH_SHORT).show()
                } else Toast.makeText(this@StripeWebViewActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (countDownTimer != null) {
            countDownTimer!!.cancel()
        }
    }

    override fun onClick(v: View) {
        onBackPressed()
    }

    companion object {
        private val builder = Retrofit.Builder()
                .baseUrl("https://connect.stripe.com/").client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())

        private fun provideOkHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                    .addInterceptor(provideHeaderInterceptor())
                    .connectTimeout(45, TimeUnit.SECONDS)
                    .readTimeout(45, TimeUnit.SECONDS)
                    .writeTimeout(45, TimeUnit.SECONDS)
                    .build()
        }

        private fun provideHeaderInterceptor(): Interceptor {
            return object : Interceptor {
                @Throws(IOException::class)
                override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                    val Authorization = "Bearer sk_live_1L9JXV99ZszWnyJqPwVrixve0008oCyaxd" //LiveKey
                  //  val Authorization = "Bearer sk_test_QWDgYccjhPknQ7Fc8s8ZOG3W00KjAoqX9O";            ///Tesing Key
                    val request = chain.request().newBuilder().addHeader("Authorization", Authorization).build()
                    return chain.proceed(request)
                }
            }
        }

        var retrofit = builder.build()
    }
}