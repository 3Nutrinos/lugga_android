package com.Lugga.lugga

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.Response
import com.Lugga.lugga.model.bookingStatus.BookingStatusResponse
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.subscription.BillingProcessor
import com.Lugga.lugga.subscription.BillingProcessor.IBillingHandler
import com.Lugga.lugga.subscription.PurchaseInfo
import com.Lugga.lugga.subscription.SubscriptionActivity
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.sub.IabBroadcastReceiver
import com.Lugga.lugga.utils.sub.IabHelper
import com.Lugga.lugga.utils.sub.IabHelper.*
import com.Lugga.lugga.utils.sub.Purchase
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import java.util.*
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private var device_token: String? = null
    var textLogin: TextView? = null
    var textRegister: TextView? = null
    var loginEmail: EditText? = null
    var loginPassword: EditText? = null
    var luggaAPI: LuggaAPI? = null
    var progressDialog: ProgressDialog? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var llForgotPass: LinearLayout? = null
    var dialog: Dialog? = null
    var etEmail: EditText? = null
    private var mHelper: IabHelper? = null
    private var mReceiver: IabBroadcastReceiver? = null
    private var isSubscribed = false
    private var bp: BillingProcessor? = null

    // PRODUCT & SUBSCRIPTION IDS
    val KEY_SILVER = "silver_pkg"
    val KEY_BRONZE = "bronze"
    val KEY_GOLD = "gold_pkg"

    private val LICENSE_KEY =
        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApliMctatlMrwbSJjpynHWC8CpnmLM//8BS5mz9Ys5pZc4mdikoy32ec0THiefo+V4pbwihFlNQS5AMs0Ak4zyCXRTmSa5IWQDJ0Z3/tnt0L4PYBhAdyHYCIv2PrqRmV8xyYxcCOZvzKaSwSSofwmrP5tb4Oe8t7RKzF+nlya3XiP0kJfB9Tzz+1RH4ChZTpeYqITYkrEWRX8sJeD+skDMRZYmr9yKB0SUwXVY1q7P0sZkqHVurWYL+3FlrUJ5fU6mWReFxJrzSDU1KmCPceIpLd1oYncu6x3by4nPsINqgVrIya0QDp1PDx2Lq3Gd5n1DrNeUecs/Ip1YNJclYJ96wIDAQAB"

    // PUT YOUR MERCHANT KEY HERE;
    // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
    // if filled library will provide protection against Freedom alike Play Market simulators

    // PUT YOUR MERCHANT KEY HERE;
    // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
    // if filled library will provide protection against Freedom alike Play Market simulators
    val MERCHANT_ID = "01610008143915814606"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        llForgotPass = findViewById(R.id.llForgotPass)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        init()
        onClick()
        llForgotPass!!.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mReceiver != null) {
            unregisterReceiver(mReceiver)
        }
        if (mHelper != null) {
            mHelper!!.disposeWhenFinished()
            mHelper = null
        }
    }

    private fun init() {
        initSubscription()
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        textLogin = findViewById(R.id.textLogin)
        textRegister = findViewById(R.id.textRegister)
        loginEmail = findViewById(R.id.loginEmail)
        loginPassword = findViewById(R.id.loginPassword)
    }

    private fun onClick() {
        textLogin!!.setOnClickListener {
            val isEmpty = validation()
            if (!isEmpty) {
                loginData()
            }
        }
        textRegister!!.setOnClickListener {
            startActivity(Intent(this@LoginActivity, SignUpAsActivity::class.java))
            finish()
        }
    }

    private fun initSubscription() {
        bp = BillingProcessor(
            this,
            LICENSE_KEY,
            MERCHANT_ID,
            object : IBillingHandler {
                override fun onProductPurchased(productId: String, purchaseInfo: PurchaseInfo) {
                }

                override fun onBillingError(errorCode: Int, error: Throwable) {
                }

                override fun onBillingInitialized() {
                    if(bp!!.isSubscribed(KEY_BRONZE)){
                        isSubscribed=true
                    }
                    else if(bp!!.isSubscribed(KEY_SILVER)){
                        isSubscribed=true
                    }
                    else if(bp!!.isSubscribed(KEY_GOLD)){
                        isSubscribed=true
                    }
                }

                override fun onPurchaseHistoryRestored() {
                }
            })

    }

    private fun loginData() {
        isShowProgress
        if (usersharedPrefernce!!.devicetoken != null) {
            device_token = usersharedPrefernce!!.devicetoken
        } else {
            device_token = FirebaseInstanceId.getInstance().token

        }
        val Email: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), loginEmail!!.text.toString())
        val Password: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), loginPassword!!.text.toString())
        val deviceType: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "Android")
        val deviceToken: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), device_token.toString())
        luggaAPI!!.login(Email, Password, deviceType, deviceToken).enqueue(
                object : Callback<Response> {
                    override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                        if (response.body()!!.success == 200) {

                            // isDismiss
                            progressBar!!.visibility = View.GONE

                            usersharedPrefernce!!.setusertype(response.body()!!.body[0].userType)
                            usersharedPrefernce!!.setemail(response.body()!!.body[0].email)
                            usersharedPrefernce!!.setname(response.body()!!.body[0].name)
                            usersharedPrefernce!!.setmobileno(response.body()!!.body[0].mobileNo)
                            usersharedPrefernce!!.setuserid(response.body()!!.body[0].id)
                            if (response.body()!!.body[0].userType == 1) {
                                usersharedPrefernce!!.stripeUserId = response.body()!!.body[0].stripe_user_id
                                usersharedPrefernce!!.setteacherid(response.body()!!.body[0].id)
                                usersharedPrefernce!!.setprofileimage(response.body()!!.body[0].profileImage)
                            } else {
                                usersharedPrefernce!!.setlearnerid(response.body()!!.body[0].id)
                                usersharedPrefernce!!.setprofileimage(response.body()!!.body[0].profileImage)
                            }

                            //   usersharedPrefernce.setlearnerid(usersharedPrefernce.getlearnerid());
                            if (usersharedPrefernce!!.getusertype() == 1) {
                                //checking user subscription
                                if (!isSubscribed) {
                                    usersharedPrefernce!!.setboolean(true);  // change to false for live
                                    startActivity(Intent(this@LoginActivity, HomeActivity::class.java))        // change to Subscrip for live
                                } else {
                                    usersharedPrefernce!!.setboolean(true)
                                    startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                                }

                            } else {
                                usersharedPrefernce!!.setboolean(true)
                                startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                            }
                        } else {
                            progressBar!!.visibility = View.GONE
                            Toast.makeText(this@LoginActivity, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(call: Call<Response>, t: Throwable) {
                        progressBar!!.visibility = View.GONE

                        Toast.makeText(this@LoginActivity, "OnFailure" + t.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                }
        )
    }


    private fun validation(): Boolean {
        var isEmpty = false
        if (loginEmail!!.text.toString().isEmpty()) {
            loginEmail!!.error = "email is empty"
            isEmpty = true
        }
        if (loginPassword!!.text.toString().isEmpty()) {
            loginPassword!!.error = "password is empty"
            isEmpty = true
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(loginEmail!!.text.toString()).matches()) {
            loginEmail!!.error = "email is not valid"
            isEmpty = true
        }
        return isEmpty
    }

    val isShowProgress: Unit
        get() {
            if (progressDialog == null) {
                progressDialog = ProgressDialog(this@LoginActivity)
                progressDialog!!.setTitle("Loading")
                progressDialog!!.setMessage("wait while loading...")
                progressDialog!!.setCancelable(false)
            }
            //progressDialog!!.show()
            progressBar!!.visibility = View.VISIBLE
        }

    val isDismiss: Unit
        get() {
            if (progressDialog != null && progressDialog!!.isShowing) {
                //progressDialog!!.dismiss()
                progressBar!!.visibility = View.GONE

            }
        }

    override fun onBackPressed() {
        super.onBackPressed()
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
        finish()
    }

    override fun onClick(v: View) {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        dialog = Dialog(this@LoginActivity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.dialog_forgot_pass)
        etEmail = dialog!!.findViewById(R.id.etEmail)
        val tvDone = dialog!!.findViewById<TextView>(R.id.tvDone)
        tvDone.setOnClickListener(View.OnClickListener {
            if (etEmail!!.getText().toString().trim { it <= ' ' }.isEmpty()) {
                Toast.makeText(this@LoginActivity, "Enter Email", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (!isValidEmailId(etEmail!!.getText().toString())) {
                Toast.makeText(this@LoginActivity, "Enter valid Email", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            hitApi()
        })
        dialog!!.show()
    }

    private fun hitApi() {
        val Email: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etEmail!!.text.toString().trim { it <= ' ' })
        ApiClient.getApiClient().create(LuggaAPI::class.java).onForgotPass(Email).enqueue(object : Callback<BookingStatusResponse?> {
            override fun onResponse(call: Call<BookingStatusResponse?>, response: retrofit2.Response<BookingStatusResponse?>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        if (response.body()!!.success == 200) {
                            Toast.makeText(this@LoginActivity, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                            dialog!!.dismiss()
                        } else {
                            Toast.makeText(this@LoginActivity, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this@LoginActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this@LoginActivity, "LogOut failed", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<BookingStatusResponse?>, t: Throwable) {
                Toast.makeText(this@LoginActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun isValidEmailId(email: String): Boolean {
        return Pattern.compile(
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    fun verifyDeveloperPayload(p: Purchase): Boolean {
        val payload = p.developerPayload
        return true
    }

//    override fun receivedBroadcast() {
//        // Received a broadcast notification that the inventory of items has changed
//        Log.d(TAG, "Received broadcast notification. Querying inventory.")
//        try {
//            mHelper!!.queryInventoryAsync(skuList, mGotInventoryListener)
//        } catch (e: IabAsyncInProgressException) {
//            Log.d(TAG, "Exception: $e")
//        }
//    }
//
//    private val mGotInventoryListener = QueryInventoryFinishedListener { result, inventory ->
//        Log.d(TAG, "Querying finished")
//        if (mHelper == null) return@QueryInventoryFinishedListener
//        if (result.isFailure) {
//            Log.d(TAG, "Querying fail")
//            return@QueryInventoryFinishedListener
//        }
//        Log.d(TAG, "Query inventory was successful.")
//
//        // First find out which subscription is auto renewing
//        val gold = inventory.getPurchase(Constants.KEY_GOLD)
//        val silver = inventory.getPurchase(Constants.KEY_SILVER)
//        val bronze = inventory.getPurchase(Constants.KEY_BRONZE)
//        if (gold != null && gold.isAutoRenewing) {
//            mActiveSub = Constants.KEY_GOLD
//            isAutoRenewable = true
//        } else if (silver != null && silver.isAutoRenewing) {
//            mActiveSub = Constants.KEY_SILVER
//            isAutoRenewable = true
//        } else if (bronze != null && bronze.isAutoRenewing) {
//            mActiveSub = Constants.KEY_BRONZE
//            isAutoRenewable = true
//        } else {
//            mActiveSub = ""
//            isAutoRenewable = false
//        }
//        isSubscribed = (gold != null && verifyDeveloperPayload(gold)
//                || silver != null && verifyDeveloperPayload(silver)
//                || bronze != null && verifyDeveloperPayload(bronze))
//        Log.d(TAG, "User " + (if (isSubscribed) "HAS" else "DOES NOT HAVE")
//                + " subscription.")
//        Log.d(TAG, "Initial inventory query finished; enabling main UI.")
//    }

    companion object {
        const val IS_LOGIN = "is_login"
    }
}