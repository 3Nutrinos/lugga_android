package com.Lugga.lugga.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.Lugga.lugga.CallReceiveActivity;
import com.Lugga.lugga.NewCallReceiveActivity;
import com.Lugga.lugga.R;
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce;
import com.Lugga.lugga.HomeActivity;
import com.Lugga.lugga.views.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;

import static com.Lugga.lugga.sharedpreferences.BaseApplication.getContext;


public class FirebaseNotification extends FirebaseMessagingService {
    private static final long WAKELOCK_TIMEOUT = 5000;
    public static UsersharedPrefernce usersharedPrefernce;
    Intent notiIntent;
    public static final int NOTIFICATON_SMALL_ICON = R.mipmap.logo;
    public static final int NOTIFICATION_BIG_ICON = R.mipmap.logo;
    private static Socket socket;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        usersharedPrefernce = UsersharedPrefernce.getInstance();
        usersharedPrefernce.setDEVICETOKEN(s);
        SharedPreferences.Editor editor = getSharedPreferences("TokenPref", MODE_PRIVATE).edit();
        editor.putString("token", s);
        editor.apply();
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().get("message") != null) {
            clearNotifications(this);

            Log.e("noti", remoteMessage.getData().get("message"));

            socket = SocketNetworking.getSocket();
            socket.connect();

            usersharedPrefernce = UsersharedPrefernce.getInstance();
            if (remoteMessage.getData().size() > 0) {
                JSONObject channel_id = null;

                try {
                    channel_id = new JSONObject(remoteMessage.getData().get("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (channel_id != null && channel_id.getString("caller_id") != null) {
                        notiIntent = new Intent(this, NewCallReceiveActivity.class);
                        notiIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        notiIntent.putExtra("channel_id", channel_id.getString("channel_id"));
                        notiIntent.putExtra("caller_id", channel_id.getString("caller_id"));
                        notiIntent.putExtra("receiver_id", channel_id.getString("receiver_id"));
                        notiIntent.putExtra("caller_name", channel_id.getString("caller_name"));
                        notiIntent.putExtra("user_type", channel_id.getString("user_type"));
                        notiIntent.putExtra("call_type", channel_id.getString("call_type"));
                        notiIntent.putExtra("sender_name", channel_id.getString("sender_name"));
                        notiIntent.putExtra("booking_id", channel_id.getString("booking_id"));
                        notiIntent.putExtra("access_token", channel_id.getString("access_token"));
                        notiIntent.putExtra("fromIntroScreen","no");

                        if (channel_id.has("sender_name")) {
                            notiIntent.putExtra("user_name", channel_id.optString("sender_name", "name"));
                        }

                        startActivity(notiIntent);
                    } else {
                        if (remoteMessage.getData().get("message").startsWith("you have missed")) {
                            sendBroadcast(new Intent("finishCallIncomingActivity"));
                        }
                        notificationManager(getApplicationContext(), remoteMessage.getData().get("message"), true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


        }

    }

    public static NotificationManager getNotificationManager(final Context context, String channel) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // The user-visible name of the channel.
            CharSequence name = context.getString(R.string.notification_channel_default);
            // The user-visible description of the channel.
            String description = context.getString(R.string.notification_channel_description_default);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channel, name, importance);
            // Configure the notification channel.
            mChannel.setDescription(description);
            notificationManager.createNotificationChannel(mChannel);
        }
        return notificationManager;
    }

    @SuppressWarnings("deprecation")
    public static void notificationManager(Context context, String message, boolean ring) {

        try {
            long when = System.currentTimeMillis();

            NotificationManager notificationManager = getNotificationManager(context, Constants.NOTIF_CHANNEL_DEFAULT);

            Log.v("message", "," + message);

            Intent notificationIntent = new Intent(context, HomeActivity.class);


            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Constants.NOTIF_CHANNEL_DEFAULT);
            builder.setAutoCancel(true);
            builder.setContentTitle(context.getResources().getString(R.string.app_name));
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            builder.setContentText(message);
            builder.setTicker(message);
            builder.setChannelId(Constants.NOTIF_CHANNEL_DEFAULT);

            if (ring) {
                builder.setLights(Color.GREEN, 500, 500);
            } else {
                builder.setDefaults(Notification.DEFAULT_ALL);
            }

            builder.setWhen(when);
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), NOTIFICATION_BIG_ICON));
            builder.setSmallIcon(NOTIFICATON_SMALL_ICON);
            builder.setContentIntent(intent);


            Notification notification = builder.build();
            if (notificationManager != null) {
                notificationManager.notify(1, notification);
            }

            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl;
            if (pm != null) {
                wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, ":notification");
                wl.acquire(WAKELOCK_TIMEOUT);
            }


            JSONObject jsonObject = new JSONObject();

            try {
                if (usersharedPrefernce.getusertype() == 2) {
                    jsonObject.put("id", usersharedPrefernce.getuserid());
                    jsonObject.put("user_type", "2");
                } else {
                    jsonObject.put("id", usersharedPrefernce.getuserid());
                    jsonObject.put("user_type", "1");
                }
                socket.emit("HomepageSocket", jsonObject);
            } catch (JSONException e) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }


}