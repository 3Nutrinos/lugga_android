package com.Lugga.lugga.utils;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.Lugga.lugga.HomeActivity;
import com.Lugga.lugga.R;
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce;
import com.Lugga.lugga.views.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import androidx.core.app.NotificationCompat;
public class GCMIntentService extends FirebaseMessagingService {


    private static final long WAKELOCK_TIMEOUT = 5000;

    public static final int NOTIFICATON_SMALL_ICON = R.mipmap.logo;
    public static final int NOTIFICATION_BIG_ICON = R.mipmap.logo;
    UsersharedPrefernce usersharedPrefernce;

    public GCMIntentService() {
    }


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        usersharedPrefernce = UsersharedPrefernce.getInstance();
        usersharedPrefernce.setDEVICETOKEN(s);
        SharedPreferences.Editor editor = getSharedPreferences("TokenPref", MODE_PRIVATE).edit();
        editor.putString("token", s);
        editor.apply();
    }

    @SuppressWarnings("deprecation")
    public static void notificationManager(Context context, String message, boolean ring) {

        try {
            long when = System.currentTimeMillis();

            NotificationManager notificationManager = GCMIntentService.getNotificationManager(context, Constants.NOTIF_CHANNEL_DEFAULT);

            Log.v("message", "," + message);

            Intent notificationIntent = new Intent(context, HomeActivity.class);


            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Constants.NOTIF_CHANNEL_DEFAULT);
            builder.setAutoCancel(true);
            builder.setContentTitle(context.getResources().getString(R.string.app_name));
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            builder.setContentText(message);
            builder.setTicker(message);
            builder.setChannelId(Constants.NOTIF_CHANNEL_DEFAULT);

            if (ring) {
                builder.setLights(Color.GREEN, 500, 500);
            } else {
                builder.setDefaults(Notification.DEFAULT_ALL);
            }

            builder.setWhen(when);
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(),NOTIFICATION_BIG_ICON));
            builder.setSmallIcon(NOTIFICATON_SMALL_ICON);
            builder.setContentIntent(intent);


            Notification notification = builder.build();
            if (notificationManager != null) {
                notificationManager.notify(1, notification);
            }

            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            WakeLock wl;
            if (pm != null) {
                wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, ":notification");
                wl.acquire(WAKELOCK_TIMEOUT);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void clearNotifications(Context context) {
        try {
            NotificationManager notificationManager = GCMIntentService.getNotificationManager(context, Constants.NOTIF_CHANNEL_DEFAULT);
            if (notificationManager != null) {
                notificationManager.cancelAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//		super.onMessageReceived(remoteMessage);

        Log.e("Recieved a gcm", "," + remoteMessage.getData());

        onHandleIntent(remoteMessage);

    }

    public void onHandleIntent(RemoteMessage intent) {
        try {
            Log.i("Recieved a gcm message", "," + intent.getData());
            try {
                if (!"".equalsIgnoreCase(intent.getData().get("message"))) {
                    String message = intent.getData().get("message");
                    try {
                        JSONObject jObj = new JSONObject(message);
                        String title = jObj.optString(Constants.KEY_TITLE, "Lugga");
                        notificationManager(this, title, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

            }
            } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getNetworkName(Context context) {
        try {
            TelephonyManager telephonyManager = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE));
            String simOperatorName = telephonyManager.getSimOperatorName();
            String operatorName = telephonyManager.getNetworkOperatorName();
            return simOperatorName + " " + operatorName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "not found";
    }


    public static NotificationManager getNotificationManager(final Context context, String channel){

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // The user-visible name of the channel.
            CharSequence name = context.getString(R.string.notification_channel_default);
            // The user-visible description of the channel.
            String description = context.getString(R.string.notification_channel_description_default);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channel, name, importance);
            // Configure the notification channel.
            mChannel.setDescription(description);
            notificationManager.createNotificationChannel(mChannel);
        }
        return notificationManager;
    }

    public static NotificationManager getNotificationManagerSilent(final Context context, String channel){

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // The user-visible name of the channel.
            CharSequence name = context.getString(R.string.notification_channel_silent);
            // The user-visible description of the channel.
            String description = context.getString(R.string.notification_channel_description_silent);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel mChannel = new NotificationChannel(channel, name, importance);
            // Configure the notification channel.
            mChannel.setDescription(description);
            notificationManager.createNotificationChannel(mChannel);
        }
        return notificationManager;
    }



}






