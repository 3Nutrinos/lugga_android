package com.Lugga.lugga.utils;

import static okhttp3.logging.HttpLoggingInterceptor.*;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

   // public static final String BASE_URL = "http://localhost:91/";    //testing
  //  public static final String IMAGE_URL = "http://luggalanguages.com";       //testing
    public static final String BASE_URL = "http://luggalanguages.com:90/";       //live
    public static final String IMAGE_URL = "http://luggalanguages.com";         //live


    public static final String AGORA_APP_ID = "a68dd92955194e92a664c6f91d1452d1";         //live


    private static Retrofit retrofit = null;

    public static Retrofit getApiClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(Level.BODY);

        if (retrofit == null) {
            OkHttpClient client = new OkHttpClient().newBuilder().connectTimeout(160, TimeUnit.SECONDS)
                    .addInterceptor(logging)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()))
                    .build();

        }

        return retrofit;
    }


    ////new code h
}
