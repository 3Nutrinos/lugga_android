package com.Lugga.lugga.utils;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

public class WebAppInterface {
    Context mContext;
    String data;

    public WebAppInterface(Context ctx){
        this.mContext=ctx;
    }


    @JavascriptInterface
    public void sendData(String data) {
        //Get the string value to process
        Log.e("data",data);
        this.data=data;
    }
}