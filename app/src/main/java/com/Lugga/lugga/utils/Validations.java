package com.Lugga.lugga.utils;

import android.util.Patterns;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validations {

    EditText nameSignup,emailSignup,mobileNumberSignup,passwordSignup;

    private boolean validationNext(){

        boolean isEmpty=false;

        if (nameSignup.getText().toString().isEmpty()){
            nameSignup.setError("name is empty");
            isEmpty=true;
        }
        if (emailSignup.getText().toString().isEmpty()){
            emailSignup.setError("email is empty");
            isEmpty=true;
        }
        if (mobileNumberSignup.getText().toString().isEmpty()){
            mobileNumberSignup.setError("mobile number is empty");
            isEmpty=true;
        }
        if (passwordSignup.getText().toString().isEmpty()){
            passwordSignup.setError("password is empty");
            isEmpty=true;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(emailSignup.getText().toString()).matches()){
            emailSignup.setError("email is not valid");
            isEmpty = true;
        }
        if(!Patterns.PHONE.matcher(mobileNumberSignup.getText().toString()).matches()){
            mobileNumberSignup.setError("number is not valid");
            isEmpty = true;
        }

        return isEmpty;
    }


    public static boolean isValidPassword(final String password) {

        Pattern special = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
        Matcher hasSpecial = special.matcher(password);
        return hasSpecial.find();
    }

}
