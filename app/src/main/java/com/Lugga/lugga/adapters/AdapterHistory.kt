package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.learnerhistory.LearnerHistoryBodyItem
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.DateHelper
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class AdapterHistory(private val context: Context, var learnerHistoryBodyItemArrayList: ArrayList<LearnerHistoryBodyItem>) : RecyclerView.Adapter<AdapterHistory.ViewHolder>() {
    var usersharedPrefernce: UsersharedPrefernce? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        val view = LayoutInflater.from(context).inflate(R.layout.item_persons, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.personName.text = learnerHistoryBodyItemArrayList[position].name
        holder.paymentamount.text = "$ " + learnerHistoryBodyItemArrayList[position].paymentAmount
        holder.ratingstar.text = learnerHistoryBodyItemArrayList[position].rating.toString()
        holder.time.text = learnerHistoryBodyItemArrayList[position].sessionTime
        holder.endTime.text = learnerHistoryBodyItemArrayList[position].endSession
        holder.date.text = DateHelper.getDate(learnerHistoryBodyItemArrayList[position].createdAt)
        Glide.with(context).load(ApiClient.IMAGE_URL + learnerHistoryBodyItemArrayList[position].profileImage).centerCrop().into(holder.profile)
        when (learnerHistoryBodyItemArrayList[position].status) {
            "1" -> holder.textStatus.text = "Rejected"
            "2" -> holder.textStatus.text = "Ready"
            "3" -> holder.textStatus.text = "Upcoming"
            "4" -> holder.textStatus.text = "Pay"
            "5" -> holder.textStatus.text = "Ongoing"
            "6" -> holder.textStatus.text = "Completed"
            "7" -> holder.textStatus.text = "Expired"
            "8" -> holder.textStatus.text = "Waiting"
            else -> {
            }
        }
    }

    override fun getItemCount(): Int {
        return learnerHistoryBodyItemArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profile: CircleImageView
        var rlPersonImage: RelativeLayout
        var personName: TextView
        var textCounter: TextView
        var textStatus: TextView
        var nobookingyet: TextView
        var ratingstar: TextView
        var llDateTime: LinearLayout
        var bookedlayout: LinearLayout
        var view: View
        var mainView: View
        var cardView: CardView
        var time: TextView
        var paymentamount: TextView
        var endTime: TextView
        var date: TextView

        init {
            rlPersonImage = itemView.findViewById(R.id.rlPersonImage)
            personName = itemView.findViewById(R.id.personName)
            llDateTime = itemView.findViewById(R.id.llDateTime)
            textStatus = itemView.findViewById(R.id.textStatus)
            textCounter = itemView.findViewById(R.id.textCounter)
            view = itemView.findViewById(R.id.view)
            mainView = itemView.findViewById(R.id.mainView)
            cardView = itemView.findViewById(R.id.cardView)
            time = itemView.findViewById(R.id.time)
            paymentamount = itemView.findViewById(R.id.paymentamount)
            profile = itemView.findViewById(R.id.iv_profile)
            endTime = itemView.findViewById(R.id.endTime)
            date = itemView.findViewById(R.id.date)
            nobookingyet = itemView.findViewById(R.id.nobookingyet)
            bookedlayout = itemView.findViewById(R.id.bookedlayout)
            ratingstar = itemView.findViewById(R.id.ratingstar)
        }
    }

}