package com.Lugga.lugga.adapters

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.ChatListingAdapter.Viewholder
import com.Lugga.lugga.model.chatList.ChatListBodyItem
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.DateHelper
import com.bumptech.glide.Glide
import com.joypixels.tools.Client
import com.potyvideo.library.AndExoPlayerView
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

class ChatListingAdapter(private val context: Context, chatView: ChatView) : RecyclerView.Adapter<Viewholder>() {
    private var chatListBodyItemArrayList: ArrayList<ChatListBodyItem>
    var usersharedPrefernce: UsersharedPrefernce? = null
    var chatView: ChatView
    private var player: MediaPlayer? = null
    var seekHandler = Handler()
    var run: Runnable? = null
    private var animation: Animation? = null
    private var isPlaying = false
    private val isProgress = false
    private val isRecVideoProgress = false
    private val isProgressRec = false
    private var client: Client? = null
    fun setListChat(chatListBodyItemArrayList: ArrayList<ChatListBodyItem>) {
        this.chatListBodyItemArrayList = chatListBodyItemArrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        val view = LayoutInflater.from(context).inflate(R.layout.chat_recycler_item, parent, false)
        client = Client(context)
        client!!.isShortcodes = true
        return Viewholder(view)
    }

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        if (usersharedPrefernce!!.getusertype() == 1) {
            if (chatListBodyItemArrayList[position].receiverId == usersharedPrefernce!!.getteacherid()) {
                if (chatListBodyItemArrayList[position].messageType == 3) {
                    holder.receiver_image.visibility = View.VISIBLE
                    Glide.with(context).load(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message).centerCrop().placeholder(R.drawable.person_girl).into(holder.Rimage)
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvImgRecTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvImgRecDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.Rimage.setOnClickListener { chatView.onClickImage(chatListBodyItemArrayList[position].message) }
                } else if (chatListBodyItemArrayList[position].messageType == 4) {
                    holder.sender_video.visibility = View.GONE
                    holder.rel_video.visibility = View.VISIBLE
                    holder.receiver_image.visibility = View.GONE
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvVideoRecTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvVideoRecDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.videoViewreciever.setOnClickListener { holder.videoProgressReceiver.visibility = View.VISIBLE }


//                    holder.videoViewreciever.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//                        @Override
//                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
//                            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == what) {
//                                holder.videoProgressReceiver.setVisibility(View.GONE);
//                            }
//                            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == what) {
//                                holder.videoProgressReceiver.setVisibility(View.VISIBLE);
//                            }
//                            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == what) {
//                                holder.videoProgressReceiver.setVisibility(View.VISIBLE);
//                            }
//                            return false;
//                        }
//                    });
                    val mediacontroller: MediaController
                    mediacontroller = MediaController(context)
                    if (chatListBodyItemArrayList[position].message != null) {
                        holder.videoViewreciever.setSource(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                        //    mediacontroller.setAnchorView(holder.videoViewreciever);
//                        holder.videoViewreciever.setMediaController(mediacontroller);
//                        holder.videoViewreciever.setVideoPath(ApiClient.IMAGE_URL + chatListBodyItemArrayList.get(position).getMessage());
//                        holder.videoViewreciever.requestFocus();
//                        holder.videoViewreciever.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                            @Override
//                            public void onPrepared(MediaPlayer mp) {
//                                holder.videoViewreciever.setMediaController(mediacontroller);
//                                mediacontroller.setAnchorView(holder.videoViewreciever);
//                            }
//                        });
                    }
                } else if (chatListBodyItemArrayList[position].messageType == 2) {
                    holder.sender_video.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    //  holder.rlSenderAudio.setVisibility(View.GONE);
                    holder.rlReceiverAudio.visibility = View.VISIBLE
                    holder.tvRecAudioTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvRecAudioDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.ivRecAudio.setOnClickListener {
                        if (isPlaying) {
                            if (player != null) {
                                if (player!!.isPlaying) {
                                    player!!.stop()
                                }
                            }
                        }
                        player = MediaPlayer()
                        if (player!!.isPlaying) {
                            holder.tvReceiverPlayVideo.text = "Audio Message"
                            holder.tvReceiverPlayVideo.animation = null
                        }
                        val uri = Uri.parse(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                        animation = AnimationUtils.loadAnimation(context,
                                R.anim.blink)
                        holder.tvReceiverPlayVideo.text = "Playing audio..."
                        holder.ivRecAudio.visibility = View.INVISIBLE
                        holder.tvReceiverPlayVideo.animation = animation
                        player!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
                        try {
                            player!!.setDataSource(context, uri)
                            player!!.prepare()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        if (!player!!.isPlaying) {
                            player!!.start()
                            isPlaying = true
                            // holder.btn_play.setText("Pause");
                            run = Runnable {
                                seekHandler.postDelayed(run!!, 100)
                                val miliSeconds = player!!.currentPosition
                                if (miliSeconds != 0) {
                                    val minutes = TimeUnit.MILLISECONDS.toMinutes(miliSeconds.toLong())
                                    val seconds = TimeUnit.MILLISECONDS.toSeconds(miliSeconds.toLong())
                                    if (minutes == 0L) {
                                        if (!player!!.isPlaying) {
                                            holder.tvReceiverPlayVideo.text = "Audio Message"
                                            holder.tvReceiverPlayVideo.animation = null
                                            holder.ivRecAudio.visibility = View.VISIBLE
                                        }
                                        holder.tvRecAudioDuration.text = "0:" + seconds + "/" + calculateDuration(player!!.duration)
                                    } else {
                                        if (seconds >= 60) {
                                            val sec = seconds - minutes * 60
                                            holder.tvRecAudioDuration.text = minutes.toString() + ":" + sec + "/" + calculateDuration(player!!.duration)
                                        }
                                    }
                                } else {
                                    val totalTime = player!!.duration
                                    val minutes = TimeUnit.MILLISECONDS.toMinutes(totalTime.toLong())
                                    val seconds = TimeUnit.MILLISECONDS.toSeconds(totalTime.toLong())
                                    if (minutes == 0L) {
                                        // holder.tvRecAudioDuration.setText("0:" + seconds);
                                    } else {
                                        if (seconds >= 60) {
                                            val sec = seconds - minutes * 60
                                            //   holder.tvRecAudioDuration.setText(minutes + ":" + sec);
                                        }
                                    }
                                }
                            }
                            run!!.run()
                        } else {
                            player!!.pause()
                            //holder.btn_play.setText("Play");
                        }
                    }
                } else {
                    holder.reciever_layout.visibility = View.VISIBLE
//                    client!!.toImage(chatListBodyItemArrayList[position].message, 60, object : com.Lugga.lugga.utils.Callback {
//                        override fun onFailure(e: IOException) {
//                            holder.reciever_message.text = e.message
//                        }
//
//                        override fun onSuccess(ssb: SpannableStringBuilder?) {
//                            holder.reciever_message.text = ssb
//                        }
//                    })

                    // holder.reciever_message.setText(chatListBodyItemArrayList.get(position).getMessage());
                    holder.receiver_image.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvTxtRecTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    Log.e("time", DateHelper.getTime(chatListBodyItemArrayList[position].createdAt))
                    holder.tvDateTxtRec.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                }
            } else {
                if (chatListBodyItemArrayList[position].messageType == 3) {
                    holder.sender_image.visibility = View.VISIBLE
                    Glide.with(context).load(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                            .centerCrop().placeholder(R.drawable.person_girl).into(holder.simage)
                    holder.sender_layout.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    holder.reciever_layout.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvImgSendrTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvImgSenderDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.simage.setOnClickListener { chatView.onClickImage(chatListBodyItemArrayList[position].message) }
                } else if (chatListBodyItemArrayList[position].messageType == 4) {
                    holder.receiver_image.visibility = View.GONE
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.sender_video.visibility = View.VISIBLE
                    holder.rel_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvVideoSendrTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvVideoSendrDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.videoViewSender.setOnClickListener {
                        //  holder.videoProgress.setVisibility(View.VISIBLE);
                    }
                    //                    holder.videoViewSender.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//                        @Override
//                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
//                            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == what) {
//                                holder.videoProgress.setVisibility(View.GONE);
//                            }
//                            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == what) {
//                                holder.videoProgress.setVisibility(View.VISIBLE);
//                            }
//                            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == what) {
//                                holder.videoProgress.setVisibility(View.VISIBLE);
//                            }
//                            return false;
//                        }
//                    });
                    val mediacontroller: MediaController
                    mediacontroller = MediaController(context)
                    if (chatListBodyItemArrayList[position].message != null) {
                        holder.videoViewSender.setSource(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                        //                        mediacontroller.setAnchorView(holder.videoViewSender);
//                        holder.videoViewSender.setMediaController(mediacontroller);
//                        holder.videoViewSender.setVideoPath(ApiClient.IMAGE_URL + chatListBodyItemArrayList.get(position).getMessage());
//                        holder.videoViewSender.requestFocus();
//                        holder.videoViewSender.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                            @Override
//                            public void onPrepared(MediaPlayer mp) {
//                                holder.videoViewSender.setMediaController(mediacontroller);
//                                mediacontroller.setAnchorView(holder.videoViewSender);
//                            }
//                        });
                    }
                } else if (chatListBodyItemArrayList[position].messageType == 2) {
                    holder.sender_video.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.VISIBLE
                    //  holder.rlSenderAudio.setVisibility(View.GONE);
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvAudioSenderTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvAudioSenderDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.ivPlayAudio.setOnClickListener {
                        if (isPlaying) {
                            if (player != null) {
                                if (player!!.isPlaying) {
                                    player!!.stop()
                                }
                            }
                        }
                        player = MediaPlayer()
                        if (player!!.isPlaying) {
                            holder.tvMyAudio.text = "Audio Message"
                            holder.tvMyAudio.animation = null
                        }
                        val uri = Uri.parse(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                        animation = AnimationUtils.loadAnimation(context,
                                R.anim.blink)
                        holder.tvMyAudio.text = "Playing audio..."
                        holder.ivPlayAudio.visibility = View.INVISIBLE
                        holder.tvMyAudio.animation = animation
                        player!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
                        try {
                            player!!.setDataSource(context, uri)
                            player!!.prepare()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        if (!player!!.isPlaying) {
                            player!!.start()
                            isPlaying = true
                            // holder.btn_play.setText("Pause");
                            run = Runnable {
                                seekHandler.postDelayed(run!!, 100)
                                val miliSeconds = player!!.currentPosition
                                if (miliSeconds != 0) {
                                    val minutes = TimeUnit.MILLISECONDS.toMinutes(miliSeconds.toLong())
                                    val seconds = TimeUnit.MILLISECONDS.toSeconds(miliSeconds.toLong())
                                    if (minutes == 0L) {
                                        if (!player!!.isPlaying) {
                                            holder.tvMyAudio.text = "Audio Message"
                                            holder.tvMyAudio.animation = null
                                            holder.ivPlayAudio.visibility = View.VISIBLE
                                        }
                                        holder.tvAudioDuration.text = "0:" + seconds + "/" + calculateDuration(player!!.duration)
                                    } else {
                                        if (seconds >= 60) {
                                            val sec = seconds - minutes * 60
                                            holder.tvAudioDuration.text = minutes.toString() + ":" + sec + "/" + calculateDuration(player!!.duration)
                                        }
                                    }
                                } else {
                                    val totalTime = player!!.duration
                                    val minutes = TimeUnit.MILLISECONDS.toMinutes(totalTime.toLong())
                                    val seconds = TimeUnit.MILLISECONDS.toSeconds(totalTime.toLong())
                                    if (minutes == 0L) {
                                        // holder.tvAudioDuration.setText("0:" + seconds);
                                    } else {
                                        if (seconds >= 60) {
                                            val sec = seconds - minutes * 60
                                            //   holder.tvAudioDuration.setText(minutes + ":" + sec);
                                        }
                                    }
                                }
                            }
                            run!!.run()
                        } else {
                            player!!.pause()
                            //holder.btn_play.setText("Play");
                        }
                    }
                } else {
                    holder.sender_layout.visibility = View.VISIBLE
//                    client!!.toImage(chatListBodyItemArrayList[position].message, 60, object :Callback{
//                        override fun onFailure(e: IOException) {
//                            holder.sender_message.text = e.message
//                        }
//
//                        override fun onSuccess(ssb: SpannableStringBuilder?) {
//                            holder.sender_message.text = ssb
//                        }
//                    })
                    holder.tvTxtMsgTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvTextmsgDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    //  holder.sender_message.setText(chatListBodyItemArrayList.get(position).getMessage());
                    holder.sender_image.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    holder.reciever_layout.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                }
            }
        } else {
            if (chatListBodyItemArrayList[position].receiverId == usersharedPrefernce!!.getlearnerid()) {
                if (chatListBodyItemArrayList[position].messageType == 3) {
                    holder.receiver_image.visibility = View.VISIBLE
                    Glide.with(context).load(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message).centerCrop().placeholder(R.drawable.person_girl).into(holder.Rimage)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvImgRecTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvImgRecDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.Rimage.setOnClickListener { chatView.onClickImage(chatListBodyItemArrayList[position].message) }
                } else if (chatListBodyItemArrayList[position].messageType == 4) {
                    holder.rel_video.visibility = View.VISIBLE
                    holder.sender_video.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvVideoRecTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvVideoRecDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.videoViewreciever.setOnClickListener { holder.videoProgressReceiver.visibility = View.VISIBLE }
                    //                    holder.videoViewreciever.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//                        @Override
//                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
//                            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == what) {
//                                holder.videoProgressReceiver.setVisibility(View.GONE);
//                            }
//                            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == what) {
//                                holder.videoProgressReceiver.setVisibility(View.VISIBLE);
//                            }
//                            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == what) {
//                                holder.videoProgressReceiver.setVisibility(View.VISIBLE);
//                            }
//                            return false;
//                        }
//                    });
                    val mediacontroller: MediaController
                    mediacontroller = MediaController(context)
                    if (chatListBodyItemArrayList[position].message != null) {
                        holder.videoViewreciever.setSource(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                        //                        mediacontroller.setAnchorView(holder.videoViewreciever);
//                        holder.videoViewreciever.setMediaController(mediacontroller);
//                        holder.videoViewreciever.setVideoPath(ApiClient.IMAGE_URL + chatListBodyItemArrayList.get(position).getMessage());
//                        holder.videoViewreciever.requestFocus();
//                        holder.videoViewreciever.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                            @Override
//                            public void onPrepared(MediaPlayer mp) {
//                                holder.videoViewreciever.setMediaController(mediacontroller);
//                                mediacontroller.setAnchorView(holder.videoViewreciever);
//                            }
//                        });
                    }
                } else if (chatListBodyItemArrayList[position].messageType == 2) {
                    holder.sender_video.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    //   holder.rlSenderAudio.setVisibility(View.VISIBLE);
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.VISIBLE
                    holder.tvRecAudioTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvRecAudioDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.ivRecAudio.setOnClickListener {
                        if (isPlaying) {
                            if (player != null) {
                                if (player!!.isPlaying) {
                                    player!!.stop()
                                }
                            }
                        }
                        player = MediaPlayer()
                        if (player!!.isPlaying) {
                            holder.tvReceiverPlayVideo.text = "Audio Message"
                            holder.tvReceiverPlayVideo.animation = null
                        }
                        val uri = Uri.parse(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                        animation = AnimationUtils.loadAnimation(context,
                                R.anim.blink)
                        holder.tvReceiverPlayVideo.text = "Playing audio..."
                        holder.ivRecAudio.visibility = View.INVISIBLE
                        holder.tvReceiverPlayVideo.animation = animation
                        player!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
                        try {
                            player!!.setDataSource(context, uri)
                            player!!.prepare()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        if (!player!!.isPlaying) {
                            player!!.start()
                            isPlaying = true
                            // holder.btn_play.setText("Pause");
                            run = Runnable {
                                seekHandler.postDelayed(run!!, 100)
                                val miliSeconds = player!!.currentPosition
                                if (miliSeconds != 0) {
                                    val minutes = TimeUnit.MILLISECONDS.toMinutes(miliSeconds.toLong())
                                    val seconds = TimeUnit.MILLISECONDS.toSeconds(miliSeconds.toLong())
                                    if (minutes == 0L) {
                                        if (!player!!.isPlaying) {
                                            holder.tvReceiverPlayVideo.text = "Audio Message"
                                            holder.tvReceiverPlayVideo.animation = null
                                            holder.ivRecAudio.visibility = View.VISIBLE
                                        }
                                        holder.tvRecAudioDuration.text = "0:" + seconds + "/" + calculateDuration(player!!.duration)
                                    } else {
                                        if (seconds >= 60) {
                                            val sec = seconds - minutes * 60
                                            holder.tvRecAudioDuration.text = minutes.toString() + ":" + sec + "/" + calculateDuration(player!!.duration)
                                        }
                                    }
                                } else {
                                    val totalTime = player!!.duration
                                    val minutes = TimeUnit.MILLISECONDS.toMinutes(totalTime.toLong())
                                    val seconds = TimeUnit.MILLISECONDS.toSeconds(totalTime.toLong())
                                    if (minutes == 0L) {
                                        // holder.tvRecAudioDuration.setText("0:" + seconds);
                                    } else {
                                        if (seconds >= 60) {
                                            val sec = seconds - minutes * 60
                                            //   holder.tvRecAudioDuration.setText(minutes + ":" + sec);
                                        }
                                    }
                                }
                            }
                            run!!.run()
                        } else {
                            player!!.pause()
                            //holder.btn_play.setText("Play");
                        }
                    }

//                    holder.tvPlayVideo.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            try {
//                                chatView.onClickPlayAudio(chatListBodyItemArrayList.get(position).getMessage());
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
                } else {
                    holder.reciever_layout.visibility = View.VISIBLE
//                    client!!.toImage(chatListBodyItemArrayList[position].message, 60, object : com.Lugga.lugga.utils.Callback {
//                        override fun onFailure(e: IOException) {
//                            holder.reciever_message.text = e.message
//                        }
//
//                        override fun onSuccess(ssb: SpannableStringBuilder?) {
//                            holder.reciever_message.text = ssb
//                        }
//                    })
                    // holder.reciever_message.setText(chatListBodyItemArrayList.get(position).getMessage());
                    holder.receiver_image.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvTxtRecTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    Log.e("time", DateHelper.getTime(chatListBodyItemArrayList[position].createdAt))
                    holder.tvDateTxtRec.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                }
            } else {
                if (chatListBodyItemArrayList[position].messageType == 3) {
                    holder.sender_image.visibility = View.VISIBLE
                    Glide.with(context).load(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                            .placeholder(R.drawable.person_girl)
                            .into(holder.simage)
                    holder.sender_layout.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    holder.reciever_layout.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvImgSendrTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvImgSenderDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.simage.setOnClickListener { chatView.onClickImage(chatListBodyItemArrayList[position].message) }
                } else if (chatListBodyItemArrayList[position].messageType == 4) {
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.VISIBLE
                    holder.receiver_image.visibility = View.GONE
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.videoProgress.visibility = View.GONE
                    holder.tvVideoSendrTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvVideoSendrDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.videoViewSender.setOnClickListener {
                        // holder.videoProgress.setVisibility(View.VISIBLE);
                    }
                    //                    holder.videoViewSender.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//                        @Override
//                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
//                            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == what) {
//                                holder.videoProgress.setVisibility(View.GONE);
//                            }
//                            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == what) {
//                                holder.videoProgress.setVisibility(View.VISIBLE);
//                            }
//                            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == what) {
//                                holder.videoProgress.setVisibility(View.VISIBLE);
//                            }
//                            return false;
//                        }
//                    });
                    val mediacontroller: MediaController
                    mediacontroller = MediaController(context)
                    if (chatListBodyItemArrayList[position].message != null) {
                        holder.videoViewSender.setSource(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                        //  mediacontroller.setAnchorView(holder.videoViewSender);
//                        holder.videoViewSender.setMediaController(mediacontroller);
//                        holder.videoViewSender.setVideoPath(ApiClient.IMAGE_URL + chatListBodyItemArrayList.get(position).getMessage());
//                        holder.videoViewSender.requestFocus();
//                        holder.videoViewSender.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                            @Override
//                            public void onPrepared(MediaPlayer mp) {
//                                holder.videoViewSender.setMediaController(mediacontroller);
//                                mediacontroller.setAnchorView(holder.videoViewSender);
//                            }
//                        });
                    }
                } else if (chatListBodyItemArrayList[position].messageType == 2) {
                    holder.sender_video.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    println("url" + ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                    holder.reciever_layout.visibility = View.GONE
                    holder.sender_image.visibility = View.GONE
                    holder.sender_layout.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.VISIBLE
                    holder.rlReceiverAudio.visibility = View.GONE
                    holder.tvAudioSenderTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvAudioSenderDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.ivPlayAudio.setOnClickListener {
                        if (isPlaying) {
                            if (player != null) {
                                if (player!!.isPlaying) {
                                    player!!.stop()
                                }
                            }
                        }
                        player = MediaPlayer()
                        if (player!!.isPlaying) {
                            holder.tvMyAudio.text = "Audio Message"
                            holder.tvMyAudio.animation = null
                        }
                        val uri = Uri.parse(ApiClient.IMAGE_URL + chatListBodyItemArrayList[position].message)
                        animation = AnimationUtils.loadAnimation(context,
                                R.anim.blink)
                        holder.tvMyAudio.text = "Playing audio..."
                        holder.ivPlayAudio.visibility = View.INVISIBLE
                        holder.tvMyAudio.animation = animation
                        player!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
                        try {
                            player!!.setDataSource(context, uri)
                            player!!.prepare()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        if (!player!!.isPlaying) {
                            player!!.start()
                            isPlaying = true
                            // holder.btn_play.setText("Pause");
                            run = Runnable {
                                seekHandler.postDelayed(run!!, 100)
                                val miliSeconds = player!!.currentPosition
                                if (miliSeconds != 0) {
                                    val minutes = TimeUnit.MILLISECONDS.toMinutes(miliSeconds.toLong())
                                    val seconds = TimeUnit.MILLISECONDS.toSeconds(miliSeconds.toLong())
                                    if (minutes == 0L) {
                                        if (!player!!.isPlaying) {
                                            holder.tvMyAudio.text = "Audio Message"
                                            holder.tvMyAudio.animation = null
                                            holder.ivPlayAudio.visibility = View.VISIBLE
                                        }
                                        holder.tvAudioDuration.text = "0:" + seconds + "/" + calculateDuration(player!!.duration)
                                    } else {
                                        if (seconds >= 60) {
                                            val sec = seconds - minutes * 60
                                            holder.tvAudioDuration.text = minutes.toString() + ":" + sec + "/" + calculateDuration(player!!.duration)
                                        }
                                    }
                                } else {
                                    val totalTime = player!!.duration
                                    val minutes = TimeUnit.MILLISECONDS.toMinutes(totalTime.toLong())
                                    val seconds = TimeUnit.MILLISECONDS.toSeconds(totalTime.toLong())
                                    if (minutes == 0L) {
                                        // holder.tvAudioDuration.setText("0:" + seconds);
                                    } else {
                                        if (seconds >= 60) {
                                            val sec = seconds - minutes * 60
                                            //   holder.tvAudioDuration.setText(minutes + ":" + sec);
                                        }
                                    }
                                }
                            }
                            run!!.run()
                        } else {
                            player!!.pause()
                            //holder.btn_play.setText("Play");
                        }
                    }
                } else {
                    holder.sender_layout.visibility = View.VISIBLE
//                    client!!.toImage(chatListBodyItemArrayList[position].message, 60, object : Callback {
//                        override fun onFailure(e: IOException) {
//                            holder.sender_message.text = e.message
//                        }
//
//                        override fun onSuccess(ssb: SpannableStringBuilder?) {
//                            holder.sender_message.text = ssb
//                        }
//                    })
                    holder.tvTxtMsgTime.text = DateHelper.getTime(chatListBodyItemArrayList[position].createdAt)
                    holder.tvTextmsgDate.text = DateHelper.getDate(chatListBodyItemArrayList[position].createdAt)
                    holder.sender_image.visibility = View.GONE
                    holder.receiver_image.visibility = View.GONE
                    holder.reciever_layout.visibility = View.GONE
                    holder.rel_video.visibility = View.GONE
                    holder.sender_video.visibility = View.GONE
                    holder.rlSenderAudio.visibility = View.GONE
                    holder.rlReceiverAudio.visibility = View.GONE
                }
            }
        }
    }

    fun getEmojiByUnicode(unicode: String): String {
        val code = unicode.toInt()
        return String(Character.toChars(code))
    }

    override fun getItemCount(): Int {
        Log.e("zhfkjshdfkshf", chatListBodyItemArrayList.size.toString())
        return chatListBodyItemArrayList.size
    }

    inner class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var sender_layout: RelativeLayout
        var reciever_layout: RelativeLayout
        var receiver_image: RelativeLayout
        var sender_image: RelativeLayout
        var rel_video: RelativeLayout
        var sender_video: RelativeLayout
        var rlSenderAudio: RelativeLayout
        var rlReceiverAudio: RelativeLayout
        var reciever_message: TextView
        var sender_message: TextView
        var tvMyAudio: TextView
        var tvAudioDuration: TextView
        var tvReceiverPlayVideo: TextView
        var tvRecAudioDuration: TextView
        var tvTextmsgDate: TextView
        var tvTxtMsgTime: TextView
        var Rimage: ImageView
        var simage: ImageView
        var ivPlayAudio: ImageView
        var ivRecAudio: ImageView
        var videoViewSender: AndExoPlayerView
        var videoViewreciever: AndExoPlayerView
        var videoProgress: ProgressBar
        var videoProgressReceiver: ProgressBar
        var tvDateTxtRec: TextView
        var tvTxtRecTime: TextView
        var tvImgRecTime: TextView
        var tvImgRecDate: TextView
        var tvImgSendrTime: TextView
        var tvImgSenderDate: TextView
        var tvVideoRecDate: TextView
        var tvVideoRecTime: TextView
        var tvVideoSendrDate: TextView
        var tvVideoSendrTime: TextView
        var tvRecAudioDate: TextView
        var tvRecAudioTime: TextView
        var tvAudioSenderTime: TextView
        var tvAudioSenderDate: TextView

        init {
            tvVideoRecDate = itemView.findViewById(R.id.tvVideoRecDate)
            tvVideoRecTime = itemView.findViewById(R.id.tvVideoRecTime)
            tvAudioSenderTime = itemView.findViewById(R.id.tvAudioSenderTime)
            sender_layout = itemView.findViewById(R.id.rel_Sender)
            tvAudioSenderDate = itemView.findViewById(R.id.tvAudioSenderDate)
            tvRecAudioTime = itemView.findViewById(R.id.tvRecAudioTime)
            rlSenderAudio = itemView.findViewById(R.id.rlSenderAudio)
            tvTextmsgDate = itemView.findViewById(R.id.tvTextmsgDate)
            tvRecAudioDate = itemView.findViewById(R.id.tvRecAudioDate)
            videoProgress = itemView.findViewById(R.id.videoProgress)
            tvImgRecDate = itemView.findViewById(R.id.tvImgRecDate)
            tvVideoSendrDate = itemView.findViewById(R.id.tvVideoSendrDate)
            tvVideoSendrTime = itemView.findViewById(R.id.tvVideoSendrTime)
            reciever_layout = itemView.findViewById(R.id.rel_reciever)
            tvImgRecTime = itemView.findViewById(R.id.tvImgRecTime)
            tvAudioDuration = itemView.findViewById(R.id.tvAudioDuration)
            tvImgSendrTime = itemView.findViewById(R.id.tvImgSendrTime)
            tvImgSenderDate = itemView.findViewById(R.id.tvImgSenderDate)
            tvTxtMsgTime = itemView.findViewById(R.id.tvTxtMsgTime)
            videoProgressReceiver = itemView.findViewById(R.id.videoProgressReceiver)
            tvRecAudioDuration = itemView.findViewById(R.id.tvRecAudioDuration)
            tvTxtRecTime = itemView.findViewById(R.id.tvTxtRecTime)
            tvDateTxtRec = itemView.findViewById(R.id.tvDateTxtRec)
            reciever_message = itemView.findViewById(R.id.reciever_message)
            sender_message = itemView.findViewById(R.id.sender_message)
            receiver_image = itemView.findViewById(R.id.rel_recieverimage)
            sender_image = itemView.findViewById(R.id.rel_senderimage)
            videoViewreciever = itemView.findViewById(R.id.videoViewreciever)
            ivRecAudio = itemView.findViewById(R.id.ivRecAudio)
            videoViewSender = itemView.findViewById(R.id.vv_sender)
            Rimage = itemView.findViewById(R.id.iv_reciever)
            tvReceiverPlayVideo = itemView.findViewById(R.id.tvReceiverPlayVideo)
            simage = itemView.findViewById(R.id.iv_sender)
            ivPlayAudio = itemView.findViewById(R.id.ivPlayAudio)
            tvMyAudio = itemView.findViewById(R.id.tvMyAudio)
            rlReceiverAudio = itemView.findViewById(R.id.rlReceiverAudio)
            rel_video = itemView.findViewById(R.id.rel_recievervideo)
            sender_video = itemView.findViewById(R.id.rel_sendervideo)
        }
    }

    interface ChatView {
        fun onClickImage(message: String?)
    }

    private fun calculateDuration(duration: Int): String {
        var finalDuration = ""
        val minutes = TimeUnit.MILLISECONDS.toMinutes(duration.toLong())
        val seconds = TimeUnit.MILLISECONDS.toSeconds(duration.toLong())
        if (minutes == 0L) {
            finalDuration = "0:0$seconds"
        } else {
            if (seconds >= 60) {
                val sec = seconds - minutes * 60
                finalDuration = "$minutes:$sec"
            }
        }
        return finalDuration
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    init {
        chatListBodyItemArrayList = ArrayList()
        // this.chatListBodyItemArrayList = chatListBodyItemArrayList;
        this.chatView = chatView
    }
}