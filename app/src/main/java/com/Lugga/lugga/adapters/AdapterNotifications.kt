package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.AdapterNotifications.viewHolder
import com.Lugga.lugga.model.usersnotification.UsersNotificationBodyItem
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class AdapterNotifications(private val context: Context, var usersNotificationBodyItemArrayList: ArrayList<UsersNotificationBodyItem>) : RecyclerView.Adapter<viewHolder>() {

    var usersharedPrefernce: UsersharedPrefernce? = null
    var onBookingUpdateInf: onBookingUpdate? = null

    fun setOnClickListener(onBookingUpdate: onBookingUpdate?) {
        this.onBookingUpdateInf = onBookingUpdate
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        val view = LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false)
        return viewHolder(view)
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        if (usersharedPrefernce!!.getusertype() == 1) {
            if (usersNotificationBodyItemArrayList[position].learnerProfile != null) {
                Glide.with(context).load(ApiClient.IMAGE_URL + usersNotificationBodyItemArrayList[position].learnerProfile).centerCrop()
                        .into(holder.iv_profile)
            } else {
                holder.iv_profile.setImageResource(R.drawable.person_girl)
            }
            holder.personName.text = usersNotificationBodyItemArrayList[position].learnerName
            if (usersNotificationBodyItemArrayList[position].bookingStatus == 0) {
                holder.notificationMessage.text = "Requested a " + usersNotificationBodyItemArrayList[position].bookingSessionTime + " session at " +
                        usersNotificationBodyItemArrayList[position].bookingStartTime
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 1) {
                holder.notificationMessage.text = "Declined session request"
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 4) {
                holder.notificationMessage.text = "Accepted session request"
            }
            if (usersNotificationBodyItemArrayList[position].bookingStatus == 1) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 2) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 3) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 4) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 5) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 0) {
                holder.llCurruntNotification.visibility = View.VISIBLE
            } else {
            }

            /* usersharedPrefernce.setlearnername(usersNotificationBodyItemArrayList.get(position).getLearnerName());
            usersharedPrefernce.setnotificationmessage(usersNotificationBodyItemArrayList.get(position).getNotificationMessage());
            holder.llcounterTimeDate.setVisibility(View.GONE);
            holder.llCurruntNotification.setVisibility(View.VISIBLE);
            holder.rlPersonImage.setBackground(context.getResources().getDrawable(R.drawable.image_background_red));
            holder.personName.setText(usersharedPrefernce.getlearnername());
            holder.personName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.notificationMessage.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.notificationMessage.setText(usersharedPrefernce.getnotificationmessage());*/
        } else {
            holder.personName.text = usersNotificationBodyItemArrayList[position].teacherName
            holder.notificationMessage.text = usersNotificationBodyItemArrayList[position].notificationMessage

            if (usersNotificationBodyItemArrayList[position].teacherProfile != null) {
                Glide.with(context).load(ApiClient.IMAGE_URL + usersNotificationBodyItemArrayList[position].teacherProfile).centerCrop()
                        .into(holder.iv_profile)
            } else {
                holder.iv_profile.setImageResource(R.drawable.person_girl)
            }
            if (usersNotificationBodyItemArrayList[position].bookingStatus == 1) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 2) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 3) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 4) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 5) {
            } else if (usersNotificationBodyItemArrayList[position].bookingStatus == 0) {
            } else {
            }
        }
        holder.textAccept.setOnClickListener { onBookingUpdateInf!!.onbookingupdate(position, "4", usersNotificationBodyItemArrayList[position].bookingID.toString(),
        usersNotificationBodyItemArrayList.get(position).leasrnerID)}

        holder.textReject.setOnClickListener { onBookingUpdateInf!!.onbookingupdate(position, "1", usersNotificationBodyItemArrayList[position].bookingID.toString(),
                usersNotificationBodyItemArrayList.get(position).leasrnerID) }


        holder.ivDelete.setOnClickListener { onBookingUpdateInf!!.onClickDeleteNotification(usersNotificationBodyItemArrayList[position].notificationID) }
    }

    override fun getItemCount(): Int {
        return usersNotificationBodyItemArrayList.size
    }

    inner class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rlPersonImage: RelativeLayout
        var llCurruntNotification: LinearLayout
        var llcounterTimeDate: LinearLayout
        var mainView: View? = null
        var ivDelete: ImageView
        var llSlideLeft: LinearLayout
        var iv_profile: CircleImageView
        var personName: TextView
        var notificationMessage: TextView
        var textReject: TextView
        var textAccept: TextView

        init {
            rlPersonImage = itemView.findViewById(R.id.rlPersonImage)
            llCurruntNotification = itemView.findViewById(R.id.llCurruntNotification)
            llcounterTimeDate = itemView.findViewById(R.id.llcounterTimeDate)
            personName = itemView.findViewById(R.id.personName)
            //    notificationMessage=itemView.findViewById(R.id.notificationMessage);
            notificationMessage = itemView.findViewById(R.id.notificationMessage)
            llSlideLeft = itemView.findViewById(R.id.llSlideLeft)
            ivDelete = itemView.findViewById(R.id.ivDelete)
            textReject = itemView.findViewById(R.id.textReject)
            textAccept = itemView.findViewById(R.id.textAccept)
            iv_profile = itemView.findViewById(R.id.iv_profile)
        }
    }

    interface onBookingUpdate {
        fun onbookingupdate(position: Int, status: String?, BookingID: String?, leasrnerID: Int)
        fun onClickDeleteNotification(notificationID: Int?)
    }

}