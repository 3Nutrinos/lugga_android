package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import java.util.*

class AllLanguageAdapter(var context: Context, var body: ArrayList<AllLanguagModel>) : BaseAdapter() {

    private var inflter: LayoutInflater? = null

    override fun getCount(): Int {
        return body.size
    }

    override fun getItem(position: Int): Any {
        return body[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, view1: View?, parent: ViewGroup): View {
        var view = view1
        view = inflter?.inflate(R.layout.spinner_item, null)!!
        val names = view.findViewById<TextView>(R.id.text1)
        names.text = body[position].language
        return view
    }

    init {
        inflter = LayoutInflater.from(context)
    }

}