package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import kotlinx.android.synthetic.main.item_days.view.*
import kotlinx.android.synthetic.main.item_language.view.*


class LanguageAdapter(val context: Context, var list: ArrayList<AllLanguagModel>?) : RecyclerView.Adapter<LanguageAdapter.Holder>() {


    private var lastCheckedRB: RadioButton?=null

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_language, null))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        val model = list?.get(position)

        holder.itemView.radioButton.text=model?.language

        if (model!!.isSelected!!) {
            model!!.isSelected = false
            holder.itemView.tvCheckLanguage.background = ContextCompat.getDrawable(context, R.drawable.back_unselected_day)

        } else {
            model!!.isSelected = true
            holder.itemView.tvCheckLanguage.background = ContextCompat.getDrawable(context, R.drawable.back_selected_day)

        }


    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)

    }
}