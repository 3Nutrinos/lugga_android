package com.Lugga.lugga.adapters;

import android.content.Context;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

import com.Lugga.lugga.model.TimerModel;
import com.bumptech.glide.Glide;
import com.Lugga.lugga.R;
import com.Lugga.lugga.model.learnerhomepage.LearnerHomePageBodyItem;
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce;
import com.Lugga.lugga.utils.ApiClient;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public class AdapterLearner extends RecyclerView.Adapter<AdapterLearner.ViewHolder> {

    private Context context;
    UsersharedPrefernce usersharedPrefernce;
    LinearLayout starlayout;
    ArrayList<LearnerHomePageBodyItem> learnerHomePageBodyItemArrayList;
    ontimerstart ontimerstart;
    onpayclick onpayclick;
    private long timeCountMili;
    private String currentdateTimeApi;
    private Date datee;
    private Date ConvertedTimeWithZone;

    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss a z";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
    private CountDownTimer countDownTimer;
    private String serverTime;
    private Date s;
    private DateFormat fr;
    private Date startSessionTim;
    private String finalMinute;
    private String finalSecond;
    private String myhour, myMinute, mySecond;
    private TimerModel timerModel;

    public AdapterLearner(Context context, ArrayList<LearnerHomePageBodyItem> learnerHomePageBodyItemArrayList) {

        this.context = context;
        this.learnerHomePageBodyItemArrayList = learnerHomePageBodyItemArrayList;

    }

    public void setoncLicklistener(ontimerstart ontimerstart) {
        this.ontimerstart = ontimerstart;
    }

    public void setonPayClickListener(onpayclick onpayeclick) {
        this.onpayclick = onpayeclick;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        usersharedPrefernce = UsersharedPrefernce.getInstance();
        View view = LayoutInflater.from(context).inflate(R.layout.item_persons, parent, false);
        return new AdapterLearner.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (learnerHomePageBodyItemArrayList.get(position).profileImage != null) {
            Glide.with(context).load(ApiClient.IMAGE_URL + learnerHomePageBodyItemArrayList.get(position).profileImage).centerCrop().into(holder.profile);
        } else {
            holder.profile.setImageResource(R.drawable.person_girl);
        }

        holder.personName.setText(learnerHomePageBodyItemArrayList.get(position).name);
        holder.tvRating.setText(String.format("%.1f", learnerHomePageBodyItemArrayList.get(position).getTotal_rating()));
        //     holder.paymentamount.setText("" + learnerHomePageBodyItemArrayList.get(position).getPaymentAmount());
        holder.time.setText(learnerHomePageBodyItemArrayList.get(position).sessionTime + " min");

        if (learnerHomePageBodyItemArrayList.get(position).bookingType == 5 ||
                learnerHomePageBodyItemArrayList.get(position).bookingType == 6) {
            holder.paymentamount.setText("$" + learnerHomePageBodyItemArrayList.get(position).paymentAmount);
        } else {
            switch (learnerHomePageBodyItemArrayList.get(position).sessionTime) {
                case "30":
                    holder.paymentamount.setText("$" + learnerHomePageBodyItemArrayList.get(position).getAmount());

                    break;
                case "60":
                    holder.paymentamount.setText("$" + learnerHomePageBodyItemArrayList.get(position).getAmount() * 2);

                    break;

                case "90":
                    holder.paymentamount.setText("$" + learnerHomePageBodyItemArrayList.get(position).getAmount() * 3);
                    break;

                case "120":
                    holder.paymentamount.setText("$" + learnerHomePageBodyItemArrayList.get(position).getAmount() * 4);
                    break;
            }
        }


        if (learnerHomePageBodyItemArrayList.get(position).getTotal_rating() == 0) {
            holder.star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.star_grey));
        } else {
            holder.star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.star_yellow));
        }

        //**************************************getting date and time****************************************//
        String createdAt = learnerHomePageBodyItemArrayList.get(position).bookingdate;
   //     System.out.println("ddcdcdc" + createdAt);
        String parts[] = createdAt.split("T");
//        System.out.println("ffks   " + parts[0] + "    " + parts[1]);
        String date = parts[0];
        String day[] = date.split("-");
   //     System.out.println("nckskdsds" + day[0] + "   " + day[1] + "   " + day[2]);
        holder.date.setText(day[2] + "/" + day[1] + "/" + day[0]);


        String sessionstarttime = learnerHomePageBodyItemArrayList.get(position).startSession;
        String timeparts[] = sessionstarttime.split(":");
        int time1 = Integer.parseInt(timeparts[0]);
        int newtime1 = 0;
        if (time1 > 12) {
            newtime1 = time1 - 12;
        } else {
            newtime1 = time1;
        }
        //holder.endTime.setText(newtime1 + ":" + timeparts[1]);
        int minute = Integer.parseInt(timeparts[1]);
        int seconds = Integer.parseInt(timeparts[2]);


        if (learnerHomePageBodyItemArrayList.get(position).bookingStatus == 1) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayclick.onPayClickClicklistener(position, learnerHomePageBodyItemArrayList, 2, timerModel);
                }
            });


            String apiTimezone = learnerHomePageBodyItemArrayList.get(position).timezone;
            String apiBookingDate = learnerHomePageBodyItemArrayList.get(position).bookingdate;
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            // ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);


            String filename = learnerHomePageBodyItemArrayList.get(position).startSession;     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);

            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);


            holder.textStatus.setText("Rejected");
            System.out.println("timeinmiikis" + getTimeInMillis(newtime1, minute, seconds) + "      " + System.currentTimeMillis());
        } else if (learnerHomePageBodyItemArrayList.get(position).bookingStatus == 2) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayclick.onPayClickClicklistener(position, learnerHomePageBodyItemArrayList, 2, timerModel);
                }
            });

            holder.textStatus.setText("Ready");


            String apiTimezone = learnerHomePageBodyItemArrayList.get(position).timezone;
            String apiBookingDate = learnerHomePageBodyItemArrayList.get(position).bookingdate;
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            // ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);


            String filename = learnerHomePageBodyItemArrayList.get(position).startSession;     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];

            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }

            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }


            ZonedDateTime currentISTime =
                    null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                currentISTime = ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                        Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                        Integer.parseInt(mySecond), 1234, fromTimeZone);
            }

            ZonedDateTime currentETime = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                currentETime = currentISTime.withZoneSameInstant(toTimeZone);
            }

            //Format date time - optional
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                System.out.println("from " + formatter.format(currentISTime));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                System.out.println("to " + formatter.format(currentETime));
            }


            String dtStart = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                dtStart = formatter.format(currentETime).substring(10, 20);
            }
            holder.endTime.setText(dtStart);


        } else if (learnerHomePageBodyItemArrayList.get(position).bookingStatus == 3) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayclick.onPayClickClicklistener(position, learnerHomePageBodyItemArrayList, 2, timerModel);
                }
            });

            holder.textStatus.setText("Upcoming");
            holder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));


            String apiTimezone = learnerHomePageBodyItemArrayList.get(position).timezone;
            String apiBookingDate = learnerHomePageBodyItemArrayList.get(position).bookingdate;
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //     ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);


            String filename = learnerHomePageBodyItemArrayList.get(position).startSession;     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);


        } else if (learnerHomePageBodyItemArrayList.get(position).bookingStatus == 4) {
            holder.textStatus.setText("Pay");
            holder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));


            String apiTimezone = learnerHomePageBodyItemArrayList.get(position).timezone;
            String apiBookingDate = learnerHomePageBodyItemArrayList.get(position).bookingdate;
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //  ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);


            String filename = learnerHomePageBodyItemArrayList.get(position).startSession;     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);


//////////////////for schedule send ending time when we pay---------------------------------------------------


            String endTimeStr = formatter.format(currentETime).substring(10, 19);        /// finalDateTime for comparing with server time


            /// adding session time with start session time===================

            String myTime = endTimeStr;
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date d = null;
            try {
                d = df.parse(myTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calender = Calendar.getInstance();
            calender.setTime(d);
            calender.add(Calendar.MINUTE, Integer.parseInt(learnerHomePageBodyItemArrayList.get(0).sessionTime));
            String newTimeAferAdding = df.format(calender.getTime());

            String sessionCloseTime = newTimeAferAdding;

            Log.e("sessionCloseTime", sessionCloseTime);
            String SessionEndTimeFormat = sessionCloseTime;


            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (learnerHomePageBodyItemArrayList.get(position).bookingType == 0) {
                        onpayclick.onPayClickClicklistener(position, learnerHomePageBodyItemArrayList, 1, timerModel);
                    } else {
                        onpayclick.onPayClick(dtStart, sessionCloseTime, position, learnerHomePageBodyItemArrayList);
                    }

                }
            });


        } else if (learnerHomePageBodyItemArrayList.get(position).bookingStatus == 5) {


            String apiTimezone = learnerHomePageBodyItemArrayList.get(position).timezone;
            String apiBookingDate = learnerHomePageBodyItemArrayList.get(position).bookingdate;
            String subStrBookingDate = apiBookingDate.substring(0, 10);
            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //     ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);


            String filename = learnerHomePageBodyItemArrayList.get(position).startSession;     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);

            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);


            //-------compare time----------


            String convert = formatter.format(currentETime).substring(0, 19);
            String reverse = convert.substring(0, 10);


            final String OLD_FORMAT = "dd-MM-yyyy";
            final String NEW_FORMAT = "yyyy-MM-dd";
            String oldDateString = reverse;
            String newDateString;
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date d1 = null;
            try {
                d1 = sdf.parse(oldDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d1);

            // Log.e("serverNewDate",newDateString);


            String endTimeStr = newDateString + "" + formatter.format(currentETime).substring(10, 19);        /// finalDateTime for comparing with server time


            String sessionStartTime = endTimeStr;

            String startSessionTime = newDateString + " " + learnerHomePageBodyItemArrayList.get(0).startSession;

            /// adding session time with start session time===================

            String myTime = sessionStartTime;
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = null;
            try {
                d = df.parse(myTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calender = Calendar.getInstance();
            calender.setTime(d);
            calender.add(Calendar.MINUTE, Integer.parseInt(learnerHomePageBodyItemArrayList.get(0).sessionTime));
            String newTimeAferAdding = df.format(calender.getTime());

            String sessionCloseTime = newTimeAferAdding;

            Log.e("sessionCloseTime", sessionCloseTime);
            String SessionEndTimeFormat = sessionCloseTime;

            ////////////convert currentDateTimeServer to local time--------------------
            // 2020-05-06 00:11:39


            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            formatter.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles")); // Or whatever IST is supposed to be
            try {
                s = formatter.parse(serverTime);
                fr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                fr.setTimeZone(TimeZone.getTimeZone(timeZone.getID()));
                Log.e("timeNeewhai", fr.format(s));

            } catch (ParseException e) {
                Log.e("error", e.toString());
            }


            String startTimeStr = fr.format(s);

            Date serverCurrentDateTime = null;
            Date endSessionTime = null;
            try {
                SimpleDateFormat sdf12 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                serverCurrentDateTime = sdf12.parse(startTimeStr);
                endSessionTime = sdf12.parse(SessionEndTimeFormat);


                Log.e("EndsessionTime---------", String.valueOf(endSessionTime));
                Log.e("ServerCurrentDateTime-----", String.valueOf(serverCurrentDateTime));

            } catch (Exception e) {
                e.printStackTrace();
            }


            /////comparing start session time


            startSessionTim = null;
            try {
                SimpleDateFormat sdf12 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                serverCurrentDateTime = sdf12.parse(startTimeStr);
                startSessionTim = sdf12.parse(startSessionTime);


                Log.e("startSessionTim-", String.valueOf(startSessionTim));
                Log.e("ServerCurrentDateT", String.valueOf(serverCurrentDateTime));


            } catch (Exception e) {
                e.printStackTrace();
            }


            //--------Compare time -------
            if (learnerHomePageBodyItemArrayList.get(position).bookingType == 1) {


                if (serverCurrentDateTime.compareTo(startSessionTim) < 0) {        /////bada hai startSessionTim   -- booking not started/ongoing booking
                    holder.textStatus.setText("Scheduled");
                    holder.textCounter.setVisibility(View.INVISIBLE);

                    holder.llDateTime.setVisibility(View.VISIBLE);


                    Log.e("app", "serverCurrentDateTime is before endSessionTime");


                } else if (serverCurrentDateTime.compareTo(endSessionTime) < 0) {        /////bada hai endSessionTime   -- booking not started/ongoing booking
                    holder.textStatus.setText("Ongoing");
                    holder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

                    holder.llDateTime.setVisibility(View.INVISIBLE);
                    holder.textCounter.setVisibility(View.VISIBLE);
                    long mills = endSessionTime.getTime() - serverCurrentDateTime.getTime();
                    long hours = mills / (1000 * 60 * 60);
                    long mins = (mills / (1000 * 60)) % 60;

                    String diff = hours + ":" + mins;
                    timerModel = new TimerModel();
                    Log.e("difffff", String.valueOf(mills));

                    countDownTimer = new CountDownTimer(mills, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            holder.textCounter.setText("" + String.format("%d : %d ",
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

                            timerModel.setMin(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            timerModel.setSec(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

                        }

                        @Override
                        public void onFinish() {
                            countDownTimer.cancel();
                            onpayclick.hitCompletedBookingApi(position, "6", learnerHomePageBodyItemArrayList.get(position).bookingId);
                        }
                    };
                    countDownTimer.start();

                    Log.e("app", "serverCurrentDateTime is before endSessionTime");


                } else {
                    onpayclick.hitCompletedBookingApi(position, "6", learnerHomePageBodyItemArrayList.get(position).bookingId);
                }
            } else {

                holder.textStatus.setText("Ongoing");
                holder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));


                holder.llDateTime.setVisibility(View.INVISIBLE);
                holder.textCounter.setVisibility(View.VISIBLE);
                //--------Compare endsession time -------

                if (serverCurrentDateTime.compareTo(endSessionTime) > 0) {       /////chota hai endSessionTime

                    Log.e("app", "serverCurrentDateTime is after endSessionTime");
                    onpayclick.hitCompletedBookingApi(position, "6", learnerHomePageBodyItemArrayList.get(position).bookingId);

                } else if (serverCurrentDateTime.compareTo(endSessionTime) < 0) {        /////bada hai endSessionTime   -- booking not started/ongoing booking


                    long mills = endSessionTime.getTime() - serverCurrentDateTime.getTime();
                    long hours = mills / (1000 * 60 * 60);
                    long mins = (mills / (1000 * 60)) % 60;
                    timerModel = new TimerModel();
                    String diff = hours + ":" + mins;

                    Log.e("difffff", String.valueOf(mills));

                    countDownTimer = new CountDownTimer(mills, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                            holder.textCounter.setText("" + String.format("%d : %d ",
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

                            timerModel.setMin(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            timerModel.setSec(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

                        }

                        @Override
                        public void onFinish() {
                            countDownTimer.cancel();
                            onpayclick.hitCompletedBookingApi(position, "6", learnerHomePageBodyItemArrayList.get(position).bookingId);
                        }
                    };
                    countDownTimer.start();

                    Log.e("app", "serverCurrentDateTime is before endSessionTime");


                } else if (serverCurrentDateTime.compareTo(endSessionTime) == 0) {

                    Log.e("app", "serverCurrentDateTime is equal to endSessionTime");
                }


            }


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayclick.onPayClickClicklistener(position, learnerHomePageBodyItemArrayList, 2,timerModel);
                }
            });


        } else if (learnerHomePageBodyItemArrayList.get(position).bookingStatus == 6) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayclick.onPayClickClicklistener(position, learnerHomePageBodyItemArrayList, 2, timerModel);
                }
            });

            holder.textStatus.setText("Completed");


            String apiTimezone = learnerHomePageBodyItemArrayList.get(position).timezone;
            String apiBookingDate = learnerHomePageBodyItemArrayList.get(position).bookingdate;
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = null;    //Source timezone
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                fromTimeZone = ZoneId.of(apiTimezone);
            }
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = null;  //Target timezone
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                toTimeZone = ZoneId.of(timeZone.getID());
            }

            //   ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);


            String filename = learnerHomePageBodyItemArrayList.get(position).startSession;     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                currentETime = currentISTime.withZoneSameInstant(toTimeZone);
            }

            //Format date time - optional
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                System.out.println("from " + formatter.format(currentISTime));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                System.out.println("to " + formatter.format(currentETime));
            }


            String dtStart = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                dtStart = formatter.format(currentETime).substring(10, 20);
            }
            holder.endTime.setText(dtStart);


        } else if (learnerHomePageBodyItemArrayList.get(position).bookingStatus == 7) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayclick.onPayClickClicklistener(position, learnerHomePageBodyItemArrayList, 2, timerModel);
                }
            });

            holder.textStatus.setText("Expired");


            String apiTimezone = learnerHomePageBodyItemArrayList.get(position).timezone;
            String apiBookingDate = learnerHomePageBodyItemArrayList.get(position).bookingdate;
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //       ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);

            String filename = learnerHomePageBodyItemArrayList.get(position).startSession;     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);

            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);


        } else if (learnerHomePageBodyItemArrayList.get(position).bookingStatus == 0) {

            holder.textStatus.setText("Waiting for teacher to Accept (teacher has 30 min to accept)");


            String apiTimezone = learnerHomePageBodyItemArrayList.get(position).timezone;
            String apiBookingDate = learnerHomePageBodyItemArrayList.get(position).bookingdate;
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //   ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);


            String filename = learnerHomePageBodyItemArrayList.get(position).startSession;     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);





            ZonedDateTime currentETime = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                ZonedDateTime currentISTime =
                        ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                                Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                                Integer.parseInt(mySecond), 1234, fromTimeZone);
                currentETime = currentISTime.withZoneSameInstant(toTimeZone);
                //Format date time - optional
                System.out.println("from " + formatter.format(currentISTime));
                System.out.println("to " + formatter.format(currentETime));


                String dtStart = formatter.format(currentETime).substring(10, 20);


                holder.endTime.setText(dtStart);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onpayclick.onPayClickClicklistener(position, learnerHomePageBodyItemArrayList, 2, timerModel);
                }
            });

        }


    }

    public static long getTimeInMillis(int hour, int minute, int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(hour, minute, seconds);
        return calendar.getTimeInMillis();
    }


    @Override
    public int getItemCount() {

        return learnerHomePageBodyItemArrayList.size();

    }

    public void setCreatetimeZone(String currentdateTime) {
        serverTime = currentdateTime;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView profile;
        RelativeLayout rlPersonImage;
        TextView personName, textCounter, textStatus, nobookingyet;
        LinearLayout llDateTime, bookedlayout;
        View view, mainView;
        CardView cardView;
        TextView time, paymentamount, endTime, date;
        ImageView star;
        TextView tvRating;


        public ViewHolder(final View itemView) {
            super(itemView);

            rlPersonImage = itemView.findViewById(R.id.rlPersonImage);
            personName = itemView.findViewById(R.id.personName);
            llDateTime = itemView.findViewById(R.id.llDateTime);
            textStatus = itemView.findViewById(R.id.textStatus);
            textCounter = itemView.findViewById(R.id.textCounter);
            view = itemView.findViewById(R.id.view);
            mainView = itemView.findViewById(R.id.mainView);

            cardView = itemView.findViewById(R.id.cardView);
            time = itemView.findViewById(R.id.time);
            paymentamount = itemView.findViewById(R.id.paymentamount);
            starlayout = itemView.findViewById(R.id.starLayout);
            profile = itemView.findViewById(R.id.iv_profile);
            endTime = itemView.findViewById(R.id.endTime);
            date = itemView.findViewById(R.id.date);
            nobookingyet = itemView.findViewById(R.id.nobookingyet);
            bookedlayout = itemView.findViewById(R.id.bookedlayout);
            star = itemView.findViewById(R.id.rating_star);
            tvRating = itemView.findViewById(R.id.ratingstar);


        }


    }


    public interface ontimerstart {
        void ontimerstart(int position, String starttime, String endtime);

    }

    public interface onpayclick {
        void onPayClickClicklistener(int position, ArrayList<LearnerHomePageBodyItem> learnerHomePageBodyItemArrayList, int i, @Nullable TimerModel timerModel);

        void hitCompletedBookingApi(int position, String status, int booking_id);


        void onPayClick(String dtStart, String sessionCloseTime, int position, ArrayList<LearnerHomePageBodyItem> learnerHomePageBodyItemArrayList);
    }


}
