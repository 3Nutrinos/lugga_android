package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.chatList.ChatListBodyItem
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.DateHelper
import com.bumptech.glide.Glide
import java.util.*

class ChatAdapter(var context: Context, var chatListBodyItemArrayList: ArrayList<ChatListBodyItem>, var chat: OnChat) : RecyclerView.Adapter<ChatAdapter.holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.chat_list_item, null)
        return holder(view)
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        val model = chatListBodyItemArrayList[position]
        if (model.name != null) {
            holder.tvChatPersonName.text = model.name
        }
        if (model.last_msg != null) {
            holder.tvLastMessage.text = model.last_msg
        }
        if (model.createdAt != null) {
            holder.tvTime.text = DateHelper.getTime(model.createdAt)
        }
        if (model.profile_image != null) {
            Glide.with(context).load(ApiClient.IMAGE_URL + model.profile_image).centerCrop().into(holder.ivChatPerson)
        } else {
            holder.ivChatPerson.setImageResource(R.drawable.person_girl)
        }
        holder.rlChat.setOnClickListener { chat.onClickChat(model.bookingId, model.name) }
    }

    override fun getItemCount(): Int {
        return chatListBodyItemArrayList.size
    }

    inner class holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivChatPerson: ImageView
        var tvChatPersonName: TextView
        var tvTime: TextView
        var tvLastMessage: TextView
        var rlChat: RelativeLayout

        init {
            ivChatPerson = itemView.findViewById(R.id.ivChatPerson)
            tvChatPersonName = itemView.findViewById(R.id.tvChatPersonName)
            tvTime = itemView.findViewById(R.id.tvTime)
            tvLastMessage = itemView.findViewById(R.id.tvLastMessage)
            rlChat = itemView.findViewById(R.id.rlChat)
        }
    }

    interface OnChat {
        fun onClickChat(bookingId: Int?, name: String?)
    }

}