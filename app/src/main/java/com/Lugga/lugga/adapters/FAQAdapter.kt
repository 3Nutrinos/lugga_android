package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.FAQModel

class FAQAdapter(var list: Array<FAQModel>, var context: Context) : RecyclerView.Adapter<FAQAdapter.holder>() {
    var isClick = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_faq, null)
        return holder(view)
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        val model = list[position]
        if (model.ques != null) {
            holder.tvQus.text = model.ques
        }
        if (model.ans != null) {
            holder.tvAns.text = model.ans
        }
        if (model.count != null) {
            holder.tvCount.text = model.count + " )"
        }
        holder.ivDownArrow.setOnClickListener {
            if (isClick) {
                isClick = false
                holder.tvAns.visibility = View.GONE
                holder.ivDownArrow.setImageResource(R.mipmap.down_arrow)
            } else {
                holder.tvAns.visibility = View.VISIBLE
                isClick = true
                holder.ivDownArrow.setImageResource(R.mipmap.up_arrow)
            }
        }
        holder.tvQus.setOnClickListener {
            if (isClick) {
                isClick = false
                holder.tvAns.visibility = View.GONE
                holder.ivDownArrow.setImageResource(R.mipmap.down_arrow)
            } else {
                holder.tvAns.visibility = View.VISIBLE
                isClick = true
                holder.ivDownArrow.setImageResource(R.mipmap.up_arrow)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvQus: TextView
        var tvAns: TextView
        var tvCount: TextView
        var ivDownArrow: ImageView

        init {
            tvAns = itemView.findViewById(R.id.tvAns)
            tvQus = itemView.findViewById(R.id.tvQus)
            tvCount = itemView.findViewById(R.id.tvCount)
            ivDownArrow = itemView.findViewById(R.id.ivDownArrow)
        }
    }

}