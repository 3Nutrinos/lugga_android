package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.searchteacher.SearchTeacherBodyItem
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.bumptech.glide.Glide
import java.util.*
import kotlin.collections.ArrayList

class AdapterAvailableTeacher   (var context: Context, var searchTeacherBodyItemArrayList: ArrayList<SearchTeacherBodyItem>, var availableTeacherView: AvailableTeacherView) : RecyclerView.Adapter<AdapterAvailableTeacher.ViewHolder>() {
    private var orig: ArrayList<SearchTeacherBodyItem>?=null
    var usersharedPrefernce: UsersharedPrefernce? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //searchTeacherBodyItemArrayList=new ArrayList<>();
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        val view = LayoutInflater.from(context).inflate(R.layout.item_available_teacher, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(ApiClient.IMAGE_URL + searchTeacherBodyItemArrayList[position].profileImage).placeholder(R.mipmap.logo).centerCrop().into(holder.profile)
        holder.availableTeacherName.text = searchTeacherBodyItemArrayList[position].name
        holder.availableTeacherRating.text = String.format("%.1f", searchTeacherBodyItemArrayList[position].total_rating)
        holder.availableTeacherAmount.text = "$" + searchTeacherBodyItemArrayList[position].amount
        holder.availableTeacherTime.text = searchTeacherBodyItemArrayList[position].time + "min"
        holder.itemView.setOnClickListener { availableTeacherView.onClickTeacherSelect(searchTeacherBodyItemArrayList[position].id) }
    }

    override fun getItemCount(): Int {
        return searchTeacherBodyItemArrayList.size
    }

    fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val oReturn = FilterResults()
                val results: ArrayList<SearchTeacherBodyItem> = ArrayList<SearchTeacherBodyItem>()
                if (orig == null) orig = searchTeacherBodyItemArrayList
                if (constraint != null) {
                    if (orig != null && orig!!.size > 0) {
                        for (g in orig!!) {
                            if (g.name.toUpperCase()
                                            .contains(constraint.toString().toUpperCase())) results.add(g)
                        }
                    }
                    oReturn.values = results
                }
                return oReturn
            }

            override fun publishResults(constraint: CharSequence,
                                        results: FilterResults) {
                searchTeacherBodyItemArrayList = results.values as ArrayList<SearchTeacherBodyItem>
                notifyDataSetChanged()
            }
        }
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var availableTeacherName: TextView
        var availableTeacherTime: TextView
        var availableTeacherAmount: TextView
        var availableTeacherRating: TextView
        var profile: ImageView

        init {
            availableTeacherName = itemView.findViewById(R.id.availableTeacherName)
            availableTeacherAmount = itemView.findViewById(R.id.availableTeacherAmount)
            availableTeacherTime = itemView.findViewById(R.id.availableTeacherTime)
            availableTeacherRating = itemView.findViewById(R.id.availableTeacherRating)
            profile = itemView.findViewById(R.id.iv_profile)
        }
    }

    interface AvailableTeacherView {
        fun onClickTeacherSelect(id: Int?)
    }

}