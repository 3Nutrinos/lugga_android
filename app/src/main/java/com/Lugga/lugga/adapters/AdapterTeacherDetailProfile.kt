package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.teacherdetails.TeacherDetailsBodyItem
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.DateHelper
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class AdapterTeacherDetailProfile(private val context: Context, var teacherDetailsBodyItemArrayList: ArrayList<TeacherDetailsBodyItem>) : RecyclerView.Adapter<AdapterTeacherDetailProfile.ViewHolder>() {
    var usersharedPrefernce: UsersharedPrefernce? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        val view = LayoutInflater.from(context).inflate(R.layout.item_teacher_detail, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.teacherDetailDate.text = DateHelper.getDate(teacherDetailsBodyItemArrayList[position].feedbackTimeing)
        holder.teacherdetailName.text = teacherDetailsBodyItemArrayList[position].name
        holder.teacherDetailTime.text = DateHelper.getTime(teacherDetailsBodyItemArrayList[position].feedbackTimeing)
        holder.tvFeedback.text = teacherDetailsBodyItemArrayList[position].feedbackMessage
        holder.tvRating.text = teacherDetailsBodyItemArrayList[position].feedbackRating
        Glide.with(context).load(ApiClient.IMAGE_URL + teacherDetailsBodyItemArrayList[position].profileImage)
                .placeholder(R.drawable.person_girl)
                .centerCrop().into(holder.teacherDetailImage)
    }

    override fun getItemCount(): Int {
        return teacherDetailsBodyItemArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var teacherDetailImage: CircleImageView
        var teacherdetailName: TextView
        var teacherDetailDate: TextView
        var teacherDetailTime: TextView
        var tvFeedback: TextView
        var tvRating: TextView

        init {
            teacherDetailImage = itemView.findViewById(R.id.teacherdetailImage)
            teacherdetailName = itemView.findViewById(R.id.teacherdetailName)
            teacherDetailDate = itemView.findViewById(R.id.teacherdetailDate)
            teacherDetailTime = itemView.findViewById(R.id.teacherdetailTime)
            tvFeedback = itemView.findViewById(R.id.tvFeedback)
            tvRating = itemView.findViewById(R.id.tvRating)
        }
    }

}