package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import kotlinx.android.synthetic.main.language_item.view.*


class OtherLangAdapter(val context: Context, var list: ArrayList<AllLanguagModel>?, val listener: Listener): RecyclerView.Adapter<OtherLangAdapter.Holder>() {

    private var orig: ArrayList<AllLanguagModel>?=null


    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.language_item,null))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        val model = list?.get(position)
        holder.itemView.tvLang.text=model?.language

        holder.itemView.tvLang.setOnClickListener {
            listener.onClickOtherLanguage(model?.language)
        }

    }
    fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val oReturn = FilterResults()
                val results: ArrayList<AllLanguagModel> = ArrayList<AllLanguagModel>()
                if (orig == null) orig = list
                if (constraint != null) {
                    if (orig != null && orig!!.size > 0) {
                        for (g in orig!!) {
                            if (g.language?.toUpperCase()!!.contains(constraint.toString().toUpperCase())) results.add(g)
                        }
                    }
                    oReturn.values = results
                }
                return oReturn
            }

            override fun publishResults(constraint: CharSequence,
                                        results: FilterResults) {
                list = results.values as ArrayList<AllLanguagModel>
                notifyDataSetChanged()
            }
        }
    }
    interface Listener{
        fun onClickOtherLanguage(language: String?)

    }
}