package com.Lugga.lugga.sharedpreferences;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.view.WindowInsets;
import android.view.WindowInsetsController;

//
//import com.Lugga.lugga.R;
//import com.Lugga.lugga.views.BasicActivity;
//import com.balsikandar.crashreporter.CrashReporter;
//import com.balsikandar.crashreporter.utils.CrashUtil;

import com.Lugga.lugga.AudioCallActivity;
import com.stripe.android.Stripe;

import org.jetbrains.annotations.NotNull;

import java.net.Socket;


public class BaseApplication extends Application {
    static Context context;
    private static Socket socket;

    public static final String TAG = BaseApplication.class.getSimpleName();
    private static BaseApplication mInstance;
    private static Stripe stripe;

    public static String online="false";

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        BaseApplication.online = online;
    }

    public static synchronized BaseApplication getInstance() {
        return mInstance;
    }

    public static void fullScreen(@NotNull Activity audioCallActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            final WindowInsetsController controller = audioCallActivity.getWindow().getInsetsController();

            if (controller != null)
                controller.hide(WindowInsets.Type.statusBars());
        } else {
            //noinspection deprecation
            audioCallActivity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        mInstance = this;
        //startService(new Intent(this, BackgroundService.class));
//
//        CrashReporter.initialize(this);
//        CrashUtil.getDefaultPath();
//        try {
//            // Do your stuff
//        } catch (Exception e) {
//            CrashReporter.logException(e);
//        }


    }

    static public Context getContext() {
        return context;
    }

    public static Stripe getStripe(Context context) {
        if (stripe == null)
            // stripe=  new Stripe(context, "pk_test_8Sn4I2zPmPVhUqOdZiijkVLL002gbmqlLa");
            stripe = new Stripe(context, "pk_live_ebFBDc64Pi0Y0lfrVPaOp1q100AAuI1u3m");

        return stripe;
    }


    public static boolean hasNetwork() {
        return mInstance.checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission") NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void doVibrate(Context context, long vibrateTime) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(vibrateTime, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(vibrateTime);
        }
    }

}
