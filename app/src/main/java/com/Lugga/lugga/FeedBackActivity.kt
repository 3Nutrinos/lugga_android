package com.Lugga.lugga

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.adapters.AdapterTeacherDetailProfile
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.teacherdetails.TeacherDetailsBodyItem
import com.Lugga.lugga.model.teacherdetails.TeacherDetailsResponse
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class FeedBackActivity : AppCompatActivity() {
    var adapter: AdapterTeacherDetailProfile? = null
    var rvFeedback: RecyclerView? = null
    var list: ArrayList<TeacherDetailsBodyItem>? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var luggaAPI: LuggaAPI? = null
    var ivBack: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_back)
        rvFeedback = findViewById(R.id.rvFeedback)
        ivBack = findViewById(R.id.ivBack)
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        list = ArrayList()
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        teacherDetails
        ivBack!!.setOnClickListener(View.OnClickListener { onBackPressed() })
    }

    private val teacherDetails: Unit
        private get() {
            val user_id: RequestBody =  RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getteacherid().toString())
            luggaAPI!!.getteacherdetails(user_id).enqueue(object : Callback<TeacherDetailsResponse?> {
                override fun onResponse(call: Call<TeacherDetailsResponse?>, response: Response<TeacherDetailsResponse?>) {
                    if (response.body() != null) {
                        if (response.isSuccessful && response.body()!!.success == 200) {
                            list = response.body()!!.feedback
                            adapter = AdapterTeacherDetailProfile(this@FeedBackActivity, list!!)
                            val layoutManager = LinearLayoutManager(this@FeedBackActivity)
                            rvFeedback!!.layoutManager = layoutManager
                            rvFeedback!!.adapter = adapter
                        }
                    } else {
                        Toast.makeText(BaseApplication.getContext(), "" + response.message(), Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<TeacherDetailsResponse?>, t: Throwable) {
                    Toast.makeText(BaseApplication.getContext(), "" + t.message, Toast.LENGTH_SHORT).show()
                }
            })
        }
}