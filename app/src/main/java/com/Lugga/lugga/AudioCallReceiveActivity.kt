package com.Lugga.lugga

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.*
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.Lugga.lugga.model.CallingDataModel
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.SocketNetworking
import com.Lugga.lugga.utils.SoundMediaPlayer
import io.agora.rtc.Constants
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import io.agora.rtc.models.ChannelMediaOptions
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_audio_call.*
import kotlinx.android.synthetic.main.activity_audio_call_receive.*
import kotlinx.android.synthetic.main.activity_audio_call_receive.ivCutCall
import kotlinx.android.synthetic.main.activity_audio_call_receive.ivMuteUnMute
import kotlinx.android.synthetic.main.activity_audio_call_receive.ivSpeakerPhone
import kotlinx.android.synthetic.main.activity_audio_call_receive.rlMuteUnMute
import kotlinx.android.synthetic.main.activity_audio_call_receive.rlSpeakerPhone
import kotlinx.android.synthetic.main.activity_audio_call_receive.tvCallStatus
import kotlinx.android.synthetic.main.activity_audio_call_receive.tvChronoMeter
import org.json.JSONObject
import kotlin.math.abs


class AudioCallReceiveActivity : AppCompatActivity(), View.OnClickListener {

    private var isCallConnected: Boolean=false
    private var timer: CountDownTimer? = null
    private var isSpeakerOn: Boolean = true
    private var isMute: Boolean = true
    private var mRtcEventHandler: IRtcEngineEventHandler? = null
    private val PERMISSION_REQ_ID_RECORD_AUDIO = 22
    private var callingData: CallingDataModel? = null
    private var mRtcEngine: RtcEngine? = null

    private var socket: Socket? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_call_receive)
        connectSocket()
        BaseApplication.fullScreen(this)
        callingData = intent.getSerializableExtra(CALLING_DATA) as CallingDataModel?

        iniCallBacks()
        iniData()
        iniListeners()
        socketCutCallResponse()

        tvCallingToNameRec.text=callingData?.caller_name
        tvCallerNameStartTextRec.text=callingData?.caller_name?.substring(0,1)
    }

    private fun connectSocket() {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket!!.connect()
    }


    private fun iniListeners() {
        ivCutCall.setOnClickListener(this)
        rlSpeakerPhone.setOnClickListener(this)
        rlMuteUnMute.setOnClickListener(this)
    }

    private fun iniCallBacks() {
        mRtcEventHandler = object : IRtcEngineEventHandler() {
            @RequiresApi(Build.VERSION_CODES.M)
            override fun onJoinChannelSuccess(channel: String?, uid: Int, elapsed: Int) {
                super.onJoinChannelSuccess(channel, uid, elapsed)
                onCallConnected()
            }

            override fun onLeaveChannel(stats: RtcStats?) {
                super.onLeaveChannel(stats)
                hitSocketForCallTerminate()
                onStopEverything()
                if(callingData?.from_intro_screen!=null && callingData?.from_intro_screen=="yes") {
                    startActivity(Intent(this@AudioCallReceiveActivity, HomeActivity::class.java))
                    finishAffinity()
                }
            }

            override fun onConnectionLost() {
                super.onConnectionLost()
                Toast.makeText(
                    this@AudioCallReceiveActivity,
                    "Connection Lost !!",
                    Toast.LENGTH_SHORT
                ).show()
                onStopEverything()
            }

            override fun onError(err: Int) {
                super.onError(err)
            }

            override fun onRejoinChannelSuccess(channel: String?, uid: Int, elapsed: Int) {
                super.onRejoinChannelSuccess(channel, uid, elapsed)
            }
        }

    }

    private fun onCallConnected() {
        runOnUiThread {
            try {
                isCallConnected = true
                SoundMediaPlayer.stopSound()
                BaseApplication.getInstance().doVibrate(this,500)
                tvCallStatus.visibility = View.GONE
                tvChronoMeter.visibility = View.VISIBLE
                tvChronoMeter.base = SystemClock.elapsedRealtime()
                tvChronoMeter.start()

                rlSpeakerPhone.visibility=View.VISIBLE
                rlMuteUnMute.visibility=View.VISIBLE

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    private fun iniData() {
        if (checkSelfPermission(Manifest.permission.RECORD_AUDIO, PERMISSION_REQ_ID_RECORD_AUDIO)) {
            Handler(Looper.getMainLooper()).postDelayed({
                //Do something after 2000ms
                initializeAndJoinChannel();
            }, 2000)
        }
    }

    private fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission) !=
            PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(permission),
                requestCode
            )
            return false
        }
        return true
    }

    private fun initializeAndJoinChannel() {
        try {
            val option = ChannelMediaOptions()
            option.autoSubscribeAudio = true
            option.autoSubscribeVideo = true
            mRtcEngine = RtcEngine.create(baseContext, ApiClient.AGORA_APP_ID, mRtcEventHandler)
            mRtcEngine!!.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING)
            mRtcEngine!!.setClientRole(IRtcEngineEventHandler.ClientRole.CLIENT_ROLE_BROADCASTER)
            mRtcEngine!!.enableAudioVolumeIndication(1000, 3, true)
            val res =
                mRtcEngine!!.joinChannel(
                    callingData?.token.toString(), callingData?.Channel_id.toString(), "",
                    0, option
                )
            if (res != 0) {
                showAlert(RtcEngine.getErrorDescription(abs(res)))
                return
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showAlert(message: String?) {
        AlertDialog.Builder(this).setTitle("Error").setMessage(message)
            .setPositiveButton("OK") { dialog: DialogInterface, _: Int -> dialog.dismiss() }
            .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        onStopEverything()
    }

    companion object {
        const val CALLING_DATA = "callingData"
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivCutCall -> {
                onStopEverything()
            }
            R.id.rlSpeakerPhone -> {
                runOnUiThread {
                    if (!isSpeakerOn) {
                        isSpeakerOn = true
                        mRtcEngine!!.setEnableSpeakerphone(true)
                        ivSpeakerPhone.setImageResource(R.mipmap.phone_speaker)
                    } else {
                        SoundMediaPlayer.adjustMinimumVolume(this)
                        isSpeakerOn = false
                        mRtcEngine!!.setEnableSpeakerphone(false)
                        ivSpeakerPhone.setImageResource(R.mipmap.normal_speaker)
                    }
                }
            }

            R.id.rlMuteUnMute -> {
                runOnUiThread {
                    if (!isMute) {
                        isMute = true
                        mRtcEngine!!.enableAudio()
                        ivMuteUnMute.setImageResource(R.mipmap.microphone_unmuted)
                    } else {
                        isMute = false
                        mRtcEngine!!.disableAudio()
                        ivMuteUnMute.setImageResource(R.mipmap.microphone_muted)
                    }
                }
            }

        }
    }

    private fun onStopEverything() {
        runOnUiThread {
            try {
                SoundMediaPlayer.stopSound()
                mRtcEngine?.leaveChannel()
                RtcEngine.destroy()
                finish()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }

    private fun socketCutCallResponse() {
        if (socket!!.connected()) {
            socket!!.on("returncallStatusRequest" + callingData?.booking_id) { args ->
                runOnUiThread {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    if (jobj.getString("status").equals("1")) {
                    } else {
                        onStopEverything()
                    }
                }
            }

        }
    }
    private fun hitSocketForCallTerminate() {
        if (socket!!.connected()) {
            val obj = JSONObject()
            obj.put("time_count", "0")
            obj.put("status", "2")
            obj.put("endCall", "sender")
            obj.put("booking_id", callingData?.booking_id)
            obj.put("receiever_id", callingData?.receiver_id)
            obj.put("channel_id", callingData?.Channel_id)
            obj.put("caller_id", callingData?.caller_id)
            obj.put("user_type", callingData?.user_type)
            obj.put("call_type", callingData?.call_type)
            obj.put("user_name", callingData?.caller_name)
            socket!!.emit("callStatusRequest", obj)

        } else {
            socket!!.connect()
            Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
        }
    }
}