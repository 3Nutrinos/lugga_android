package com.Lugga.lugga.model.learnerhistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LearnerHistoryResponse{
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("body")
	@Expose
	private ArrayList<LearnerHistoryBodyItem> body = null;
	@SerializedName("success")
	@Expose
	private Integer success;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<LearnerHistoryBodyItem> getBody() {
		return body;
	}

	public void setBody(ArrayList<LearnerHistoryBodyItem> body) {
		this.body = body;
	}

	public Integer getSuccess() {
		return success;
	}

	public void setSuccess(Integer success) {
		this.success = success;
	}

}