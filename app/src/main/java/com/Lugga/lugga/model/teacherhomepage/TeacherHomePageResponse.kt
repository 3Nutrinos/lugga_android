package com.Lugga.lugga.model.teacherhomepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class TeacherHomePageResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<TeacherHomePageBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null
    var currentdateTime: String? = null

}