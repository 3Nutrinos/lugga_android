package com.Lugga.lugga.model.alllanguage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AllLanguagModel {
    @SerializedName("l_id")
    @Expose
    var lId: Int? = null

    @SerializedName("language")
    @Expose
    var language: String? = null

    @SerializedName("l_created_at")
    @Expose
    var lCreatedAt: String? = null

    var rating: String? = null

    var isSelected:Boolean?=false
}