package com.Lugga.lugga.model.teacheronlinestatus

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class TeacherOnlineStatusResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<TeacherOnlineStatusBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

}