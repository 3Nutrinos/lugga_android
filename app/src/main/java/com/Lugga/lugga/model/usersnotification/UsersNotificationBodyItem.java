package com.Lugga.lugga.model.usersnotification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsersNotificationBodyItem {
	@SerializedName("notificationID")
	@Expose
	private Integer notificationID;
	@SerializedName("notificationMessage")
	@Expose
	private String notificationMessage;
	@SerializedName("notificationTime")
	@Expose
	private String notificationTime;
	@SerializedName("bookingID")
	@Expose
	private Integer bookingID;
	@SerializedName("bookingSessionTime")
	@Expose
	private String bookingSessionTime;
	@SerializedName("bookingLanguage")
	@Expose
	private String bookingLanguage;
	@SerializedName("bookingStartTime")
	@Expose
	private String bookingStartTime;
	@SerializedName("bookingEndTime")
	@Expose
	private String bookingEndTime;
	@SerializedName("bookingStatus")
	@Expose
	private Integer bookingStatus;
	@SerializedName("teacherID")
	@Expose
	private Integer teacherID;
	@SerializedName("teacherName")
	@Expose
	private String teacherName;
	@SerializedName("teacherEmail")
	@Expose
	private String teacherEmail;
	@SerializedName("teacherProfile")
	@Expose
	private String teacherProfile;
	@SerializedName("leasrnerID")
	@Expose
	private Integer leasrnerID;
	@SerializedName("learnerName")
	@Expose
	private String learnerName;
	@SerializedName("learnerEmail")
	@Expose
	private String learnerEmail;
	@SerializedName("learnerProfile")
	@Expose
	private String learnerProfile;

	public Integer getNotificationID() {
		return notificationID;
	}

	public void setNotificationID(Integer notificationID) {
		this.notificationID = notificationID;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public String getNotificationTime() {
		return notificationTime;
	}

	public void setNotificationTime(String notificationTime) {
		this.notificationTime = notificationTime;
	}

	public Integer getBookingID() {
		return bookingID;
	}

	public void setBookingID(Integer bookingID) {
		this.bookingID = bookingID;
	}

	public String getBookingSessionTime() {
		return bookingSessionTime;
	}

	public void setBookingSessionTime(String bookingSessionTime) {
		this.bookingSessionTime = bookingSessionTime;
	}

	public String getBookingLanguage() {
		return bookingLanguage;
	}

	public void setBookingLanguage(String bookingLanguage) {
		this.bookingLanguage = bookingLanguage;
	}

	public String getBookingStartTime() {
		return bookingStartTime;
	}

	public void setBookingStartTime(String bookingStartTime) {
		this.bookingStartTime = bookingStartTime;
	}

	public String getBookingEndTime() {
		return bookingEndTime;
	}

	public void setBookingEndTime(String bookingEndTime) {
		this.bookingEndTime = bookingEndTime;
	}

	public Integer getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(Integer bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public Integer getTeacherID() {
		return teacherID;
	}

	public void setTeacherID(Integer teacherID) {
		this.teacherID = teacherID;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTeacherEmail() {
		return teacherEmail;
	}

	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}

	public String getTeacherProfile() {
		return teacherProfile;
	}

	public void setTeacherProfile(String teacherProfile) {
		this.teacherProfile = teacherProfile;
	}

	public Integer getLeasrnerID() {
		return leasrnerID;
	}

	public void setLeasrnerID(Integer leasrnerID) {
		this.leasrnerID = leasrnerID;
	}

	public String getLearnerName() {
		return learnerName;
	}

	public void setLearnerName(String learnerName) {
		this.learnerName = learnerName;
	}

	public String getLearnerEmail() {
		return learnerEmail;
	}

	public void setLearnerEmail(String learnerEmail) {
		this.learnerEmail = learnerEmail;
	}

	public String getLearnerProfile() {
		return learnerProfile;
	}

	public void setLearnerProfile(String learnerProfile) {
		this.learnerProfile = learnerProfile;
	}
}
