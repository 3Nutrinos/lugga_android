package com.Lugga.lugga.model.learnerhomepage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LearnerHomePageBodyItem implements Serializable {
	@SerializedName("id")
	@Expose
	public Integer id;
	@SerializedName("learner_id")
	@Expose
	public Integer learnerId;
	@SerializedName("teacher_id")
	@Expose
	public Integer teacherId;
	@SerializedName("booking_type")
	@Expose
	public Integer bookingType;
	@SerializedName("session_time")
	@Expose
	public String sessionTime;
	@SerializedName("language")
	@Expose
	public String language;
	@SerializedName("start_session")
	@Expose
	public String startSession;
	@SerializedName("end_session")
	@Expose
	public String endSession;
	@SerializedName("rating")
	@Expose
	public String rating;
	@SerializedName("comment")
	@Expose
	public Object comment;
	@SerializedName("payment_id")
	@Expose
	public String paymentId;
	@SerializedName("payment_amount")
	@Expose
	public Integer paymentAmount;
	@SerializedName("level")
	@Expose
	public String level;
	@SerializedName("status")
	@Expose
	public int status;
	@SerializedName("created_at")
	@Expose
	public String createdAt;
	@SerializedName("booking_id")
	@Expose
	public Integer bookingId;
	@SerializedName("booking_status")
	@Expose
	public Integer bookingStatus;
	@SerializedName("name")
	@Expose
	public String name;
	@SerializedName("email")
	@Expose
	public String email;
	@SerializedName("mobile_no")
	@Expose
	public String mobileNo;
	@SerializedName("password")
	@Expose
	public String password;
	@SerializedName("profile_image")
	@Expose
	public String profileImage;
	@SerializedName("user_type")
	@Expose
	public Integer userType;
	@SerializedName("device_type")
	@Expose
	public String deviceType;
	@SerializedName("device_token")
	@Expose
	public String deviceToken;
	@SerializedName("push_kit_token")
	@Expose
	public String pushKitToken;

	@SerializedName("booking_id")
	@Expose
	public transient Integer booking_id;

	@SerializedName("timezone")
	@Expose
	public String timezone;



	private float total_rating;

	public float getTotal_rating() {
		return total_rating;
	}

	public void setTotal_rating(float total_rating) {
		this.total_rating = total_rating;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}


	@SerializedName("booking_date")
	@Expose
	public String bookingdate;

	@SerializedName("currentdateTime")
	@Expose
	public String currentdateTime;


	public String getCurrentdateTime() {
		return currentdateTime;
	}

	public void setCurrentdateTime(String currentdateTime) {
		this.currentdateTime = currentdateTime;
	}

	public String getBookingdate() {
		return bookingdate;
	}

	public void setBookingdate(String bookingdate) {
		this.bookingdate = bookingdate;
	}

	public Integer getBooking_id() {
		return booking_id;
	}

	public void setBooking_id(Integer booking_id) {
		this.booking_id = booking_id;
	}

	private int amount;
	private String stripe_user_id;

	public String getStripe_user_id() {
		return stripe_user_id;
	}

	public void setStripe_user_id(String stripe_user_id) {
		this.stripe_user_id = stripe_user_id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLearnerId() {
		return learnerId;
	}

	public void setLearnerId(Integer learnerId) {
		this.learnerId = learnerId;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public Integer getBookingType() {
		return bookingType;
	}

	public void setBookingType(Integer bookingType) {
		this.bookingType = bookingType;
	}

	public String getSessionTime() {
		return sessionTime;
	}

	public void setSessionTime(String sessionTime) {
		this.sessionTime = sessionTime;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getStartSession() {
		return startSession;
	}

	public void setStartSession(String startSession) {
		this.startSession = startSession;
	}

	public String getEndSession() {
		return endSession;
	}

	public void setEndSession(String endSession) {
		this.endSession = endSession;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public Object getComment() {
		return comment;
	}

	public void setComment(Object comment) {
		this.comment = comment;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public Integer getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Integer paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public Integer getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(Integer bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getPushKitToken() {
		return pushKitToken;
	}

	public void setPushKitToken(String pushKitToken) {
		this.pushKitToken = pushKitToken;
	}
}
