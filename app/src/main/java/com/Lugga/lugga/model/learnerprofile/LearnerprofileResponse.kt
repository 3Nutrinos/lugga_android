package com.Lugga.lugga.model.learnerprofile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class LearnerprofileResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<LearnerProfileBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null
    var language: String? = null

}