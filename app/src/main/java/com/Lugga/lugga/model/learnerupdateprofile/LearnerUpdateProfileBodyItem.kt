package com.Lugga.lugga.model.learnerupdateprofile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LearnerUpdateProfileBodyItem {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("mobile_no")
    @Expose
    var mobileNo: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null

    @SerializedName("profile_image")
    @Expose
    var profileImage: Any? = null

    @SerializedName("language")
    @Expose
    var language: String? = null

    @SerializedName("user_type")
    @Expose
    var userType: Int? = null

    @SerializedName("device_type")
    @Expose
    var deviceType: String? = null

    @SerializedName("device_token")
    @Expose
    var deviceToken: String? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

}