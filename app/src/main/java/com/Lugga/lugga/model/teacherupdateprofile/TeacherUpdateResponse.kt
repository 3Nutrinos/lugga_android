package com.Lugga.lugga.model.teacherupdateprofile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class TeacherUpdateResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<TeacherUpdateBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

}