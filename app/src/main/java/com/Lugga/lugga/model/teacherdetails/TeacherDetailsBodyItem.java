package com.Lugga.lugga.model.teacherdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class TeacherDetailsBodyItem {

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("mobile_no")
	@Expose
	private String mobileNo;
	@SerializedName("password")
	@Expose
	private String password;
	@SerializedName("profile_image")
	@Expose
	private String profileImage;
	@SerializedName("language")
	@Expose
	private String language;
	@SerializedName("user_type")
	@Expose
	private Integer userType;
	@SerializedName("device_type")
	@Expose
	private String deviceType;
	@SerializedName("device_token")
	@Expose
	private String deviceToken;
	@SerializedName("created_at")
	@Expose
	private Date createdAt;
	@SerializedName("user_id")
	@Expose
	private Integer userId;
	@SerializedName("experience")
	@Expose
	private String experience;
	@SerializedName("document")
	@Expose
	private String document;
	@SerializedName("level")
	@Expose
	private Integer level;
	@SerializedName("amount")
	@Expose
	private Integer amount;
	@SerializedName("time")
	@Expose
	private String time;
	@SerializedName("is_online")
	@Expose
	private Integer isOnline;
	@SerializedName("completeBooking")
	@Expose
	private Integer completeBooking;
	@SerializedName("incompleteBooking")
	@Expose
	private Integer incompleteBooking;
	@SerializedName("ratingPeople")
	@Expose
	private Integer ratingPeople;

	private String feedbackMessage;

	private String feedbackRating;

	private Date feedbackTimeing;

	private String other_language;

	public String getOther_language() {
		return other_language;
	}

	public void setOther_language(String other_language) {
		this.other_language = other_language;
	}

	public Date getFeedbackTimeing() {
		return feedbackTimeing;
	}

	public void setFeedbackTimeing(Date feedbackTimeing) {
		this.feedbackTimeing = feedbackTimeing;
	}

	public String getFeedbackMessage() {
		return feedbackMessage;
	}

	public void setFeedbackMessage(String feedbackMessage) {
		this.feedbackMessage = feedbackMessage;
	}

	public String getFeedbackRating() {
		return feedbackRating;
	}

	public void setFeedbackRating(String feedbackRating) {
		this.feedbackRating = feedbackRating;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(Integer isOnline) {
		this.isOnline = isOnline;
	}

	public Integer getCompleteBooking() {
		return completeBooking;
	}

	public void setCompleteBooking(Integer completeBooking) {
		this.completeBooking = completeBooking;
	}

	public Integer getIncompleteBooking() {
		return incompleteBooking;
	}

	public void setIncompleteBooking(Integer incompleteBooking) {
		this.incompleteBooking = incompleteBooking;
	}

	public Integer getRatingPeople() {
		return ratingPeople;
	}

	public void setRatingPeople(Integer ratingPeople) {
		this.ratingPeople = ratingPeople;
	}
}
