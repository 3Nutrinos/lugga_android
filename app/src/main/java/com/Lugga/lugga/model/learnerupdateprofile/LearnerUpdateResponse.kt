package com.Lugga.lugga.model.learnerupdateprofile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class LearnerUpdateResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<LearnerUpdateProfileBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null
    var language: String? = null

}