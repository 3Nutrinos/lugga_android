package com.Lugga.lugga.model.learnerhomepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class LearnerHomePageResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<LearnerHomePageBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

    @SerializedName("currentdateTime")
    @Expose
    var currentdateTime: String? = null

}