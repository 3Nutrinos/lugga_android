package com.Lugga.lugga.model.sendRequest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class SendRequestResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<SendRequestBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

}