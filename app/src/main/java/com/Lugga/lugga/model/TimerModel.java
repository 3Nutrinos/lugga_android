package com.Lugga.lugga.model;

public class TimerModel {
    private long min;
    private long sec;
    private long downSec;

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public long getSec() {
        return sec;
    }

    public void setSec(long sec) {
        this.sec = sec;
    }

    public long getDownSec() {
        return downSec;
    }

    public void setDownSec(long downSec) {
        this.downSec = downSec;
    }
}
