package com.Lugga.lugga.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Response{

	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("body")
	@Expose
	private ArrayList<BodyItem> body = null;
	@SerializedName("success")
	@Expose
	private Integer success;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<BodyItem> getBody() {
		return body;
	}

	public void setBody(ArrayList<BodyItem> body) {
		this.body = body;
	}

	public Integer getSuccess() {
		return success;
	}

	public void setSuccess(Integer success) {
		this.success = success;
	}
}