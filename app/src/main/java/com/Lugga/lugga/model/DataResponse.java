package com.Lugga.lugga.model;


public class DataResponse<Model> extends CommonResponse{

    public Model getData() {
        return data;
    }

    public void setData(Model data) {
        this.data = data;
    }
    public Model data;
}
