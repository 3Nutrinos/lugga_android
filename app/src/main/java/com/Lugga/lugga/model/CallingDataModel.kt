package com.Lugga.lugga.model

import java.io.Serializable

class CallingDataModel : Serializable {
    var call_type:String?=""
    var Channel_id:String?=""
    var booking_id:String?=""
    var caller_id:String?=""
    var user_type:String?=""
    var receiver_id:String?=""
    var token:String?=""
    var uid:String?=""
    var caller_name:String?=""
    var caller_name_to:String?=""
    var from_intro_screen:String?=""

}