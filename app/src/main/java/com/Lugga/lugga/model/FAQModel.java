package com.Lugga.lugga.model;

public class FAQModel {
    private String ques;
    private String ans;
    private String count;


    public FAQModel(String ques, String ans,String count) {
        this.ques = ques;
        this.ans = ans;
        this.count = count;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getQues() {
        return ques;
    }

    public void setQues(String ques) {
        this.ques = ques;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }
}
