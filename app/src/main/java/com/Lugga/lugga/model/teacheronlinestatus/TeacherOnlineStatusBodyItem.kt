package com.Lugga.lugga.model.teacheronlinestatus

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TeacherOnlineStatusBodyItem {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("mobile_no")
    @Expose
    var mobileNo: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null

    @SerializedName("profile_image")
    @Expose
    var profileImage: Any? = null

    @SerializedName("language")
    @Expose
    var language: String? = null

    @SerializedName("user_type")
    @Expose
    var userType: Int? = null

    @SerializedName("device_type")
    @Expose
    var deviceType: String? = null

    @SerializedName("device_token")
    @Expose
    var deviceToken: String? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("user_id")
    @Expose
    var userId: Int? = null

    @SerializedName("experience")
    @Expose
    var experience: String? = null

    @SerializedName("document")
    @Expose
    var document: String? = null

    @SerializedName("level")
    @Expose
    var level: Int? = null

    @SerializedName("amount")
    @Expose
    var amount: Int? = null

    @SerializedName("time")
    @Expose
    var time: String? = null

    @SerializedName("is_online")
    @Expose
    var isOnline: Int? = null

}