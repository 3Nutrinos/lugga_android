package com.Lugga.lugga

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.Lugga.lugga.adapters.OtherLangAdapter
import com.Lugga.lugga.adapters.PopularLangAdapter
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne
import com.Lugga.lugga.utils.ApiClient
import kotlinx.android.synthetic.main.activity_languages.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class LanguagesActivity : AppCompatActivity(), OtherLangAdapter.Listener, PopularLangAdapter.Listener {
    private var popularLangAdapter: PopularLangAdapter? = null
    private var otherLangAdapter: OtherLangAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_languages)
        getlanguages()
        iniSearch()

        ivBack.setOnClickListener {
            finish()
        }

    }

    private fun getlanguages() {
        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages()?.enqueue(object : Callback<AllLanguageResposne> {
            override fun onResponse(call: Call<AllLanguageResposne>, response: Response<AllLanguageResposne>) {
                progressBar.visibility = View.GONE
                if (response.isSuccessful && response.body() != null) {
                    popularLangAdapter = PopularLangAdapter(this@LanguagesActivity, response.body()?.popular, this@LanguagesActivity)
                    rvPopularLanguage.adapter = popularLangAdapter

                    otherLangAdapter = OtherLangAdapter(this@LanguagesActivity, response.body()?.other, this@LanguagesActivity)
                    rvOtherLanguage.adapter = otherLangAdapter

                    llSearch.visibility = View.VISIBLE
                    nestedSearch.visibility = View.VISIBLE
                } else {
                    Toast.makeText(this@LanguagesActivity, "please try after sometime !!", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<AllLanguageResposne>, t: Throwable) {
                if (isFinishing) {
                    return
                }
                if (t is UnknownHostException) {
                    Toast.makeText(this@LanguagesActivity, "No Internet Connection", Toast.LENGTH_SHORT).show()
                } else if (t is SocketTimeoutException) {
                    Toast.makeText(
                            this@LanguagesActivity,
                            "Server is not responding. Please try again",
                            Toast.LENGTH_SHORT
                    )
                            .show()
                } else if (t is ConnectException) {
                    Toast.makeText(this@LanguagesActivity, "Failed to connect server", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(
                            this@LanguagesActivity,
                            "something went wrong !! please try again",
                            Toast.LENGTH_SHORT
                    )
                            .show()
                }
            }
        })

    }

    override fun onClickOtherLanguage(language: String?) {
        val intent = Intent()
        intent.putExtra("lang", language)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onClickPopLanguage(language: String?) {
        val intent = Intent()
        intent.putExtra("lang", language)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }


    private fun iniSearch() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                popularLangAdapter?.getFilter()?.filter(s.toString())
                otherLangAdapter?.getFilter()?.filter(s.toString())
            }

        })
    }
}