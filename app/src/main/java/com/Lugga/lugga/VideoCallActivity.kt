package com.Lugga.lugga

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Message
import android.os.SystemClock
import android.util.Log
import android.view.SurfaceView
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.Lugga.lugga.model.CallingDataModel
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.utils.SocketNetworking
import com.Lugga.lugga.utils.SoundMediaPlayer
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import io.agora.rtc.video.VideoCanvas
import io.agora.rtc.video.VideoEncoderConfiguration
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_video_call.*
import kotlinx.android.synthetic.main.activity_video_call.tvCallStatus
import org.json.JSONObject


class VideoCallActivity : AppCompatActivity() {
    private var isCallConnected: Boolean =false
    private var mRtcEngine: RtcEngine? = null
    private var socket: Socket? = null
    private var callingData: CallingDataModel? = null


    private val mRtcEventHandler = object : IRtcEngineEventHandler() {
        override fun onUserJoined(uid: Int, elapsed: Int) {
            runOnUiThread { setupRemoteVideo(uid) }
        }

        override fun onUserOffline(uid: Int, reason: Int) {
          //  runOnUiThread { onRemoteUserLeft() }
        }

        override fun onUserMuteVideo(uid: Int, muted: Boolean) {
            runOnUiThread { onRemoteUserVideoMuted(uid, muted) }
        }

        override fun onJoinChannelSuccess(channel: String?, uid: Int, elapsed: Int) {
            super.onJoinChannelSuccess(channel, uid, elapsed)
            iniSocketForCall()
        }

        override fun onLeaveChannel(stats: RtcStats?) {
            super.onLeaveChannel(stats)
            runOnUiThread {
                onRemoteUserLeft()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_call)
        BaseApplication.fullScreen(this)
        callingData = intent.getSerializableExtra(CALLING_DATA) as CallingDataModel?
        connectSocket()

        if (checkSelfPermission(
                Manifest.permission.RECORD_AUDIO,
                PERMISSION_REQ_ID_RECORD_AUDIO
            ) && checkSelfPermission(Manifest.permission.CAMERA, PERMISSION_REQ_ID_CAMERA)
        ) {
            initAgoraEngineAndJoinChannel()
        }
    }

    private fun connectSocket() {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket!!.connect()
    }

    private fun initAgoraEngineAndJoinChannel() {
        initializeAgoraEngine()
        setupVideoProfile()
        setupLocalVideo()
        joinChannel()
    }

    private fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        Log.i(LOG_TAG, "checkSelfPermission $permission $requestCode")
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this,
                arrayOf(permission),
                requestCode
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(LOG_TAG, "onRequestPermissionsResult " + grantResults[0] + " " + requestCode)

        when (requestCode) {
            PERMISSION_REQ_ID_RECORD_AUDIO -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkSelfPermission(Manifest.permission.CAMERA, PERMISSION_REQ_ID_CAMERA)
                } else {
                    showLongToast("No permission for " + Manifest.permission.RECORD_AUDIO)
                    finish()
                }
            }
            PERMISSION_REQ_ID_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initAgoraEngineAndJoinChannel()
                } else {
                    showLongToast("No permission for " + Manifest.permission.CAMERA)
                    finish()
                }
            }
        }
    }

    private fun showLongToast(msg: String) {
        this.runOnUiThread { Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG).show() }
    }

    override fun onDestroy() {
        super.onDestroy()
        onStopEverything()
    }

    fun onLocalVideoMuteClicked(view: View) {
        val iv = view as ImageView
        if (iv.isSelected) {
            iv.isSelected = false
            iv.clearColorFilter()
        } else {
            iv.isSelected = true
            iv.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)
        }

        // Stops/Resumes sending the local video stream.
        mRtcEngine!!.muteLocalVideoStream(iv.isSelected)

        val container = findViewById(R.id.local_video_view_container) as FrameLayout
        val surfaceView = container.getChildAt(0) as SurfaceView
        surfaceView.setZOrderMediaOverlay(!iv.isSelected)
        surfaceView.visibility = if (iv.isSelected) View.GONE else View.VISIBLE
    }

    fun onLocalAudioMuteClicked(view: View) {
        val iv = view as ImageView
        if (iv.isSelected) {
            iv.isSelected = false
            iv.clearColorFilter()
        } else {
            iv.isSelected = true
            iv.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)
        }

        // Stops/Resumes sending the local audio stream.
        mRtcEngine!!.muteLocalAudioStream(iv.isSelected)
    }

    fun onSwitchCameraClicked(view: View) {
        // Switches between front and rear cameras.
        mRtcEngine!!.switchCamera()
    }

    fun onEncCallClicked(view: View) {
        hitSocketForCallTerminate()
        onStopEverything()
    }

    private fun initializeAgoraEngine() {
        try {
            mRtcEngine = RtcEngine.create(baseContext, APP_ID, mRtcEventHandler)
        } catch (e: Exception) {
            Log.e(LOG_TAG, Log.getStackTraceString(e))
            throw RuntimeException(
                "NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(
                    e
                )
            )
        }
    }

    private fun setupVideoProfile() {
        mRtcEngine!!.enableVideo()
        mRtcEngine!!.setVideoEncoderConfiguration(
            VideoEncoderConfiguration(
                VideoEncoderConfiguration.VD_640x360,
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT
            )
        )
    }

    private fun setupLocalVideo() {
        val container = findViewById(R.id.local_video_view_container) as FrameLayout
        val surfaceView = RtcEngine.CreateRendererView(baseContext)
        surfaceView.setZOrderMediaOverlay(true)
        container.addView(surfaceView)
        // Initializes the local video view.
        // RENDER_MODE_FIT: Uniformly scale the video until one of its dimension fits the boundary. Areas that are not filled due to the disparity in the aspect ratio are filled with black.
        mRtcEngine!!.setupLocalVideo(VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_FIT, 0))
    }

    private fun joinChannel() {
        var token: String? = callingData?.token
        if (token!!.isEmpty()) {
            token = null
        }
        mRtcEngine!!.joinChannel(
            token,
            callingData?.Channel_id,
            "",
            0
        ) // if you do not specify the uid, we will generate the uid for you
    }

    private fun setupRemoteVideo(uid: Int) {
        val container = findViewById<FrameLayout>(R.id.remote_video_view_container)

        if (container.childCount >= 1) {
            return
        }

        val surfaceView = RtcEngine.CreateRendererView(baseContext)
        container.addView(surfaceView)
        // Initializes the video view of a remote user.
        mRtcEngine!!.setupRemoteVideo(VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_FIT, uid))
        surfaceView.tag = uid // for mark purpose
        onCallConnected()
    }

    private fun onRemoteUserLeft() {
        val container = findViewById<FrameLayout>(R.id.remote_video_view_container)
        container.removeAllViews()
        //hitSocketForCallTerminate()
        onStopEverything()
    }

    private fun onRemoteUserVideoMuted(uid: Int, muted: Boolean) {
        val container = findViewById<FrameLayout>(R.id.remote_video_view_container)

        val surfaceView = container.getChildAt(0) as SurfaceView

        val tag = surfaceView.tag
        if (tag != null && tag as Int == uid) {
            surfaceView.visibility = if (muted) View.GONE else View.VISIBLE
        }
    }

    companion object {
        private val LOG_TAG = VideoCallActivity::class.java.simpleName
        const val CALLING_DATA = "callingData"
        private const val PERMISSION_REQ_ID_RECORD_AUDIO = 22
        private const val APP_ID = "a68dd92955194e92a664c6f91d1452d1"
        private const val PERMISSION_REQ_ID_CAMERA = PERMISSION_REQ_ID_RECORD_AUDIO + 1
    }

    private fun iniSocketForCall() {
        if (socket!!.connected()) {
            val obj = JSONObject()
            obj.put("call_type", callingData?.call_type)
            obj.put("Channel_id", callingData?.Channel_id)
            obj.put("booking_id", callingData?.booking_id)
            obj.put("caller_id", callingData?.caller_id)
            obj.put("user_type", callingData?.user_type)
            obj.put("receiver_id", callingData?.receiver_id)
            obj.put("token", callingData?.token)
            obj.put("user_name", callingData?.caller_name_to)

            socket!!.emit("callRequest", obj)
            getReturnForVideoCallSocket()
            socketCutCallResponse()

        }
    }

    private fun socketCutCallResponse() {
        if (socket!!.connected()) {
            socket!!.on("returncallStatusRequest" + callingData?.booking_id) { args ->
                runOnUiThread {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    if (jobj.getString("status").equals("1")) {
                    } else {
                        onStopEverything()
                    }
                }
            }

        }
    }

    private fun hitSocketForCallTerminate() {
        if (socket!!.connected()) {
            val obj = JSONObject()
            obj.put("time_count", "0")
            obj.put("status", "2")
            obj.put("endCall", "sender")
            obj.put("booking_id", callingData?.booking_id)
            obj.put("receiever_id", callingData?.receiver_id)
            obj.put("channel_id", callingData?.Channel_id)
            obj.put("caller_id", callingData?.caller_id)
            obj.put("user_type", callingData?.user_type)
            obj.put("call_type", callingData?.call_type)
            obj.put("user_name", callingData?.caller_name_to)
            if (isCallConnected) {
                obj.put("push_send", "0")
            } else {
                obj.put("push_send", "1")
            }

            socket!!.emit("callStatusRequest", obj)

        } else {
            socket!!.connect()
            Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun getReturnForVideoCallSocket() {
        socket!!.on("returncallRequest" + callingData?.booking_id) { args ->
            val obj = args[0] as JSONObject
            if (obj.getInt("success") == 200) {
                handleUpdateUI()
            } else {
                runOnUiThread {
                    Toast.makeText(
                        this,
                        "something went wrong !! please try again",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }
    }

    private fun handleUpdateUI() {
        runOnUiThread {
            tvCallStatus.text = getString(R.string.ringing)
            SoundMediaPlayer.startSound(
                this,
                R.raw.tring_tring,
                10,
                false
            )
        }

    }

    private fun onStopEverything() {
        runOnUiThread {
            try {
//                timer?.cancel()
//                timer = null
                SoundMediaPlayer.stopSound()
                mRtcEngine?.leaveChannel()
                RtcEngine.destroy()
                mRtcEngine = null
                finish()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }

    private fun onCallConnected() {
        runOnUiThread {
            try {
                isCallConnected=true
                SoundMediaPlayer.stopSound()
                BaseApplication.getInstance().doVibrate(this, 500)
                tvCallStatus.visibility = View.GONE
                tvChronoMeterVideo.visibility = View.VISIBLE
                tvChronoMeterVideo.base = SystemClock.elapsedRealtime()
                tvChronoMeterVideo.start()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }
}