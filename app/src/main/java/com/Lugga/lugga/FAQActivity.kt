package com.Lugga.lugga

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.adapters.FAQAdapter
import com.Lugga.lugga.model.FAQModel
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce

class FAQActivity : AppCompatActivity(), View.OnClickListener {
    var rvFAQ: RecyclerView? = null
    var adapter: FAQAdapter? = null
    var myListData: Array<FAQModel>? = null
    var prefernce: UsersharedPrefernce? = null
    var ivBack: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_f_a_q)
        rvFAQ = findViewById(R.id.rvFAQ)
        ivBack = findViewById(R.id.ivBack)
        prefernce = UsersharedPrefernce.getInstance()
        myListData = if (prefernce!!.getusertype() == 1) {
            arrayOf(
                    FAQModel("how do I sign up?",
                            """click on sign up at the bottom of the welcome page. Select teacher. Enter your name, email address, phone number and password. Click next.
Enter your Language Experience using the drop down menu. Enter the price of your lessons in US dollars per 30min. Choose the language you intend to teach as well as other language you speak.
Now proceed to create your Stripe account (we have included a step by step guide to show you exactly how to do this). Start the process by clicking on the Blue connect with stripe button. As stripe deals with payments to businesses and individuals alike, sign up as individual, click on the blue product description link and complete the rest of the details. If you already have a Stripe account simply login. Great news you are now set up
and ready to get paid.
Now click signup.
You will receive a welcome
message sent to your email.
** Remember to turn your availability button to online on
your homepage to signal to the learners that you are available to teach**""",
                            "1"),
                    FAQModel("how do I change my profile?",
                            """Answer: click = symbol on the top left-hand side of your homepage. Select profile. To add a profile picture, click on the camera symbol, select image from gallery or take a picture using the camera off your device and attach image.

 You can also change your phone number, name, Password, experience, language or the price of your lessons by clicking on the relevant tabs on your profile page.

"""
                            ,
                            "2"),
                    FAQModel("How do I subscribe?", "when you sign up for the first time as a teacher you will be prompted to subscribe to one of the three packages Bronze, Silver or Gold. To change subscription package click = symbol on the homepage , Select subscription. Upgrade by choosing the Silver or Gold packages as required. (We are currently running a promotional 1 month free to new users on selecting the Bronze Package) \n \n", "3"),
                    FAQModel("How will I know when a learner requests a lesson ?",
                            """Answer: If you have allowed notifications from Lugga after signup, you should get a notification when a learner requests a lesson. You will have 30 minutes to accept the request if you are available to teach by going to the app Homepage and clicking Accept. The learner will have 30 minutes to pay for the lesson. Once payment has been completed, the lesson will commence.
If it is a lesson that was requested for a future date or time it will be scheduled as soon as the payment is completed (please take note of the date and time on the request when you receive it). When the scheduled time arrives just refresh the page to start the lesson.""",
                            "4"),
                    FAQModel("What options are there to communicate with the learner during the lesson?",
                            "Answer: The lesson can be conducted by text messages, voice notes, voice or video calls , all done through the Lugga app.", "5"),
                    FAQModel("How do I view feedback from learners?",
                            "Answer: click = symbol on the homepage and select profile. Go to the account section on the profile page and select view feedback.", "6"),
                    FAQModel("How do I view previous chats?",
                            "Answer: click on =symbol on the homepage, click on Chats. All previous chats will appear.", "7"),
                    FAQModel("How do I get help for anything not listed on frequently asked questions?",
                            "Answer: please send your enquiry to admin@luggalanguages.com We will endeavour to answer to your enquiry within 72 hours.", "8"))
        } else {
            arrayOf(
                    FAQModel("how do I sign up?",
                            """Answer: select language from drop down. Select any other language you would want your teacher to be able to speak. Choose session time. Choose the level at which you want to learn the language (beginner, intermediate or advanced).
Create booking. You can request an immediate lesson by clicking on "Current booking". You will be given a list of all the available teachers of that language . Select the teacher you want from the list, teachers have ratings 1 to 5 (5 is excellent). You can view how many lessons a teacher has completed as well as any feedback they have received. Click Request booking. The teacher gets a message and is given 30 minutes to either accept your request if they are available or reject if not available. If teacher accepts, you will be prompted to pay for the lesson and the lesson will begin immediately (you will have 30 minutes to pay for your lesson). You can use voice note, text, voice or video calling during the lesson and and it will terminate as soon as the selected time runs out.
You can also schedule a lesson for a future date/time by clicking 'schedule booking'. When the time arrives please refresh the screen and the lesson will start.
NB- Once the lesson has commenced there is no refund.""",
                            "1"),
                    FAQModel("How do I Create a booking?",
                            "Answer: select language from drop down. Select any other language you would want your teacher to be able to speak. Choose session time. Choose the level at which you want to learn the language (beginner, intermediate or advanced).\n" +
                                    "Create booking. You can request an immediate lesson by clicking on \"Current booking\". You will be given a list of all the available teachers of that language . Select the teacher you want from the list, teachers have ratings 1 to 5 (5 is excellent). You can view how many lessons a teacher has completed as well as any feedback they have received. Click Request booking. The teacher gets a message and is given 30 minutes to either accept your request if they are available or reject if not available. If teacher accepts, you will be prompted to pay for the lesson and the lesson will begin immediately (you will have 30 minutes to pay for your lesson). You can use voice note, text, voice or video calling during the lesson and and it will terminate as soon as the selected time runs out.\n" +
                                    "You can also schedule a lesson for a future date/time by clicking 'schedule booking'. When the time arrives please refresh the screen and the lesson will start.\n" +
                                    "NB- Once the lesson has commenced there is no refund.",
                            "2"),
                    FAQModel("How will I know when a teacher accepts a lesson ?",
                            """Answer: If you have allowed notifications from Lugga during signup, you should get an alert when a teacher accepts a lesson.

To view notifications click = symbol on the homepage then select notifications. Alternatively just click on the notification symbol on the top right hand corner of the homepage.""",
                            "3"),
                    FAQModel("How do I change my profile?",
                            """Answer: click = symbol on the top left-hand side of your homepage. Select profile. To add a profile picture, click on the camera symbol, select image from gallery or take a picture using the camera off your device and attach image.

You can also change your phone number, name, Password, experience, language or the price of your lessons by clicking on the relevant tabs on your profile page.""",
                            "4"),
                    FAQModel("what options are there to communicate with the teacher during the lesson?",
                            "Answer: The lesson can be conducted by text messages, voice notes, voice or video calls , all done through the Lugga app.",
                            "5"),
                    FAQModel("how do I view previous chats?",
                            "Answer: click on =symbol on the homepage, click on Chats. All previous chats will appear.",
                            "6"),
                    FAQModel("How do I get help for anything not listed on frequently asked questions?",
                            "Answer: please send your enquiry admin@luggalanguages.com We will endeavour to answer to your enquiry within 72.",
                            "7"))
        }
        setAdapter()
        ivBack!!.setOnClickListener(this)
    }

    private fun setAdapter() {
        adapter = FAQAdapter(myListData!!, this)
        val layoutManager = LinearLayoutManager(this)
        rvFAQ!!.layoutManager = layoutManager
        rvFAQ!!.adapter = adapter
    }

    override fun onClick(v: View) {
        onBackPressed()
    }
}