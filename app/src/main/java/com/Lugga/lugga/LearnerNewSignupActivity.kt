package com.Lugga.lugga

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.Toast
import com.Lugga.lugga.adapters.AllLanguageAdapter
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import kotlinx.android.synthetic.main.activity_learner_new_signup.*
import kotlinx.android.synthetic.main.activity_learner_new_signup.etEmail
import kotlinx.android.synthetic.main.activity_learner_new_signup.etName
import kotlinx.android.synthetic.main.activity_learner_new_signup.etNumber
import kotlinx.android.synthetic.main.activity_learner_new_signup.etPassword
import kotlinx.android.synthetic.main.activity_new_log_in.*
import kotlinx.android.synthetic.main.sign_up.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern

class LearnerNewSignupActivity : AppCompatActivity(), View.OnClickListener {
    private var usersharedPrefernce: UsersharedPrefernce?=null
    private var selectedLanguage: String?=""
    private var allLanguageAdapter: AllLanguageAdapter? = null
    var luggaAPI: LuggaAPI? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_learner_new_signup)

        usersharedPrefernce= UsersharedPrefernce.getInstance()
        etDescription.setRawInputType(InputType.TYPE_CLASS_TEXT);
        etDescription.imeOptions = EditorInfo.IME_ACTION_DONE;

        tvLearnerSignUp.setOnClickListener(this)
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)

        getLanguage()
        iniSpinner()
    }

    private fun iniSpinner() {
        spinnerLanguage!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedLanguage = (allLanguageAdapter!!.getItem(position) as AllLanguagModel).language
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    override fun onClick(v: View?) {

        if (etName.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter Name", Toast.LENGTH_SHORT).show()
            return
        }
        if (etEmail.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show()
            return
        }

        if (!isValidEmailId(etEmail.getText().toString().trim())) {
            Toast.makeText(this, "Enter valid Email", Toast.LENGTH_SHORT).show()
            return
        }

        if (etNumber.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter Number", Toast.LENGTH_SHORT).show()
            return
        }

        if (etPassword.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show()
            return
        }

        if (etDescription.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter Description", Toast.LENGTH_SHORT).show()
            return
        }


        callLearnerApi()

    }

    private fun callLearnerApi() {
        progressBarLearner.visibility=View.VISIBLE
        val name: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etName!!.text.toString().trim())
        val email: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etEmail!!.text.toString().trim())
        val mobileno: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etNumber!!.text.toString().trim())
        val password: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etPassword!!.text.toString().trim())
        val user_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "2")
        val language: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedLanguage!!)
        val description: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), etDescription.text.toString().trim())
        val device_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "Android")
        val device_token: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.devicetoken)
        luggaAPI!!.signup2(description,name, email, mobileno, password, user_type, language, device_type, device_token).enqueue(object : Callback<com.Lugga.lugga.model.Response?> {
            override fun onResponse(call: Call<com.Lugga.lugga.model.Response?>, response: Response<com.Lugga.lugga.model.Response?>) {
                if (response.body() != null && response.isSuccessful) {
                    progressBarLearner.visibility = View.GONE
                    if (response.body()!!.success == 200) {
                        Toast.makeText(this@LearnerNewSignupActivity, "Registered successfully", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this@LearnerNewSignupActivity, LoginActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this@LearnerNewSignupActivity, response.body()?.message, Toast.LENGTH_SHORT).show()
                    }
                }
                else{
                    progressBarLearner.visibility = View.GONE
                    Toast.makeText(this@LearnerNewSignupActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<com.Lugga.lugga.model.Response?>, t: Throwable) {
                progressBarSignUp.visibility = View.GONE
                Toast.makeText(this@LearnerNewSignupActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun isValidEmailId(email: String): Boolean {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches()
    }

    private fun getLanguage() {
        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages().enqueue(object : Callback<AllLanguageResposne> {
            override fun onResponse(call: Call<AllLanguageResposne>, response: Response<AllLanguageResposne>) {
                if (response.isSuccessful && response.body() != null) {
                    allLanguageAdapter = AllLanguageAdapter(this@LearnerNewSignupActivity, response.body()!!.body!!)
                    spinnerLanguage!!.adapter = allLanguageAdapter
                } else {
                    Toast.makeText(this@LearnerNewSignupActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<AllLanguageResposne>, t: Throwable) {
                Toast.makeText(this@LearnerNewSignupActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()

            }
        })
    }



}