package com.Lugga.lugga

import android.app.Dialog
import android.app.NotificationManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.res.TypedArray
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.Lugga.lugga.adapters.IntroSliderAdapter
import com.Lugga.lugga.fragments.IntroFirstFragment
import com.Lugga.lugga.fragments.IntroSecondFragment
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.AppUpdateModel
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_introduction.*
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.android.synthetic.main.item_sender_chat.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class IntroductionActivity : AppCompatActivity() {

    private var usersharedPrefernce: UsersharedPrefernce? = null
    private val fragmentList = ArrayList<Fragment>()

    private var versionCode: Int? = 0
    var luggaAPI: LuggaAPI? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)


        setContentView(R.layout.activity_introduction)
        checkAppUpdate()
        if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!![key]
                if (key == "message") {
                    clearNotifications(this)
                    try {
                        val jsOb = JSONObject(value.toString())
                        if (jsOb.has("channel_id")) {
                            val notiIntent = Intent(this, NewCallReceiveActivity::class.java)
                            notiIntent.putExtra("channel_id", jsOb.getString("channel_id"))
                            notiIntent.putExtra("caller_id", jsOb.getString("caller_id"))
                            notiIntent.putExtra("receiver_id", jsOb.getString("receiver_id"))
                            notiIntent.putExtra("caller_name", jsOb.getString("caller_name"))
                            notiIntent.putExtra("user_type", jsOb.getString("user_type"))
                            notiIntent.putExtra("call_type", jsOb.getString("call_type"))
                            notiIntent.putExtra("sender_name", jsOb.getString("sender_name"))
                            notiIntent.putExtra("booking_id", jsOb.getString("booking_id"))
                            notiIntent.putExtra(
                                "access_token",
                                jsOb.getString("access_token")
                            )
                            notiIntent.putExtra("fromIntroScreen","yes")
                            startActivity(notiIntent)
                        }
                    } catch (e: Exception) {
                        if(value=="New lesson request."){
                            val intent = Intent(this, HomeActivity::class.java)
                            startActivity(intent)
                            finishAffinity()
                        }
                    }
                }
                Log.d("MainActivity: ", "Key: $key Value: $value")
            }
        }

        usersharedPrefernce = UsersharedPrefernce.getInstance()

        val adapter = IntroSliderAdapter(this)

        vpIntroSlider.adapter = adapter
        fragmentList.addAll(
            listOf(
                IntroFirstFragment(), IntroSecondFragment()
            )
        )
        adapter.setFragmentList(fragmentList)
        indicatorLayout.setIndicatorCount(adapter.itemCount)
        indicatorLayout.selectCurrentPosition(0)
        registerListeners()

    }

    private fun checkAppUpdate() {
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)

        luggaAPI?.checkForAppUpdate()?.enqueue(object : Callback<ArrayList<AppUpdateModel>> {
            override fun onResponse(
                call: Call<ArrayList<AppUpdateModel>>,
                response: Response<ArrayList<AppUpdateModel>>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    try {
                        val dataArrayList = response.body();

                        if(dataArrayList!=null && dataArrayList[0].android_version!=null){
                            versionCode = dataArrayList[0].android_version
                            getApplicationVersionCode(versionCode)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ArrayList<AppUpdateModel>>, t: Throwable) {

            }

        })
    }

    private fun registerListeners() {
        vpIntroSlider.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                indicatorLayout.selectCurrentPosition(position)
                if (position < fragmentList.lastIndex) {
                    tvSkip.visibility = View.VISIBLE
                    tvNext.text = this@IntroductionActivity.getString(R.string.next)
                } else {
                    tvSkip.visibility = View.GONE
                    tvNext.text = this@IntroductionActivity.getString(R.string.getstarted)

                }
            }
        })
        tvSkip.setOnClickListener {
            if (usersharedPrefernce!!.getboolean()) {
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        tvNext.setOnClickListener {
            val position = vpIntroSlider.currentItem
            if (position < fragmentList.lastIndex) {
                vpIntroSlider.currentItem = position + 1
            } else {
                if (usersharedPrefernce!!.getboolean()) {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    fun clearNotifications(context: Context) {
        try {
            val notificationManager =
                context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.cancelAll()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    private fun getApplicationVersionCode(versionCode: Int?): String {
        val verCode = ""
        try {
            val pInfo: PackageInfo = this.packageManager.getPackageInfo(this.packageName, 0)
            val verCode = pInfo.versionCode
            if (verCode < versionCode!!) {
                showUpdateAppDialog()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return verCode
    }

    private fun showUpdateAppDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.update_app_dialog);


        dialog.show()

        val btUpdate = dialog.findViewById<Button>(R.id.btUpdate)
        btUpdate.setOnClickListener {
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=${this.packageName}")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=${this.packageName}")
                    )
                )
            }
        }
    }
}