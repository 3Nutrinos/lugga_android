package com.Lugga.lugga

import android.Manifest
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.Lugga.lugga.fragments.HomeFragment
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.teacheronlinestatus.TeacherOnlineStatusResponse
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.subscription.BillingProcessor
import com.Lugga.lugga.subscription.PurchaseInfo
import com.Lugga.lugga.subscription.SubscriptionActivity
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.sub.IabBroadcastReceiver
import com.Lugga.lugga.utils.sub.IabBroadcastReceiver.IabBroadcastListener
import com.Lugga.lugga.utils.sub.IabHelper
import com.Lugga.lugga.utils.sub.IabHelper.*
import com.Lugga.lugga.utils.sub.Purchase
import com.Lugga.lugga.views.BookingHistory
import com.Lugga.lugga.views.Constants
import com.Lugga.lugga.views.NotificationScreen
import com.Lugga.lugga.views.TeacherProfile
import com.Lugga.lugga.views.subscription.Subscription
import com.bumptech.glide.Glide
import com.facebook.appevents.AppEventsLogger
import com.google.android.material.navigation.NavigationView
import com.infideap.drawerbehavior.AdvanceDrawerLayout
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_basic.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class HomeActivity : AppCompatActivity(), View.OnClickListener, IabBroadcastListener {
    private var appEventsLogger: AppEventsLogger? = null
    private var drawer: AdvanceDrawerLayout? = null
    var nav_view: NavigationView? = null
    var nav_llHome: LinearLayout? = null
    var nav_subscription: LinearLayout? = null
    var nav_llProfile: LinearLayout? = null
    var nav_llChat: LinearLayout? = null
    var nav_llNotification: LinearLayout? = null
    var nav_llPrivacyPolicy: LinearLayout? = null
    var nav_llHelp: LinearLayout? = null
    var nav_llLogout: LinearLayout? = null
    var nav_l1CreateBooking: LinearLayout? = null
    var nav_l1BookingHistory: LinearLayout? = null
    var ivHome: ImageView? = null
    var ivSubs: ImageView? = null
    var ivProfile: ImageView? = null
    var ivChat: ImageView? = null
    var ivNotification: ImageView? = null
    var ivPrivayPolicy: ImageView? = null
    var ivHelp: ImageView? = null
    var ivLogout: ImageView? = null
    var side_nav: ImageView? = null
    var ivCreateBookingIcon: ImageView? = null
    var ivCreateBooking: ImageView? = null
    var ivBookingHistory: ImageView? = null
    var notificationBell: ImageView? = null
    var tvHome: TextView? = null
    var tvSubs: TextView? = null
    var tvProfile: TextView? = null
    var tvChat: TextView? = null
    var tvNotification: TextView? = null
    var tvPrivayPolicy: TextView? = null
    var tvHelp: TextView? = null
    var tvLogout: TextView? = null
    var tvCreateBooking: TextView? = null
    var tvBookingHistory: TextView? = null
    var createBookingBelowToggle: TextView? = null
    var homeUserName: TextView? = null
    var createBookingview: View? = null
    var bookingHistoryview: View? = null
    var view1: View? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var toggleTeacherView: ToggleButton? = null

    // Loader loader;
    var luggaAPI: LuggaAPI? = null
    var isonline: String? = null
    var iv_profile: ImageView? = null
    private var builder: AlertDialog.Builder? = null
    private var userId = 0
    var tvBookingCount: TextView? = null
    var tvOnlineOffline: TextView? = null
    var receiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            setData(intent.getIntExtra("count", 0))
        }
    }
    private var mHelper: IabHelper? = null
    private var mReceiver: IabBroadcastReceiver? = null
    private var isSubscribed = false
    private var isAutoRenewable = false
    private var mActiveSub = ""
    private var skuList: ArrayList<String>? = null
    private fun setData(count: Int) {
        if (count == 0) {
            tvBookingCount!!.text = "0 Bookings Open"
        } else tvBookingCount!!.text = "$count Bookings Open"
    }

    private var bp: BillingProcessor? = null

    // PRODUCT & SUBSCRIPTION IDS
    val KEY_SILVER = "silver_pkg"
    val KEY_BRONZE = "bronze"
    val KEY_GOLD = "gold_pkg"

    private val LICENSE_KEY =
        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApliMctatlMrwbSJjpynHWC8CpnmLM//8BS5mz9Ys5pZc4mdikoy32ec0THiefo+V4pbwihFlNQS5AMs0Ak4zyCXRTmSa5IWQDJ0Z3/tnt0L4PYBhAdyHYCIv2PrqRmV8xyYxcCOZvzKaSwSSofwmrP5tb4Oe8t7RKzF+nlya3XiP0kJfB9Tzz+1RH4ChZTpeYqITYkrEWRX8sJeD+skDMRZYmr9yKB0SUwXVY1q7P0sZkqHVurWYL+3FlrUJ5fU6mWReFxJrzSDU1KmCPceIpLd1oYncu6x3by4nPsINqgVrIya0QDp1PDx2Lq3Gd5n1DrNeUecs/Ip1YNJclYJ96wIDAQAB"

    // PUT YOUR MERCHANT KEY HERE;
    // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
    // if filled library will provide protection against Freedom alike Play Market simulators

    // PUT YOUR MERCHANT KEY HERE;
    // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
    // if filled library will provide protection against Freedom alike Play Market simulators
    val MERCHANT_ID = "01610008143915814606"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basic)
        builder = AlertDialog.Builder(this)

        appEventsLogger = AppEventsLogger.newLogger(this)

        logSentFriendRequestEvent()
        tvBookingCount = findViewById(R.id.tvBookingCount)
        tvOnlineOffline = findViewById(R.id.tvOnlineOffline)
        iv_profile = findViewById(R.id.iv_profile)
        view1 = findViewById(R.id.view1)
        tvSubs = findViewById(R.id.tvSubs)
        ivSubs = findViewById(R.id.ivSubs)
        audIorequestStoragePermission()
        usersharedPrefernce = UsersharedPrefernce.getInstance()

        if (usersharedPrefernce!!.getprofileimage() != null) {
            Glide.with(this@HomeActivity)
                .load(ApiClient.IMAGE_URL + usersharedPrefernce!!.getprofileimage())
                .placeholder(R.drawable.person_girl)
                .centerCrop().into(iv_profile!!)
        }

        init()
        firstFragment()
        setUpListners()
        toogleListener()

//        if (usersharedPrefernce.getusertype()==2){
//            getLearnerHomepage();
//        }
//        else if (usersharedPrefernce.getusertype()==1){
//            getTeacherHomePage();
//        }
        if (usersharedPrefernce!!.getname() != null) {
            homeUserName!!.text = usersharedPrefernce!!.getname()
        }
        if (usersharedPrefernce!!.getusertype() == 1) {
            if(BaseApplication.getInstance().getOnline()=="false") {
                getTeacherOnlineOfflineStatus("1")
                tvOnlineOffline!!.text = "Online"
                toggleTeacherView?.isChecked = true
                BaseApplication.getInstance().setOnline("true")
            }
        }
    }

    /* @Override
    protected void onResume() {
        super.onResume();
        init();
        firstFragment();
        setUpListners();
        toogleListener();
    }*/
    override fun onResume() {
        super.onResume()
        if (receiver == null) {
            registerReceiver(receiver, IntentFilter("update"))
        }
    }

    fun logSentFriendRequestEvent() {
        appEventsLogger?.logEvent("App DashBoard")

    }

    private fun audIorequestStoragePermission() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Toast.makeText(baseContext, "Error occurred! ", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (usersharedPrefernce!!.getusertype() == 1) {
            if (mReceiver != null) {
                unregisterReceiver(mReceiver)
            }
            if (mHelper != null) {
                mHelper!!.disposeWhenFinished()
                mHelper = null
            }
        }
    }

    fun firstFragment() {
        ivHome!!.setImageResource(R.drawable.home_red)
        ivProfile!!.setImageResource(R.drawable.profile_wht)
        ivChat!!.setImageResource(R.drawable.chat_wht)
        ivNotification!!.setImageResource(R.drawable.notification_wht)
        ivCreateBooking!!.setImageResource(R.drawable.create_booking)
        ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
        ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
        ivHelp!!.setImageResource(R.drawable.help_wht)
        ivLogout!!.setImageResource(R.drawable.logout_wht)
        tvHome!!.setTextColor(resources.getColor(R.color.colorPrimary))
        tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
        tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
        tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
        tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
        tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
        tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
        tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
        tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
        replaceFragment(HomeFragment(), resources.getString(R.string.home))
    }

    fun init() {
        //  loader = new Loader(this);
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        nav_view = findViewById(R.id.nav_view)
        side_nav = findViewById(R.id.side_nav)
        nav_llHome = findViewById(R.id.nav_llHome)
        nav_subscription = findViewById(R.id.nav_subscription)
        nav_llProfile = findViewById(R.id.nav_llProfile)
        nav_llChat = findViewById(R.id.nav_llChat)
        nav_llNotification = findViewById(R.id.nav_llNotification)
        nav_llPrivacyPolicy = findViewById(R.id.nav_llPrivacyPolicy)
        nav_l1CreateBooking = findViewById(R.id.nav_llCreateBooking)
        nav_l1BookingHistory = findViewById(R.id.nav_llBookingHistory)
        nav_llHelp = findViewById(R.id.nav_llHelp)
        nav_llLogout = findViewById(R.id.nav_llLogout)
        ivHome = findViewById(R.id.ivHome)
        ivProfile = findViewById(R.id.ivProfile)
        ivChat = findViewById(R.id.ivChat)
        ivNotification = findViewById(R.id.ivNotification)
        ivPrivayPolicy = findViewById(R.id.ivPrivayPolicy)
        ivCreateBooking = findViewById(R.id.ivCreateBooking)
        ivBookingHistory = findViewById(R.id.ivBookingHistory)
        ivHelp = findViewById(R.id.ivHelp)
        ivLogout = findViewById(R.id.ivLogout)
        tvHome = findViewById(R.id.tvHome)
        tvProfile = findViewById(R.id.tvProfile)
        tvChat = findViewById(R.id.tvChat)
        tvNotification = findViewById(R.id.tvNotification)
        tvCreateBooking = findViewById(R.id.tvCreateBooking)
        tvBookingHistory = findViewById(R.id.tvBookingHistory)
        tvPrivayPolicy = findViewById(R.id.tvPrivayPolicy)
        tvHelp = findViewById(R.id.tvHelp)
        tvLogout = findViewById(R.id.tvLogout)
        ivCreateBookingIcon = findViewById(R.id.createbookingIcon)
        notificationBell = findViewById(R.id.notification)
        createBookingview = findViewById(R.id.createbookingView)
        bookingHistoryview = findViewById(R.id.bookingHistoryView)
        toggleTeacherView = findViewById(R.id.toggleTeacherView)
        createBookingBelowToggle = findViewById(R.id.createBookingBelowToggle)
        homeUserName = findViewById(R.id.homeUserName)
        nav_view!!.setItemIconTintList(null)
        drawer = findViewById(R.id.drawer)
        //        drawer.setRadius(Gravity.START, 25);
        drawer!!.setViewScale(Gravity.START, 0.9f)
        drawer!!.setViewElevation(Gravity.START, 20f)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        if (usersharedPrefernce!!.getusertype() == 1) {
            initSubscription()
        } else {
        }
    }

//    private fun initSubscription() {
//        skuList = ArrayList()
//        skuList!!.add(Constants.KEY_BRONZE)
//        skuList!!.add(Constants.KEY_GOLD)
//        skuList!!.add(Constants.KEY_SILVER)
//        mHelper = IabHelper(this, Constants.BASE64_LICENSE)
//        mHelper!!.enableDebugLogging(true)
//        mHelper!!.startSetup(OnIabSetupFinishedListener { result ->
//            if (!result.isSuccess) {
//                Log.d(TAG, "IabHelper fail")
//            }
//            if (mHelper == null) return@OnIabSetupFinishedListener
//            mReceiver = IabBroadcastReceiver(this@HomeActivity)
//            val broadcastFilter = IntentFilter(IabBroadcastReceiver.ACTION)
//            registerReceiver(mReceiver, broadcastFilter)
//            try {
//                mHelper!!.queryInventoryAsync(skuList, mGotInventoryListener)
//            } catch (e: IabAsyncInProgressException) {
//                Log.d(TAG, "Exception: $e")
//            }
//        })
//    }

    fun setUpListners() {
        ivCreateBookingIcon!!.setOnClickListener(this)
        nav_llHome!!.setOnClickListener(this)
        nav_llProfile!!.setOnClickListener(this)
        nav_llChat!!.setOnClickListener(this)
        nav_l1CreateBooking!!.setOnClickListener(this)
        nav_l1BookingHistory!!.setOnClickListener(this)
        nav_llNotification!!.setOnClickListener(this)
        nav_subscription!!.setOnClickListener(this)
        nav_llPrivacyPolicy!!.setOnClickListener(this)
        nav_llHelp!!.setOnClickListener(this)
        nav_llLogout!!.setOnClickListener(this)
        side_nav!!.setOnClickListener(this)
        notificationBell!!.setOnClickListener(this)
        ivCreateBookingIcon!!.setOnClickListener(this)
        if (usersharedPrefernce!!.getusertype() == 1) {
            nav_l1CreateBooking!!.visibility = View.GONE
            nav_l1BookingHistory!!.visibility = View.GONE
            createBookingview!!.visibility = View.GONE
            bookingHistoryview!!.visibility = View.GONE
            ivCreateBookingIcon!!.visibility = View.GONE
            createBookingBelowToggle!!.visibility = View.GONE
            toggleTeacherView!!.visibility = View.VISIBLE
            tvOnlineOffline!!.visibility = View.VISIBLE
            nav_subscription!!.visibility = View.GONE
            view1!!.visibility = View.VISIBLE

        }
        else{
            viewNotifiationLine.visibility=View.VISIBLE
        }
    }

    fun replaceFragment(frag: Fragment, TAG: String) {
        val manager = supportFragmentManager
        if (manager != null) {
            val t = manager.beginTransaction()
            val currentFrag = manager.findFragmentByTag(TAG)
            if (currentFrag != null && currentFrag.javaClass == frag.javaClass) {
                t.replace(R.id.container, frag).commit()
            } else {
                if (TAG != resources.getString(R.string.home)) {
                    t.replace(R.id.container, frag, TAG)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(null).commit()
                } else {
                    t.replace(R.id.container, frag, TAG).commit()
                }
            }
        }
    }

    private fun toogleListener() {
        toggleTeacherView!!.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                usersharedPrefernce!!.setstatus(10)
                tvOnlineOffline!!.text = "Online"
                //Toast.makeText(BasicActivity.this, "Teacher is online", Toast.LENGTH_SHORT).show();
                getTeacherOnlineOfflineStatus("1")
            } else {
                usersharedPrefernce!!.setstatus(20)
                tvOnlineOffline!!.text = "Offline"

                //  Toast.makeText(BasicActivity.this, "Teacher is offline", Toast.LENGTH_SHORT).show();
                getTeacherOnlineOfflineStatus("0")
            }
        }
        if (usersharedPrefernce!!.getstatus() == 10) {
            toggleTeacherView!!.isChecked = true
            tvOnlineOffline!!.text = "Online"
        } else {
            toggleTeacherView!!.isChecked = false
            tvOnlineOffline!!.text = "Offline"
        }
    }

    private fun getTeacherOnlineOfflineStatus(online: String) {
        //     loader.isShowProgress();
        var id = ""
        if (usersharedPrefernce!!.getusertype() == 1) {
            id = usersharedPrefernce!!.getteacherid().toString()
        } else {
            id = usersharedPrefernce!!.getlearnerid().toString()
            println(
                "kjhksahcks" + usersharedPrefernce!!.getlearnerid()
            )
        }
        val user_id: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), id)
        val is_online: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), online)
        luggaAPI!!.getteacheronlinestatus(user_id, is_online)
            .enqueue(object : Callback<TeacherOnlineStatusResponse?> {
                override fun onResponse(
                    call: Call<TeacherOnlineStatusResponse?>,
                    response: Response<TeacherOnlineStatusResponse?>
                ) {
                    if (response.body() != null) {
                        if (response.isSuccessful) {
                            //loader.isDismiss();
//                        Log.d("fsdas", "dasfasdf" + response.body().getBody().get(0).getCreatedAt() + "////" + response.body().getBody().get(0).getIsOnline() + "////" + response.body().getBody().get(0).getName());
                            //    Toast.makeText(BasicActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            //loader.isDismiss();
                            Toast.makeText(
                                this@HomeActivity,
                                "error" + response.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(call: Call<TeacherOnlineStatusResponse?>, t: Throwable) {
                    //loader.isDismiss();
                    Toast.makeText(this@HomeActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            })
    }

    override fun onClick(v: View) {
        if (v === nav_llHome) {
            ivHome!!.setImageResource(R.drawable.home_red)
            ivProfile!!.setImageResource(R.drawable.profile_wht)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            tvHome!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            drawer!!.closeDrawer(GravityCompat.START)
            replaceFragment(HomeFragment(), resources.getString(R.string.home))
        } else if (v === nav_llProfile) {
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.profile_red)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            drawer!!.closeDrawer(GravityCompat.START)
            if (usersharedPrefernce!!.getusertype() == 2) {
                usersharedPrefernce!!.getuserid()
                startActivity(Intent(applicationContext, LearnerProfileActivity::class.java))
            } else if (usersharedPrefernce!!.getusertype() == 1) {
                usersharedPrefernce!!.getuserid()
                startActivity(Intent(applicationContext, TeacherProfile::class.java))
            }
        } else if (v === nav_l1CreateBooking) {
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.profile_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking_red)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            drawer!!.closeDrawer(GravityCompat.START)
            startActivity(Intent(applicationContext, CreateBooking::class.java))
        } else if (v === nav_l1BookingHistory) {
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.profile_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_red)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            drawer!!.closeDrawer(GravityCompat.START)
            startActivity(Intent(applicationContext, BookingHistory::class.java))
        } else if (v === nav_llChat) {
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.privacy_wht)
            ivChat!!.setImageResource(R.drawable.chat_red)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            drawer!!.closeDrawer(GravityCompat.START)
            startActivity(Intent(applicationContext, ChatListActivity::class.java))
        } else if (v === nav_subscription) {
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.privacy_wht)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            ivSubs!!.setImageResource(R.drawable.logout_red)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvSubs!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            drawer!!.closeDrawer(GravityCompat.START)
            startActivity(Intent(applicationContext, SubscriptionActivity::class.java))
        } else if (v === nav_llNotification) {
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.privacy_wht)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivNotification!!.setImageResource(R.drawable.notification_red)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            drawer!!.closeDrawer(GravityCompat.START)
            startActivity(Intent(applicationContext, NotificationScreen::class.java))
        } else if (v === nav_llPrivacyPolicy) {
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.privacy_wht)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_red)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            drawer!!.closeDrawer(GravityCompat.START)
            val intent = Intent(this@HomeActivity, PrivacyPolicyActivity::class.java)
            startActivity(intent)
        } else if (v === nav_llHelp) {
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.privacy_wht)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_red)
            ivLogout!!.setImageResource(R.drawable.logout_wht)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorPrimary))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorWhite))
            drawer!!.closeDrawer(GravityCompat.START)
            val intent = Intent(this@HomeActivity, FAQActivity::class.java)
            startActivity(intent)
        } else if (v === nav_llLogout) {
            tvSubs!!.setTextColor(resources.getColor(R.color.colorWhite))
            ivSubs!!.setImageResource(R.drawable.logout_wht)
            ivHome!!.setImageResource(R.drawable.home_wht)
            ivProfile!!.setImageResource(R.drawable.privacy_wht)
            ivChat!!.setImageResource(R.drawable.chat_wht)
            ivBookingHistory!!.setImageResource(R.drawable.booking_history_wht)
            ivCreateBooking!!.setImageResource(R.drawable.create_booking)
            ivNotification!!.setImageResource(R.drawable.notification_wht)
            ivPrivayPolicy!!.setImageResource(R.drawable.privacy_wht)
            ivHelp!!.setImageResource(R.drawable.help_wht)
            ivLogout!!.setImageResource(R.drawable.logout_red)
            tvHome!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvProfile!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvBookingHistory!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvCreateBooking!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvChat!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvNotification!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvPrivayPolicy!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvHelp!!.setTextColor(resources.getColor(R.color.colorWhite))
            tvLogout!!.setTextColor(resources.getColor(R.color.colorPrimary))

            //drawer.closeDrawer(GravityCompat.START);
            openLogOutDialog()
        } else if (v === side_nav) {
            drawer!!.openDrawer(GravityCompat.START)
        } else if (v === notificationBell) {
            startActivity(Intent(this@HomeActivity, NotificationScreen::class.java))
        } else if (v === ivCreateBookingIcon) {
            startActivity(Intent(this@HomeActivity, CreateBooking::class.java))
        } else {
            drawer!!.closeDrawer(GravityCompat.START)
        }
    }

    private fun openLogOutDialog() {
        builder!!.setMessage("Do you want to Logout ?")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id -> hitApi() }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder!!.create()
        alert.setTitle("Logout")
        alert.show()
    }

    private fun hitApi() {
        userId = if (usersharedPrefernce!!.getusertype() == 1) {
            usersharedPrefernce!!.getteacherid()
        } else {
            usersharedPrefernce!!.getlearnerid()
        }
        val UserId: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), userId.toString())
        ApiClient.getApiClient().create(LuggaAPI::class.java).onLogOut(UserId)
            .enqueue(object : Callback<ResponseBody?> {
                override fun onResponse(
                    call: Call<ResponseBody?>,
                    response: Response<ResponseBody?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            startActivity(Intent(applicationContext, LoginActivity::class.java))
                            usersharedPrefernce!!.setboolean(false)
                            usersharedPrefernce!!.setprofileimage(null)
                            usersharedPrefernce!!.receiverProfileimage = null
                            usersharedPrefernce!!.setname(null)
                            usersharedPrefernce!!.setlearnerid(0)
                            usersharedPrefernce!!.setteacherid(0)
                            finish()
                            Toast.makeText(
                                applicationContext, "Logout Successfully",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(
                                this@HomeActivity,
                                "something went wrong !! please try again",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(this@HomeActivity, "LogOut failed", Toast.LENGTH_SHORT)
                            .show()
                    }
                }

                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    Toast.makeText(this@HomeActivity, t.message, Toast.LENGTH_SHORT).show()
                }
            })
    }

    override fun onBackPressed() {
        val alertbox = AlertDialog.Builder(this)
            .setMessage("Do you want to exit application?")
            .setPositiveButton("Yes") { arg0, arg1 ->

                // do something when the button is clicked
                val a = Intent(Intent.ACTION_MAIN)
                a.addCategory(Intent.CATEGORY_HOME)
                a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(a)
            }
            .setNegativeButton(
                "No" // do something when the button is clicked
            ) { arg0, arg1 -> }
            .show()
    }

    fun verifyDeveloperPayload(p: Purchase): Boolean {
        val payload = p.developerPayload
        return true
    }

    override fun receivedBroadcast() {
        // Received a broadcast notification that the inventory of items has changed
        Log.d(TAG, "Received broadcast notification. Querying inventory.")
        try {
            mHelper!!.queryInventoryAsync(skuList, mGotInventoryListener)
        } catch (e: IabAsyncInProgressException) {
            Log.d(TAG, "Exception: $e")
        }
    }

    private val mGotInventoryListener = QueryInventoryFinishedListener { result, inventory ->
        Log.d(TAG, "Querying finished")
        if (mHelper == null) return@QueryInventoryFinishedListener
        if (result.isFailure) {
            Log.d(TAG, "Querying fail")
            return@QueryInventoryFinishedListener
        }
        Log.d(TAG, "Query inventory was successful.")

        // First find out which subscription is auto renewing
        val gold = inventory.getPurchase(Constants.KEY_GOLD)
        val silver = inventory.getPurchase(Constants.KEY_SILVER)
        val bronze = inventory.getPurchase(Constants.KEY_BRONZE)
        if (gold != null && gold.isAutoRenewing) {
            mActiveSub = Constants.KEY_GOLD
            isAutoRenewable = true
        } else if (silver != null && silver.isAutoRenewing) {
            mActiveSub = Constants.KEY_SILVER
            isAutoRenewable = true
        } else if (bronze != null && bronze.isAutoRenewing) {
            mActiveSub = Constants.KEY_BRONZE
            isAutoRenewable = true
        } else {
            mActiveSub = ""
            isAutoRenewable = false
        }
        isSubscribed = (gold != null && verifyDeveloperPayload(gold)
                || silver != null && verifyDeveloperPayload(silver)
                || bronze != null && verifyDeveloperPayload(bronze))
        Log.d(
            TAG, "User " + (if (isSubscribed) "HAS" else "DOES NOT HAVE")
                    + " subscription."
        )
        Log.d(TAG, "User subscribed with $mActiveSub")
        /**
         * @variable isSubscribed = defines if the subscription is taken
         * @variable mActiveSub = defines which subscription is taken (if empty then free)
         */
        /**
         * @variable isSubscribed = defines if the subscription is taken
         * @variable mActiveSub = defines which subscription is taken (if empty then free)
         */
        /**
         * @variable isSubscribed = defines if the subscription is taken
         * @variable mActiveSub = defines which subscription is taken (if empty then free)
         */
        /**
         * @variable isSubscribed = defines if the subscription is taken
         * @variable mActiveSub = defines which subscription is taken (if empty then free)
         */
//
//        if (!isSubscribed && mActiveSub.isEmpty()) {
//            usersharedPrefernce!!.setboolean(false)
//            usersharedPrefernce!!.setprofileimage(null)
//            usersharedPrefernce!!.setname(null)
//            usersharedPrefernce!!.setlearnerid(0)
//            usersharedPrefernce!!.setteacherid(0)
//            startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
//            finishAffinity()
//        }
    }

    companion object {
        private const val TAG = "BasicActivity"
    }

    private fun initSubscription() {
        bp = BillingProcessor(
            this,
            LICENSE_KEY,
            MERCHANT_ID,
            object : BillingProcessor.IBillingHandler {
                override fun onProductPurchased(productId: String, purchaseInfo: PurchaseInfo) {
                }

                override fun onBillingError(errorCode: Int, error: Throwable) {
                }

                override fun onBillingInitialized() {
                    if (bp!!.isSubscribed(KEY_BRONZE)) {
                        isSubscribed = true
                    } else if (bp!!.isSubscribed(KEY_SILVER)) {
                        isSubscribed = true
                    } else if (bp!!.isSubscribed(KEY_GOLD)) {
                        isSubscribed = true
                    }

//                    runOnUiThread {
//                        if (!isSubscribed) {
//                            Toast.makeText(this@HomeActivity, "Subscription has been expired", Toast.LENGTH_SHORT).show()
//                            usersharedPrefernce!!.setboolean(false)
//                            usersharedPrefernce!!.setprofileimage(null)
//                            usersharedPrefernce!!.setname(null)
//                            usersharedPrefernce!!.setlearnerid(0)
//                            usersharedPrefernce!!.setteacherid(0)
//                            startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
//                            finishAffinity()
//                        }
//                    }
                }

                override fun onPurchaseHistoryRestored() {
                }
            })

    }
}