package com.Lugga.lugga

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaRecorder
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Patterns
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.adapters.AllLanguageAdapter
import com.Lugga.lugga.adapters.DaysAdapter
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.DaysList
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.Loader
import com.Lugga.lugga.views.FilePath
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.select_days_dialog.*
import kotlinx.android.synthetic.main.sign_up.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class SignUpActivity : AppCompatActivity(), View.OnClickListener {
    private var daysModel: DaysList?=null
    private var daysList: ArrayList<DaysList>? = null

    private var selectedExperience: String? = null
    var luggaAPI: LuggaAPI? = null
    var llBasicInformation: LinearLayout? = null
    var llPersonalInformation: LinearLayout? = null
    var textNext: TextView? = null
    var textNextoSignUp: TextView? = null
    var tvBegineer: TextView? = null
    var tvIntermidiate: TextView? = null
    var tvAdavanvced: TextView? = null
    var login: TextView? = null
    var levelSignup: TextView? = null
    var uploadDocuments: TextView? = null
    var languageSignup: EditText? = null
    var languageSignUp2: EditText? = null
    var teacherRadioButton: RadioButton? = null
    var learnerRadioButton: RadioButton? = null
    var nameSignup: EditText? = null
    var emailSignup: EditText? = null
    var mobileNumberSignup: EditText? = null
    var passwordSignup: EditText? = null
    var amountSignup: EditText? = null
    var radioGroup: RadioGroup? = null
    var selectedfile: String? = null
    var loader: Loader? = null
    var countRadio = 2
    var uploadDocumentsImage: ImageView? = null
    var viewUploadDocuments: View? = null
    var viewLevel: View? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var layoutLanguage: LinearLayout? = null
    var levelcount = 0
    var language_spinner: Spinner? = null
    var language_spinner2: Spinner? = null
    var allLanguagModelArrayList: ArrayList<AllLanguagModel>? = null
    var spinnerposition = 0
    var stripe_publishable_key: String? = null
    var stripe_user_id: String? = null
    var ivGreenTick: ImageView? = null
    private var allLanguageAdapter: AllLanguageAdapter? = null
    var selectedLanguage: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_up)
        val ivConnectStripe = findViewById<ImageView>(R.id.ivConnectStripe)
        ivGreenTick = findViewById(R.id.ivGreenTick)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        ivConnectStripe.setOnClickListener(this)
        init()
        onClick()


        val languages = resources.getStringArray(R.array.Experience)

        // access the spinner
        val spinnerLang = findViewById<Spinner>(R.id.experienceSignup)
        if (spinnerLang != null) {
            val adapter = ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, languages)
            spinnerLang.adapter = adapter

            spinnerLang.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    selectedExperience = languages[position]

                }

            }
        }

        tvTeacherSignUpLink.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/DllZKQk5Ps0"))
            startActivity(browserIntent)
        }
    }

    fun init() {
        llBasicInformation = findViewById(R.id.llBasicInformation)
        llPersonalInformation = findViewById(R.id.llPersonalInformation)
        textNext = findViewById(R.id.textNext)
        textNextoSignUp = findViewById(R.id.textNextoSignUp)
        tvBegineer = findViewById(R.id.tvBegineer)
        tvIntermidiate = findViewById(R.id.tvIntermidiate)
        tvAdavanvced = findViewById(R.id.tvAdavanvced)
        levelSignup = findViewById(R.id.levelSignup)
        uploadDocuments = findViewById(R.id.uploadDocuments)
        languageSignup = findViewById(R.id.languageSignup)
        nameSignup = findViewById(R.id.nameSignup)
        emailSignup = findViewById(R.id.emailSignup)
        mobileNumberSignup = findViewById(R.id.mobileNumberSignup)
        passwordSignup = findViewById(R.id.passwordSignup)
        login = findViewById(R.id.login)
        amountSignup = findViewById(R.id.price)
        uploadDocumentsImage = findViewById(R.id.uploadDocumentsImage)
        teacherRadioButton = findViewById(R.id.teacherRadioButton)
        learnerRadioButton = findViewById(R.id.learnerRadioButton)
        radioGroup = findViewById(R.id.radioGroup)
        viewUploadDocuments = findViewById(R.id.viewUploadDocuments)
        viewLevel = findViewById(R.id.viewLevel)
        layoutLanguage = findViewById(R.id.layoutLanguage)
        //  languageSignUp2=findViewById(R.id.languageSignup2);
        language_spinner = findViewById(R.id.spinner_languages)
        language_spinner2 = findViewById(R.id.spinner_languages2)
        setHideKeyboardOnTouch(this@SignUpActivity, findViewById(R.id.rlKey))
        amountSignup?.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                if (!amountSignup?.getText().toString().isEmpty()) {
                    //
                    if (amountSignup?.getText().toString().contains("/30 min")) {
                        amountSignup?.setText(amountSignup?.getText().toString())
                    } else {
                        amountSignup?.setText(amountSignup?.getText().toString() + "/30 min")
                    }
                }
            }
            false
        })
        radiogroup()
        getlanguages()
        selectDays()
        // loader!!.isShowProgress()
        progressBarSignUp.visibility = View.VISIBLE

    }

    private fun selectDays() {
        daysList = ArrayList()

        for (i in 0..6) {
            daysModel = DaysList()
            when (i) {
                0 ->daysModel!!.day="Monday"
                1 ->daysModel!!.day="Tuesday"
                2 ->daysModel!!.day="Wednesday"
                3 ->daysModel!!.day="Thursday"
                4 ->daysModel!!.day="Friday"
                5 ->daysModel!!.day="Saturday"
                6 ->daysModel!!.day="Sunday"
            }
            daysList!!.add(daysModel!!)

        }

        tvSelectDays.setOnClickListener {
            val dialog = Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.select_days_dialog)

            val daysAdapter=DaysAdapter(this,daysList)
            dialog.rvDays.adapter=daysAdapter


            dialog.show()
        }
    }

    private fun setHideKeyboardOnTouch(context: Context, view: View) {

        //Set up touch listener for non-text box views to hide keyboard.
        try {
            //Set up touch listener for non-text box views to hide keyboard.
            if (!(view is EditText || view is ScrollView)) {
                view.setOnTouchListener { v, event ->
                    val `in` = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    `in`.hideSoftInputFromWindow(v.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                    if (!amountSignup!!.text.toString().isEmpty()) {
                        //
                        if (amountSignup!!.text.toString().contains("/30 min")) {
                            amountSignup!!.setText(amountSignup!!.text.toString())
                        } else {
                            amountSignup!!.setText(amountSignup!!.text.toString() + "/30 min")
                        }
                    }
                    false
                }
            }
            //If a layout container, iterate over children and seed recursion.
//            if (view instanceof ViewGroup) {
//
//                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
//
//                    View innerView = ((ViewGroup) view).getChildAt(i);
//
//                    setHideKeyboardOnTouch(context, innerView);
//                }
//            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getlanguages() {
        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages().enqueue(object : Callback<AllLanguageResposne> {
            override fun onResponse(call: Call<AllLanguageResposne>, response: Response<AllLanguageResposne>) {
                if (response.isSuccessful) {
                    allLanguagModelArrayList = ArrayList()
                    progressBarSignUp.visibility = View.GONE
                    allLanguagModelArrayList = response.body()!!.body
                    allLanguageAdapter = AllLanguageAdapter(this@SignUpActivity, allLanguagModelArrayList!!)
                    language_spinner!!.adapter = allLanguageAdapter
                    language_spinner2!!.adapter = allLanguageAdapter
                } else {
                    progressBarSignUp.visibility = View.GONE

                }
            }

            override fun onFailure(call: Call<AllLanguageResposne>, t: Throwable) {
                progressBarSignUp.visibility = View.GONE

            }
        })
        language_spinner!!.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedLanguage = (allLanguageAdapter!!.getItem(position) as AllLanguagModel).language
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        language_spinner2!!.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedLanguage = (allLanguageAdapter!!.getItem(position) as AllLanguagModel).language
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun radiogroup() {

        learnerRadioButton!!.isChecked = true
        radioGroup!!.setOnCheckedChangeListener { radioGroup, checkedId ->
            val rb = radioGroup.findViewById<RadioButton>(checkedId)
            if (null != rb && checkedId > 0) {
                if (rb == learnerRadioButton) {
                    countRadio = 2
                    textNext!!.visibility = View.GONE
                    textNextoSignUp!!.visibility = View.VISIBLE
                    layoutLanguage!!.visibility = View.VISIBLE
                    tvTeacherSignUpLink.visibility = View.GONE
                    tvBlink.visibility = View.GONE
                } else if (rb == teacherRadioButton) {
                    getlanguages()
                    progressBarSignUp.visibility = View.VISIBLE
                    textNext!!.visibility = View.VISIBLE
                    textNextoSignUp!!.visibility = View.GONE
                    layoutLanguage!!.visibility = View.GONE
                    countRadio = 1

                    val animation: Animation
                    animation = AnimationUtils.loadAnimation(applicationContext,
                            R.anim.blink)
                    tvBlink.visibility = View.VISIBLE
                    tvBlink.animation = animation
                    tvTeacherSignUpLink.visibility = View.VISIBLE

                }
            }
        }
    }

    fun onClick() {
        uploadDocuments!!.setOnClickListener { requestStoragePermission() }
        textNext!!.setOnClickListener {
            if (!validation() && isValidPhone(mobileNumberSignup!!.text.toString())) {
                getlanguages()
                radioGroup!!.visibility = View.GONE
                textNext!!.visibility = View.GONE
                llBasicInformation!!.visibility = View.GONE
                llPersonalInformation!!.visibility = View.VISIBLE
                textNext!!.visibility = View.GONE
                textNextoSignUp!!.visibility = View.VISIBLE
            }
        }
        uploadDocumentsImage!!.setOnClickListener { requestStoragePermission() }
        textNextoSignUp!!.setOnClickListener { //   boolean isEmpty=validationNext();
            if (countRadio == 2) {
                if (!validation() && isValidPhone(mobileNumberSignup!!.text.toString())) {
                    learnerData()
                }
            } else {
                if (!validation() && !validationNext()) {
                    teacherData()
                }
            }
        }
        tvBegineer!!.setOnClickListener {
            tvBegineer!!.background = resources.getDrawable(R.drawable.button_style_corner)
            tvBegineer!!.setTextColor(resources.getColor(R.color.colorWhite))
            levelSignup!!.text = "Begineer"
            levelcount = 1
            viewLevel!!.setBackgroundColor(resources.getColor(R.color.colorAccent))
            tvAdavanvced!!.background = resources.getDrawable(R.drawable.button_style_corner_red_stroke)
            tvAdavanvced!!.setTextColor(resources.getColor(R.color.textDark))
            tvIntermidiate!!.background = resources.getDrawable(R.drawable.button_style_corner_red_stroke)
            tvIntermidiate!!.setTextColor(resources.getColor(R.color.textDark))
        }
        tvIntermidiate!!.setOnClickListener {
            tvIntermidiate!!.background = resources.getDrawable(R.drawable.button_style_corner)
            tvIntermidiate!!.setTextColor(resources.getColor(R.color.colorWhite))
            levelSignup!!.text = "Intermidiater"
            levelcount = 2
            viewLevel!!.setBackgroundColor(resources.getColor(R.color.colorAccent))
            tvAdavanvced!!.background = resources.getDrawable(R.drawable.button_style_corner_red_stroke)
            tvAdavanvced!!.setTextColor(resources.getColor(R.color.textDark))
            tvBegineer!!.background = resources.getDrawable(R.drawable.button_style_corner_red_stroke)
            tvBegineer!!.setTextColor(resources.getColor(R.color.textDark))
        }
        tvAdavanvced!!.setOnClickListener {
            tvAdavanvced!!.background = resources.getDrawable(R.drawable.button_style_corner)
            tvAdavanvced!!.setTextColor(resources.getColor(R.color.colorWhite))
            levelSignup!!.text = "Advanced"
            levelcount = 3
            viewLevel!!.setBackgroundColor(resources.getColor(R.color.colorAccent))
            tvIntermidiate!!.background = resources.getDrawable(R.drawable.button_style_corner_red_stroke)
            tvIntermidiate!!.setTextColor(resources.getColor(R.color.textDark))
            tvBegineer!!.background = resources.getDrawable(R.drawable.button_style_corner_red_stroke)
            tvBegineer!!.setTextColor(resources.getColor(R.color.textDark))
        }
        login!!.setOnClickListener {
            startActivity(Intent(this@SignUpActivity, LoginActivity::class.java))
            finish()
        }
    }

    private fun teacherData() {
        progressBarSignUp.visibility = View.VISIBLE
        val name = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), nameSignup!!.text.toString())
        val email: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), emailSignup!!.text.toString())
        val mobile_number: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), mobileNumberSignup!!.text.toString())
        val password: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), passwordSignup!!.text.toString())
        val user_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), countRadio.toString())
        val device_token: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.devicetoken)
        val language: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedLanguage!!)
        val device_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "A")
        val experience: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedExperience.toString())
        val finalAmount = amountSignup!!.text.toString().replace("/30 min", "")
        val amount: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), finalAmount)
        val time: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "30")
        val Stripe_publishable_key: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), stripe_publishable_key!!)
        val Stripe_user_id: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), stripe_user_id!!)
        val level: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), levelcount.toString())
        val levelO: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), levelcount.toString())
        luggaAPI!!.signup(levelO,level,name, email, mobile_number, password, user_type, language, device_type, device_token, experience, amount, time, Stripe_publishable_key, Stripe_user_id).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                if (response.body() != null) {
                    if (response.isSuccessful) {
                        progressBarSignUp.visibility = View.GONE

                        Toast.makeText(this@SignUpActivity, "Registered" + levelSignup!!.text.toString(), Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this@SignUpActivity, LoginActivity::class.java))
                        finish()
                    } else {
                        progressBarSignUp.visibility = View.GONE

                        Toast.makeText(this@SignUpActivity, "Not registered" + response.body(), Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                progressBarSignUp.visibility = View.GONE

                Toast.makeText(this@SignUpActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun learnerData() {
        progressBarSignUp.visibility = View.VISIBLE
        val name: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), nameSignup!!.text.toString())
        val des: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), nameSignup!!.text.toString())
        val email: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), emailSignup!!.text.toString())
        val mobileno: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), mobileNumberSignup!!.text.toString())
        val password: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), passwordSignup!!.text.toString())
        val user_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), countRadio.toString())
        val language: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedLanguage!!)
        val device_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "A")
        val device_token: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.devicetoken)
        luggaAPI!!.signup2(des,name, email, mobileno, password, user_type, language, device_type, device_token).enqueue(object : Callback<com.Lugga.lugga.model.Response?> {
            override fun onResponse(call: Call<com.Lugga.lugga.model.Response?>, response: Response<com.Lugga.lugga.model.Response?>) {
                if (response.body() != null) {
                    if (response.body()!!.success == 200) {
                        progressBarSignUp.visibility = View.GONE

                        usersharedPrefernce!!.setusertype(usersharedPrefernce!!.getusertype())
                        Toast.makeText(this@SignUpActivity, "register successfully", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this@SignUpActivity, LoginActivity::class.java))
                        finish()
                    } else {
                        progressBarSignUp.visibility = View.GONE

                        Toast.makeText(this@SignUpActivity, "Not registered", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<com.Lugga.lugga.model.Response?>, t: Throwable) {
                progressBarSignUp.visibility = View.GONE
                Toast.makeText(this@SignUpActivity, "Not Registered", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun validation(): Boolean {
        var isEmpty = false
        if (nameSignup!!.text.toString().isEmpty()) {
            nameSignup!!.error = "Please enter the name"
            isEmpty = true
        }
        if (emailSignup!!.text.toString().isEmpty()) {
            emailSignup!!.error = "Please enter the email"
            isEmpty = true
        }
        if (mobileNumberSignup!!.text.toString().isEmpty()) {
            mobileNumberSignup!!.error = "Please enter the mobile number"
            isEmpty = true
        }
        if (passwordSignup!!.text.toString().isEmpty()) {
            passwordSignup!!.error = "Please enter the password"
            isEmpty = true
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailSignup!!.text.toString()).matches()) {
            emailSignup!!.error = "Please enter valid email address"
            isEmpty = true
        }
        return isEmpty
    }

    private fun isValidPhone(phone: String): Boolean {
        var check = false
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length < 6 || phone.length > 13) {
                mobileNumberSignup!!.error = "please enter valid mobile number"
                check = false
            } else {
                check = true
            }
        } else {
            check = false
        }
        return check
    }

    private fun validationNext(): Boolean {
        var isEmpty = false
        if (selectedExperience!!.isEmpty()) {
            Toast.makeText(this, "Please select experience", Toast.LENGTH_SHORT).show()
            isEmpty = true
        }
        if (amountSignup!!.text.toString().isEmpty()) {
            amountSignup!!.error = "please enter the amount"
            isEmpty = true
        }
        if (stripe_publishable_key == null && stripe_user_id == null) {
            Toast.makeText(this, "Please register your stripe account", Toast.LENGTH_SHORT).show()
            isEmpty = true
        }
        return isEmpty
    }

    private fun requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            showPictureDialog()
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).withErrorListener { Toast.makeText(baseContext, "Error occurred! ", Toast.LENGTH_SHORT).show() }
                .onSameThread()
                .check()
    }

    fun showPictureDialog() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library",
                "Cancel")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            val userChoosenTask: String
            if (items[item] == "Take Photo") {
                userChoosenTask = "Take Photo"
                cameraIntent()
            } else if (items[item] == "Choose from Library") {
                userChoosenTask = "Choose from Library"
                galleryIntent()
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT //
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY)
    }

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, MediaRecorder.VideoSource.CAMERA)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            return
        }
        if (requestCode == STRIPE_ACCOUNT) {
            if (resultCode == Activity.RESULT_OK) {
                ivGreenTick!!.visibility = View.VISIBLE
                stripe_publishable_key = data!!.extras!!.getString("stripe_publishable_key")
                stripe_user_id = data.extras!!.getString("stripe_user_id")
            }
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                selectedfile = FilePath.getPath(this, contentURI)
                uploadDocuments!!.text = selectedfile
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (requestCode == MediaRecorder.VideoSource.CAMERA) {
            val thumbnail = data!!.extras!!["data"] as Bitmap?
            val tempUri = getImageUri(this, thumbnail)
            selectedfile = getRealPathFromURI(tempUri)
            uploadDocuments!!.text = selectedfile
        }
    }

    private fun getImageUri(applicationContext: Context, photo: Bitmap?): Uri {
        val bytes = ByteArrayOutputStream()
        photo!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(this.contentResolver, photo, "Title", null)
        return Uri.parse(path)
    }

    fun getRealPathFromURI(uri: Uri?): String {
        val cursor = this.contentResolver.query(uri!!, null, null, null, null)
        cursor!!.moveToFirst()
        val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        return cursor.getString(idx)
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onClick(v: View) {
        val intent = Intent(this@SignUpActivity, StripeWebViewActivity::class.java)
        startActivityForResult(intent, STRIPE_ACCOUNT)
    }

    companion object {
        private const val GALLERY = 7
        private const val STRIPE_ACCOUNT = 333
    }
}