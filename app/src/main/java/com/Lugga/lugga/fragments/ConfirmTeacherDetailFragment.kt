package com.Lugga.lugga.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.Lugga.lugga.R


class ConfirmTeacherDetailFragment : Fragment() {
    var proceedToPayment: TextView? = null
    var layoutConfirmTeacherDetail: RelativeLayout? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(container!!.context).inflate(R.layout.confirm_detail_teacher, container, false)
        proceedToPayment = view.findViewById(R.id.proceedToPayment)
        layoutConfirmTeacherDetail = view.findViewById(R.id.layoutConfirmTeacherDetail)
        proceedToPayment!!.setOnClickListener(View.OnClickListener {
            layoutConfirmTeacherDetail!!.setVisibility(View.GONE)
            val paymentConfirmed = PaymentConfirmedFragment()
            fragmentManager!!.beginTransaction().replace(R.id.containerConfirmTeacherDetail, paymentConfirmed).commit()
        })
        return view
    }
}