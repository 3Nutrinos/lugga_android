package com.Lugga.lugga.fragments

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import android.widget.RatingBar.OnRatingBarChangeListener
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.Lugga.lugga.CreateBooking
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.AdapterLearner
import com.Lugga.lugga.adapters.AdapterLearner.ontimerstart
import com.Lugga.lugga.adapters.AdapterTeacher
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.TimerModel
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne
import com.Lugga.lugga.model.bookingStatus.BookingStatusResponse
import com.Lugga.lugga.model.learnerhomepage.LearnerHomePageBodyItem
import com.Lugga.lugga.model.learnerhomepage.LearnerHomePageResponse
import com.Lugga.lugga.model.teacherhomepage.TeacherHomePageBodyItem
import com.Lugga.lugga.model.teacherhomepage.TeacherHomePageResponse
import com.Lugga.lugga.newchatcall.NewChatActivity
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.SocketNetworking
import com.Lugga.lugga.views.ConfirmDetailActivity
import com.google.gson.Gson
import io.socket.client.Socket
import kotlinx.android.synthetic.main.fragment_home.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.MalformedURLException
import java.net.URL
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment(), ontimerstart, AdapterLearner.onpayclick, AdapterTeacher.onpayclick,
    View.OnClickListener, OnRefreshListener, OnRatingBarChangeListener {
    private var s: Date? = null
    private var fr: SimpleDateFormat? = null
    private var learnerList: ArrayList<LearnerHomePageBodyItem>? = null
    private var teacherList: ArrayList<TeacherHomePageBodyItem>? = null
    var socket: Socket? = null
    var callBrodcst: BroadcastReceiver? = null
    var serverURL: URL? = null
    var recyclerPersons: RecyclerView? = null
    private var adapterTeacher: AdapterTeacher? = null
    var adapterLearner: AdapterLearner? = null
    var fragmentHome: RelativeLayout? = null
    var luggaAPI: LuggaAPI? = null

    var usersharedPrefernce: UsersharedPrefernce? = null
    var currentime: Long = 0
    var nodata: TextView? = null
    var btCreateBooking: Button? = null
    private var status: Int? = null
    private var stats: String? = null
    private var counter = 0
    private var dialog: Dialog? = null
    private var progressBar: ProgressBar? = null
    private var givenRating = 0f


    private var swipeRefreshLay: SwipeRefreshLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        var view: View? = null
        view = inflater.inflate(R.layout.fragment_home, container, false)

        recyclerPersons = view.findViewById(R.id.recyclerPersons)
        fragmentHome = view.findViewById(R.id.homeFragment)
        nodata = view.findViewById(R.id.tvNodata)
        btCreateBooking = view.findViewById(R.id.btCreateBooking)
        progressBar = view.findViewById(R.id.progressBarHome)
        swipeRefreshLay = view.findViewById(R.id.swipeRefreshLay)
        progressBar!!.visibility = View.VISIBLE
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket!!.connect()
        serverURL = try {
            URL("https://meet.jit.si")
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            throw RuntimeException("Invalid server URL!")
        }


        swipeRefreshLay!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                if (usersharedPrefernce!!.getusertype() == 1) {
                    //  teacherHomePage
                    getTeacherHomeListingSocket()
                } else if (usersharedPrefernce!!.getusertype() == 2) {

                    getLearnerHomeListingSocket()

                    // learnerHomepage
                    hitRatingGivenOrNotApi()
                }
            }

        })

        btCreateBooking?.setOnClickListener {
            startActivity(Intent(requireContext(), CreateBooking::class.java))
        }
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val countDownTimer = object : CountDownTimer(2000, 1000) {
            override fun onFinish() {
                if (usersharedPrefernce!!.getusertype() == 1) {
                    //  teacherHomePage
                    getTeacherHomeListingSocket()

                } else if (usersharedPrefernce!!.getusertype() == 2) {

                    getLearnerHomeListingSocket()
                    // learnerHomepage
                    hitRatingGivenOrNotApi()
                }
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        countDownTimer.start()

    }

    private fun getTeacherHomeListingSocket() {
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            jsonObject.put("id", usersharedPrefernce!!.getuserid().toString())
            jsonObject.put("user_type", "1")
            socket?.emit("HomepageSocket", jsonObject)

            getTeacherList()
        } else {
            socket?.connect()
            val jsonObject = JSONObject()
            jsonObject.put("id", usersharedPrefernce!!.getuserid().toString())
            jsonObject.put("user_type", "1")
            socket?.emit("HomepageSocket", jsonObject)

            getTeacherList()
        }
    }

    private fun getTeacherList() {
        if (socket!!.connected()) {
            socket!!.on(
                "returnHomepageSocket" + usersharedPrefernce!!.getuserid().toString()
            ) { args ->

                Log.d("dataResponse", Gson().toJson(args))
                val msgresponse = args[0].toString()
                val gson = Gson()
                val teacherListing = gson.fromJson(msgresponse, TeacherHomePageResponse::class.java)


                activity?.runOnUiThread {
                    swipeRefreshLay?.isRefreshing = false
                    progressBar!!.visibility = View.GONE
                    if (teacherListing.success == 200) {
                        teacherList = teacherListing.body
                        Log.e("response", Gson().toJson(teacherListing))

                        if (teacherList?.size!! > 0) {
                            nodata!!.visibility = View.GONE
                            recyclerPersons!!.visibility = View.VISIBLE

                            if (teacherList != null)
                                adapterTeacher = AdapterTeacher(requireContext(), teacherList!!)
                            adapterTeacher!!.setServerTime(teacherListing.currentdateTime)
                            for (i in teacherList!!.indices) {
                                status = teacherList!![i].status
                                val value = status.toString()
                                if (value.equals("5", ignoreCase = true)) {
                                    counter += 1
                                }
                            }
                            val intent = Intent()
                            intent.action = "update"
                            intent.putExtra("count", counter)
                            adapterTeacher!!.setonPayClickListener(this)
                            recyclerPersons!!.adapter = adapterTeacher
                        } else {
                            nodata!!.visibility = View.VISIBLE
                            recyclerPersons!!.visibility = View.INVISIBLE
                        }
                    } else {
                        nodata!!.visibility = View.VISIBLE
                        recyclerPersons!!.visibility = View.INVISIBLE
                    }
                }


            }


        }
    }

    private fun getLearnerHomeListingSocket() {
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            jsonObject.put("id", usersharedPrefernce!!.getuserid().toString())
            jsonObject.put("user_type", "2")
            socket?.emit("HomepageSocket", jsonObject)

            getLearnerList()
        } else {
            socket?.connect()
            val jsonObject = JSONObject()
            jsonObject.put("id", usersharedPrefernce!!.getuserid().toString())
            jsonObject.put("user_type", "2")
            socket?.emit("HomepageSocket", jsonObject)

            getLearnerList()
        }
    }

    private fun getLearnerList() {
        if (socket!!.connected()) {
            Log.e("innnnnnn", "okk")
            socket!!.on(
                "returnHomepageSocket" + usersharedPrefernce!!.getuserid().toString()
            ) { args ->
                val msgresponse = args[0].toString()
                val gson = Gson()


                val learnerListing = gson.fromJson(msgresponse, LearnerHomePageResponse::class.java)
                activity?.runOnUiThread {
                    if (usersharedPrefernce!!.getusertype() != 1) {
                        nodata?.text =
                            "Oops !! Looks like you haven't booked any lesson today click button below to request a lesson."
                    }
                    swipeRefreshLay?.isRefreshing = false
                    progressBar!!.visibility = View.GONE
                    if (learnerListing.success == 200) {
                        learnerList = learnerListing.body
                        Log.e("response", Gson().toJson(learnerListing))
                        //setLearnerAdapter()

                        if (learnerList!!.size > 0) {
                            for (i in learnerList!!.indices) {
//                                stats = learnerList!!.get(i).bookingStatus.toString()
//                                val value = stats.toString()
//                                if (value.equals("5", ignoreCase = true)) {
//                                    counter += 1
//                                }
                            }
                            val intent = Intent()
                            intent.action = "update"
                            intent.putExtra("count", counter)
                            if (activity != null) {
                                requireActivity().sendBroadcast(intent)
                            }
                            adapterLearner = AdapterLearner(context, learnerList)
                            adapterLearner!!.setCreatetimeZone(learnerListing.currentdateTime)
                            adapterLearner!!.setoncLicklistener(this@HomeFragment)
                            adapterLearner!!.setonPayClickListener(this@HomeFragment)
                            recyclerPersons!!.adapter = adapterLearner
                        } else {
                            btCreateBooking?.visibility = View.VISIBLE
                            nodata!!.visibility = View.VISIBLE
                            recyclerPersons!!.visibility = View.INVISIBLE
                        }
                    } else {
                        btCreateBooking?.visibility = View.VISIBLE
                        nodata!!.visibility = View.VISIBLE
                        recyclerPersons!!.visibility = View.INVISIBLE
                    }
                }


            }
        }
    }

    private fun hitRatingGivenOrNotApi() {
        val LearnerId: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            usersharedPrefernce!!.getlearnerid().toString()
        )
        luggaAPI!!.checkRatingGivenOrNot(LearnerId)
            .enqueue(object : Callback<AllLanguageResposne?> {
                override fun onResponse(
                    call: Call<AllLanguageResposne?>,
                    response: Response<AllLanguageResposne?>
                ) {
                    if (response != null && response.body() != null && response.body()!!.success == 200) {
                        if (response.body()!!.body!!.size != 0) {
                            if (response.body()!!.body!!.get(0).rating != null) {
                                if (response.body()!!.body!!.get(0).rating.equals(
                                        "0",
                                        ignoreCase = true
                                    )
                                ) {
                                    shotRatingDialog()
                                } else {
                                }
                            }
                        }
                    } else {
                    }
                }

                override fun onFailure(call: Call<AllLanguageResposne?>, t: Throwable) {}
            })
    }

    private fun shotRatingDialog() {
        dialog = Dialog(requireActivity())
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_rating)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        val ratingBar = dialog!!.findViewById<RatingBar>(R.id.ratingBar)
        val tvSubmit = dialog!!.findViewById<TextView>(R.id.tvSubmit)
        val etComment = dialog!!.findViewById<EditText>(R.id.etComment)
        tvSubmit.setOnClickListener(View.OnClickListener {
            if (givenRating == 0f) {
                Toast.makeText(activity, "Please give Rating", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (etComment.text.toString().isEmpty()) {
                Toast.makeText(activity, "Please write comment", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            hitRatingGivenApi(etComment.text.toString().trim { it <= ' ' })
        })
        ratingBar.onRatingBarChangeListener = this
        dialog!!.show()
    }

    private fun hitRatingGivenApi(comment: String) {
        val LearnerId: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            usersharedPrefernce!!.getlearnerid().toString()
        )
        val Rating: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), givenRating.toString())
        val Comment: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), comment)
        luggaAPI!!.giveRating(LearnerId, Rating, Comment)
            .enqueue(object : Callback<AllLanguageResposne?> {
                override fun onResponse(
                    call: Call<AllLanguageResposne?>,
                    response: Response<AllLanguageResposne?>
                ) {
                    if (response != null && response.body() != null && response.body()!!.success == 200) {
                        dialog!!.dismiss()
                        Toast.makeText(activity, "" + response.body()!!.message, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                    }
                }

                override fun onFailure(call: Call<AllLanguageResposne?>, t: Throwable) {
                    Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            })
    }

    override fun onClick(v: View) {}
    override fun onRefresh() {
        if (usersharedPrefernce!!.getusertype() == 1) {
            getTeacherHomeListingSocket()
        } else if (usersharedPrefernce!!.getusertype() == 2) {
            //learnerHomepage
            getLearnerHomeListingSocket()
        }
    }

    override fun onRatingChanged(ratingBar: RatingBar, rating: Float, fromUser: Boolean) {
        givenRating = rating
    }

//    private val teacherHomePage: Unit
//         private get() {
//            teacherHomePageBodyItemArrayList = ArrayList()
//            val user_id: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), usersharedPrefernce!!.getuserid().toString())
//            luggaAPI?.getteacherhomepageresponse(user_id)?.enqueue(object : Callback<TeacherHomePageResponse?>, AdapterTeacher.onpayclick {
//                override fun onResponse(call: Call<TeacherHomePageResponse?>, response: Response<TeacherHomePageResponse?>) {
//                    progressBar!!.visibility = View.GONE
//                    if (response.body() != null) {
//                        if (response.isSuccessful && response.body()!!.success == 200) {
//                            teacherHomePageBodyItemArrayList = response.body()!!.body
//                            if (teacherHomePageBodyItemArrayList?.size!! > 0) {
//                                if (teacherHomePageBodyItemArrayList != null)
//                                    adapterTeacher = AdapterTeacher(context!!, teacherHomePageBodyItemArrayList!!)
//                                adapterTeacher!!.setServerTime(response.body()!!.currentdateTime)
//                                for (i in teacherHomePageBodyItemArrayList!!.indices) {
//                                    status = teacherHomePageBodyItemArrayList!!.get(i).status
//                                    val value = status.toString()
//                                    if (value.equals("5", ignoreCase = true)) {
//                                        counter += 1
//                                    }
//                                }
//                                val intent = Intent()
//                                intent.action = "update"
//                                intent.putExtra("count", counter)
//                                adapterTeacher!!.setonPayClickListener(this)
//                                recyclerPersons!!.adapter = adapterTeacher
//                            } else {
//                                nodata!!.visibility = View.VISIBLE
//                                recyclerPersons!!.visibility = View.INVISIBLE
//                            }
//                        } else {
//                            Toast.makeText(context, "error" + response.body()!!.message, Toast.LENGTH_SHORT).show()
//                        }
//                    }
//                }
//
//                override fun onFailure(call: Call<TeacherHomePageResponse?>, t: Throwable) {
//                    Toast.makeText(context, t.localizedMessage, Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onPayClickClicklistenerteacher(position: Int, learnerHomePageBodyItemArrayList: ArrayList<TeacherHomePageBodyItem>?, i: Int, timerModel: TimerModel?) {
//                    val intent = Intent(context, NewChatActivity::class.java)
//                    intent.putExtra("learnerlist1", learnerHomePageBodyItemArrayList)
//                    intent.putExtra("position1", position)
//                    intent.putExtra("iscomingfrom", "2")
//                    if (timerModel != null) {
//                        val mills = TimeUnit.MINUTES.toMillis(timerModel.min) + TimeUnit.SECONDS.toMillis(timerModel.sec)
//                        intent.putExtra("mills", mills)
//                    }
//                    intent.putExtra("status", learnerHomePageBodyItemArrayList!![position].status)
//                    startActivity(intent)
//                }
//
//
//                override fun hitCompletedBookingApi(position: Int, status: String?, booking_id: Int) {
//                    val BookingId: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), booking_id.toString())
//                    val status1: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), status!!)
//                    luggaAPI!!.updateBookingStatus(BookingId, status1).enqueue(object : Callback<BookingStatusResponse> {
//                        override fun onResponse(call: Call<BookingStatusResponse>, response: Response<BookingStatusResponse>) {
//                            if (response.isSuccessful && response.body()!!.success == 200) {
//                                if (usersharedPrefernce!!.getusertype() == 1) {
//                                   getTeacherHomeListingSocket()
//                                } else if (usersharedPrefernce!!.getusertype() == 2) {
//                                    getLearnerHomeListingSocket()
//                                    //learnerHomepage
//                                    shotRatingDialog()
//                                }
//                            } else {
//                                Toast.makeText(activity, response.body()!!.message, Toast.LENGTH_SHORT).show()
//                            }
//                        }
//
//                        override fun onFailure(call: Call<BookingStatusResponse>, t: Throwable) {
//                            Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
//                        }
//                    })
//                }
//            })
//        };
//    private val learnerHomepage: Unit
//        private get() {
//            val learner_id: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), usersharedPrefernce!!.getuserid().toString())
//            learnerHomePageBodyItemArrayList = ArrayList()
//            luggaAPI!!.getlearnerhomepage(learner_id).enqueue(object : Callback<LearnerHomePageResponse?> {
//                override fun onResponse(call: Call<LearnerHomePageResponse?>, response: Response<LearnerHomePageResponse?>) {
//                    progressBar!!.visibility = View.GONE
//                    if (response.body() != null) {
//                        if (response.isSuccessful && response.body()!!.success == 200) {
//                            swipeRefresh!!.isRefreshing = false
//                            learnerHomePageBodyItemArrayList = response.body()!!.body
//                            if (learnerHomePageBodyItemArrayList!!.size > 0) {
//                                for (i in learnerHomePageBodyItemArrayList!!.indices) {
//                                    stats = learnerHomePageBodyItemArrayList!!.get(i).bookingStatus.toString()
//                                    val value = stats.toString()
//                                    if (value.equals("5", ignoreCase = true)) {
//                                        counter += 1
//                                    }
//                                }
//                                val intent = Intent()
//                                intent.action = "update"
//                                intent.putExtra("count", counter)
//                                if (activity != null) {
//                                    activity!!.sendBroadcast(intent)
//                                }
//                                adapterLearner = AdapterLearner(context, learnerHomePageBodyItemArrayList)
//                                adapterLearner!!.setCreatetimeZone(response.body()!!.currentdateTime)
//                                adapterLearner!!.setoncLicklistener(this@HomeFragment)
//                                adapterLearner!!.setonPayClickListener(this@HomeFragment)
//                                recyclerPersons!!.adapter = adapterLearner
//                            } else {
//                                nodata!!.visibility = View.VISIBLE
//                                recyclerPersons!!.visibility = View.INVISIBLE
//                            }
//
//                        } else {
//                            swipeRefresh!!.isRefreshing = false
//                            Toast.makeText(context, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
//                        }
//                    }
//                }
//
//                override fun onFailure(call: Call<LearnerHomePageResponse?>, t: Throwable) {
//                    Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show();
//                }
//            })
//        }

    override fun ontimerstart(position: Int, starttime: String, endtime: String) {
        currentime = System.currentTimeMillis()
    }

    override fun onPayClickClicklistener(
        position: Int,
        learnerHomePageBodyItemArrayList: ArrayList<LearnerHomePageBodyItem>,
        i: Int,
        timerModel: TimerModel?
    ) {
        if (i == 1) {
            val intent = Intent(context, ConfirmDetailActivity::class.java)
            intent.putExtra("learnerlist", learnerHomePageBodyItemArrayList)
            intent.putExtra("position", position)
            startActivity(intent)
        } else {
            val intent = Intent(context, NewChatActivity::class.java)
            intent.putExtra("learnerlist1", learnerHomePageBodyItemArrayList)
            intent.putExtra("position1", position)
            intent.putExtra("iscomingfrom", "1")
            if (timerModel != null) {
                val mills =
                    TimeUnit.MINUTES.toMillis(timerModel.min) + TimeUnit.SECONDS.toMillis(timerModel.sec)
                intent.putExtra("mills", mills)
            }
            intent.putExtra("status", learnerHomePageBodyItemArrayList[position].bookingStatus)
            startActivity(intent)
        }
    }


    override fun onPayClick(
        dtStart: String,
        sessionCloseTime: String,
        position: Int,
        learnerHomePageBodyItemArrayList: ArrayList<LearnerHomePageBodyItem>
    ) {
        val intent = Intent(context, ConfirmDetailActivity::class.java)
        intent.putExtra("learnerlist", learnerHomePageBodyItemArrayList)
        intent.putExtra("position", position)
        startActivity(intent)
    }

    override fun onRejectRequest(position: Int, status: String?, booking_id: Int, learnerId: Int?) {
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            jsonObject.put("bookingID", booking_id)
            jsonObject.put("bookingStatus", status)
            socket?.emit("teacherBookingUpdate", jsonObject)

            getReturn(booking_id.toString())
        }
    }

    override fun onPayClickClicklistenerteacher(
        position: Int,
        learnerHomePageBodyItemArrayList: ArrayList<TeacherHomePageBodyItem>?,
        i: Int,
        timerModel: TimerModel?
    ) {
        val intent = Intent(context, NewChatActivity::class.java)
        intent.putExtra("learnerlist1", learnerHomePageBodyItemArrayList)
        intent.putExtra("position1", position)
        intent.putExtra("iscomingfrom", "2")
        if (timerModel != null) {
            val mills =
                TimeUnit.MINUTES.toMillis(timerModel.min) + TimeUnit.SECONDS.toMillis(timerModel.sec)
            intent.putExtra("mills", mills)
        }
        intent.putExtra("status", learnerHomePageBodyItemArrayList!![position].status)
        startActivity(intent)
    }

    override fun onAcceptRequest(position: Int, status: String?, booking_id: Int, learnerId: Int?) {
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            jsonObject.put("bookingID", booking_id)
            jsonObject.put("bookingStatus", status)
            socket?.emit("teacherBookingUpdate", jsonObject)

            getReturn(booking_id.toString())
        }
    }

    private fun getReturn(bookingID: String?) {
        if (socket!!.connected()) {
            socket!!.on("returnTeacherBookingUpdate" + bookingID) { args ->
                requireActivity().runOnUiThread {
                    getTeacherHomeListingSocket()
                }

            }
        }
    }

    override fun hitCompletedBookingApi(position: Int, status: String?, booking_id: Int) {
        val BookingId: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), booking_id.toString())
        val status1: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), status!!)
        luggaAPI!!.updateBookingStatus(BookingId, status1)
            .enqueue(object : Callback<BookingStatusResponse> {
                override fun onResponse(
                    call: Call<BookingStatusResponse>,
                    response: Response<BookingStatusResponse>
                ) {
                    if (response.isSuccessful && response.body()!!.success == 200) {
                        if (usersharedPrefernce!!.getusertype() == 1) {
                            getTeacherHomeListingSocket()
                        } else if (usersharedPrefernce!!.getusertype() == 2) {
                            //  learnerHomepage
                            getLearnerHomeListingSocket()
                            shotRatingDialog()
                        }
                    } else {
                        Toast.makeText(activity, response.body()!!.message, Toast.LENGTH_SHORT)
                            .show()
                    }
                }

                override fun onFailure(call: Call<BookingStatusResponse>, t: Throwable) {
                    Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                }
            })
    }

}