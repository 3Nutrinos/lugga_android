package com.Lugga.lugga.fragments

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.Lugga.lugga.R
import com.Lugga.lugga.utils.ApiClient
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target


class ImageLoadFragment : Fragment() {
    var ivImage: ImageView? = null
    var ivBack: ImageView? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.frag_image_load, container, false)
        ivImage = view.findViewById(R.id.ivImage)
        ivBack = view.findViewById(R.id.ivBack)
        val imageUrl = arguments!!.getString("imageUrl")

        Glide.with(activity!!)
                .load(ApiClient.IMAGE_URL + imageUrl)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                })
                .into(ivImage!!)

//
//        Picasso.get().load(ApiClient.IMAGE_URL + imageUrl).rotate(90f)
//                .into(ivImage!!)
        ivBack!!.setOnClickListener(View.OnClickListener { fragmentManager!!.popBackStack() })
        return view


    }

}