package com.Lugga.lugga.interfaces

import android.view.View

interface onClickImageChooserInterface {
    fun onClickImageChooser(imagepath: String?, view: View?)
}