package com.Lugga.lugga.interfaces;


import com.Lugga.lugga.model.AppUpdateModel;
import com.Lugga.lugga.model.CallingDataModel;
import com.Lugga.lugga.model.DataResponse;
import com.Lugga.lugga.model.Response;
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne;
import com.Lugga.lugga.model.amountchange.AmountChangeResponse;
import com.Lugga.lugga.model.bookingStatus.BookingStatusResponse;
import com.Lugga.lugga.model.createbooking.CreateBookingResponse;
import com.Lugga.lugga.model.learnerhistory.LearnerHistoryResponse;
import com.Lugga.lugga.model.learnerhomepage.LearnerHomePageResponse;
import com.Lugga.lugga.model.learnerprofile.LearnerprofileResponse;
import com.Lugga.lugga.model.learnerupdateprofile.LearnerUpdateResponse;
import com.Lugga.lugga.model.passwordchange.PasswordChangeResponse;
import com.Lugga.lugga.model.profilechange.ProfileChangeResponse;
import com.Lugga.lugga.model.teacherdetails.TeacherDetailsResponse;
import com.Lugga.lugga.model.teacherhomepage.TeacherHomePageResponse;
import com.Lugga.lugga.model.teacheronlinestatus.TeacherOnlineStatusResponse;
import com.Lugga.lugga.model.teacherprofile.TeacherProfileResponse;
import com.Lugga.lugga.model.teacherupdateprofile.TeacherUpdateResponse;
import com.Lugga.lugga.model.usersnotification.UsersNotificationResponse;
import com.Lugga.lugga.views.PaymentModel;
import com.Lugga.lugga.views.StripeModelClass;
import com.stripe.android.model.StripeModel;

import java.util.ArrayList;

import okhttp3.MultipartBody;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface LuggaAPI {

    @Multipart
    @POST("signup")
    Call<ResponseBody> signup(@Part("other_language") RequestBody otherLanguage, @Part("available_days") RequestBody days, @Part("name") RequestBody name, @Part("email") RequestBody email, @Part("mobile_no")
            RequestBody mobile_no, @Part("password") RequestBody password, @Part("user_type") RequestBody user_type,
                              @Part("language") RequestBody language, @Part("device_type") RequestBody device_type,
                              @Part("device_token") RequestBody device_token, @Part("experience") RequestBody
                                      experience, @Part("amount") RequestBody amount, @Part("time") RequestBody time,
                              @Part("stripe_publishable_key") RequestBody stripe_publishable_key,
                              @Part("stripe_user_id") RequestBody stripe_user_id);

    @Multipart
    @POST("signup")
    Call<Response> signup2(@Part("learner_description") RequestBody description, @Part("name") RequestBody name, @Part("email") RequestBody email, @Part("mobile_no")
            RequestBody mobile_no, @Part("password") RequestBody password, @Part("user_type")
                                   RequestBody user_type, @Part("language") RequestBody language, @Part("device_type")
                                   RequestBody device_type, @Part("device_token") RequestBody device_token);

    @Multipart
    @POST("login")
    Call<Response> login(@Part("email") RequestBody email, @Part("password") RequestBody password,
                         @Part("device_type") RequestBody deviceType, @Part("device_token") RequestBody deviceToken);


    @Multipart
    @POST("teacher_profile_deatil")
    Call<TeacherProfileResponse> getteacherprofile(@Part("user_id") RequestBody user_id);


    @Multipart
    @POST("learner_profile_deatil")
    Call<LearnerprofileResponse> getlearnerprofile(@Part("user_id") RequestBody user_id);


    @Multipart
    @POST("createBooking")
    Call<CreateBookingResponse> getcreatebooking(@Part("learnerId") RequestBody id, @Part("learnerLanguage")
            RequestBody language, @Part("sessionTime") RequestBody time, @Part("level") RequestBody level, @Part
                                                         ("startTime") RequestBody startTime, @Part("endTime") RequestBody endTime, @Part("teacherId")
                                                         RequestBody teacherid, @Part("bookingType") RequestBody bookingtype);


    @Multipart
    @POST("change_password")
    Call<PasswordChangeResponse> passwordchange(@Part("user_id") RequestBody user_id, @Part("password")
            RequestBody password, @Part("new_password") RequestBody new_password, @Part("confirm_password")
                                                        RequestBody confirm_password);

    @Multipart
    @POST("update_learner_profile")
    Call<LearnerUpdateResponse> getlearnerupdateprofile(@Part("learner_description") RequestBody earningGoals,@Part("user_id") RequestBody userid, @Part("mobile_no")
            RequestBody mobileno, @Part("language") RequestBody language, @Part("name") RequestBody name);

    @Multipart
    @POST("update_teacher_profile")
    Call<TeacherUpdateResponse> getteacherupdateprofile(@Part("teacher_available_days") RequestBody teacherAvaiLableDays, @Part("other_language") RequestBody otherlanguage, @Part("user_id") RequestBody user_id, @Part("experience")
            RequestBody experience, @Part("name") RequestBody name, @Part("mobile_no")
                                                                RequestBody mobile_no, @Part("language") RequestBody language, @Part("level")
                                                                RequestBody level, @Part("amount") RequestBody amount, @Part("upload_image") RequestBody upload_image);

    @Multipart
    @POST("profile_image")
    Call<ProfileChangeResponse> getprofilechange(@Part("user_id") RequestBody user_id, @Part
            MultipartBody.Part profileImage);

    @Multipart
    @POST("teacher_online_status")
    Call<TeacherOnlineStatusResponse> getteacheronlinestatus(@Part("user_id") RequestBody user_id, @Part
            ("is_online") RequestBody is_online);

    @Multipart
    @POST("learnerHomepage")
    Call<LearnerHomePageResponse> getlearnerhomepage(@Part("learner_id") RequestBody learner_id);

    @Multipart
    @POST("teacherDetails")
    Call<TeacherDetailsResponse> getteacherdetails(@Part("user_id") RequestBody user_id);

    @Multipart
    @POST("teacherHomepage")
    Call<TeacherHomePageResponse> getteacherhomepageresponse(@Part("user_id") RequestBody user_id);

    @Multipart
    @POST("change_amount")
    Call<AmountChangeResponse> getamountchange(@Part("user_id") RequestBody user_id, @Part("amount") RequestBody amount);

    @Multipart
    @POST("learnerHistory")
    Call<LearnerHistoryResponse> getlearnerhistory(@Part("learner_id") RequestBody learner_id);

    @Multipart
    @POST("usersNotification")
    Call<UsersNotificationResponse> getusersnotification(@Part("user_id") RequestBody user_id);

   /* @FormUrlEncoded
    @POST("teacherDetails")
    Call<TeacherDetailsResponse> getteacherDeatil(@Part("user_id") RequestBody user_id);*/

    @Multipart
    @POST("teacherBookingUpdate")
    Call<BookingStatusResponse> updateBookingStatus(@Part("bookingID") RequestBody bookingID,
                                                    @Part("bookingStatus") RequestBody bookingStatus);

    @Multipart
    @POST("Notification_Delete")
    Call<BookingStatusResponse> deleteNotification(@Part("notification_id") RequestBody notification_id
    );

    @Multipart
    @POST("create_Payment")
    Call<PaymentModel> makePayment(@Part("booking_id") RequestBody bookingID,
                                   @Part("amount") RequestBody amount,
                                   @Part("startTime") RequestBody startTime,
                                   @Part("endTime") RequestBody endTime,
                                   @Part("booking_type") RequestBody booking_type,
                                   @Part("payment_method_id") RequestBody id,
                                   @Part("teacher_amount") RequestBody teacher_amount,
                                   @Part("teacher_stripe_user_id") RequestBody teacher_stripe_user_id);

    @Multipart
    @POST("LogOut")
    Call<ResponseBody> onLogOut(@Part("user_id") RequestBody userId
    );


    @Multipart
    @POST("forgot_password")
    Call<BookingStatusResponse> onForgotPass(@Part("user_email") RequestBody email
    );


    @Multipart
    @POST("SendVideo_ForChat")
    Call<BookingStatusResponse> sendVideo(@Part("booking_id") RequestBody booking_id,
                                          @Part("sender_id") RequestBody sender_id,
                                          @Part("receiver_id") RequestBody receiver_id,
                                          @Part MultipartBody.Part message,
                                          @Part("message_type") RequestBody message_type);

    @Multipart
    @POST("Chat_SendMessage")
    Call<ResponseBody> onSendAudio(@Part("booking_id") RequestBody booking_id,
                                   @Part("sender_id") RequestBody sender_id,
                                   @Part("receiver_id") RequestBody receiver_id,
                                   @Part("message") RequestBody message,
                                   @Part("message_type") RequestBody message_type);

    @GET("All_Language")
    Call<AllLanguageResposne> getlanguages();


    @POST("update_version")
    Call<ArrayList<AppUpdateModel>> checkForAppUpdate();

    @Multipart
    @POST("rating_given_or_not")
    Call<AllLanguageResposne> checkRatingGivenOrNot(@Part("learner_id") RequestBody learner_id);


    @Multipart
    @POST("give_rating")
    Call<AllLanguageResposne> giveRating(@Part("learner_id") RequestBody learner_id,
                                         @Part("rating") RequestBody rating,
                                         @Part("comment") RequestBody comment);

    @FormUrlEncoded
    @POST("oauth/token")
    Call<StripeModelClass> getStripeResponse(@Field("code") String code,
                                             @Field("grant_type") String grant_type);


    @FormUrlEncoded
    @POST("v1/payment_methods")
    Call<StripeModelClass> PaymentMethod(@Field("type") String type,
                                         @Field("card[number]") String cardNo,
                                         @Field("card[exp_month]") String expMonth,
                                         @Field("card[exp_year]") String expYear,
                                         @Field("card[cvc]") String cvc);

    


    @POST("rtctoken")
    Call<CallingDataModel> generateAgoraToken(@Query("channelName") String channelName);
}

