package com.Lugga.lugga.newchatcall.adapter

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.net.Uri
import android.os.Handler
import android.os.Message
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.chatList.ChatListBodyItem
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.DateHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.joypixels.tools.Callback
import com.joypixels.tools.Client

import kotlinx.android.synthetic.main.item_new_audio_receiver.view.*
import kotlinx.android.synthetic.main.item_new_audio_sender.view.*
import kotlinx.android.synthetic.main.item_new_audio_sender.view.ivPlayPause
import kotlinx.android.synthetic.main.item_new_audio_sender.view.progressBar
import kotlinx.android.synthetic.main.item_new_audio_sender.view.sbProgress
import kotlinx.android.synthetic.main.item_new_audio_sender.view.tvIndex
import kotlinx.android.synthetic.main.item_new_image_receiver.view.*
import kotlinx.android.synthetic.main.item_new_image_sender.view.*
import kotlinx.android.synthetic.main.item_new_text_receiver.view.*
import kotlinx.android.synthetic.main.item_new_text_sender.view.*
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class NewChatAdapter(var activity: Activity, var listener: Listener, var context: Context, val list: ArrayList<ChatListBodyItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var timeFormatter: SimpleDateFormat? = null
    var sharedPreferences: UsersharedPrefernce? = null
    private var client: Client? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        sharedPreferences = UsersharedPrefernce.getInstance()
        var view: View? = null
        client = Client(context)
        client!!.isShortcodes = true
        when (viewType) {
            1 -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_text_sender, parent, false)
                return HolderSenderText(view)
            }
            2 -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_image_sender, parent, false)
                return HolderSenderImage(view)
            }
            3 -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_audio_sender, parent, false)
                return HolderSenderAudio(view)
            }
            4 -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_text_receiver, parent, false)
                return HolderReceiverText(view)
            }
            5 -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_image_receiver, parent, false)
                return HolderReceiverImage(view)
            }
            else -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_audio_receiver, parent, false)
                return HolderReceiverAudio(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model: ChatListBodyItem = list.get(position)

        when (holder.itemViewType) {
            1 -> {
                 holder.itemView.tvSenderTime.text = DateHelper.getTime(model.createdAt)
                client!!.toImage(model.message, 60, object : Callback {
                    override fun onFailure(e: IOException?) {
                        holder.itemView.tvSenderTextMessage.text = e?.message
                    }

                    override fun onSuccess(ssb: SpannableStringBuilder?) {
                        holder.itemView.tvSenderTextMessage.text = ssb
                    }

                })


                holder.itemView.tvSenderDate.text = DateHelper.getDate(model.createdAt)

                if (sharedPreferences?.getprofileimage() != null) {
                    Glide.with(context).load(ApiClient.IMAGE_URL + sharedPreferences?.getprofileimage())
                            .placeholder(R.drawable.person_girl)
                            .into(holder.itemView.ivSenderProfileText)
                }
            }
            2 -> {
                if (sharedPreferences?.getprofileimage() != null) {
                    Glide.with(context).load(ApiClient.IMAGE_URL + sharedPreferences?.getprofileimage())
                            .placeholder(R.drawable.person_girl)
                            .into(holder.itemView.ivSenderProfileImg)
                }


//                Picasso.get().load(ApiClient.IMAGE_URL + model.message).into(holder.itemView.ivSenderNewImage, object : Callback {
//                    override fun onSuccess() {
//                        holder.itemView.tvLoadingImage?.visibility = View.GONE
//                    }
//
//                    override fun onError(e: Exception?) {
//                        holder.itemView.tvLoadingImage?.visibility = View.GONE
//
//                    }
//                })

                Glide.with(context)
                        .load(ApiClient.IMAGE_URL + model.message)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                holder.itemView.tvLoadingImage?.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                holder.itemView.tvLoadingImage?.visibility = View.GONE
                                return false
                            }

                        })
                        .into(holder.itemView.ivSenderNewImage)

                holder.itemView.setOnClickListener(View.OnClickListener {
                    listener.onClickImage(model.message)
                })
            }
            3 -> {
                if (sharedPreferences?.getprofileimage() != null) {
                    Glide.with(context).load(ApiClient.IMAGE_URL + sharedPreferences?.getprofileimage())
                            .placeholder(R.drawable.person_girl)
                            .into(holder.itemView.ivSenderProfileAudio)
                }
                holder.itemView.tvIndex.text = "Audio"
            }
            4 -> {

                if (sharedPreferences?.receiverProfileimage  != null) {
                    Glide.with(context).load(ApiClient.IMAGE_URL + sharedPreferences?.receiverProfileimage )
                            .placeholder(R.drawable.person_girl)
                            .into(holder.itemView.ivReceiverProfileNew)
                }

                //  holder.itemView.tvReceiverTime.text = DateHelper.getTime(model.createdAt)
                holder.itemView.tvReceiverDate.text = DateHelper.getDate(model.createdAt)
                client!!.toImage(model.message, 60, object : Callback {
                    override fun onFailure(e: IOException?) {
                        holder.itemView.tvReceiverTextMessage.text = e?.message

                    }

                    override fun onSuccess(ssb: SpannableStringBuilder?) {
                        holder.itemView.tvReceiverTextMessage.text = ssb
                    }

                })




            }
            5 -> {
                if (sharedPreferences?.receiverProfileimage != null) {
                    Glide.with(context).load(ApiClient.IMAGE_URL + sharedPreferences?.receiverProfileimage )
                            .placeholder(R.drawable.person_girl)
                            .into(holder.itemView.ivReceiverProfilePic)
                }

//                Picasso.get().load(ApiClient.IMAGE_URL + model.message).rotate(90f).into(holder.itemView.ivReceiverNewImage, object : Callback {
//                    override fun onSuccess() {
//                        holder.itemView.tvReceiverLoadingImage?.visibility = View.GONE
//                    }
//
//                    override fun onError(e: Exception?) {
//                        holder.itemView.tvReceiverLoadingImage?.visibility = View.GONE
//                    }
//
//                })

                Glide.with(context)
                        .load(ApiClient.IMAGE_URL + model.message)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                holder.itemView.tvReceiverLoadingImage?.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                holder.itemView.tvReceiverLoadingImage?.visibility = View.GONE
                                return false
                            }

                        })
                        .into(holder.itemView.ivReceiverNewImage)


                holder.itemView.setOnClickListener(View.OnClickListener {
                    listener.onClickImage(model.message)
                })
            }
            6 -> {
                if (sharedPreferences?.receiverProfileimage != null) {
                    Glide.with(context).load(ApiClient.IMAGE_URL + sharedPreferences!!.receiverProfileimage)
                            .placeholder(R.drawable.person_girl)
                            .into(holder.itemView.ivSenderProfileAudioNew)
                }
                holder.itemView.tvIndex.text = "Audio"
            }
        }


    }


    override fun getItemViewType(position: Int): Int {
        if (sharedPreferences?.getusertype() == 1) {                   //teacher           >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (sharedPreferences!!.getteacherid() == list.get(position).receiverId) {
                if (list.get(position).messageType == 1) {                            // receiverText
                    return 4
                }
                if (list.get(position).messageType == 3) {                            // receiverImage
                    return 5
                }
                if (list.get(position).messageType == 2) {                            // receiverAudio
                    return 6
                }

            } else {
                if (list.get(position).messageType == 1) {                            // senderText
                    return 1
                }
                if (list.get(position).messageType == 3) {                            // senderImage
                    return 2
                }
                if (list.get(position).messageType == 2) {                           // senderAudio
                    return 3
                }
            }
        } else if (sharedPreferences?.getusertype() == 2) {                                                                          //teacher>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (sharedPreferences!!.getlearnerid() == list.get(position).receiverId) {
                if (list.get(position).messageType == 1) {                            // receiverText
                    return 4
                }
                if (list.get(position).messageType == 3) {                            // receiverImage
                    return 5
                }
                if (list.get(position).messageType == 2) {                            // receiverAudio
                    return 6
                }

            } else {
                if (list.get(position).messageType == 1) {                            // senderText
                    return 1
                }
                if (list.get(position).messageType == 3) {                            // senderImage
                    return 2
                }
                if (list.get(position).messageType == 2) {                           // senderAudio
                    return 3
                }
            }
        }

        return 0
    }

    interface Listener {
        fun onClickImage(message: String?)
    }

    class HolderSenderText(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    class HolderSenderImage(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    inner class HolderSenderAudio(itemView: View) : RecyclerView.ViewHolder(itemView), OnSeekBarChangeListener, Handler.Callback, View.OnClickListener {

        private var contextAudio: Context? = null
        private var playingPosition = 0
        private var mediaPlayer: MediaPlayer? = null
        private var playerholder: HolderSenderAudio? = null
        private val MSG_UPDATE_SEEK_BAR = 1000
        private var uiUpdateHandler: Handler? = null

        init {
            itemView.sbProgress.setOnSeekBarChangeListener(this)
            contextAudio = context!!
            uiUpdateHandler = Handler()
            playingPosition = -1
            itemView.ivPlayPause.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (getAdapterPosition() == playingPosition) {
                if (mediaPlayer!!.isPlaying) {
                    mediaPlayer?.pause()
                } else {
                    mediaPlayer?.start()
                }
            } else {
                playingPosition = getAdapterPosition()
                if (mediaPlayer != null) {
                    if (null != playerholder) {
                        updateNonPlayingView(playerholder!!)
                    }
                    mediaPlayer?.release()
                }
                playerholder = this
                startMediaPlayer(list.get(playingPosition).message)
            }
        }

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                mediaPlayer?.seekTo(progress)
            }

        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }


        fun startMediaPlayer(audioResId: String) {
            try {
                mediaPlayer = MediaPlayer()
                mediaPlayer?.setDataSource(contextAudio!!, Uri.parse(ApiClient.IMAGE_URL + "" + list.get(playingPosition).message))
                mediaPlayer?.setAudioStreamType(AudioManager.STREAM_MUSIC)

                mediaPlayer?.prepare()
                playerholder?.itemView?.sbProgress?.max = mediaPlayer!!.duration.div(1000)
                activity.runOnUiThread(object : Runnable {
                    override fun run() {

                        if (mediaPlayer?.duration != mediaPlayer?.currentPosition) {
                            playerholder?.itemView?.progressBar?.visibility = View.VISIBLE
                            playerholder?.itemView?.sbProgress?.setProgress(mediaPlayer?.currentPosition?.div(1000)!!)
                            if (mediaPlayer!!.isPlaying()) {
                                playerholder?.itemView?.progressBar?.visibility = View.GONE
                                playerholder?.itemView?.ivPlayPause?.setImageResource(R.drawable.ic_baseline_pause_24)

                            } else {
                                playerholder?.itemView?.ivPlayPause?.setImageResource(R.drawable.ic_baseline_play_arrow_24)
                            }
                            uiUpdateHandler?.postDelayed(this, 1000);
                        }

                    }

                })



                mediaPlayer?.start()

            } catch (e: IOException) {
                e.printStackTrace()
            }


            mediaPlayer?.setOnCompletionListener(OnCompletionListener { releaseMediaPlayer() })

            playerholder?.itemView?.sbProgress?.setOnSeekBarChangeListener(this)


        }

        private fun updateNonPlayingView(holder: HolderSenderAudio) {
            if (holder === playerholder) {
                uiUpdateHandler!!.removeMessages(MSG_UPDATE_SEEK_BAR)
            }
            playerholder?.itemView?.sbProgress?.setEnabled(false)
            playerholder?.itemView?.sbProgress?.setProgress(0)
            playerholder?.itemView?.ivPlayPause?.setImageResource(R.drawable.ic_baseline_play_arrow_24)
        }

        fun stopPlayer() {
            if (null != mediaPlayer) {
                releaseMediaPlayer()
            }
        }

        private fun releaseMediaPlayer() {
            if (null != playerholder) {
                updateNonPlayingView(playerholder!!)
            }
            mediaPlayer?.reset()
            mediaPlayer = null
            playingPosition = -1
        }

        override fun handleMessage(msg: Message): Boolean {

            when (msg.what) {

                MSG_UPDATE_SEEK_BAR -> {
                    playerholder?.itemView?.sbProgress?.setOnSeekBarChangeListener(this)
                    playerholder?.itemView?.sbProgress?.setProgress(mediaPlayer!!.currentPosition / 1000)
                    uiUpdateHandler?.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 1000)
                    return true
                }
            }
            return false
        }

    }

    class HolderReceiverText(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    class HolderReceiverImage(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    inner class HolderReceiverAudio(itemView: View) : RecyclerView.ViewHolder(itemView), OnSeekBarChangeListener, Handler.Callback, View.OnClickListener {

        private var isPrepared = false
        private var contextAudio: Context? = null
        private var playingPosition = 0
        private var mediaPlayer: MediaPlayer? = null
        private var playerholder: HolderReceiverAudio? = null
        private val MSG_UPDATE_SEEK_BAR = 1000
        private var uiUpdateHandler: Handler? = null

        init {
            itemView.sbProgress.setOnSeekBarChangeListener(this)
            var timeFormatter = SimpleDateFormat("m:ss", Locale.getDefault())
            contextAudio = context!!
            uiUpdateHandler = Handler()

            playingPosition = -1
            itemView.ivPlayPause.setOnClickListener(this)


        }

        override fun onClick(v: View?) {
            if (getAdapterPosition() == playingPosition) {
                if (mediaPlayer!!.isPlaying) {
                    mediaPlayer?.pause()
                } else {
                    mediaPlayer?.start()
                }
            } else {
                playingPosition = getAdapterPosition()
                if (mediaPlayer != null) {
                    if (null != playerholder) {
                        updateNonPlayingView(playerholder!!)
                    }
                    mediaPlayer?.release()
                }
                playerholder = this
                startMediaPlayer(list.get(playingPosition).message)
            }

        }

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                mediaPlayer?.seekTo(progress)
            }

        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

        fun getAudioTime(time: Long): String? {
            var time = time
            time *= 1000
            timeFormatter?.setTimeZone(TimeZone.getTimeZone("UTC"))
            return timeFormatter?.format(Date(time))
        }

        fun startMediaPlayer(audioResId: String) {

            try {
                mediaPlayer = MediaPlayer()
                mediaPlayer?.setDataSource(contextAudio!!, Uri.parse(ApiClient.IMAGE_URL + "" + list.get(playingPosition).message))
                mediaPlayer?.setAudioStreamType(AudioManager.STREAM_MUSIC)

                mediaPlayer?.prepare()
                playerholder?.itemView?.sbProgress?.max = mediaPlayer!!.duration.div(1000)
                activity.runOnUiThread(object : Runnable {
                    override fun run() {
                        if (mediaPlayer?.duration != mediaPlayer?.currentPosition) {
                            playerholder?.itemView?.progressBar?.visibility = View.VISIBLE
                            playerholder?.itemView?.sbProgress?.setProgress(mediaPlayer?.currentPosition?.div(1000)!!)
                            if (mediaPlayer!!.isPlaying()) {
                                playerholder?.itemView?.progressBar?.visibility = View.GONE
                                playerholder?.itemView?.ivPlayPause?.setImageResource(R.drawable.ic_baseline_pause_24)

                            } else {
                                playerholder?.itemView?.ivPlayPause?.setImageResource(R.drawable.ic_baseline_play_arrow_24)
                            }
                            uiUpdateHandler?.postDelayed(this, 1000);

                        }

                    }

                })



                mediaPlayer?.start()

            } catch (e: IOException) {
                e.printStackTrace()
            }


            mediaPlayer?.setOnCompletionListener(OnCompletionListener { releaseMediaPlayer() })

            playerholder?.itemView?.sbProgress?.setOnSeekBarChangeListener(this)


        }

        private fun updateNonPlayingView(holder: HolderReceiverAudio) {
            if (holder === playerholder) {
                uiUpdateHandler!!.removeMessages(MSG_UPDATE_SEEK_BAR)
            }
            playerholder?.itemView?.sbProgress?.setEnabled(false)
            playerholder?.itemView?.sbProgress?.setProgress(0)
            playerholder?.itemView?.ivPlayPause?.setImageResource(R.drawable.ic_baseline_play_arrow_24)
        }

        fun stopPlayer() {
            if (null != mediaPlayer) {
                releaseMediaPlayer()
            }
        }

        private fun releaseMediaPlayer() {
            if (null != playerholder) {
                updateNonPlayingView(playerholder!!)
            }
            mediaPlayer?.reset()
            mediaPlayer = null
            playingPosition = -1
        }

        override fun handleMessage(msg: Message): Boolean {

            when (msg.what) {

                MSG_UPDATE_SEEK_BAR -> {
                    playerholder?.itemView?.sbProgress?.setOnSeekBarChangeListener(this)
                    playerholder?.itemView?.sbProgress?.setProgress(mediaPlayer!!.currentPosition / 1000)
                    uiUpdateHandler?.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 1000)
                    return true
                }
            }
            return false
        }
    }


}

