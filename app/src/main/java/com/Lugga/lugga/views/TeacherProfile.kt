package com.Lugga.lugga.views

import android.Manifest
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaRecorder.VideoSource
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.FeedBackActivity
import com.Lugga.lugga.HomeActivity
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.AllLanguageAdapter
import com.Lugga.lugga.adapters.DaysAdapter
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.DaysList
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne
import com.Lugga.lugga.model.amountchange.AmountChangeResponse
import com.Lugga.lugga.model.passwordchange.PasswordChangeResponse
import com.Lugga.lugga.model.profilechange.ProfileChangeResponse
import com.Lugga.lugga.model.teacherprofile.TeacherProfileResponse
import com.Lugga.lugga.model.teacherupdateprofile.TeacherUpdateResponse
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_new_log_in.*
import kotlinx.android.synthetic.main.select_days_dialog.*
import kotlinx.android.synthetic.main.teacher_profile.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.*

class TeacherProfile : AppCompatActivity(), View.OnClickListener {
    private var teacherAvailableDays: String? = ""
    private var daysModel: DaysList? = null
    private var selectedOtherLanguage: String? = ""
    private var daysList: ArrayList<DaysList>? = null
    private var commaSeperatedDays: String? = ""
    private var daysAdapter: DaysAdapter? = null
    private var otherlanguage: String? = null
    var changePassword: TextView? = null
    var tapTochangeNumber: TextView? = null
    var tapTochangeExperience: TextView? = null
    var tapTochangeName: TextView? = null
    var tapTochangeLevel: TextView? = null
    var tapTochangeLanguage: TextView? = null
    var tapTochangeDocument: TextView? = null
    var tapTochangeAmount: TextView? = null
    var teacherAmount: TextView? = null
    var teacherHeaderName: TextView? = null
    var teacherHeaderEmail: TextView? = null
    var teacherName: TextView? = null
    var teacherExperience: TextView? = null
    var teacherNumber: TextView? = null
    var teacherLanguage: TextView? = null
    var teacherOtherLanguage: TextView? = null
    var teacherLevel: TextView? = null
    var completedJob: TextView? = null
    var uncompletedJob: TextView? = null
    var rating: TextView? = null
    var teacherDocument: TextView? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var oldpassword: EditText? = null
    var newpassword: EditText? = null
    var confirmnewpassword: EditText? = null
    var mdialognumber: EditText? = null
    var mdialogname: EditText? = null
    var mdialoglevel: EditText? = null
    var mdialoglanguage: EditText? = null
    var mdialogexperience: EditText? = null
    var personGirlProfile: CircleImageView? = null
    var relativeLayoutProfile: RelativeLayout? = null
    var lowerlayout: LinearLayout? = null
    var llViewFeedBack: LinearLayout? = null
    var llDaysofWeek: LinearLayout? = null
    var selectedfile = " "
    var changeProfile: ImageView? = null
    var teacherProfileBackPress: ImageView? = null
    var luggaAPI: LuggaAPI? = null
    var filePath: FilePath? = null
    var imageFile: File? = null

    //Loader loader;
    var spinnerLanguage: Spinner? = null
    var spinnerOtherLanguage: Spinner? = null
    var arraySpinner: MutableList<String>? = null
    var adapter: ArrayAdapter<String>? = null
    private var allLanguageAdapter: AllLanguageAdapter? = null
    private var allOtherLanguageAdapter: AllLanguageAdapter? = null
    private var selectedLanguage: String? = null
    private var language: String? = null
    var body: ArrayList<AllLanguagModel>? = null
    private var spinnerTouched = false
    private var spinnerOtherTouched = false
    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.teacher_profile)
        spinnerLanguage = findViewById(R.id.spinnerLanguage)
        spinnerOtherLanguage = findViewById(R.id.spinnerOtherLanguage)
        llViewFeedBack = findViewById(R.id.llViewFeedBack)
        llDaysofWeek = findViewById(R.id.llDaysofWeek)
        // iniSpinner();
        body = ArrayList()
        init()
        onClick()
        teacherProfileDeatil
        getlanguages()
        getOtherlanguages()
        llViewFeedBack?.setOnClickListener(this)

        spinnerLanguage?.setOnTouchListener { _, _ ->
            spinnerTouched = true
            false
        }
        spinnerOtherLanguage?.setOnTouchListener { _, _ ->
            spinnerOtherTouched = true
            false
        }

        spinnerLanguage?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (spinnerTouched) {
                    selectedLanguage =
                        (allLanguageAdapter!!.getItem(position) as AllLanguagModel).language
                    teacherUpdateLangUpdate
                }
                spinnerTouched = false
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinnerOtherLanguage?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (spinnerOtherTouched) {
                    selectedOtherLanguage =
                        (allOtherLanguageAdapter!!.getItem(position) as AllLanguagModel).language
                    teacherUpdateLangUpdate
                }
                spinnerOtherTouched = false
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        llDaysofWeek!!.setOnClickListener {
            daysList = ArrayList()
            daysList?.clear()

            for (i in 0..6) {
                daysModel = DaysList()
                when (i) {
                    0 -> daysModel!!.day = "Monday"
                    1 -> daysModel!!.day = "Tuesday"
                    2 -> daysModel!!.day = "Wednesday"
                    3 -> daysModel!!.day = "Thursday"
                    4 -> daysModel!!.day = "Friday"
                    5 -> daysModel!!.day = "Saturday"
                    6 -> daysModel!!.day = "Sunday"
                }
                daysList!!.add(daysModel!!)

            }
            val dialog = Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(R.layout.select_days_dialog)

            daysAdapter = DaysAdapter(this, daysList)
            dialog.rvDays.adapter = daysAdapter


            dialog.show()

            dialog.tvDone.setOnClickListener {
                if (daysAdapter!!.getSelected()!!.size <= 0) {
                    Toast.makeText(this, "Select days", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                for (i in daysAdapter!!.getSelected()!!.indices) {
                    commaSeperatedDays =
                        daysAdapter!!.getSelected()!!.joinToString { it -> it.day.toString() }
                }

                tvDaysTeacher.text = commaSeperatedDays?.replace(" ", "")
                teacherAvailableDays = commaSeperatedDays?.replace(" ", "")
              //  Log.d("commaSeperatedDays", commaSeperatedDays)
                teacherUpdateLangUpdate
                dialog.dismiss()
            }
        }
    }

    private fun selectedLanguageee() {
        if (language != null && !language!!.isEmpty()) {
            teacherLanguage!!.text = "Selected Language : $language"
        }
        if (otherlanguage != null && !otherlanguage!!.isEmpty() && otherlanguage != "0") {
            teacherOtherLanguage!!.text = "Selected Other Language : $otherlanguage"
            rating!!.text =otherlanguage
        }
    }

    private fun iniSpinner() {

        arraySpinner = ArrayList()
        arraySpinner?.add("Hindi")
        arraySpinner?.add("English")
        arraySpinner?.add("French")
        adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, arraySpinner!!)
        spinnerLanguage!!.adapter = adapter

    }

    private fun getlanguages() {
        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages()
            .enqueue(object : Callback<AllLanguageResposne?> {
                override fun onResponse(
                    call: Call<AllLanguageResposne?>,
                    response: Response<AllLanguageResposne?>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        allLanguageAdapter =
                            AllLanguageAdapter(this@TeacherProfile, response.body()!!.body!!)
                        spinnerLanguage!!.adapter = allLanguageAdapter
                        allLanguageAdapter!!.notifyDataSetChanged()
                    } else {
                    }
                }

                override fun onFailure(call: Call<AllLanguageResposne?>, t: Throwable) {
                    Toast.makeText(this@TeacherProfile, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            })
    }//loader.isDismiss();//loader.isDismiss();// loader.isDismiss();

    // loader.isShowProgress();

    private fun getOtherlanguages() {
        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages()
            .enqueue(object : Callback<AllLanguageResposne?> {
                override fun onResponse(
                    call: Call<AllLanguageResposne?>,
                    response: Response<AllLanguageResposne?>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        allOtherLanguageAdapter =
                            AllLanguageAdapter(this@TeacherProfile, response.body()!!.body!!)
                        spinnerOtherLanguage!!.adapter = allOtherLanguageAdapter
                        allOtherLanguageAdapter!!.notifyDataSetChanged()
                    } else {
                    }
                }

                override fun onFailure(call: Call<AllLanguageResposne?>, t: Throwable) {
                    Toast.makeText(this@TeacherProfile, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            })
    }

    private val teacherUpdateLangUpdate: Unit
        private get() {
            // loader.isShowProgress();
            val user_id: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getuserid().toString()
            )
            val teacherAvaiLableDays: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherAvailableDays.toString()
            )
            val experience: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherExperience!!.text.toString()
            )
            val name: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherName!!.text.toString()
            )
            val mobile_no: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherNumber!!.text.toString()
            )
            val Language: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedLanguage!!)
            val OtherLanguage: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedOtherLanguage!!)
            val level: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherLevel!!.text.toString().trim { it <= ' ' })
            val amount: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getamount().toString()
            )
            val upload_image: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherDocument!!.text.toString().trim { it <= ' ' })

            luggaAPI!!.getteacherupdateprofile(
                teacherAvaiLableDays,
                OtherLanguage,
                user_id,
                experience,
                name,
                mobile_no,
                Language,
                level,
                amount,
                upload_image
            ).enqueue(object : Callback<TeacherUpdateResponse?> {
                override fun onResponse(
                    call: Call<TeacherUpdateResponse?>,
                    response: Response<TeacherUpdateResponse?>
                ) {
                    if (response.body() != null) {
                        if (response.isSuccessful && response.body()!!.success == 200) {
                            language = response.body()!!.body!![0].language
                            otherlanguage = response.body()!!.body!![0].other_language
                            selectedLanguageee()
                            // loader.isDismiss();
                            Toast.makeText(
                                this@TeacherProfile,
                                "Updated successfully",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            //loader.isDismiss();
                            Toast.makeText(
                                this@TeacherProfile,
                                "" + response.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(call: Call<TeacherUpdateResponse?>, t: Throwable) {
                    //loader.isDismiss();
                    Toast.makeText(this@TeacherProfile, t.localizedMessage, Toast.LENGTH_SHORT)
                        .show()
                }
            })
        }

    fun init() {
        // loader = new Loader(this);
        filePath = FilePath()
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        lowerlayout = findViewById(R.id.lower_layout)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        tapTochangeNumber = findViewById<View>(R.id.tapToChangeNumber) as TextView
        changePassword = findViewById<View>(R.id.tapToChangePassword) as TextView
        tapTochangeName = findViewById<View>(R.id.tapToChangeName) as TextView
        tapTochangeExperience = findViewById<View>(R.id.tapToChangeExperience) as TextView
        tapTochangeLevel = findViewById<View>(R.id.tapToChangeLevel) as TextView
        //   tapTochangeLanguage = (TextView) findViewById(R.id.tapToChangeLanguage);
        tapTochangeDocument = findViewById(R.id.tapToChangeDocument)
        tapTochangeAmount = findViewById(R.id.tapToChangeAmount)
        personGirlProfile = findViewById<View>(R.id.personGirlProfile) as CircleImageView
        relativeLayoutProfile = findViewById<View>(R.id.relativeLayoutContainer) as RelativeLayout
        changeProfile = findViewById<View>(R.id.changeProfile) as ImageView
        teacherHeaderName = findViewById(R.id.teacherHeaderName)
        teacherHeaderEmail = findViewById(R.id.teacherHeaderEmail)
        teacherName = findViewById(R.id.teacherName)
        teacherExperience = findViewById(R.id.teacherExperience)
        teacherLanguage = findViewById(R.id.teacherLanguage)
        teacherOtherLanguage = findViewById(R.id.teacherOtherLanguage)
        teacherNumber = findViewById(R.id.teacherNumber)
        teacherLevel = findViewById(R.id.teacherLevel)
        teacherDocument = findViewById<View>(R.id.changeDocument) as TextView
        teacherAmount = findViewById(R.id.teacherAmount)
        teacherProfileBackPress = findViewById(R.id.teacherProfileBackPress)
        uncompletedJob = findViewById(R.id.teacherUncompletedJob)
        completedJob = findViewById(R.id.teacherCompletedJob)
        rating = findViewById(R.id.teacherRating)
        val path = usersharedPrefernce?.getprofileimage()
        Log.d("path", "path: $path")
        /* if(path!=null && !path.equals("")){
            Glide.with(this)
                    .load(path)
                    .into(personGirlProfile);
        }*/
    }

    fun onClick() {
        changeProfile!!.setOnClickListener { requestStoragePermission() }
        personGirlProfile!!.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                personGirlProfile!!.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                personGirlProfile!!.systemUiVisibility = View.STATUS_BAR_HIDDEN
            }
        }
        tapTochangeNumber!!.setOnClickListener(View.OnClickListener { view ->
            val mDialog = Dialog(view.context)
            mDialog.setContentView(R.layout.change_number)
            mDialog.show()
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val changeNumberDismiss = mDialog.findViewById<ImageView>(R.id.changeNumberDismiss)
            val changeNumberSubmit = mDialog.findViewById<Button>(R.id.changeNumberSubmit)
            mdialognumber = mDialog.findViewById(R.id.mdialognumber)
            changeNumberDismiss.setOnClickListener { mDialog.dismiss() }
            changeNumberSubmit.setOnClickListener(View.OnClickListener {
                if (mdialognumber?.getText().toString().isEmpty()) {
                    Toast.makeText(this@TeacherProfile, "Enter number", Toast.LENGTH_SHORT).show()
                    return@OnClickListener
                }
                if (mdialognumber?.getText().toString().length < 7 || mdialognumber?.getText()
                        .toString().trim { it <= ' ' }.length > 12
                ) {
                    Toast.makeText(this@TeacherProfile, "Enter valid number", Toast.LENGTH_SHORT)
                        .show()
                    return@OnClickListener
                }
                usersharedPrefernce!!.setmobileno(mdialognumber?.getText().toString())
                mDialog.dismiss()
                teacherNumber!!.text = mdialognumber?.getText().toString()
                teacherUpdateProfile
            })
        })
        //        spinnerLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                if (spinnerLanguage.getSelectedItem().toString().isEmpty()) {
//                    //   learnerLanguage.setText("please select any language");
//                } else {
//                    usersharedPrefernce.setlanguage(spinnerLanguage.getSelectedItem().toString());
//                    getTeacherUpdateProfile();
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        changePassword!!.setOnClickListener { view ->
            val mDialog = Dialog(view.context)
            mDialog.setContentView(R.layout.change_password)
            mDialog.show()
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val changePasswordDismiss = mDialog.findViewById<ImageView>(R.id.changePasswordDismiss)
            oldpassword = mDialog.findViewById(R.id.oldPassword)
            newpassword = mDialog.findViewById(R.id.newPassword)
            confirmnewpassword = mDialog.findViewById(R.id.confirmNewPassword)
            val passwordsubmit = mDialog.findViewById<TextView>(R.id.passwordSubmitButton)
            passwordsubmit.setOnClickListener {
                if (newpassword?.getText().toString() != confirmnewpassword?.getText().toString()) {
                    confirmnewpassword?.setError("password is not same")
                } else {
                    passwordChange
                    mDialog.dismiss()
                }
            }
            changePasswordDismiss.setOnClickListener { mDialog.dismiss() }
        }
        tapTochangeName!!.setOnClickListener(View.OnClickListener { view ->
            val mDialog = Dialog(view.context)
            mDialog.setContentView(R.layout.change_name)
            mDialog.show()
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            teacherName!!.text = usersharedPrefernce!!.getname()
            mdialogname = mDialog.findViewById(R.id.mdialogname)
            val nameSubmit = mDialog.findViewById<TextView>(R.id.changenamesubmit)
            nameSubmit.setOnClickListener(View.OnClickListener {
                if (mdialogname?.getText().toString().isEmpty()) {
                    Toast.makeText(this@TeacherProfile, "Enter Name", Toast.LENGTH_SHORT).show()
                    return@OnClickListener
                }
                if (mdialogname?.getText().toString().length < 2) {
                    Toast.makeText(this@TeacherProfile, "Enter valid Name", Toast.LENGTH_SHORT)
                        .show()
                    return@OnClickListener
                }
                if (mdialogname?.getText().toString().contains(".")) {
                    Toast.makeText(this@TeacherProfile, "Enter valid Name", Toast.LENGTH_SHORT)
                        .show()
                    return@OnClickListener
                }
                // usersharedPrefernce.setname(mdialogname.getText().toString());
                mDialog.dismiss()
                Log.d("asfgsa", "dfafsadf" + usersharedPrefernce!!.getname())
                teacherUpdateProfile
                usersharedPrefernce!!.setname(mdialogname?.getText().toString())
                teacherName!!.text = mdialogname?.getText().toString()
                teacherHeaderName!!.text = mdialogname?.getText().toString()
            })
            val changeNameDismiss = mDialog.findViewById<ImageView>(R.id.changeNameDismiss)
            changeNameDismiss.setOnClickListener { mDialog.dismiss() }
        })
        tapTochangeExperience!!.setOnClickListener { view ->

            val dialog = Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.dialog_experience);


            val done = dialog.findViewById<TextView>(R.id.tvDoneExperience)

            val radioGroup = dialog.findViewById(R.id.radioGroupExperience) as RadioGroup

            done.setOnClickListener {
                val intSelectButton: Int = radioGroup!!.checkedRadioButtonId
                val radioButton = dialog.findViewById(intSelectButton) as RadioButton
                val selectedExperienceValue = radioButton.text.toString()
                teacherExperience?.text = selectedExperienceValue
                dialog.dismiss()
                teacherUpdateLangUpdate

            }

            dialog.show()

//            val mDialog = Dialog(view.context)
//            mDialog.setContentView(R.layout.change_experience)
//            mDialog.show()
//            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            mdialogexperience = mDialog.findViewById(R.id.mdialogexperience)
//            val experienceSubmit = mDialog.findViewById<TextView>(R.id.changeExperienceSubmit)
//            experienceSubmit.setOnClickListener {
//                if (mdialogexperience?.getText().toString().isEmpty()) {
//                    mdialogexperience?.setError("please enter experience")
//                } else {
//                    usersharedPrefernce!!.setexperience(mdialogexperience?.getText().toString())
//                    mDialog.dismiss()
//                    teacherExperience!!.text = usersharedPrefernce!!.getexperience()
//                    teacherUpdateProfile
//                }
//            }
//            val changeExperienceDismiss = mDialog.findViewById<ImageView>(R.id.changeExperienceDismiss)
//            changeExperienceDismiss.setOnClickListener { mDialog.dismiss() }
        }
        tapTochangeLevel!!.setOnClickListener { view ->
            val mDialog = Dialog(view.context)
            mDialog.setContentView(R.layout.change_level)
            mDialog.show()
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mdialoglevel = mDialog.findViewById(R.id.mdialoglevel)
            val levelSubmit = mDialog.findViewById<TextView>(R.id.changeLevelSubmit)
            levelSubmit.setOnClickListener {
                if (mdialoglevel?.getText().toString().isEmpty()) {
                    mdialoglevel?.setError("please enter level")
                } else {
                    usersharedPrefernce!!.setlevel(mdialoglevel?.getText().toString().toInt())
                    mDialog.dismiss()
                    teacherLevel!!.text = usersharedPrefernce!!.getlevel().toString()
                    teacherUpdateProfile
                }
            }
            val changeLevelDismiss = mDialog.findViewById<ImageView>(R.id.changeLevelDismiss)
            changeLevelDismiss.setOnClickListener { mDialog.dismiss() }
        }

//        tapTochangeLanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                final Dialog mdialog = new Dialog(view.getContext());
//                mdialog.setContentView(R.layout.change_language);
//                mdialog.show();
//                mdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                mdialoglanguage = mdialog.findViewById(R.id.mdialoglanguage);
//                TextView languageSubmit = mdialog.findViewById(R.id.changeLanguageSubmit);
//                languageSubmit.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//
//                        if (mdialoglanguage.getText().toString().isEmpty()) {
//
//                            mdialoglanguage.setError("please enter language");
//
//                        } else {
//
//                            usersharedPrefernce.setlanguage(mdialoglanguage.getText().toString());
//                            mdialog.dismiss();
//
//                            teacherLanguage.setText(usersharedPrefernce.getlanguage());
//                            getTeacherUpdateProfile();
//                        }
//
//
//                    }
//                });
//                ImageView changeLanguageDismiss = mdialog.findViewById(R.id.changeLanguageDismiss);
//                changeLanguageDismiss.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        mdialog.dismiss();
//                    }
//                });
//
//
//            }
//        });
        tapTochangeAmount!!.setOnClickListener { view ->
            val dialog = Dialog(view.context)
            dialog.setContentView(R.layout.change_amount)
            dialog.show()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val mdialogAmount = dialog.findViewById<EditText>(R.id.mdialogamount)
            val changeAmountSubmit = dialog.findViewById<TextView>(R.id.changeAmountSubmit)
            val changeAmountDismiss = dialog.findViewById<ImageView>(R.id.changeAmountDismiss)
            mdialogAmount.setText("$ " + usersharedPrefernce!!.getamount().toString())
            changeAmountSubmit.setOnClickListener {
                if (mdialogAmount.text.toString().isEmpty()) {
                    mdialogAmount.error = "please enter amount"
                } else {
                    val value = mdialogAmount.text.toString().replace("$", "")
                    usersharedPrefernce!!.setamount(value)
                    dialog.dismiss()
                    teacherAmount!!.text = "$ " + usersharedPrefernce!!.getamount().toString()
                    amountChange
                }
            }
            changeAmountDismiss.setOnClickListener { dialog.dismiss() }
        }
        tapTochangeDocument!!.setOnClickListener { requestStoragePermission() }
        teacherProfileBackPress!!.setOnClickListener {
            startActivity(
                Intent(
                    this@TeacherProfile,
                    HomeActivity::class.java
                )
            )
        }
    }//      loader.isDismiss();//            loader.isDismiss();//setData(response.body().getBody().get(0).getLanguage());//                loader.isDismiss();

    //   usersharedPrefernce.setname(response.body().getBody().get(0).getName());
    //     usersharedPrefernce.setisonline(response.body().getBody().get(0).getIsOnline());
    //   teacherLanguage.setText(usersharedPrefernce.getlanguage());
    //  loader.isShowProgress();
    private val teacherProfileDeatil: Unit
        private get() {
            //  loader.isShowProgress();
            if (usersharedPrefernce!!.getprofileimage() != null) {
                Glide.with(this@TeacherProfile)
                    .load(ApiClient.IMAGE_URL + usersharedPrefernce!!.getprofileimage())
                    .placeholder(R.drawable.person_girl)
                    .centerCrop().into(personGirlProfile!!)
            }
            val user_id: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getuserid().toString()
            )
            luggaAPI!!.getteacherprofile(user_id)
                .enqueue(object : Callback<TeacherProfileResponse?> {
                    override fun onResponse(
                        call: Call<TeacherProfileResponse?>,
                        response: Response<TeacherProfileResponse?>
                    ) {
                        if (response.body() != null) {
                            if (response.isSuccessful && response.body()!!.success == 200) {
                                //                loader.isDismiss();
                                if (response.body()!!.body!![0].level == 1) {
                                    teacherLevel!!.text = "Begineer"
                                } else if (response.body()!!.body!![0].level == 2) {
                                    teacherLevel!!.text = "Intermidiater"
                                } else if (response.body()!!.body!![0].level == 3) {
                                    teacherLevel!!.text = "Advanced"
                                }

                                Log.d("dataList", Gson().toJson(response.body()))
                                usersharedPrefernce!!.setemail(response.body()!!.body!![0].email)
                                //   usersharedPrefernce.setname(response.body().getBody().get(0).getName());
                                usersharedPrefernce!!.setexperience(response.body()!!.body!![0].experience)
                                usersharedPrefernce!!.setlanguage(response.body()!!.body!![0].language)
                                usersharedPrefernce!!.setlevel(response.body()!!.body!![0].level!!)
                                usersharedPrefernce!!.setmobileno(response.body()!!.body!![0].mobileNo)
                                usersharedPrefernce!!.setcompletedjob(response.body()!!.body!![0].completeJob!!)
                                usersharedPrefernce!!.setuncompletedjob(response.body()!!.body!![0].experience!!)
                                // usersharedPrefernce!!.setrating(response.body()!!.body!![0].other_language!!)
                                usersharedPrefernce!!.setdocumentfile(response.body()!!.body!![0].document)
                                usersharedPrefernce!!.setamount(response.body()!!.body!![0].amount.toString())
                                //     usersharedPrefernce.setisonline(response.body().getBody().get(0).getIsOnline());
                                teacherHeaderName!!.text = usersharedPrefernce!!.getname()
                                teacherHeaderEmail!!.text = usersharedPrefernce!!.getemail()
                                teacherName!!.text = usersharedPrefernce!!.getname()
                                teacherExperience!!.text = usersharedPrefernce!!.getexperience()
                                //   teacherLanguage.setText(usersharedPrefernce.getlanguage());
                                teacherNumber!!.text = usersharedPrefernce!!.getmobileno()
                                completedJob!!.text =
                                    usersharedPrefernce!!.getcompletedjob().toString()
                                uncompletedJob!!.text =
                                    usersharedPrefernce?.getexperience().toString()
                                if (response.body()!!.body?.get(0)?.other_language != null)
                                    rating!!.text =
                                        (response.body()!!.body!![0].other_language!!.toString())
                                teacherDocument!!.text = usersharedPrefernce!!.getdocumentfile()
                                teacherAmount!!.text =
                                    "$ " + usersharedPrefernce!!.getamount().toString()
                                Log.d("name", "username" + usersharedPrefernce!!.getname())
                                Log.d(
                                    "mobile",
                                    "usermobileno" + usersharedPrefernce!!.getmobileno()
                                )
                                language = response.body()!!.body!![0].language
                                otherlanguage = response.body()!!.body!![0].other_language
                                if (response.body()!!.body!![0].teacher_available_days != "0") {
                                    tvDaysTeacher.text =
                                        response.body()!!.body!![0].teacher_available_days
                                    teacherAvailableDays =
                                        response.body()!!.body!![0].teacher_available_days
                                } else {
                                    teacherAvailableDays = ""
                                }

                                selectedLanguage = response.body()!!.body!![0].language
                                selectedOtherLanguage = response.body()!!.body!![0].other_language


                                selectedLanguageee()
                                if (response.body()!!.body!![0].language != null && !response.body()!!.body!![0].language!!.isEmpty()) {
                                    //setData(response.body().getBody().get(0).getLanguage());
                                }
                            } else {
                                //            loader.isDismiss();
                                Toast.makeText(
                                    this@TeacherProfile,
                                    response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<TeacherProfileResponse?>, t: Throwable) {
                        //      loader.isDismiss();
                        Toast.makeText(this@TeacherProfile, t.message, Toast.LENGTH_SHORT).show()
                    }
                })
        }

    private fun setData(language: String) {
        if (language.equals("English", ignoreCase = true)) {
            arraySpinner!![0] = "English"
            arraySpinner!![1] = "Hindi"
            arraySpinner!![2] = "French"
        } else if (language.equals("Hindi", ignoreCase = true)) {
            arraySpinner!![0] = "Hindi"
            arraySpinner!![1] = "English"
            arraySpinner!![2] = "French"
        } else if (language.equals("French", ignoreCase = true)) {
            arraySpinner!![0] = "French"
            arraySpinner!![1] = "English"
            arraySpinner!![2] = "Hindi"
        }
        spinnerLanguage!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }//loader.isDismiss();// loader.isDismiss();//   loader.isDismiss();

    //    loader.isShowProgress();
    private val passwordChange: Unit
        private get() {

            //    loader.isShowProgress();
            val user_id: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getuserid().toString()
            )
            val oldPassword: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                oldpassword!!.text.toString()
            )
            val newPassword: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                newpassword!!.text.toString()
            )
            val confirmPassword: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                confirmnewpassword!!.text.toString()
            )

            luggaAPI!!.passwordchange(user_id, oldPassword, newPassword, confirmPassword)
                .enqueue(object : Callback<PasswordChangeResponse?> {
                    override fun onResponse(
                        call: Call<PasswordChangeResponse?>,
                        response: Response<PasswordChangeResponse?>
                    ) {
                        if (response.body() != null) {
                            if (response.body()!!.success == 200) {
                                //   loader.isDismiss();
                                Toast.makeText(
                                    this@TeacherProfile,
                                    "" + response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                // loader.isDismiss();
                                Toast.makeText(
                                    this@TeacherProfile,
                                    "failed" + response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<PasswordChangeResponse?>, t: Throwable) {
                        //loader.isDismiss();
                        Toast.makeText(this@TeacherProfile, t.message, Toast.LENGTH_SHORT).show()
                    }
                })
        }

    private fun getProfileChange(selectedFile: File) {

        // Log.d("lajdlkadlkad","aakdkdka: "+selectedFile);

        // loader.isShowProgress();
        val requestFile: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedFile)
        val body: MultipartBody.Part =
            MultipartBody.Part.createFormData("profileImage", selectedFile.name, requestFile)
        val user_id: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            usersharedPrefernce!!.getuserid().toString()
        )

        luggaAPI!!.getprofilechange(user_id, body)
            .enqueue(object : Callback<ProfileChangeResponse?> {
                override fun onResponse(
                    call: Call<ProfileChangeResponse?>,
                    response: Response<ProfileChangeResponse?>
                ) {
                    if (response.body() != null) {
                        if (response.body()!!.success == 200) {
                            //loader.isDismiss();
                            usersharedPrefernce!!.setprofileimage(response.body()!!.body!![0].profileImage)
                            Log.d(
                                "lajdlkadlkad",
                                "aakdkdka: " + response.body()!!.body!![0].profileImage + "  " +
                                        ApiClient.IMAGE_URL + usersharedPrefernce!!.getprofileimage()
                            )
                            //   personGirlProfile.setBackgroundResource(Integer.parseInt(ApiClient.BASE_URL+usersharedPrefernce.getprofileimage()));
                            Glide.with(this@TeacherProfile)
                                .load(ApiClient.IMAGE_URL + usersharedPrefernce!!.getprofileimage())
                                .placeholder(R.drawable.person_girl)
                                .centerCrop().into(personGirlProfile!!)
                            //   Toast.makeText(TeacherProfile.this, "successfull" + usersharedPrefernce.getuserid(), Toast.LENGTH_SHORT).show();
                        } else {
                            // loader.isDismiss();
                            Toast.makeText(
                                this@TeacherProfile,
                                "unsuccessfull" + usersharedPrefernce!!.getuserid(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(call: Call<ProfileChangeResponse?>, t: Throwable) {
                    //  loader.isDismiss();
                    Toast.makeText(this@TeacherProfile, t.message, Toast.LENGTH_SHORT).show()
                }
            })
    }//loader.isDismiss();//loader.isDismiss();// loader.isDismiss();

    //  Toast.makeText(TeacherProfile.this,  response.body().getMessage(), Toast.LENGTH_SHORT).show();
    // loader.isShowProgress();
    private val teacherUpdateProfile: Unit
        private get() {
            // loader.isShowProgress();
            val user_id: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getuserid().toString()
            )
            val teacherAvaiLableDays: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherAvailableDays.toString()
            )
            val experience: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherExperience!!.text.toString()
            )
            val name: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherName!!.text.toString()
            )
            val mobile_no: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherNumber!!.text.toString()
            )
            val language: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                spinnerLanguage!!.selectedItem.toString()
            )
            val otherlanguage: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                spinnerOtherLanguage!!.selectedItem.toString()
            )
            val level: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedLanguage!!)
            val amount: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getamount().toString()
            )
            val upload_image: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                teacherDocument!!.text.toString().trim { it <= ' ' })

            luggaAPI!!.getteacherupdateprofile(
                teacherAvaiLableDays,
                otherlanguage,
                user_id,
                experience,
                name,
                mobile_no,
                language,
                level,
                amount,
                upload_image
            ).enqueue(object : Callback<TeacherUpdateResponse?> {
                override fun onResponse(
                    call: Call<TeacherUpdateResponse?>,
                    response: Response<TeacherUpdateResponse?>
                ) {
                    if (response.body() != null) {
                        if (response.isSuccessful && response.body()!!.success == 200) {

                            // loader.isDismiss();
                            //  Toast.makeText(TeacherProfile.this,  response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            //loader.isDismiss();
                            Toast.makeText(
                                this@TeacherProfile,
                                "" + response.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(call: Call<TeacherUpdateResponse?>, t: Throwable) {
                    //loader.isDismiss();
                    Toast.makeText(this@TeacherProfile, t.localizedMessage, Toast.LENGTH_SHORT)
                        .show()
                }
            })
        }//loader.isDismiss();//     loader.isDismiss();//       loader.isDismiss();

    //loader.isShowProgress();
    private val amountChange: Unit
        private get() {

            //loader.isShowProgress();
            val amountt = teacherAmount!!.text.toString().replace("$", "")
            val user_id: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getuserid().toString()
            )
            val amount: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), amountt)
            luggaAPI!!.getamountchange(user_id, amount)
                .enqueue(object : Callback<AmountChangeResponse?> {
                    override fun onResponse(
                        call: Call<AmountChangeResponse?>,
                        response: Response<AmountChangeResponse?>
                    ) {
                        if (response.body() != null) {
                            if (response.isSuccessful && response.body()!!.success == 200) {
                                //       loader.isDismiss();
                                Toast.makeText(
                                    this@TeacherProfile,
                                    "" + response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                //     loader.isDismiss();
                                Toast.makeText(
                                    this@TeacherProfile,
                                    "" + response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<AmountChangeResponse?>, t: Throwable) {
                        //loader.isDismiss();
                        Toast.makeText(
                            this@TeacherProfile,
                            "" + t.localizedMessage,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
        }

    private fun requestStoragePermission() {

        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showPictureDialog()
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Toast.makeText(baseContext, "Error occurred! ", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()

    }

    fun showPictureDialog() {
        val items = arrayOf<CharSequence>(
            "Take Photo", "Choose from Library",
            "Cancel"
        )
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            val userChoosenTask: String
            if (items[item] == "Take Photo") {
                userChoosenTask = "Take Photo"
                cameraIntent()
            } else if (items[item] == "Choose from Library") {
                userChoosenTask = "Choose from Library"
                galleryIntent()
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT //
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY)
    }

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, VideoSource.CAMERA)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            return
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                val selectedfile = FilePath.getPath(this, contentURI)
                val bitmap = BitmapFactory.decodeFile(selectedfile)
                saveImage(bitmap)
                //topprofileimage.setImageBitmap(bitmap);
                personGirlProfile!!.post {
                    val bmp = BitmapFactory.decodeFile(selectedfile)
                    if (selectedfile != null) {
                        getProfileChange(File(selectedfile))
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                    personGirlProfile!!.setImageBitmap(bmp)
                }

//                try {
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
//
//                    topprofileimage.setImageBitmap(bitmap);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
        } else if (requestCode == VideoSource.CAMERA) {
            val thumbnail = data!!.extras!!["data"] as Bitmap?
            saveImage(thumbnail)

            //
            // iv_profile.setImageBitmap(thumbnail);
            Log.e("jhvcdchjdscvuj", thumbnail.toString())

            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            val tempUri = getImageUri(this, thumbnail)
            val selectedfile = FilePath.getPath(this, tempUri)
            val myfile = File(tempUri.path)
            if (selectedfile != null) {
                getProfileChange(File(selectedfile))
                //  progressBar.setVisibility(View.VISIBLE);
            }
            personGirlProfile!!.setImageBitmap(thumbnail)
        }
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    private fun saveImage(bitmap: Bitmap?): String {
        val bytes = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 60, bytes)
        val wallpaperDirectory = File(
            Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY
        )
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            imageFile = File(
                wallpaperDirectory, Calendar.getInstance().timeInMillis.toString() + ".jpg"
            )
            imageFile!!.createNewFile()
            val fo = File(imageFile.toString())
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO) {
                MediaScannerConnection.scanFile(
                    BaseApplication.getContext(),
                    arrayOf(imageFile!!.path),
                    arrayOf("image/jpeg"),
                    null
                )
            }
            return imageFile!!.absolutePath
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return ""
    }

    private fun getImageUri(applicationContext: Context, photo: Bitmap?): Uri {
        val bytes = ByteArrayOutputStream()
        photo!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(
            this.contentResolver,
            photo,
            "Title" + SystemClock.currentThreadTimeMillis(),
            null
        )
        return Uri.parse(path)
    }

    fun getRealPathFromURI(uri: Uri?): String {
        val cursor = this.contentResolver.query(uri!!, null, null, null, null)
        cursor!!.moveToFirst()
        val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        return cursor.getString(idx)
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    override fun onClick(v: View) {
        startActivity(Intent(this@TeacherProfile, FeedBackActivity::class.java))
    }

    companion object {
        private const val GALLERY = 7
        private const val IMAGE_DIRECTORY = "/Lugga"
    }
}