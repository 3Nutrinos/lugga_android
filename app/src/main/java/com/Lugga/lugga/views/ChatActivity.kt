package com.Lugga.lugga.views

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.*
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.MediaStore.Files.FileColumns
import android.provider.Settings
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemAnimator
import com.Lugga.lugga.HomeActivity
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.ChatListingAdapter
import com.Lugga.lugga.adapters.ChatListingAdapter.ChatView
import com.Lugga.lugga.adapters.ImageChooserRecylerViewAdap
import com.Lugga.lugga.fragments.ImageLoadFragment
import com.Lugga.lugga.interfaces.Interface_ImageChooser
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.interfaces.onClickImageChooserInterface
import com.Lugga.lugga.model.bookingStatus.BookingStatusResponse
import com.Lugga.lugga.model.chatList.ChatListBodyItem
import com.Lugga.lugga.model.chatList.ChatListResponse
import com.Lugga.lugga.model.learnerhomepage.LearnerHomePageBodyItem
import com.Lugga.lugga.model.teacherhomepage.TeacherHomePageBodyItem
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.Loader
import com.Lugga.lugga.utils.SocketNetworking
import com.google.gson.Gson
import com.joypixels.tools.Client
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.makeramen.roundedimageview.RoundedImageView
import com.theartofdev.edmodo.cropper.CropImage
import io.socket.client.Ack
import io.socket.client.Socket
import io.socket.emitter.Emitter
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.MalformedURLException
import java.net.URL
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class ChatActivity : FragmentActivity(), View.OnClickListener, ChatView {

    private val startbtn: Button? = null
    private val stopbtn: Button? = null
    private val playbtn: Button? = null
    private val stopplay: Button? = null
    private val mRecorder: MediaRecorder? = null
    private val mPlayer: MediaPlayer? = null
    var chatLayout: RelativeLayout? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var add_media: ImageView? = null
    var socket: Socket? = null
    var tv_photo: TextView? = null
    var tv_video: TextView? = null

    private var imageChooserRecylerViewAdap: ImageChooserRecylerViewAdap? = null
    private val manager: RecyclerView.LayoutManager? = null
    private var roundedImageView: RoundedImageView? = null

    var intentData: Intent? = null
    var position = 0
    var learnerHomePageBodyItemArrayList: ArrayList<LearnerHomePageBodyItem>? = null
    var teacherHomePageBodyItemArrayList: ArrayList<TeacherHomePageBodyItem>? = null
    var send_Message: ImageView? = null
    var chatBackPress: ImageView? = null
    var ivAudioCall: ImageView? = null
    var ivVideoCall: ImageView? = null
    var chatListBodyItemArrayList: ArrayList<ChatListBodyItem>? = null
    var chatListingAdapter: ChatListingAdapter? = null
    var recyclerView: RecyclerView? = null
    var name: TextView? = null
    var tvFile: TextView? = null
    var loader: Loader? = null
    var filePath: FilePath? = null
    var imageFile: File? = null
    var mills: Long = 0
    var iscomingfrom: String? = null
    var ll_bottom: LinearLayout? = null
    private val BASE64_IMAGE = ""
    private val MY_CAMERA_PERMISSION_CODE = 421
    private val CAMERA_REQUEST = 422
    private var rv_galleryimages: RecyclerView? = null
    var tv_cancel: TextView? = null
    var tvTime: TextView? = null
    var serverURL: URL? = null
    var status = 0
    var client: Client? = null

    private var url: String? = null
    private var isShow = false


    private val sharedpreferences: SharedPreferences? = null
    private var BookingId: Int? = null
    private var mediaPlayer: MediaPlayer? = null
    var ivRecordAudio: ImageView? = null
    var ivStopRecording: ImageView? = null
    private var body: MultipartBody.Part? = null
    private var bodyVideo: MultipartBody.Part? = null
    var llCalls: LinearLayout? = null
    var text_layout: CardView? = null
    var obj: JSONObject? = null
    var sendMessage: EditText? = null
    var countDownTimer: CountDownTimer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        client = Client(this)
        client!!.isShortcodes = true // convert shortcodes? :joy:
        ivRecordAudio = findViewById(R.id.ivRecordAudio)
        llCalls = findViewById(R.id.llCalls)
        text_layout = findViewById(R.id.text_layout)
        chatBackPress = findViewById(R.id.chatBackPress)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        loader = Loader(this)
        tvFile = findViewById(R.id.tvFile)
        mediaPlayer = MediaPlayer()
        tvTime = findViewById(R.id.tvTime)
        val intent = getIntent()
        status = intent.getIntExtra("status", 0)
        if (status != 5) {
            llCalls?.setVisibility(View.GONE)
            text_layout?.setVisibility(View.GONE)
            tvTime?.setVisibility(View.GONE)
        } else {
            llCalls?.setVisibility(View.VISIBLE)
            tvTime?.setVisibility(View.VISIBLE)
            text_layout?.setVisibility(View.VISIBLE)
        }
        serverURL = try {
            URL("https://meet.jit.si")
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            throw RuntimeException("Invalid server URL!")
        }

        tvFile?.setOnClickListener(this)
        chatLayout = findViewById<View>(R.id.chatLayout) as RelativeLayout
        init()
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket!!.connect()
        mFileName = Environment.getExternalStorageDirectory().absolutePath
        mFileName += "/AudioRecording.mp3"
        notifyToUserByCallStatus()

        //loader.isShowProgress();
        getmessagelisting()
        chatBackPress?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@ChatActivity, HomeActivity::class.java)
            startActivity(intent)
        })
        ivRecordAudio?.setOnClickListener(this)

    }

    fun notifyToUserByCallStatus() {
        if (socket!!.connected()) {
            Log.e("book", BookingId.toString())
            socket!!.on("returncallStatusRequest$BookingId", object : Emitter.Listener {
                override fun call(vararg args: Any) {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    try {
                        if (jobj.getString("status") != null) {
                            if (jobj.getString("status").equals("3", ignoreCase = true)) {
                               // handler_getdta.sendMessage(msg)
                            } else if (jobj.getString("status").equals("2", ignoreCase = true)) {
                                //handler_getdta1.sendMessage(msg)
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    run {}
                }
            })
        } else {
            socket!!.connect()
            //  Toast.makeText(this, "Socket was not connected down", Toast.LENGTH_SHORT).show();
        }
    }

    private fun init() {
        intentData = getIntent()
        add_media = findViewById(R.id.iv_add_image)
        tv_photo = findViewById(R.id.tv_photo)
        tv_video = findViewById(R.id.tv_video)
        tv_video?.setOnClickListener(this)
        tv_photo?.setOnClickListener(this)
        add_media?.setOnClickListener(this)
        ll_bottom = findViewById(R.id.li_bottomlayout)
        tv_cancel = findViewById(R.id.tv_cancel)
        recyclerView = findViewById(R.id.rv_chat)
        rv_galleryimages = findViewById(R.id.rv_galleryimages)
        iscomingfrom = intentData?.getStringExtra("iscomingfrom")
        chatListingAdapter = ChatListingAdapter(this@ChatActivity, this)
        recyclerView?.setAdapter(chatListingAdapter)
        position = intentData?.getIntExtra("position1", 0)!!
        if (intentData?.getStringExtra("iscomingfrom") != null) {
            if (iscomingfrom == "1") {
                learnerHomePageBodyItemArrayList = intentData?.getSerializableExtra("learnerlist1") as ArrayList<LearnerHomePageBodyItem>
                BookingId = learnerHomePageBodyItemArrayList!![position].getBookingId()
            } else {
                teacherHomePageBodyItemArrayList = intentData?.getSerializableExtra("learnerlist1") as ArrayList<TeacherHomePageBodyItem>
                BookingId = teacherHomePageBodyItemArrayList!![position].id
            }
        }
        sendMessage = findViewById(R.id.ed_typemessage)
        sendMessage?.setOnClickListener(this)
        send_Message = findViewById(R.id.iv_send)
        send_Message?.setOnClickListener(this)
        name = findViewById(R.id.top_name)
        mills = intentData?.getLongExtra("mills", 0)!!
        val countDownTimer: CountDownTimer = object : CountDownTimer(mills, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                tvTime!!.text = "" + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))
            }

            override fun onFinish() {
                finish()
            }
        }
        if (tvTime!!.visibility == View.VISIBLE) {
            countDownTimer.start()
        }
        if (iscomingfrom != null) {
            if (iscomingfrom == "1") {
                name?.setText(learnerHomePageBodyItemArrayList!![position].getName())
            } else {
                name?.setText(teacherHomePageBodyItemArrayList!![position].name)
            }
        }
        ivAudioCall = findViewById(R.id.ivAudioCall)
        ivVideoCall = findViewById(R.id.ivVideoCall)
        ivAudioCall?.setOnClickListener(this)
        ivVideoCall?.setOnClickListener(this)
        tv_cancel?.setOnClickListener(this)
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {}

    //    @Override
    //    public void onClickPlayAudio(String path) throws IOException {
    //        if (path == null) {
    //            return;
    //        }
    //        Log.e("link", ApiClient.IMAGE_URL + path);
    ////        Uri uri = Uri.parse(ApiClient.IMAGE_URL + path);
    ////        MediaPlayer player = new MediaPlayer();
    ////        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
    ////        player.setDataSource(this, uri);
    ////        player.prepare();
    ////        player.start();
    //    }
    //    private class CallsBroadCast extends BroadcastReceiver {
    //        @Override
    //        public void onReceive(Context context, Intent intent) {
    //            if (intent == null || intent.getAction() == null) {
    //                return;
    //            }
    //            try {
    //                socket = SocketNetworking.getSocket();
    //            } catch (Exception e) {
    //                e.printStackTrace();
    //            }
    //            socket.connect();
    //
    //            if (socket.connected()) {
    //
    //                Intent call = new Intent(ChatActivity.this, CallReceiveActivity.class);
    //                call.putExtra("channel_id", intent.getStringExtra("channel_id"));
    //                call.putExtra("caller_id", intent.getStringExtra("caller_id"));
    //                call.putExtra("receiver_id", intent.getStringExtra("receiver_id"));
    //                call.putExtra("caller_name", intent.getStringExtra("caller_name"));
    //                call.putExtra("user_type", intent.getStringExtra("user_type"));
    //                call.putExtra("call_type", intent.getStringExtra("call_type"));
    //                call.putExtra("sender_name", intent.getStringExtra("sender_name"));
    //                call.putExtra("booking_id", intent.getStringExtra("booking_id"));
    //                startActivity(call);
    //
    //            }
    //
    //        }
    //    }

    private fun getAllShownImagesPath(activity: Activity) {
        val uri: Uri
        val cursor: Cursor?
        val column_index_data: Int
        val column_index_folder_name: Int
        val listOfAllImages = ArrayList<String?>()
        var absolutePathOfImage: String? = null
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
        cursor = activity.contentResolver.query(uri, projection, null,
                null, null)
        column_index_data = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data)
            listOfAllImages.add(absolutePathOfImage)
        }
        rv_galleryimages!!.layoutManager = manager
        imageChooserRecylerViewAdap = ImageChooserRecylerViewAdap(this@ChatActivity, applicationContext, listOfAllImages)
        rv_galleryimages!!.adapter = imageChooserRecylerViewAdap

        imageChooserRecylerViewAdap!!.performClick(object : onClickImageChooserInterface{
            override fun onClickImageChooser(imagepath: String?, view: View?) {
                roundedImageView = view as RoundedImageView
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(arrayOf(Manifest.permission.CAMERA), MY_CAMERA_PERMISSION_CODE)
                    } else {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(cameraIntent, CAMERA_REQUEST)
                    }
                }
            }

        })

        imageChooserRecylerViewAdap!!.setOnImageSelectListner(object : Interface_ImageChooser{
            override fun OnSelectedImage(file: String?) {
                ll_bottom!!.visibility = View.GONE
                val bitmap: Bitmap
                bitmap = BitmapFactory.decodeFile(file)
                val resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false)
                ConvertBitmapToString(resizedBitmap)
                Log.d("base6433ff", BASE64_IMAGE)            }

        })

    }

    fun generateString(length: Int): String {
        val random = Random()
        val builder = StringBuilder(length)
        for (i in 0 until length) {
            builder.append(ALPHABET[random.nextInt(ALPHABET.length)])
        }
        return builder.toString()
    }

    private fun sendMessage(message: String, type: String, id: String) {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        connectWithChatSocket(message, type, id)
    }

    private fun getmessagelisting() {
        if (socket!!.connected()) {
            joinChatRoom()
        } else {
            socket!!.on(Socket.EVENT_CONNECT) {
                if (socket!!.connected()) {
                    joinChatRoom()
                } else {
                }
            }
            // socket.connect();
        }
    }

    private fun joinChatRoom() {
        val obj = JSONObject()
        try {
            if (iscomingfrom == "1") {
                obj.put("booking_id", learnerHomePageBodyItemArrayList!![position].getBookingId())
            } else {
                obj.put("booking_id", teacherHomePageBodyItemArrayList!![position].id)
            }
            Log.e("bookingTypebookingType", "jewgdfjgewfewf$obj")
            if (socket!!.connected()) {
                Log.e("connected", "connected$obj")
                socket!!.send(obj)
                socket!!.emit("all_Messages_List", obj, Ack { })
                getlistfromserver()
            } else {
                Log.e("bookingTypebookingType", "jewgdfjgewfewf$obj")
                socket!!.connect()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getlistfromserver() {
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            Log.e("connecnnectedListOn", "asfconnected")
            if (iscomingfrom == "1") {
                socket!!.on("returnAll_Messages_List" + learnerHomePageBodyItemArrayList!![position].getBookingId()) { args ->
                    loader!!.isDismiss()
                    Log.d("vani", args[0].toString())
                    val msgresponse = args[0].toString()
                    val gson = Gson()
                    val chatListResponse = gson.fromJson(msgresponse, ChatListResponse::class.java)
                    chatListBodyItemArrayList = chatListResponse.body
                    runOnUiThread { setdata() }
                }
            } else {
                socket!!.on("returnAll_Messages_List" + teacherHomePageBodyItemArrayList!![position].id) { args ->
                    loader!!.isDismiss()
                    Log.d("vani", args[0].toString())
                    val msgresponse = args[0].toString()
                    val gson = Gson()
                    val chatListResponse = gson.fromJson(msgresponse, ChatListResponse::class.java)
                    chatListBodyItemArrayList = chatListResponse.body
                    runOnUiThread { setdata() }
                }
            }
        } else {
            //   Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show();
            socket!!.connect()
        }
    }

    private fun setdata() {
        chatListingAdapter!!.setListChat(chatListBodyItemArrayList!!)
        chatListingAdapter!!.notifyDataSetChanged()
        recyclerView!!.smoothScrollToPosition(chatListBodyItemArrayList!!.size)
    }

    private fun connectWithChatSocket(message: String, type: String, id: String) {
        if (socket!!.connected()) {
            //   getChatList();
            sendMessagToserver(message, type, id)
        } else {
            socket!!.on(Socket.EVENT_CONNECT) { // getChatList()
                sendMessagToserver(message, type, id)
            }
            socket!!.connect()
        }
    }

    fun sendMessagToserver(message: String?, type: String?, id: String?) {
        val obj = JSONObject()
        try {
            if (iscomingfrom == "1") {
                obj.put("booking_id", learnerHomePageBodyItemArrayList!![position].getBookingId())
            } else {
                obj.put("booking_id", teacherHomePageBodyItemArrayList!![position].id)
            }
            if (id != null && !id.isEmpty()) {
                obj.put("msgId", id)
            }
            if (usersharedPrefernce!!.getusertype() == 1) {
                obj.put("sender_id", usersharedPrefernce!!.getteacherid())
                if (iscomingfrom == "1") {
                    obj.put("receiver_id", learnerHomePageBodyItemArrayList!![position].getLearnerId())
                } else {
                    obj.put("receiver_id", teacherHomePageBodyItemArrayList!![position].learnerId)
                }
            } else {
                obj.put("sender_id", usersharedPrefernce!!.getlearnerid())
                if (iscomingfrom == "1") {
                    obj.put("receiver_id", learnerHomePageBodyItemArrayList!![position].getTeacherId())
                } else {
                    obj.put("receiver_id", teacherHomePageBodyItemArrayList!![position].learnerId)
                }
            }
            //  String createBookingId= String.valueOf(obj.put("booking_id",usersharedPrefernce.getlearnerid()));
            obj.put("message", message)
            obj.put("message_type", type)
            //       obj.put("bookingType", "0");
            Log.e("bookingTypebookingType", "jewgdfjgewfewf$obj")
            if (socket!!.connected()) {
                Log.e("connected", "connected$obj")
                socket!!.send(obj)
                socket!!.emit("Chat_SendMessage", obj, Ack { })
                if (!isShow) {
                    isShow = true
                    returnsearchTeacher
                }
            } else {
                Log.e("bookingTypebookingType", "jewgdfjgewfewf$obj")
                socket!!.connect()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }//


    // getmessagelisting();
////


    //  getmessagelisting();
//
    //  searchTeacherBodyItemArrayList=new ArrayList<>();
    val returnsearchTeacher: Unit
        get() {
            if (socket!!.connected()) {
                val jsonObject = JSONObject()
                Log.e("connecnnectedListOn", "asfconnected")
                //  searchTeacherBodyItemArrayList=new ArrayList<>();
                if (iscomingfrom == "1") {
                    socket!!.on("returnChat_SendMessage" + learnerHomePageBodyItemArrayList!![position].getBookingId()) { args ->

                        runOnUiThread {
                            loader!!.isDismiss()
                            val msg = Message()
                            val jobj = args[0] as JSONObject
                            msg.obj = jobj
                            var Jarray: JSONArray? = null
                            try {
                                Jarray = jobj.getJSONArray("body")
                                val `object` = Jarray.getJSONObject(0)
                                val item = ChatListBodyItem()
                                item.id = `object`.getInt("id")
                                item.bookingId = `object`.getInt("booking_id")
                                item.senderId = `object`.getInt("sender_id")
                                item.receiverId = `object`.getInt("receiver_id")
                                item.message = `object`.getString("message")
                                item.messageType = `object`.getInt("message_type") //
                                val format: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                                val date = format.parse(`object`.getString("created_at"))
                                item.createdAt = date
                                val insertIndex = chatListBodyItemArrayList!!.size
                                chatListBodyItemArrayList!!.add(insertIndex, item)
                                chatListingAdapter!!.notifyItemInserted(insertIndex)
                                val itemAnimator: ItemAnimator = DefaultItemAnimator()
                                itemAnimator.addDuration = 1000
                                recyclerView!!.itemAnimator = itemAnimator
                                recyclerView!!.smoothScrollToPosition(chatListBodyItemArrayList!!.size)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            } catch (e: ParseException) {
                                e.printStackTrace()
                            }
                        }


                        //  getmessagelisting();
                    }
                } else {
                    socket!!.on("returnChat_SendMessage" + teacherHomePageBodyItemArrayList!![position].id) { args ->
                        Log.d("retrmLirmListretrmList", args[0].toString())
                        //
                        runOnUiThread {
                            loader!!.isDismiss()
                            val msg = Message()
                            val jobj = args[0] as JSONObject
                            msg.obj = jobj
                            var Jarray: JSONArray? = null
                            try {
                                Jarray = jobj.getJSONArray("body")
                                val `object` = Jarray?.getJSONObject(0)
                                val item = ChatListBodyItem()
                                item.id = `object`!!.getInt("id")
                                item.bookingId = `object`.getInt("booking_id")
                                item.senderId = `object`.getInt("sender_id")
                                item.receiverId = `object`.getInt("receiver_id")
                                item.message = `object`.getString("message")
                                item.messageType = `object`.getInt("message_type") //
                                val format: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                                val date = format.parse(`object`!!.getString("created_at"))
                                item.createdAt = date
                                val insertIndex = chatListBodyItemArrayList!!.size
                                chatListBodyItemArrayList!!.add(insertIndex, item)
                                chatListingAdapter!!.notifyItemInserted(insertIndex)
                                val itemAnimator: ItemAnimator = DefaultItemAnimator()
                                itemAnimator.addDuration = 1000
                                recyclerView!!.itemAnimator = itemAnimator
                                recyclerView!!.smoothScrollToPosition(chatListBodyItemArrayList!!.size)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            } catch (e: ParseException) {
                                e.printStackTrace()
                            }
                        }


                        // getmessagelisting();
                    }
                }
            } else {
                socket!!.connect()
            }
        }

    private fun setHideKeyboardOnTouch(context: Context, view: View) {

        //Set up touch listener for non-text box views to hide keyboard.
        try {
            //Set up touch listener for non-text box views to hide keyboard.
            if (!(view is EditText || view is ScrollView)) {
                view.setOnTouchListener { v, event ->
                    val `in` = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    `in`.hideSoftInputFromWindow(v.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                    false
                }
            }
            //If a layout container, iterate over children and seed recursion.
            if (view is ViewGroup) {
                for (i in 0 until view.childCount) {
                    val innerView = view.getChildAt(i)
                    setHideKeyboardOnTouch(context, innerView)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.iv_send -> {
                if (!TextUtils.isEmpty(sendMessage!!.text.toString().trim { it <= ' ' })) {
                    val toServer: String
                    toServer = client!!.toShortname(sendMessage!!.text.toString())
                    setHideKeyboardOnTouch(this, findViewById(R.id.container))
                    //  String toServerUnicodeEncoded = StringEscapeUtils.escapeJava(toServer);
                    loader!!.isShowProgress()
                    sendMessage(toServer, "1", "")
                    sendMessage!!.setText("")
                } else {
                    Toast.makeText(this, "Enter message", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.iv_add_image -> {
                requestStoragePermission()
            }
            R.id.tv_photo -> {
                ll_bottom!!.visibility = View.GONE
                CropImage.activity().start(this@ChatActivity)
            }
            R.id.tv_cancel -> ll_bottom!!.visibility = View.GONE
            R.id.tv_video -> {
                val vidoe_intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                vidoe_intent.type = "video/*"
                vidoe_intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(vidoe_intent, "Select Video"), 10)
            }
            R.id.ivAudioCall -> audIorequestStoragePermission("audioCall")
            R.id.ivVideoCall -> audIorequestStoragePermission("videoCall")
            R.id.tvFile -> {
            }
            R.id.ivRecordAudio -> audIorequestStoragePermission("recordAudio")
            else -> {
            }
        }
    }

    private fun startAudio() {
        val intent = Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION)
        startActivityForResult(intent, RQS_RECORDING)
    }

    private fun audIorequestStoragePermission(recordAudio: String) {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            if (recordAudio.equals("videoCall", ignoreCase = true)) {
                                connectSocketForAudioCall("2")
                            } else if (recordAudio.equals("audioCall", ignoreCase = true)) {
                                connectSocketForAudioCall("1")
                            } else {
                                startAudio()
                            }
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).withErrorListener { Toast.makeText(baseContext, "Error occurred! ", Toast.LENGTH_SHORT).show() }
                .onSameThread()
                .check()
    }

    private fun requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            ll_bottom!!.visibility = View.VISIBLE
                            getAllShownImagesPath(this@ChatActivity)
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).withErrorListener { Toast.makeText(baseContext, "Error occurred! ", Toast.LENGTH_SHORT).show() }
                .onSameThread()
                .check()
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    fun showPictureDialog() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library",
                "Cancel")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            val userChoosenTask: String
            if (items[item] == "Take Photo") {
                userChoosenTask = "Take Photo"
                cameraIntent()
            } else if (items[item] == "Choose from Library") {
                userChoosenTask = "Choose from Library"
                galleryIntent()
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun showAddMediaDialog() {
        val items = arrayOf<CharSequence>("Send Picture", "Send video",
                "Send Audio")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            val userChoosenTask: String
            if (items[item] == "Send Picture") {
                userChoosenTask = "Send Picture"
                usersharedPrefernce!!.setIMAGE("image")
                showPictureDialog()
            } else if (items[item] == "Send video") {
                userChoosenTask = "Send video"
                usersharedPrefernce!!.setIMAGE("video")
                showVideoDialog()
                //   galleryIntent();
            } else if (items[item] == "Send Audio") {
                userChoosenTask = "Send Audio"
                // galleryIntent();
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT //
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY)
    }

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, MediaRecorder.VideoSource.CAMERA)
    }

    /*    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(usersharedPrefernce.getImage().equals("image"))
        {
            if (resultCode == this.RESULT_CANCELED) {
                return;
            }
            if (requestCode == GALLERY) {

                if (data != null) {
                    Uri contentURI = data.getData();
                    final String selectedfile = FilePath.getPath(this, contentURI);
                    Bitmap bitmap = BitmapFactory.decodeFile(selectedfile);
                    saveImage(bitmap);
                    Log.e("kdfghjukhljsrg",bitmap+"////////"+new File(selectedfile));
                    sendMessage(String.valueOf(new File(selectedfile)), "3");
                    // usersharedPrefernce.setIMAGE(selectedfile);
                    loader.isShowProgress();



                */
    /*personGirlProfile.post(new Runnable() {
                    @Override
                    public void run()
                    {
                        Bitmap bmp = BitmapFactory.decodeFile(selectedfile);
                        if(selectedfile!=null)

                        {
                            getProfileChange(new File(selectedfile));
                            // progressBar.setVisibility(View.VISIBLE);
                        }
                        personGirlProfile.setImageBitmap(bmp);
                    }
                });*/
    /*



                }

            } else if (requestCode == CAMERA) {

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                saveImage(thumbnail);

                //
                // iv_profile.setImageBitmap(thumbnail);
                Log.e("jhvcdchjdscvuj",thumbnail.toString());

                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP

                Uri tempUri = getImageUri(this, thumbnail);
                final String selectedfile = FilePath.getPath(this, tempUri);

                File myfile=new File(tempUri.getPath());
                if(selectedfile!=null)


                {
                    Log.e("selectedfile", String.valueOf(new File(selectedfile)));
                    sendMessage(String.valueOf(new File(selectedfile)), "3");
                    //  usersharedPrefernce.setIMAGE(selectedfile);
                    loader.isShowProgress();
                    //getProfileChange(new File(selectedfile));
                    //  progressBar.setVisibility(View.VISIBLE);
                }
                //personGirlProfile.setImageBitmap(thumbnail);


            }
        }
        else
        {
            Log.d("result",""+resultCode);
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == this.RESULT_CANCELED) {
                Log.d("what","cancle");
                return;
            }
            if (requestCode == GALLERY) {
                Log.d("what","gale");
                if (data != null) {
                    Uri contentURI = data.getData();

                    String selectedVideoPath = getPath(contentURI);
                    Log.d("path",selectedVideoPath);
                    saveVideoToInternalStorage(selectedVideoPath);
                    sendVideoINChat(selectedVideoPath);
               */
    /* videoView.setVideoURI(contentURI);
                videoView.requestFocus();
                videoView.start();*/
    /*

                }

            } else if (requestCode == CAMERA) {
                Uri contentURI = data.getData();
                String recordedVideoPath = getPath(contentURI);
                Log.d("frrr",recordedVideoPath);
                saveVideoToInternalStorage(recordedVideoPath);
                sendVideoINChat(recordedVideoPath);
           */
    /* videoView.setVideoURI(contentURI);
            videoView.requestFocus();
            videoView.start();*/
    /*
            }
        }



    }*/
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == RQS_RECORDING) {
                if (resultCode == RESULT_OK) {
                    // Great! User has recorded and saved the audio file
                    if (data != null) {
                        val fileUri = data.data
                        if (fileUri != null) {
                            Log.e("uri", data.data.toString())
                            sendAudio(fileUri)
                        }
                    }
                }
            } else if (requestCode == 10) {
                val url = data!!.data
                println("urldsa$url")
                if (url.toString().contains("video") || url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                        url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                    if (data != null) {
                        val fileUri = data.data
                        fileUri?.let { sendVideoINChat(it) }
                    }
                    loader!!.isShowProgress()
                } else if (url.toString().contains(".gif")) {
                    // GIF file
                } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png") || url.toString().contains("image")) {
                    // JPG file
                    val bitmap: Bitmap
                    try {
                        bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(url!!))
                        val resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false)
                        ConvertBitmapToString(resizedBitmap)
                        Log.d("base6433ff", BASE64_IMAGE)
                    } catch (e: FileNotFoundException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                }
            } else {
                val result = CropImage.getActivityResult(data)
                val resultUri = result.uri
                var bitmap: Bitmap? = null
                try {
                    bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(resultUri))
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
                ConvertBitmapToString(bitmap)
            }
            if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
                val photo = data!!.extras!!["data"] as Bitmap?
                /*  roundedImageView.setVisibility(View.VISIBLE);
                roundedImageView.setImageBitmap(photo);*/
                val imagefile = getOutputMediaFile(1)
                if (imagefile != null) getFilefromBitmap(photo, "123", imagefile)
            }
        }
    }

    private fun getOutputMediaFile(type: Int): File? {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        val fullPath = Environment.getExternalStorageDirectory().absolutePath + "/WhatsApp/Media"
        val mediaStorageDir = File(fullPath, "WhatsApp Images")
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Buddo_Images", "failed to create directory")
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val mediaFile: File
        mediaFile = if (type == FileColumns.MEDIA_TYPE_IMAGE) {
            File(mediaStorageDir.path + File.separator +
                    "IMG_" + timeStamp + ".jpg")
        } else if (type == FileColumns.MEDIA_TYPE_VIDEO) {
            File(mediaStorageDir.path + File.separator +
                    "VID_" + timeStamp + ".mp4")
        } else {
            return null
        }
        return mediaFile
    }

    private fun getFilefromBitmap(bitmap: Bitmap?, filename: String, imagefile: File) {
        Log.d("lhfkhfkhfkhkfh", imagefile.toString())

//Convert bitmap to byte array
        val bos = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
        val bitmapdata = bos.toByteArray()

//write the bytes in file
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(imagefile)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        try {
            fos!!.write(bitmapdata)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        try {
            fos!!.flush()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        try {
            fos!!.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun ConvertBitmapToString(bitmap: Bitmap?) {
        var encodedImage = ""
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
        println("image$encodedImage")
        sendMessage(encodedImage, "3", "")
        loader!!.isShowProgress()
    }

    private fun saveImage(bitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes)
        val wallpaperDirectory = File(
                Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY)
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            imageFile = File(
                    wallpaperDirectory, Calendar.getInstance().timeInMillis.toString() + ".jpg")
            imageFile!!.createNewFile()
            val fo = File(imageFile.toString())
            MediaScannerConnection.scanFile(
                    BaseApplication.getContext(), arrayOf(imageFile!!.path), arrayOf("image/jpeg"), null
            )
            return imageFile!!.absolutePath
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return ""
    }

    private fun getImageUri(applicationContext: Context, photo: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(this.contentResolver, photo, "Title", null)
        return Uri.parse(path)
    }

    fun showVideoDialog() {
        val items = arrayOf<CharSequence>("Take Video", "Choose from Library",
                "Cancel")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            val userChoosenTask: String
            if (items[item] == "Take Video") {
                userChoosenTask = "Take Video"
                takeVideoFromCamera()
            } else if (items[item] == "Choose from Library") {
                userChoosenTask = "Choose from Library"
                chooseVideoFromGallary()
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    fun chooseVideoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takeVideoFromCamera() {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        startActivityForResult(intent, MediaRecorder.VideoSource.CAMERA)
    }

    private fun saveVideoToInternalStorage(filePath: String) {
        val newfile: File
        try {
            val currentFile = File(filePath)
            val wallpaperDirectory = File(Environment.getExternalStorageDirectory().toString() + "LuggaVideo")
            newfile = File(wallpaperDirectory, Calendar.getInstance().timeInMillis.toString() + ".mp4")
            if (!wallpaperDirectory.exists()) {
                wallpaperDirectory.mkdirs()
            }
            if (currentFile.exists()) {
                val `in`: InputStream = FileInputStream(currentFile)
                val out: OutputStream = FileOutputStream(newfile)

                // Copy the bits from instream to outstream
                val buf = ByteArray(1024)
                var len: Int
                while (`in`.read(buf).also { len = it } > 0) {
                    out.write(buf, 0, len)
                }
                `in`.close()
                out.close()
                Log.v("vii", "Video file saved successfully.")
            } else {
                Log.v("vii", "Video saving failed. Source file missing.")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Video.Media.DATA)
        val cursor = contentResolver.query(uri!!, projection, null, null, null)
        return if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            val column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(column_index)
        } else null
    }

    private fun sendVideoINChat(videopath: Uri) {
        val bookingId: RequestBody
        bookingId = if (iscomingfrom == "1") {
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position].getBookingId().toString())
        } else {
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position].id.toString())
        }
        val senderId: RequestBody
        var receiver_id: RequestBody? = null
        if (usersharedPrefernce!!.getusertype() == 1) {

            senderId = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getteacherid().toString())
            receiver_id = if (iscomingfrom == "1") {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position].getTeacherId().toString())
            } else {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position].learnerId.toString())
            }

        } else {
            senderId = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getlearnerid().toString())
            receiver_id = if (iscomingfrom == "1") {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position].getTeacherId().toString())
            } else {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position].learnerId.toString())
            }
        }

       // val filePath = getRealPathFromUri(videopath)
//        if (filePath != null && filePath.isNotEmpty()) {
//            val file = File(filePath)
//            val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
//            bodyVideo = MultipartBody.Part.createFormData("message", file.name, requestFile)
//        }
        val message_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "4")
        ApiClient.getApiClient().create(LuggaAPI::class.java).sendVideo(bookingId, senderId, receiver_id, bodyVideo, message_type).enqueue(object : Callback<BookingStatusResponse> {
            override fun onResponse(call: Call<BookingStatusResponse>, response: Response<BookingStatusResponse>) {
                if (response.isSuccessful) {
                    ll_bottom!!.visibility = View.GONE
                    Toast.makeText(this@ChatActivity, "video sent", Toast.LENGTH_SHORT).show()
                    sendMessage("", "4", response.body()!!.body!![0].id.toString())
                    Log.e("iddja", response.body()!!.body!![0].id.toString())
                    getmessagelisting()
                }
            }

            override fun onFailure(call: Call<BookingStatusResponse>, t: Throwable) {}
        })
    }

    private fun sendAudio(audioPath: Uri) {

        var bookingId: RequestBody? = null

        bookingId = if (iscomingfrom == "1") {
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position].getBookingId().toString())
        } else {
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position].id.toString())
        }

        val senderId: RequestBody
        var receiver_id: RequestBody? = null

        if (usersharedPrefernce!!.getusertype() == 1) {
            senderId = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getteacherid().toString())
            receiver_id = if (iscomingfrom == "1") {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position].getTeacherId().toString())
            } else {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position].learnerId.toString())
            }
        } else {
            senderId = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getlearnerid().toString())
            receiver_id = if (iscomingfrom == "1") {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position].getTeacherId().toString())
            } else {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position].learnerId.toString())
            }
        }
        val filePath = "getRealPathFromUri(audioPath)"
        if (filePath != null && !filePath.isEmpty()) {
            url = if (filePath.contains("amr")) {
                filePath.replace("amr", "mp3")
            } else if (filePath.contains("m4a")) {
                filePath.replace("m4a", "mp3")
            } else {
                filePath
            }
            Log.e("file", filePath.toString())
            val file = File(url)
            val requestFile: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
            body = MultipartBody.Part.createFormData("message", file.name, requestFile)
        }

        val message_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "2")

        ApiClient.getApiClient().create(LuggaAPI::class.java).sendVideo(bookingId, senderId, receiver_id, body, message_type).enqueue(object : Callback<BookingStatusResponse> {
            override fun onResponse(call: Call<BookingStatusResponse>, response: Response<BookingStatusResponse>) {
                if (response.isSuccessful) {

                    Toast.makeText(this@ChatActivity, "audio sent", Toast.LENGTH_SHORT).show()
                    sendMessage("", "2", response.body()!!.body!![0].id.toString())

                    getmessagelisting()

                }
            }

            override fun onFailure(call: Call<BookingStatusResponse>, t: Throwable) {}
        })
    }


    private fun connectSocketForAudioCall(type: String) {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        joincallroom(type)
    }

    private fun joincallroom(type: String) {
        if (socket!!.connected()) {
            socketforcall(type)
        } else {
            socket!!.on(Socket.EVENT_CONNECT) {
                if (socket!!.connected()) {
                    socketforcall(type)
                } else {
                }
            }
            socket!!.connect()
        }
    }

    private fun socketforcall(type: String) {
        obj = JSONObject()
        try {
            obj!!.put("call_type", type)
            obj!!.put("Channel_id", generateString(20))
            if (iscomingfrom == "1") {
                obj!!.put("booking_id", learnerHomePageBodyItemArrayList!![position].getBookingId())
            } else {
                obj!!.put("booking_id", teacherHomePageBodyItemArrayList!![position].id)
            }
            if (usersharedPrefernce!!.getusertype() == 1) {
                obj!!.put("caller_id", usersharedPrefernce!!.getteacherid())
                obj!!.put("user_type", "2")
                if (iscomingfrom == "1") {
                    obj!!.put("receiver_id", learnerHomePageBodyItemArrayList!![position].getLearnerId())
                } else {
                    obj!!.put("receiver_id", teacherHomePageBodyItemArrayList!![position].learnerId)
                }
            } else {
                obj!!.put("caller_id", usersharedPrefernce!!.getlearnerid())
                obj!!.put("user_type", "1")
                if (iscomingfrom == "1") {
                    obj!!.put("receiver_id", learnerHomePageBodyItemArrayList!![position].getTeacherId())
                } else {
                    obj!!.put("receiver_id", teacherHomePageBodyItemArrayList!![position].teacherId)
                }
            }
            Log.e("bookingTypebookingType", "jewgdfjgewfewf" + obj.toString())
            if (socket!!.connected()) {
                socket!!.send(obj)
                Log.e("dataSent", obj.toString())
                Toast.makeText(this, "calling", Toast.LENGTH_SHORT).show()
                //setRoomForCall(type, obj!!.getString("Channel_id"))
                // Log.e("connected", "connected" + obj.toString());
                socket!!.emit("callRequest", obj, Ack { })
                returnresponseofcall()
            } else {
                Log.e("bookingTypebookingType", "jewgdfjgewfewf" + obj.toString())
                socket!!.connect()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Throws(JSONException::class)
    private fun hitSocket(status: String) {
        // obj = new JSONObject();
        obj!!.put("time_count", "0")
        obj!!.put("status", status)
        obj!!.put("endCall", "sender")
        if (usersharedPrefernce!!.getusertype() == 1) {
            if (iscomingfrom == "1") {
                obj!!.put("receiever_id", learnerHomePageBodyItemArrayList!![position].getLearnerId())
            } else {
                obj!!.put("receiever_id", teacherHomePageBodyItemArrayList!![position].learnerId)
            }
        } else {
            if (iscomingfrom == "1") {
                obj!!.put("receiever_id", learnerHomePageBodyItemArrayList!![position].getTeacherId())
            } else {
                obj!!.put("receiever_id", teacherHomePageBodyItemArrayList!![position].teacherId)
            }
        }
        socket!!.send(obj)
        Log.e("onCallEnd", obj.toString())
        socket!!.emit("callStatusRequest", obj, Emitter.Listener { })
        socketCutCallResponse()
    }

    private fun socketCutCallResponse() {
        if (socket!!.connected()) {
            socket!!.on("returncallStatusRequest$BookingId") { args ->
                val msg = Message()
                val jobj = args[0] as JSONObject
                msg.obj = jobj
                try {
                    if (jobj.getString("status") != null) {
                        if (jobj.getString("status").equals("2", ignoreCase = true)) {
                            //handler_getdta1.sendMessage(msg)
                        } else if (jobj.getString("status").equals("3", ignoreCase = true)) {
                            //handler_getdta.sendMessage(msg)
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        } else {
            socket!!.connect()
        }
    }

    private fun returnresponseofcall() {
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            Log.e("connecnnectedListOn", "asfconnected")
            if (iscomingfrom == "1") {
                socket!!.on("returncallRequest" + learnerHomePageBodyItemArrayList!![position].getBookingId()) { loader!!.isDismiss() }
            } else {
                socket!!.on("returncallRequest" + teacherHomePageBodyItemArrayList!![position].id) { args ->
                    loader!!.isDismiss()
                    Log.d("vanikjfksd", args[0].toString())
                }
            }
        } else {
            socket!!.connect()
        }
    }

    private fun getDataColumn(context: Context, uri: Uri?, selection: String?,
                              selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
                column
        )
        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs,
                    null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    override fun onClickImage(message: String?) {
        if (message == null) {
            return
        }
        val bundle = Bundle()
        bundle.putString("imageUrl", message)
        val fragment: Fragment = ImageLoadFragment()
        fragment.arguments = bundle
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("")
                .commit()
    }

    companion object {
        private const val LOG_TAG = "AudioRecording"
        private var mFileName: String? = null
        const val REQUEST_AUDIO_PERMISSION_CODE = 1
        const val MyPREFERENCES = "MyPrefs"
        private const val RQS_RECORDING = 1
        private const val GALLERY = 7
        private const val IMAGE_DIRECTORY = "/Lugga"
        private const val ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    }
}