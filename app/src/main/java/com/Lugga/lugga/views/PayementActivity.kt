package com.Lugga.lugga.views

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.Lugga.lugga.R
import com.Lugga.lugga.fragments.PaymentSuccessFragment
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.SocketNetworking
import com.stripe.android.Stripe
import com.stripe.android.model.Card
import com.stripe.android.view.CardMultilineWidget
import io.socket.client.Socket
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit

class PayementActivity : AppCompatActivity() {
    private var tv_signUp: TextView? = null
    private var cardMultilineWidget: CardMultilineWidget? = null
    private var stripe: Stripe? = null
    private var cardToSave: Card? = null
    var progressBar: ProgressBar? = null
    var price: String? = null
    var token_gen: String? = null
    var startTime: String? = null
    var endTime: String? = null
    var sessionTime: String? = null
    var teacherId: String? = null
    var intentData: Intent? = null
    var tv_amount: TextView? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var payment_id: String? = null
    var subtotal: String? = null
    var total: String? = null
    var centertexttoolbar: TextView? = null
    var back_toolbar: ImageView? = null
    var name: EditText? = null
    private var amount = 0
    private var bookingId = 0
    private var booking_type = 0
    private val countDownTimer: CountDownTimer? = null
    private var luggaAPI: LuggaAPI? = null
    private var cardNumber: String? = null
    private var cardExpiryMonth: String? = null
    private var cardExpiryYear: String? = null
    private var cvc: String? = null
    private var Id: String? = null
    private var stripe_user_id: String? = null
    private var time: String? = null
    private var sessionStartTime: String? = null
    private var endsecond: String? = null
    private var Endminutes: String? = null
    private var sessionEndTime: String? = null
    private var second: String? = null
    private var minutes: String? = null
    private var StartTime: RequestBody? = null
    private var EndTime: RequestBody? = null
    var notificationBackPress: ImageView? = null
    var socket: Socket? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_)
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket?.connect()
        luggaAPI = retrofit.create(LuggaAPI::class.java)
        progressBar = findViewById(R.id.progressBar)
        notificationBackPress = findViewById(R.id.notificationBackPress)
        notificationBackPress?.setOnClickListener(View.OnClickListener { onBackPressed() })
        initView()

    }

    private fun initView() {
        name = findViewById(R.id.ed_name)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        tv_signUp = findViewById(R.id.tv_signUp)
        tv_amount = findViewById(R.id.tv_amount)
        cardMultilineWidget = findViewById(R.id.card_multiline_widget)
        cardMultilineWidget?.setShouldShowPostalCode(false)
        stripe = BaseApplication.getStripe(applicationContext)
        //     bookingid=getIntent().getStringExtra("bookingid");
        intentData = getIntent()
        price = intentData?.getStringExtra("price")
        amount = intentData?.getIntExtra("amount", 0)!!
        bookingId = intentData?.getIntExtra("bookingId", 0)!!
        startTime = intentData?.getStringExtra("startTime")
        endTime = intentData?.getStringExtra("endTime")
        booking_type = intentData?.getIntExtra("booking_type", 0)!!
        sessionTime = intentData?.getStringExtra("sessionTime")
        teacherId = intentData?.getStringExtra("teacherId")

        if (intentData?.getStringExtra("stripe_user_id") != null) {
            stripe_user_id = intentData?.getStringExtra("stripe_user_id")
        }
        Log.e("bookngId", bookingId.toString())
        // tv_amount.setText(price);
        tv_amount?.setText("" + amount)

        // usersharedPrefernce=UsersharedPrefernce.getInstance();
        setupOnClickListners()
    }

    private fun setupOnClickListners() {
        tv_signUp!!.setOnClickListener {
            if (name!!.text.toString().trim { it <= ' ' } != "") {
                if (checkCardValidation()) {
                    progressBar!!.visibility = View.VISIBLE
                    hitPaymentMethodApi()

                    //   generatePaymentToken();
                }
            } else {
                Toast.makeText(this@PayementActivity, "Please enter name", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun hitPaymentMethodApi() {
        val onCallBackStripe = luggaAPI!!.PaymentMethod("card", cardNumber, cardExpiryMonth, cardExpiryYear, cvc)
        onCallBackStripe.enqueue(object : Callback<StripeModelClass?> {
            override fun onResponse(call: Call<StripeModelClass?>, response: Response<StripeModelClass?>) {
                if (response.isSuccessful && response.body() != null) {
                    Id = response.body()!!.id
                    makepayment(Id)
                }
            }

            override fun onFailure(call: Call<StripeModelClass?>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                if (t is UnknownHostException) {
                    Toast.makeText(this@PayementActivity, "No Internet Connection", Toast.LENGTH_SHORT).show()
                } else if (t is SocketTimeoutException) {
                    Toast.makeText(this@PayementActivity, "Server is not responding. Please try again", Toast.LENGTH_SHORT).show()
                } else if (t is ConnectException) {
                    Toast.makeText(this@PayementActivity, "Failed to connect server", Toast.LENGTH_SHORT).show()
                } else Toast.makeText(this@PayementActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun checkCardValidation(): Boolean {
        cardToSave = cardMultilineWidget!!.card
        if (cardToSave == null) {
            Toast.makeText(this, "Card details are invalid", Toast.LENGTH_SHORT).show()
            return false
        }
        cardNumber = cardToSave!!.number
        cardExpiryMonth = cardToSave!!.expMonth.toString()
        cardExpiryYear = cardToSave!!.expYear.toString()
        cvc = cardToSave!!.cvc
        return true
    }

    /*private fun generatePaymentToken() {

        stripe?.createToken(cardToSave, object : ApiResultCallback<Token?> {
            override fun onSuccess(token: Token) {
                Log.e("sucessssssssssssss", token.id)
                token_gen = token.id
                if (token_gen != null) {
                }
                // Commn.dismiss();
            }

            override fun onError(e: Exception) {
                //Commn.dismiss();
                Log.d("errortokengenearet", e.message)
            }
        })
    }*/

    private fun makepayment(id: String?) {
        if (booking_type == 0) {
            setSessionTime()
            StartTime = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), sessionStartTime!!)
            EndTime = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), sessionEndTime!!)
        }

        val amountTeacher = amount.toString().toDouble()
        val teacherAmount = 70 / 100.0f * amountTeacher
        val finalTeacherAmount = DecimalFormat("##.##").format(teacherAmount)
        val bookingId1: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), bookingId.toString())
        //        RequestBody token = RequestBody.create(MediaType.parse("plain/text"), token_gen);
        val amountt: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), amount.toString())
        val bookingTyp: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), booking_type.toString())
        val Id: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), id!!)
        val TeacherAmount: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), finalTeacherAmount)
        val stripeUserId: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), stripe_user_id!!)

        ApiClient.getApiClient().create(LuggaAPI::class.java).makePayment(bookingId1, amountt, StartTime, EndTime, bookingTyp, Id, TeacherAmount, stripeUserId).enqueue(object : Callback<PaymentModel?> {
            override fun onResponse(call: Call<PaymentModel?>, response: Response<PaymentModel?>) {
                if (response.isSuccessful) {
                    progressBar!!.visibility = View.GONE
                    if (response.body() != null) {
                        if (response.body()!!.success == 200) {
                            val fragment: Fragment = PaymentSuccessFragment()
                            val bundle = Bundle()
                            bundle.putString("transId", response.body()!!.body!!.id.toString())
                            fragment.arguments = bundle
                            supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.container, fragment)
                                    .addToBackStack("")
                                    .commit()


                            val jsonObject= JSONObject()
                            jsonObject.put("id", teacherId.toString())
                            jsonObject.put("user_type", "1")
                            socket?.emit("HomepageSocket", jsonObject)
                        }
                    } else {
                        progressBar!!.visibility = View.GONE
                        Toast.makeText(this@PayementActivity, response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(this@PayementActivity, "Payment failed", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<PaymentModel?>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                Toast.makeText(this@PayementActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun setSessionTime() {

        val sessiontime = sessionTime.toString()
        val currenttimeinmilliseconds = System.currentTimeMillis()
        val timeinmilliseconds: Long
        val extenedetimemilliseconds: Long
        if (sessiontime == "30 min") {
            time = "30"
            timeinmilliseconds = 1800000
            extenedetimemilliseconds = currenttimeinmilliseconds + timeinmilliseconds
        } else if (sessiontime == "60 min") {
            time = "60"
            timeinmilliseconds = 3600000
            extenedetimemilliseconds = currenttimeinmilliseconds + timeinmilliseconds
        } else if (sessiontime == "90 min") {
            time = "90"
            timeinmilliseconds = 5400000
            extenedetimemilliseconds = currenttimeinmilliseconds + timeinmilliseconds
        } else {
            time = "120"
            timeinmilliseconds = 7200000
            extenedetimemilliseconds = currenttimeinmilliseconds + timeinmilliseconds
        }
        val cl = Calendar.getInstance()
        val c2 = Calendar.getInstance()
        cl.timeInMillis = currenttimeinmilliseconds
        c2.timeInMillis = extenedetimemilliseconds
        //here your time in miliseconds
        //     String date = "" + cl.get(Calendar.DAY_OF_MONTH) + ":" + cl.get(Calendar.MONTH) + ":" + cl.get(Calendar.YEAR);
        val sec = cl[Calendar.SECOND].toString()
        second = if (sec.length < 2) {
            "0$sec"
        } else {
            sec
        }
        val min = cl[Calendar.MINUTE].toString()
        minutes = if (min.length < 2) {
            "0$min"
        } else {
            min
        }
        sessionStartTime = "" + cl[Calendar.HOUR_OF_DAY] + ":" + minutes + ":" + second
        val Endsec = c2[Calendar.SECOND].toString()
        endsecond = if (Endsec.length < 2) {
            "0$Endsec"
        } else {
            Endsec
        }
        val Endmin = c2[Calendar.MINUTE].toString()
        Endminutes = if (Endmin.length < 2) {
            "0$Endmin"
        } else {
            Endmin
        }
        sessionEndTime = "" + c2[Calendar.HOUR_OF_DAY] + ":" + Endminutes + ":" + endsecond
        println("c   $currenttimeinmilliseconds    $extenedetimemilliseconds")
        println("sfjskdfjksjgksdg    $sessionStartTime        $sessionEndTime")
    }

    companion object {
        private val builder = Retrofit.Builder()
                .baseUrl("https://connect.stripe.com/").client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())

        private fun provideOkHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                    .addInterceptor(provideHeaderInterceptor())
                    .connectTimeout(45, TimeUnit.SECONDS)
                    .readTimeout(45, TimeUnit.SECONDS)
                    .writeTimeout(45, TimeUnit.SECONDS)
                    .build()
        }

        private fun provideHeaderInterceptor(): Interceptor {
            return object : Interceptor {
                @Throws(IOException::class)
                override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                    val Authorization = "Bearer sk_live_1L9JXV99ZszWnyJqPwVrixve0008oCyaxd" //LiveKey
                    //val  Authorization = "Bearer sk_test_QWDgYccjhPknQ7Fc8s8ZOG3W00KjAoqX9O";            ///Tesing Key
                    val request = chain.request().newBuilder().addHeader("Authorization", Authorization).build()
                    return chain.proceed(request)
                }
            }
        }

        var retrofit = builder.build()
    }
}