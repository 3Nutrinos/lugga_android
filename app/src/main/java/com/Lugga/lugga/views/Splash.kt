package com.Lugga.lugga.views

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageInfo
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.HomeActivity
import com.Lugga.lugga.LoginActivity
import com.Lugga.lugga.R
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.AppUpdateModel
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import kotlinx.android.synthetic.main.activity_welcome.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Splash : AppCompatActivity() {
    private var versionCode: Int? = 0
    var luggaAPI: LuggaAPI? = null

    var tvCutPrice: TextView? = null

    var usersharedPrefernce: UsersharedPrefernce? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        tvCutPrice = findViewById(R.id.tv11)
        tvCutPrice!!.paintFlags = tvCutPrice!!.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

        usersharedPrefernce = UsersharedPrefernce.getInstance()

    }

    private fun getApplicationVersionCode(versionCode: Int?): String? {
        val verCode: String? = ""
        try {
            val pInfo: PackageInfo = this.packageManager.getPackageInfo(packageName, 0)
            val verCode = pInfo.versionCode
            if (verCode < versionCode!!) {
                showUpdateAppDialog()
            } else {
                tvContinue.setOnClickListener {
                    if (usersharedPrefernce!!.getboolean()) {
                        val intent = Intent(this, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        val intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            }
            Log.d("versionCode", verCode.toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return verCode
    }

    private fun showUpdateAppDialog() {
        val dialog = Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.update_app_dialog);


        dialog.show()

        val btUpdate = dialog.findViewById<Button>(R.id.btUpdate)
        btUpdate.setOnClickListener {
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$packageName")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                    )
                )
            }
            finish()
        }
    }
}