package com.Lugga.lugga.views

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.HomeActivity
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.AdapterHistory
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.learnerhistory.LearnerHistoryBodyItem
import com.Lugga.lugga.model.learnerhistory.LearnerHistoryResponse
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.Loader
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class BookingHistory : AppCompatActivity() {

    var recyclerView: RecyclerView? = null
    var adapterHistory: AdapterHistory? = null
    var learnerHistoryBodyItemArrayList: ArrayList<LearnerHistoryBodyItem>? = null
    var bookingHistoryBackPress: ImageView? = null
    var luggaAPI: LuggaAPI? = null
    var loader: Loader? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.booking_history)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        init()
        onClick()
        learnerHistory
        val linearLayoutManager = LinearLayoutManager(BaseApplication.getContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.hasFixedSize()
    }

    private fun init() {
        bookingHistoryBackPress = findViewById(R.id.bookinHistoryBackPress)
        recyclerView = findViewById(R.id.recyclerBookingHistory)
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        loader = Loader(BaseApplication.getContext())
    }

    private fun onClick() {
        bookingHistoryBackPress!!.setOnClickListener {
            val i = Intent(this@BookingHistory, HomeActivity::class.java)
            startActivity(i)
        }
    }

    //Toast.makeText(BookingHistory.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
    private val learnerHistory: Unit
        private get() {
            learnerHistoryBodyItemArrayList = ArrayList()
            val learner_id: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getlearnerid().toString())
            luggaAPI!!.getlearnerhistory(learner_id).enqueue(object : Callback<LearnerHistoryResponse?> {
                override fun onResponse(call: Call<LearnerHistoryResponse?>, response: Response<LearnerHistoryResponse?>) {
                    if (response.body() != null) {
                        if (response.isSuccessful && response.body()!!.success == 200) {
                            learnerHistoryBodyItemArrayList = response.body()!!.body
                            adapterHistory = AdapterHistory(BaseApplication.getContext(), learnerHistoryBodyItemArrayList!!)
                            recyclerView!!.adapter = adapterHistory
                            //Toast.makeText(BookingHistory.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this@BookingHistory, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }

                override fun onFailure(call: Call<LearnerHistoryResponse?>, t: Throwable) {
                    Toast.makeText(this@BookingHistory, "" + t.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            })
        }
}