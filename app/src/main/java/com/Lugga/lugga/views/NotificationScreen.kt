package com.Lugga.lugga.views

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.HomeActivity
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.AdapterNotifications
import com.Lugga.lugga.adapters.AdapterNotifications.onBookingUpdate
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.bookingStatus.BookingStatusResponse
import com.Lugga.lugga.model.usersnotification.UsersNotificationBodyItem
import com.Lugga.lugga.model.usersnotification.UsersNotificationResponse
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.Loader
import com.Lugga.lugga.utils.SocketNetworking
import com.Lugga.lugga.views.RecyclerTouchListener.OnRowClickListener
import com.Lugga.lugga.views.RecyclerTouchListener.OnSwipeOptionsClickListener
import io.socket.client.Socket
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class NotificationScreen : AppCompatActivity(), onBookingUpdate {
    private var learnerId: Int?=0
    private var socket: Socket?=null
    private var recyclerNotification: RecyclerView? = null///////////////////////////////////////////////////////////////////////////
    private var mAdapter: AdapterNotifications? = null
    private var touchListener: RecyclerTouchListener? = null
    var notificaionBackPress: ImageView? = null
    var luggaAPI: LuggaAPI? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var loader: Loader? = null
    var usersNotificationBodyItemArrayList: ArrayList<UsersNotificationBodyItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_screen)
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket!!.connect()
        usersNotificationBodyItemArrayList = ArrayList()
        init()
        onClick()



        usersNotification
    }

    private fun getNotificationListing() {
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            jsonObject.put("user_id", usersharedPrefernce!!.getuserid().toString())
            jsonObject.put("user_type", "2")
            socket?.emit("HomepageSocket", jsonObject)

            getNotificationList()
        } else {
            socket?.connect()
            val jsonObject = JSONObject()
            jsonObject.put("user_id", usersharedPrefernce!!.getuserid().toString())
            jsonObject.put("user_type", "2")
            socket?.emit("HomepageSocket", jsonObject)
            getNotificationListing()

        }
    }

    private fun getNotificationList() {

    }

    private fun init() {
        loader = Loader(this)
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        recyclerNotification = findViewById(R.id.recyclerNotification)
        notificaionBackPress = findViewById(R.id.notificationBackPress)
    }

    private fun onClick() {
        touchListener = RecyclerTouchListener(this@NotificationScreen, recyclerNotification!!)
        touchListener!!
                .setClickable(object : OnRowClickListener {
                    override fun onRowClicked(position: Int) {}
                    override fun onIndependentViewClicked(independentViewID: Int, position: Int) {}
                })
                .setSwipeOptionViews(R.id.ivDelete)
                .setSwipeable(R.id.llSlideLeft, R.id.ivDelete, object : OnSwipeOptionsClickListener {
                    override fun onSwipeOptionClicked(viewID: Int, position: Int) {
                        when (viewID) {
                            R.id.ivDelete -> //                                taskList.remove(position);
//                                recyclerviewAdapter.setTaskList(taskList);
                                mAdapter!!.notifyItemRemoved(position)
                        }
                    }
                })
        notificaionBackPress!!.setOnClickListener {
            val i = Intent(this@NotificationScreen, HomeActivity::class.java)
            startActivity(i)
        }
    }

    public override fun onResume() {
        super.onResume()
        recyclerNotification!!.addOnItemTouchListener(touchListener!!)
    }//Toast.makeText(NotificationScreen.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

    // Toast.makeText(NotificationScreen.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
    private val usersNotification: Unit
        private get() {
            loader!!.isShowProgress()
            val user_id: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getuserid().toString())
            luggaAPI!!.getusersnotification(user_id).enqueue(object : Callback<UsersNotificationResponse?> {
                override fun onResponse(call: Call<UsersNotificationResponse?>, response: Response<UsersNotificationResponse?>) {
                    if (response.body() != null) {
                        if (response.isSuccessful && response.body()!!.success == 200) {
                            loader!!.isDismiss()
                            usersNotificationBodyItemArrayList = response.body()!!.body
                            mAdapter = AdapterNotifications(applicationContext, usersNotificationBodyItemArrayList!!)
                            mAdapter!!.setOnClickListener(this@NotificationScreen)
                            recyclerNotification!!.adapter = mAdapter
                            // Toast.makeText(NotificationScreen.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            loader!!.isDismiss()
                            //Toast.makeText(NotificationScreen.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                override fun onFailure(call: Call<UsersNotificationResponse?>, t: Throwable) {
                    loader!!.isDismiss()
                    Toast.makeText(this@NotificationScreen, t.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            })
        }

    override fun onbookingupdate(position: Int, status: String?, BookingID: String?, leasrnerID: Int) {
     learnerId=leasrnerID
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            jsonObject.put("bookingID", BookingID)
            jsonObject.put("bookingStatus", status)
            socket?.emit("teacherBookingUpdate", jsonObject)

            getReturn(BookingID)
        } else {
            socket?.connect()
            val jsonObject = JSONObject()
            jsonObject.put("bookingID", BookingID)
            jsonObject.put("bookingStatus", status)
            socket?.emit("teacherBookingUpdate", jsonObject)

            getReturn(BookingID)
        }
//        val bookingId: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), BookingID!!)
//        val status1: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), status!!)
//
//        luggaAPI!!.updateBookingStatus(bookingId, status1).enqueue(object : Callback<BookingStatusResponse> {
//            override fun onResponse(call: Call<BookingStatusResponse>, response: Response<BookingStatusResponse>) {
//                if (response.isSuccessful && response.body()!!.success == 200) {
//                    Toast.makeText(this@NotificationScreen, response.body()!!.message, Toast.LENGTH_SHORT).show()
//
//
//                    val jsonObject = JSONObject()
//                    jsonObject.put("user_id", usersharedPrefernce!!.getuserid().toString())
//                    jsonObject.put("user_type","2")
//                    socket?.emit("HomepageSocket", jsonObject)
//
//
//                    socket?.on("returnHomepageSocket" + BookingID){
//
//                    }
//
//                    usersNotification
//                } else {
//                    Toast.makeText(this@NotificationScreen, response.body()!!.message, Toast.LENGTH_SHORT).show()
//                }
//            }
//
//            override fun onFailure(call: Call<BookingStatusResponse>, t: Throwable) {
//                Toast.makeText(this@NotificationScreen, t.message, Toast.LENGTH_SHORT).show()
//            }
//        })
    }

    private fun getReturn(bookingID: String?) {
        if (socket!!.connected()) {
            socket!!.on("returnTeacherBookingUpdate" + bookingID) { args ->
                runOnUiThread {
                    val jsonObject=JSONObject()
                    jsonObject.put("id", learnerId.toString())
                    jsonObject.put("user_type", "2")
                    socket?.emit("HomepageSocket", jsonObject)
                    usersNotification
                }

            }
        }
    }

    override fun onClickDeleteNotification(notificationID: Int?) {
        if (notificationID == null) {
            return
        }
        val notificationIdd: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), notificationID.toString())
        luggaAPI!!.deleteNotification(notificationIdd).enqueue(object : Callback<BookingStatusResponse> {
            override fun onResponse(call: Call<BookingStatusResponse>, response: Response<BookingStatusResponse>) {
                if (response.isSuccessful && response.body()!!.success == 200) {
                    Toast.makeText(this@NotificationScreen, response.body()!!.message, Toast.LENGTH_SHORT).show()
                    usersNotification
                } else {
                    Toast.makeText(this@NotificationScreen, response.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<BookingStatusResponse>, t: Throwable) {
                Toast.makeText(this@NotificationScreen, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}