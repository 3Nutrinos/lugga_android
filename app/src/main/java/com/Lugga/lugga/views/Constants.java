package com.Lugga.lugga.views;

public class Constants {
    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    public static final String  NOTIF_CHANNEL_DEFAULT = "Notifications";
    public static final String  KEY_TITLE = "title";

    public static final String KEY_SILVER =  "silver_pkg";
    public static final String KEY_BRONZE =  "bronze";
    public static final String KEY_GOLD =  "gold_pkg";
    public static final String BASE64_LICENSE =  "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApliMctatlMrwbSJjpynHWC8CpnmLM//8BS5mz9Ys5pZc4mdikoy32ec0THiefo+V4pbwihFlNQS5AMs0Ak4zyCXRTmSa5IWQDJ0Z3/tnt0L4PYBhAdyHYCIv2PrqRmV8xyYxcCOZvzKaSwSSofwmrP5tb4Oe8t7RKzF+nlya3XiP0kJfB9Tzz+1RH4ChZTpeYqITYkrEWRX8sJeD+skDMRZYmr9yKB0SUwXVY1q7P0sZkqHVurWYL+3FlrUJ5fU6mWReFxJrzSDU1KmCPceIpLd1oYncu6x3by4nPsINqgVrIya0QDp1PDx2Lq3Gd5n1DrNeUecs/Ip1YNJclYJ96wIDAQAB";

}
