package com.Lugga.lugga.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.AdapterHistory

class History : AppCompatActivity() {
    var recycleHistory: RecyclerView? = null
    private val mAdapter: AdapterHistory? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        recycleHistory = findViewById(R.id.recycleHistory)

//        mAdapter = new AdapterHistory(getApplicationContext());
        recycleHistory?.adapter = mAdapter
    }
}