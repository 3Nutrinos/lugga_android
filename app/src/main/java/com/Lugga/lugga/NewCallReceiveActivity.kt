package com.Lugga.lugga

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.model.CallingDataModel
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.utils.FirebaseNotification
import com.Lugga.lugga.utils.SocketNetworking
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_new_call_receive.*
import org.json.JSONObject


class NewCallReceiveActivity : AppCompatActivity(), View.OnClickListener {
    private var fromIntroScreen: String?=null
    private var socket: Socket? = null
    var callerId: String? = null
    var receiverId: String? = null
    var callerName: String? = null
    var userType: String? = null
    var callType: String? = null
    var senderName: String? = null
    var bookingId: String? = null
    var accessToken: String? = null
    var channelId: String? = null


    val broadcastListener = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            finish()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_call_receive)
        BaseApplication.fullScreen(this)

        //  setRingTone()
        connectSocket()
        getData()
        checkSocketForCallCut()
        ivNewAcceptCall.setOnClickListener(this)
        ivNewRejectCall.setOnClickListener(this)
        val animShake = AnimationUtils.loadAnimation(this, R.anim.shake)
        ivNewAcceptCall.animation = animShake

    }

    override fun onResume() {
        registerReceiver(broadcastListener, IntentFilter("finishCallIncomingActivity"))
        super.onResume()
    }

    private fun checkSocketForCallCut() {
        if (socket!!.connected()) {
            Log.d("isFinish", "3")
            socket!!.on("returncallStatusRequest" + bookingId) { args ->
                runOnUiThread {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    try {
                        Log.d("isFinish", "1")
                        finish()
                    } catch (e: Exception) {
                        Log.d("isFinish", "2")
                        e.printStackTrace()
                    }
                }


            }
        }
    }

    private fun getData() {
        val intent = intent ?: return
        if (intent.getStringExtra("channel_id") != null) {
            channelId = intent.getStringExtra("channel_id")
            callerId = intent.getStringExtra("caller_id")
            receiverId = intent.getStringExtra("receiver_id")
            callerName = intent.getStringExtra("caller_name")
            userType = intent.getStringExtra("user_type")
            callType = intent.getStringExtra("call_type")
            senderName = intent.getStringExtra("user_name")
            bookingId = intent.getStringExtra("booking_id")
            accessToken = intent.getStringExtra("access_token")
            fromIntroScreen= intent.getStringExtra("fromIntroScreen")
        }

        setData()

    }

    private fun setData() {
        if (callType.equals("1")) {
            tvNewCallType.text = getString(R.string.audiocall)
        } else {
            tvNewCallType.text = getString(R.string.videocall)
        }
        tvNewCallerName.text = callerName
    }


    private fun connectSocket() {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            ivNewRejectCall -> {
                emitForCallCutResponse("3")
                finish()
            }
            ivNewAcceptCall -> {
                val callingDataModel = CallingDataModel()
                callingDataModel.Channel_id = channelId
                callingDataModel.caller_id = callerId
                callingDataModel.receiver_id = receiverId
                callingDataModel.user_type = userType
                callingDataModel.call_type = callType
                callingDataModel.booking_id = bookingId
                callingDataModel.token = accessToken
                callingDataModel.caller_name = callerName
                callingDataModel.from_intro_screen=fromIntroScreen

                val callIntent: Intent
                if (callingDataModel.call_type == "2") { //For VideoCall
                    callIntent = Intent(this, VideoCallReceiveActivity::class.java)
                } else {
                    callIntent = Intent(this, AudioCallReceiveActivity::class.java)
                }
                callIntent.putExtra("callingData", callingDataModel)
                startActivity(callIntent)
                finish()
            }
        }

    }


    private fun emitForCallCutResponse(status: String) {
        if (socket!!.connected()) {
            val obj = JSONObject()
            obj.put("user_type", userType)
            obj.put("call_type", callType)
            obj.put("caller_id", callerId)
            obj.put("receiever_id", receiverId)
            obj.put("booking_id", bookingId)
            obj.put("time_count", "0")
            obj.put("status", status)
            obj.put("push_send", "0")
            obj.put("endCall", "receiver")
            socket!!.emit("callStatusRequest", obj)
        } else {
            socket!!.connect()
        }

    }

}