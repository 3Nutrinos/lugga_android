package com.Lugga.lugga.newchatcall

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaRecorder
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.AudioRecorderActivity
import com.Lugga.lugga.R
import com.Lugga.lugga.fragments.ImageLoadFragment
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.bookingStatus.BookingStatusResponse
import com.Lugga.lugga.model.chatList.ChatListBodyItem
import com.Lugga.lugga.model.chatList.ChatListResponse
import com.Lugga.lugga.model.learnerhomepage.LearnerHomePageBodyItem
import com.Lugga.lugga.model.teacherhomepage.TeacherHomePageBodyItem
import com.Lugga.lugga.newchatcall.adapter.NewChatAdapter
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.utils.AudioRecordView
import com.Lugga.lugga.utils.SocketNetworking
import com.google.gson.Gson
import com.joypixels.tools.Client
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_new_chat.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class ChatShowActivity : AppCompatActivity(), View.OnClickListener, AudioRecordView.RecordingListener, NewChatAdapter.Listener {
    private var audioId: String? = null
    private var audioPath: String? = null
    private var audioRecordView: AudioRecordView? = null
    private var selectedfile: String? = null
    private val GALLERY = 11
    private val CAMERA_MIC_PERMISSION_REQUEST_CODE = 111
    private var userType: String? = null
    private var callType: String? = null
    private var callerId: String? = null
    private var receiverId: Int? = 0
    private var usersharedPrefernce: UsersharedPrefernce? = null
    private var chatListBodyItemArrayList: ArrayList<ChatListBodyItem>? = null
    private var mills: Long? = 0
    private var socket: Socket? = null
    private var BookingId: String? = null
    private var position: Int? = 0
    private var iscomingfrom: String? = null
    var adapter: NewChatAdapter? = null
    var list: ArrayList<ChatListBodyItem>? = null
    var learnerHomePageBodyItemArrayList: java.util.ArrayList<LearnerHomePageBodyItem>? = null
    var teacherHomePageBodyItemArrayList: java.util.ArrayList<TeacherHomePageBodyItem>? = null
    var isShow: Boolean? = false
    private var body: MultipartBody.Part? = null
    var client: Client? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_chat)
        if (!checkPermissionForCameraAndMicrophone()) {
            requestPermissionForCameraAndMicrophone()
        } else {
            //showPictureDialog()
        }
        client = Client(this)
        client!!.isShortcodes = true // convert shortcodes? :joy:
        audioRecordView = AudioRecordView()
        audioRecordView!!.setRecordingListener(this)
        // this is to make your layout the root of audio record view, root layout supposed to be empty..
        audioRecordView!!.initView(findViewById(R.id.container))
        setListener()
        progressBar.visibility = View.VISIBLE
        list = ArrayList()
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        connectSocket()
        getIntentData()
        emitForgetMessage()

        ivChatBack.setOnClickListener(this)
        ivNewAudioCall.setOnClickListener(this)
        imageViewAudio.setOnClickListener(this)
        ivNewVideoCall.setOnClickListener(this)


    }

    private fun emitForgetMessage() {
        if (socket!!.connected()) {
            val jsonObject: JSONObject = JSONObject()
            jsonObject.put("booking_id", BookingId)
            socket!!.emit("all_Messages_List", jsonObject)
            getMessageList()
        } else {
            socket!!.connect()
            val jsonObject: JSONObject = JSONObject()
            jsonObject.put("booking_id", BookingId)
            socket!!.emit("all_Messages_List", jsonObject)
            getMessageList()
        }
    }

    private fun getMessageList() {
        if (socket!!.connected()) {
            socket!!.on("returnAll_Messages_List" + BookingId) { args ->
                val msgresponse = args[0].toString()
                val gson = Gson()
                val chatListResponse = gson.fromJson(msgresponse, ChatListResponse::class.java)
                chatListBodyItemArrayList = chatListResponse.body
                runOnUiThread { setMessageListdata() }
            }
        } else {
            socket!!.connect()
            progressBar.visibility = View.GONE
            finish()
            Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setMessageListdata() {
        progressBar.visibility = View.GONE
        rlchatLayout.visibility = View.VISIBLE
        adapter = NewChatAdapter(this, this, this, chatListBodyItemArrayList!!)
        rvNewChat.adapter = adapter
    }

    private fun connectSocket() {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket!!.connect()
    }

    private fun getIntentData() {
        val intentData = getIntent()
        iscomingfrom = intentData?.getStringExtra("iscomingfrom")
        position = intentData?.getIntExtra("position1", 0)!!
//        if (intentData.getStringExtra("iscomingfrom") != null) {
//            if (iscomingfrom == "1") {
//                learnerHomePageBodyItemArrayList = intentData.getSerializableExtra("learnerlist1") as java.util.ArrayList<LearnerHomePageBodyItem>  //fromTeacher
//                BookingId = learnerHomePageBodyItemArrayList!![position!!].getBookingId()
//            } else {
//                teacherHomePageBodyItemArrayList = intentData.getSerializableExtra("learnerlist1") as java.util.ArrayList<TeacherHomePageBodyItem>   //fromLearner
//                BookingId = teacherHomePageBodyItemArrayList!![position!!].id
//            }
//        }
        BookingId = intent?.getStringExtra("bookingId").toString()
        if (learnerHomePageBodyItemArrayList?.get(position!!)?.getProfileImage() != null) {
            usersharedPrefernce?.receiverProfileimage = learnerHomePageBodyItemArrayList?.get(position!!)?.getProfileImage()
        }

        val status = intent.getIntExtra("status", 0)
        if (status != 5) {
            llCalls?.setVisibility(View.GONE)
            recording?.setVisibility(View.GONE)
            tvTime?.setVisibility(View.GONE)
            imageViewAudio?.visibility = View.GONE
        } else {
            llCalls?.setVisibility(View.VISIBLE)
            tvTime?.setVisibility(View.VISIBLE)
            recording?.setVisibility(View.VISIBLE)
            imageViewAudio?.visibility = View.VISIBLE

        }
        mills = intentData.getLongExtra("mills", 0)
        val countDownTimer: CountDownTimer = object : CountDownTimer(mills!!, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                tvTime!!.text = "" + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))
            }

            override fun onFinish() {
                finish()
            }
        }
        if (tvTime!!.visibility == View.VISIBLE) {
            countDownTimer.start()
        }

        if (iscomingfrom != null) {
            if (iscomingfrom == "1") {
                tvHeadingName.setText(learnerHomePageBodyItemArrayList!![position!!].getName())
            } else {
                tvHeadingName?.setText(teacherHomePageBodyItemArrayList!![position!!].name)
            }
        }
    }


    override fun onClick(v: View?) {
        when (v) {

            ivChatBack -> {
                finish()
            }
            ivNewAudioCall -> {
                progressBar.visibility = View.VISIBLE
                ivNewAudioCall.isEnabled = false
                startAudioCall()

            }
            ivNewVideoCall -> {
                Toast.makeText(this, "Under Development", Toast.LENGTH_SHORT).show()
                //startAudioCall()
            }
            imageViewAudio -> {
                val intent = Intent(this, AudioRecorderActivity::class.java);
                startActivityForResult(intent, 201)
                // val intent = Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION)
                //startActivityForResult(intent, Companion.RQS_RECORDING)
            }
        }
    }


    private fun sendMessage(messageType: String) {
        if (socket!!.connected()) {
            val jsonObject = JSONObject()
            jsonObject.put("booking_id", BookingId)
            if (usersharedPrefernce!!.getusertype() == 1) {
                jsonObject.put("sender_id", usersharedPrefernce!!.getteacherid())
                if (iscomingfrom == "1") {
                    jsonObject.put("receiver_id", learnerHomePageBodyItemArrayList!![position!!].getLearnerId())
                } else {
                    jsonObject.put("receiver_id", teacherHomePageBodyItemArrayList!![position!!].learnerId)
                }
            } else {
                jsonObject.put("sender_id", usersharedPrefernce!!.getlearnerid())
                if (iscomingfrom == "1") {
                    jsonObject.put("receiver_id", learnerHomePageBodyItemArrayList!![position!!].getTeacherId())
                } else {
                    jsonObject.put("receiver_id", teacherHomePageBodyItemArrayList!![position!!].learnerId)
                }
            }
            if (messageType.equals("3")) {
                jsonObject.put("message", selectedfile)
            } else if (messageType.equals("2")) {
                jsonObject.put("message", audioPath)
                jsonObject.put("msgId", audioId)
            } else {
                val toServer = client!!.toShortname(editTextMessage.text.toString())
                jsonObject.put("message", toServer)
            }

            jsonObject.put("message_type", messageType)

            socket!!.emit("Chat_SendMessage", jsonObject)
            if (isShow == false) {
                isShow = true
                getMessageSentInReturn()
            }

        } else {
            socket!!.connect()
        }
    }

    private fun getMessageSentInReturn() {
        if (socket!!.connected()) {
            socket!!.on("returnChat_SendMessage" + BookingId) { args ->
                runOnUiThread {
                    progressBar.visibility = View.GONE
                    val msg = Message()
                    val jobj = args[0] as JSONObject

                    msg.obj = jobj
                    var Jarray: JSONArray? = null
                    try {
                        Jarray = jobj.getJSONArray("body")
                        val `object` = Jarray.getJSONObject(0)
                        val item = ChatListBodyItem()
                        item.id = `object`.getInt("id")
                        item.bookingId = `object`.getInt("booking_id")
                        item.senderId = `object`.getInt("sender_id")
                        item.receiverId = `object`.getInt("receiver_id")
                        item.message = `object`.getString("message")
                        item.messageType = `object`.getInt("message_type") //

                        val format: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
                        val date = format.parse(`object`.getString("created_at"))
                        item.createdAt = date

                        val insertIndex = chatListBodyItemArrayList!!.size
                        chatListBodyItemArrayList!!.add(insertIndex, item)
                        adapter!!.notifyItemInserted(insertIndex)
                        val itemAnimator: RecyclerView.ItemAnimator = DefaultItemAnimator()
                        itemAnimator.addDuration = 200
                        rvNewChat!!.itemAnimator = itemAnimator
                        rvNewChat!!.smoothScrollToPosition(chatListBodyItemArrayList!!.size)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                }
            }
        } else {
            socket!!.connect()
            Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
        }
    }


    private fun startAudioCall() {                                     //emit socket to start a audio call
        if (socket!!.connected()) {
            val obj: JSONObject = JSONObject()
            obj.put("call_type", "1")
            callType = "1"
            obj.put("Channel_id", generateString(20))
            if (iscomingfrom == "1") {
                obj.put("booking_id", learnerHomePageBodyItemArrayList!![position!!].getBookingId())
            } else {
                obj.put("booking_id", teacherHomePageBodyItemArrayList!![position!!].id)
            }
            if (usersharedPrefernce!!.getusertype() == 1) {
                obj.put("caller_id", usersharedPrefernce!!.getteacherid())
                callerId = obj.getString("caller_id")
                obj.put("user_type", "2")
                userType = "2"
                if (iscomingfrom == "1") {
                    receiverId = learnerHomePageBodyItemArrayList!![position!!].getLearnerId()
                    obj.put("receiver_id", learnerHomePageBodyItemArrayList!![position!!].getLearnerId())
                } else {
                    receiverId = teacherHomePageBodyItemArrayList!![position!!].learnerId
                    obj.put("receiver_id", teacherHomePageBodyItemArrayList!![position!!].learnerId)
                }
            } else {
                obj.put("caller_id", usersharedPrefernce!!.getlearnerid())
                callerId = obj.getString("caller_id")
                obj.put("user_type", "1")
                userType = "1"
                if (iscomingfrom == "1") {
                    receiverId = learnerHomePageBodyItemArrayList!![position!!].getTeacherId()
                    obj.put("receiver_id", learnerHomePageBodyItemArrayList!![position!!].getTeacherId())
                } else {
                    receiverId = teacherHomePageBodyItemArrayList!![position!!].teacherId
                    obj.put("receiver_id", teacherHomePageBodyItemArrayList!![position!!].teacherId)
                }
            }

            socket!!.emit("callRequest", obj)
            getReturnForAudioCallSocket(obj.getString("Channel_id"))


        } else {
            socket!!.connect();
            if (socket!!.connected()) {
                val obj: JSONObject = JSONObject()
                obj.put("call_type", "1")
                callType = "1"
                obj.put("Channel_id", generateString(20))
                if (iscomingfrom == "1") {
                    obj.put("booking_id", learnerHomePageBodyItemArrayList!![position!!].getBookingId())
                } else {
                    obj.put("booking_id", teacherHomePageBodyItemArrayList!![position!!].id)
                }
                if (usersharedPrefernce!!.getusertype() == 1) {
                    obj.put("caller_id", usersharedPrefernce!!.getteacherid())
                    callerId = obj.getString("caller_id")
                    obj.put("user_type", "2")
                    userType = "2"
                    if (iscomingfrom == "1") {
                        receiverId = learnerHomePageBodyItemArrayList!![position!!].getLearnerId()
                        obj.put("receiver_id", learnerHomePageBodyItemArrayList!![position!!].getLearnerId())
                    } else {
                        receiverId = teacherHomePageBodyItemArrayList!![position!!].learnerId
                        obj.put("receiver_id", teacherHomePageBodyItemArrayList!![position!!].learnerId)
                    }
                } else {
                    obj.put("caller_id", usersharedPrefernce!!.getlearnerid())
                    callerId = obj.getString("caller_id")
                    obj.put("user_type", "1")
                    userType = "1"
                    if (iscomingfrom == "1") {
                        receiverId = learnerHomePageBodyItemArrayList!![position!!].getTeacherId()
                        obj.put("receiver_id", learnerHomePageBodyItemArrayList!![position!!].getTeacherId())
                    } else {
                        receiverId = teacherHomePageBodyItemArrayList!![position!!].teacherId
                        obj.put("receiver_id", teacherHomePageBodyItemArrayList!![position!!].teacherId)
                    }
                }
                socket!!.emit("callRequest", obj)
                getReturnForAudioCallSocket(obj.getString("Channel_id"))
            }
        }
    }


    private fun getReturnForAudioCallSocket(channelId: String) {
        socket!!.on("returncallRequest" + BookingId) { args ->
            val obj = args[0] as JSONObject
            if (obj.getInt("success") == 200) {
                runOnUiThread(Runnable {
                    progressBar.visibility = View.GONE
                })
               // val intent: Intent = Intent(this, CallingActivity::class.java)
                intent.putExtra("channelId", obj.getString("channel_id"))
                Log.e("cId", channelId.toString())
                intent.putExtra("bookingId", BookingId.toString())
                intent.putExtra("receiverId", receiverId.toString())
                intent.putExtra("callerId", callerId)
                intent.putExtra("callType", callType)
                intent.putExtra("userType", userType)
                startActivity(intent)
                finish()
            } else {
                runOnUiThread(Runnable {
                    Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
                })
            }

        }
    }

    fun generateString(length: Int): String {
        val random = java.util.Random()
        val builder = StringBuilder(length)
        for (i in 0 until length) {
            builder.append(ALPHABET[random.nextInt(ALPHABET.length)])
        }
        return builder.toString()
    }

    companion object {
        private const val ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        private const val RQS_RECORDING = 1234
    }

    private fun checkPermissionForCameraAndMicrophone(): Boolean {
        val resultCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val resultExternal = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return resultCamera == PackageManager.PERMISSION_GRANTED && resultExternal == PackageManager.PERMISSION_GRANTED && resultMic == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissionForCameraAndMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    CAMERA_MIC_PERMISSION_REQUEST_CODE)
            //requestPermissionForCameraAndMicrophone();
        } else {
            ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    CAMERA_MIC_PERMISSION_REQUEST_CODE)
            // createAudioAndVideoTracks();
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_MIC_PERMISSION_REQUEST_CODE) {
            var cameraAndMicPermissionGranted = true
            for (grantResult in grantResults) {
                cameraAndMicPermissionGranted = cameraAndMicPermissionGranted and (grantResult == PackageManager.PERMISSION_GRANTED)
            }
            if (cameraAndMicPermissionGranted) {
                showPictureDialog()
            } else {
                Toast.makeText(this,
                        R.string.permissions_needed,
                        Toast.LENGTH_LONG).show()
                this.finish()
            }
        }
    }


    private fun showPictureDialog() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library",
                "Cancel")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            val userChoosenTask: String
            if (items[item] == "Take Photo") {
                userChoosenTask = "Take Photo"
                cameraIntent()
            } else if (items[item] == "Choose from Library") {
                userChoosenTask = "Choose from Library"
                galleryIntent()
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT //
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY)
    }

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, MediaRecorder.VideoSource.CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                val resultUri = Uri.parse(contentURI.toString())
                var bitmap: Bitmap? = null
                try {
                    bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(resultUri))
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
                progressBar.visibility = View.VISIBLE
                ConvertBitmapToString(bitmap)


            }
        } else if (requestCode == 201) {
            if (resultCode == Activity.RESULT_OK) {
                progressBar.visibility = View.VISIBLE
                // Great! User has recorded and saved the audio file
                var audioNewFile: String? = null
                audioNewFile = data?.extras?.getString("audioUrl")
                hitAudioRecordApi(audioNewFile.toString())
            }

        } else if (requestCode == MediaRecorder.VideoSource.CAMERA) {
            val thumbnail = data!!.extras!!["data"] as Bitmap?
            val tempUri = getImageUri(this, thumbnail)

            var bitmap: Bitmap? = null
            try {
                bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(tempUri))
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            progressBar.visibility = View.VISIBLE
            ConvertBitmapToString(bitmap)
        }
    }

    private fun getImageUri(applicationContext: Context, photo: Bitmap?): Uri {
        val bytes = ByteArrayOutputStream()
        photo!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(this.getContentResolver(), photo, "Title" + System.currentTimeMillis(), null)
        return Uri.parse(path)
    }

    fun ConvertBitmapToString(bitmap: Bitmap?) {
        var encodedImage = ""
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
        selectedfile = encodedImage;
        sendMessage("3")
    }

    private fun setListener() {

        audioRecordView!!.cameraView.setOnClickListener {
            audioRecordView!!.hideAttachmentOptionView()
            if (!checkPermissionForCameraAndMicrophone()) {
                requestPermissionForCameraAndMicrophone()
            } else {
                showPictureDialog()
            }

        }
        audioRecordView!!.sendView.setOnClickListener {
            val msg = audioRecordView!!.messageView.text.toString().trim()
            sendMessage("1")
            audioRecordView!!.messageView.setText("")


        }
    }

    override fun onRecordingCanceled() {
        Toast.makeText(this, "record canceled", Toast.LENGTH_SHORT).show()
    }

    override fun onRecordingStarted() {
        Toast.makeText(this, "record started", Toast.LENGTH_SHORT).show()
    }

    override fun onRecordingLocked() {
        Toast.makeText(this, "record locked", Toast.LENGTH_SHORT).show()
    }

    override fun onRecordingCompleted(fileName: String?) {

    }

    private fun hitAudioRecordApi(fileName: String) {

        val file1 = File(fileName)
        val requestFile: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file1)
        body = MultipartBody.Part.createFormData("message", file1.name, requestFile)
        var bookingId: RequestBody? = null

        bookingId = if (iscomingfrom == "1") {
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position!!].getBookingId().toString())
        } else {
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position!!].id.toString())
        }

        val senderId: RequestBody
        var receiver_id: RequestBody? = null

        if (usersharedPrefernce!!.getusertype() == 1) {
            senderId = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getteacherid().toString())
            receiver_id = if (iscomingfrom == "1") {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position!!].getTeacherId().toString())
            } else {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position!!].learnerId.toString())
            }
        } else {
            senderId = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), usersharedPrefernce!!.getlearnerid().toString())
            receiver_id = if (iscomingfrom == "1") {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), learnerHomePageBodyItemArrayList!![position!!].getTeacherId().toString())
            } else {
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), teacherHomePageBodyItemArrayList!![position!!].learnerId.toString())
            }
        }
        val message_type: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "2")



        ApiClient.getApiClient().create(LuggaAPI::class.java).sendVideo(bookingId, senderId, receiver_id, body, message_type).enqueue(object : Callback<BookingStatusResponse> {
            override fun onResponse(call: Call<BookingStatusResponse>, response: Response<BookingStatusResponse>) {
                if (response != null && response.body() != null && response.isSuccessful) {
                    if (response.body()?.body?.size!! > 0) {
                        audioId = response.body()!!.body!![0].id.toString()
                        audioPath = response.body()!!.body!![0].message.toString()
                        sendMessage("2")
                    } else {
                        Toast.makeText(this@ChatShowActivity, response.body()?.message, Toast.LENGTH_SHORT).show()
                    }

                } else {
                    Toast.makeText(this@ChatShowActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<BookingStatusResponse>, t: Throwable) {
                if (t is UnknownHostException) {
                    Toast.makeText(this@ChatShowActivity, "No Internet Connection", Toast.LENGTH_SHORT).show()
                } else if (t is SocketTimeoutException) {
                    Toast.makeText(this@ChatShowActivity, "Server is not responding. Please try again", Toast.LENGTH_SHORT).show()
                } else if (t is ConnectException) {
                    Toast.makeText(this@ChatShowActivity, "Failed to connect server", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@ChatShowActivity, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }


    override fun onClickImage(message: String?) {
        if (message == null) {
            return
        }
        val bundle = Bundle()
        bundle.putString("imageUrl", message)
        val fragment: Fragment = ImageLoadFragment()
        fragment.arguments = bundle
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("")
                .commit()
    }


}
