package com.Lugga.lugga.subscription;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.Lugga.lugga.HomeActivity;
import com.Lugga.lugga.R;
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionActivity extends AppCompatActivity implements View.OnClickListener {

    // PRODUCT & SUBSCRIPTION IDS


    public static final String KEY_SILVER = "silver_pkg";
    public static final String KEY_BRONZE = "bronze";
    public static final String KEY_GOLD = "gold_pkg";

    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApliMctatlMrwbSJjpynHWC8CpnmLM//8BS5mz9Ys5pZc4mdikoy32ec0THiefo+V4pbwihFlNQS5AMs0Ak4zyCXRTmSa5IWQDJ0Z3/tnt0L4PYBhAdyHYCIv2PrqRmV8xyYxcCOZvzKaSwSSofwmrP5tb4Oe8t7RKzF+nlya3XiP0kJfB9Tzz+1RH4ChZTpeYqITYkrEWRX8sJeD+skDMRZYmr9yKB0SUwXVY1q7P0sZkqHVurWYL+3FlrUJ5fU6mWReFxJrzSDU1KmCPceIpLd1oYncu6x3by4nPsINqgVrIya0QDp1PDx2Lq3Gd5n1DrNeUecs/Ip1YNJclYJ96wIDAQAB";
    ;

    // PUT YOUR MERCHANT KEY HERE;
    // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
    // if filled library will provide protection against Freedom alike Play Market simulators

    public static final String MERCHANT_ID = "01610008143915814606";
    private BillingProcessor bp;
    private boolean readyToPurchase = false;
    private SkuDetails skuDetails;

    private TextView packagePriceTxt, packageDurationTxt, tvFree, tvFreeThreeMonths;
    private ImageView packageTypeImg,subBackPress;
    private Button btBronze, btSilver, btGold, btBuySubsciption;
    private String selectedSubscription = KEY_BRONZE;
    UsersharedPrefernce usersharedPrefernce;
    private List<SkuDetails> priceListing;
    private ProgressBar progressBarSubs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        usersharedPrefernce = UsersharedPrefernce.getInstance();
        progressBarSubs=findViewById(R.id.progressBarSubs);
        subBackPress=findViewById(R.id.subBackPress);
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Toast.makeText(this, "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16", Toast.LENGTH_SHORT).show();
        }
        priceListing=new ArrayList<>();

        bp = new BillingProcessor(this, LICENSE_KEY, MERCHANT_ID, new BillingProcessor.IBillingHandler() {
            @Override
            public void onProductPurchased(String productId, PurchaseInfo purchaseInfo) {

                runOnUiThread(() -> {
                    Toast.makeText(SubscriptionActivity.this, "Subscription has been purchased successfully !!", Toast.LENGTH_SHORT).show();
                    usersharedPrefernce.setboolean(true);
                    Intent intent = new Intent(SubscriptionActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finishAffinity();
                });

            }

            @Override
            public void onBillingError(int errorCode, Throwable error) {
                Toast.makeText(SubscriptionActivity.this, "error in Purchased !!  " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onBillingInitialized() {
                readyToPurchase = true;

                ArrayList<String> productIdList = new ArrayList<>();
                productIdList.add(KEY_BRONZE);
                productIdList.add(KEY_SILVER);
                productIdList.add(KEY_GOLD);


                bp.getSubscriptionsListingDetailsAsync(productIdList, new BillingProcessor.ISkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(List<SkuDetails> products) {

                        Log.d("dataDetails", new Gson().toJson(products)+" isSubs>>>"+String.valueOf(bp.isSubscribed(selectedSubscription)));
                        packagePriceTxt.setPaintFlags(packagePriceTxt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        packagePriceTxt.setText(products.get(0).priceText);
                        priceListing=products;
                    }

                    @Override
                    public void onSkuDetailsError(String error) {
                        Log.d("dataDetailsErr", error);

                    }
                });

            }

            @Override
            public void onPurchaseHistoryRestored() {
       //         Toast.makeText(SubscriptionActivity.this, "onPurchaseHistoryRestored", Toast.LENGTH_SHORT).show();
//                showToast("onPurchaseHistoryRestored");
         //       Log.d("SubscriptionData", "Owned Subscription: " + bp.listOwnedSubscriptions());
//                updateTextViews();
            }
        });


        iniIds();



    }


    private void iniIds() {
        packagePriceTxt = findViewById(R.id.textView_package_price);
        packageDurationTxt = findViewById(R.id.textView_package_duration);
        tvFreeThreeMonths = findViewById(R.id.tvFreeThreeMonths);
        tvFree = findViewById(R.id.tvFree);
        packageTypeImg = findViewById(R.id.imageView_package_type);

        btBronze = findViewById(R.id.button_bronze);
        btSilver = findViewById(R.id.button_silver);
        btGold = findViewById(R.id.button_gold);
        btBuySubsciption = findViewById(R.id.button_buy_now);

        btBronze.setOnClickListener(this);
        btSilver.setOnClickListener(this);
        btGold.setOnClickListener(this);
        btBuySubsciption.setOnClickListener(this);
        subBackPress.setOnClickListener(this);

     //   packagePriceTxt.setText(String.format("%s is%s subscribed", selectedSubscription, bp.isSubscribed(selectedSubscription) ? "" : " not"));

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        if (!readyToPurchase) {
            Toast.makeText(this, "Please wait Billing is Initializing", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (v.getId()) {
            case R.id.button_bronze:


                packagePriceTxt.setPaintFlags(packagePriceTxt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                tvFree.setVisibility(View.VISIBLE);
                tvFreeThreeMonths.setVisibility(View.VISIBLE);
                packageDurationTxt.setText("Free Trial (3 months)");

                packageTypeImg.setImageResource(R.drawable.bronze);
                selectedSubscription = KEY_BRONZE;
                packagePriceTxt.setText(priceListing.get(0).priceText);

                break;

            case R.id.button_silver:

                packagePriceTxt.setPaintFlags(0);

                tvFree.setVisibility(View.GONE);
                tvFreeThreeMonths.setVisibility(View.GONE);
                packageDurationTxt.setText("6 months");

                packageTypeImg.setImageResource(R.drawable.silver);

                selectedSubscription = KEY_SILVER;
                packagePriceTxt.setText( priceListing.get(2).priceText);

                break;

            case R.id.button_gold:
                packagePriceTxt.setPaintFlags(0);

                tvFree.setVisibility(View.GONE);
                tvFreeThreeMonths.setVisibility(View.GONE);
                packageDurationTxt.setText("1 year");

                packageTypeImg.setImageResource(R.drawable.gold);
                selectedSubscription = KEY_GOLD;
                packagePriceTxt.setText( priceListing.get(1).priceText);

                break;

            case R.id.button_buy_now:
                bp.subscribe(this, selectedSubscription);
                break;

            case R.id.subBackPress:
                finish();
                break;



        }
    }
}