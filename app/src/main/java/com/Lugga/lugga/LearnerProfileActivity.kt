package com.Lugga.lugga

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaRecorder
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.adapters.AllLanguageAdapter
import com.Lugga.lugga.interfaces.LuggaAPI
import com.Lugga.lugga.model.alllanguage.AllLanguagModel
import com.Lugga.lugga.model.alllanguage.AllLanguageResposne
import com.Lugga.lugga.model.learnerprofile.LearnerprofileResponse
import com.Lugga.lugga.model.learnerupdateprofile.LearnerUpdateResponse
import com.Lugga.lugga.model.passwordchange.PasswordChangeResponse
import com.Lugga.lugga.model.profilechange.ProfileChangeResponse
import com.Lugga.lugga.sharedpreferences.BaseApplication
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.ApiClient
import com.Lugga.lugga.views.FilePath
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.*

class LearnerProfileActivity : AppCompatActivity() {
    private var learningGoals: String?=""
    var changeNumber: TextView? = null
    var changeName: TextView? = null
    var changePassword: TextView? = null
    var taptochangename: TextView? = null
    var taptochangenumber: TextView? = null
    var taptochangepassword: TextView? = null
    var headerName: TextView? = null
    var headerEmail: TextView? = null
    var learnerLanguage: TextView? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var luggaAPI: LuggaAPI? = null
    var personGirlProfile: CircleImageView? = null
    var oldpassword: EditText? = null
    var newpassword: EditText? = null
    var confirmnewpassword: EditText? = null
    var number: EditText? = null
    var learnerProfileBackPress: ImageView? = null
    var changeProfile: ImageView? = null
    var spinnerLanguage: Spinner? = null
    var arraySpinner: MutableList<String>? = null
    var adapter: ArrayAdapter<String>? = null
    var imageFile: File? = null
    private var allLanguageAdapter: AllLanguageAdapter? = null
    private var selectedLanguage: String? = null
    private var spinnerTouched = false
    private var language: String? = null
    private var llChangeLearningGoals: LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.learner_profile)
        init()
        onClick()
        learnerProfileDeatil
        //spinnerItems();
        getlanguages()
        spinnerLanguage!!.setOnTouchListener { v, event ->
            spinnerTouched = true
            false
        }
        spinnerLanguage!!.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                if (spinnerTouched) {
                    selectedLanguage =
                        (allLanguageAdapter!!.getItem(position) as AllLanguagModel).language
                    learnerUpdateLanguage()
                }
                spinnerTouched = false
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun getlanguages() {
        ApiClient.getApiClient().create(LuggaAPI::class.java).getlanguages()
            .enqueue(object : Callback<AllLanguageResposne?> {
                override fun onResponse(
                    call: Call<AllLanguageResposne?>,
                    response: Response<AllLanguageResposne?>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        allLanguageAdapter = AllLanguageAdapter(
                            this@LearnerProfileActivity,
                            response.body()!!.body!!
                        )
                        spinnerLanguage!!.adapter = allLanguageAdapter
                    } else {
                    }
                }

                override fun onFailure(call: Call<AllLanguageResposne?>, t: Throwable) {}
            })
    }

    fun init() {
        personGirlProfile = findViewById(R.id.learnerpersonProfile)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        if (usersharedPrefernce!!.getprofileimage() != null) {
            Glide.with(this@LearnerProfileActivity)
                .load(ApiClient.IMAGE_URL + usersharedPrefernce!!.getprofileimage())
                .placeholder(R.drawable.person_girl)
                .centerCrop().into(personGirlProfile!!)
        }
        // loader = new Loader(this);
        luggaAPI = ApiClient.getApiClient().create(LuggaAPI::class.java)
        changeNumber = findViewById(R.id.learnernumber)
        changeName = findViewById(R.id.learnername)
        changePassword = findViewById(R.id.learnerchangepassword)
        taptochangename = findViewById(R.id.learnertapToChangeName)
        taptochangenumber = findViewById(R.id.learnertapToChangeNumber)
        taptochangepassword = findViewById(R.id.learnertapToChangePassword)
        headerName = findViewById(R.id.learnerheaderName)
        headerEmail = findViewById(R.id.learnerheaderEmail)
        learnerProfileBackPress = findViewById(R.id.learnerProfileBackPress)
        changeProfile = findViewById(R.id.learnerchangeProfile)
        spinnerLanguage = findViewById(R.id.spinnerlanguage)
        learnerLanguage = findViewById(R.id.learnerLanguage)
        llChangeLearningGoals = findViewById(R.id.llChangeLearningGoals)
    }

    fun onClick() {
        changeProfile!!.setOnClickListener { requestStoragePermission() }

        llChangeLearningGoals!!.setOnClickListener {
            val mDialog = Dialog(this)
            mDialog.setContentView(R.layout.change_learning_goals)
            mDialog.show()
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            changeNumber!!.text = usersharedPrefernce!!.getmobileno()
            val changenumbersubmit = mDialog.findViewById<TextView>(R.id.changeNumberSubmit)
            number = mDialog.findViewById(R.id.etLearningGoals)
            changenumbersubmit.setOnClickListener(View.OnClickListener {

                if (number!!.text.toString().isEmpty()) {
                    Toast.makeText(
                        this@LearnerProfileActivity,
                        "Enter Learning Goals",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@OnClickListener
                }

                learningGoals=number!!.text.toString().trim()
                mDialog.dismiss()
                learnerUpdateProfile
            })
            number?.setText(learningGoals.toString())


            val changeNumberDismiss = mDialog.findViewById<ImageView>(R.id.changeNumberDismiss)
            changeNumberDismiss.setOnClickListener { mDialog.dismiss() }
        }
        taptochangenumber!!.setOnClickListener(View.OnClickListener { view ->
            val mDialog = Dialog(view.context)
            mDialog.setContentView(R.layout.change_number)
            mDialog.show()
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            changeNumber!!.text = usersharedPrefernce!!.getmobileno()
            val changenumbersubmit = mDialog.findViewById<TextView>(R.id.changeNumberSubmit)
            number = mDialog.findViewById(R.id.mdialognumber)
            changenumbersubmit.setOnClickListener(View.OnClickListener {
                if (number!!.getText().toString().isEmpty()) {
                    Toast.makeText(this@LearnerProfileActivity, "Enter number", Toast.LENGTH_SHORT)
                        .show()
                    return@OnClickListener
                }
                if (number!!.getText().toString().length < 7 || number!!.getText().toString()
                        .trim { it <= ' ' }.length > 12
                ) {
                    Toast.makeText(
                        this@LearnerProfileActivity,
                        "Enter valid number",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@OnClickListener
                }
                usersharedPrefernce!!.setmobileno(number!!.getText().toString())
                mDialog.dismiss()
                changeNumber!!.text = usersharedPrefernce!!.getmobileno()
                learnerUpdateProfile
            })
            val changeNumberDismiss = mDialog.findViewById<ImageView>(R.id.changeNumberDismiss)
            changeNumberDismiss.setOnClickListener { mDialog.dismiss() }
        })
        taptochangepassword!!.setOnClickListener { view ->
            val mDialog = Dialog(view.context)
            mDialog.setContentView(R.layout.change_password)
            mDialog.show()
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val changePasswordDismiss = mDialog.findViewById<ImageView>(R.id.changePasswordDismiss)
            oldpassword = mDialog.findViewById(R.id.oldPassword)
            newpassword = mDialog.findViewById(R.id.newPassword)
            confirmnewpassword = mDialog.findViewById(R.id.confirmNewPassword)
            val passwordsubmit = mDialog.findViewById<TextView>(R.id.passwordSubmitButton)
            passwordsubmit.setOnClickListener {
                if (newpassword!!.getText().toString() != confirmnewpassword!!.getText()
                        .toString()
                ) {
                    confirmnewpassword!!.setError("password mismatch")
                } else {
                    passwordChange
                    mDialog.dismiss()
                }
            }
            changePasswordDismiss.setOnClickListener { mDialog.dismiss() }
        }
        taptochangename!!.setOnClickListener(View.OnClickListener { view ->
            val mDialog = Dialog(view.context)
            mDialog.setContentView(R.layout.change_name)
            mDialog.show()
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            changeName!!.text = usersharedPrefernce!!.getname()
            val mdilogname = mDialog.findViewById<EditText>(R.id.mdialogname)
            val changenamesubmitbutton = mDialog.findViewById<TextView>(R.id.changenamesubmit)
            changenamesubmitbutton.setOnClickListener(View.OnClickListener {
                if (mdilogname.text.toString().isEmpty()) {
                    Toast.makeText(this@LearnerProfileActivity, "Enter Name", Toast.LENGTH_SHORT)
                        .show()
                    return@OnClickListener
                }
                if (mdilogname.text.toString().length < 2) {
                    Toast.makeText(
                        this@LearnerProfileActivity,
                        "Enter valid Name",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@OnClickListener
                }
                if (mdilogname.text.toString().contains(".")) {
                    Toast.makeText(
                        this@LearnerProfileActivity,
                        "Enter valid Name",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@OnClickListener
                }


                //    usersharedPrefernce.setname(mdilogname.getText().toString());
                mDialog.dismiss()
                headerName!!.text = mdilogname.text.toString()
                usersharedPrefernce!!.setname(mdilogname.text.toString())
                changeName!!.text = mdilogname.text.toString()
                learnerUpdateProfile
            })
            val changeNameDismiss = mDialog.findViewById<ImageView>(R.id.changeNameDismiss)
            changeNameDismiss.setOnClickListener { mDialog.dismiss() }
        })
        personGirlProfile!!.setOnClickListener { }
        learnerProfileBackPress!!.setOnClickListener {
            val i = Intent(this@LearnerProfileActivity, HomeActivity::class.java)
            startActivity(i)
        }
    }

    private fun selectedLanguageee() {
        if (language != null && !language!!.isEmpty()) {
            learnerLanguage!!.text = "Selected Language : $language"
            selectedLanguage = language

        }
    }

    private fun learnerUpdateLanguage() {
        val user_id: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            usersharedPrefernce!!.getuserid().toString()
        )
        val goals: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            learningGoals.toString()
        )
        val mobileno: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            changeNumber!!.text.toString()
        )
        val Language: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedLanguage!!)
        val name: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), changeName!!.text.toString())
        luggaAPI!!.getlearnerupdateprofile(goals ,user_id, mobileno, Language, name)
            .enqueue(object : Callback<LearnerUpdateResponse?> {
                override fun onResponse(
                    call: Call<LearnerUpdateResponse?>,
                    response: Response<LearnerUpdateResponse?>
                ) {
                    if (response.body() != null) {
                        if (response.body()!!.success == 200) {
                            language = response.body()!!.body!![0].language
                            selectedLanguageee()
                            Toast.makeText(
                                this@LearnerProfileActivity,
                                "Language Updated Successfully",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            // loader.isDismiss();
                            // Toast.makeText(LearnerProfile.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                override fun onFailure(call: Call<LearnerUpdateResponse?>, t: Throwable) {
                    // loader.isDismiss();
                    // Toast.makeText(LearnerProfile.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            })
    }

    private val learnerProfileDeatil: Unit
        private get() {
            val user_id: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getuserid().toString()
            )
            luggaAPI!!.getlearnerprofile(user_id)
                .enqueue(object : Callback<LearnerprofileResponse?> {
                    override fun onResponse(
                        call: Call<LearnerprofileResponse?>,
                        response: Response<LearnerprofileResponse?>
                    ) {
                        if (response.body() != null) {
                            if (response.isSuccessful && response.body()!!.success == 200) {
                                Log.d("dataList", Gson().toJson(response.body()))
                                usersharedPrefernce!!.setemail(response.body()!!.body!![0].email)
                                usersharedPrefernce!!.setmobileno(response.body()!!.body!![0].mobileNo)
                                usersharedPrefernce!!.setlanguage(response.body()!!.body!![0].language)
                                changeName!!.text = usersharedPrefernce!!.getname()
                                changeNumber!!.text = usersharedPrefernce!!.getmobileno()
                                headerEmail!!.text = usersharedPrefernce!!.getemail()
                                headerName!!.text = usersharedPrefernce!!.getname()

                                if (response.body()?.body?.get(0)?.learner_description != null && response.body()?.body?.get(
                                        0
                                    )?.learner_description != "0"
                                ) {
                                learningGoals=response.body()?.body?.get(0)?.learner_description
                                }
                                //    learnerLanguage.setText(usersharedPrefernce.getlanguage());
                                Glide.with(this@LearnerProfileActivity)
                                    .load(ApiClient.IMAGE_URL + response.body()!!.body!![0].profileImage)
                                    .centerCrop().into(personGirlProfile!!)
                                usersharedPrefernce!!.setprofileimage(response.body()!!.body!![0].profileImage.toString())
                                if (response.body()!!.body!![0].language != null && !response.body()!!.body!![0].language!!.isEmpty()) {
                                    //  setData(response.body().getBody().get(0).getLanguage());
                                    language = response.body()!!.body!![0].language
                                    selectedLanguageee()
                                }
                                //         Toast.makeText(LearnerProfile.this, "" + response.body(), Toast.LENGTH_SHORT).show();
                            } else {
                                // loader.isDismiss();
                                Toast.makeText(
                                    this@LearnerProfileActivity,
                                    "" + response.body(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<LearnerprofileResponse?>, t: Throwable) {
                        //loader.isDismiss();
                        Toast.makeText(this@LearnerProfileActivity, t.message, Toast.LENGTH_SHORT)
                            .show()
                    }
                })
        }// loader.isDismiss();//  loader.isDismiss();// loader.isDismiss();

    //loader.isShowProgress();
    private val passwordChange: Unit
        private get() {
            //loader.isShowProgress();
            val user_id: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getuserid().toString()
            )
            val oldPassword: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                oldpassword!!.text.toString()
            )
            val newPassword: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                newpassword!!.text.toString()
            )
            val confirmPassword: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                confirmnewpassword!!.text.toString()
            )
            luggaAPI!!.passwordchange(user_id, oldPassword, newPassword, confirmPassword)
                .enqueue(object : Callback<PasswordChangeResponse?> {
                    override fun onResponse(
                        call: Call<PasswordChangeResponse?>,
                        response: Response<PasswordChangeResponse?>
                    ) {
                        if (response.body() != null) {
                            if (response.body()!!.success == 200) {
                                // loader.isDismiss();
                                Toast.makeText(
                                    this@LearnerProfileActivity,
                                    "" + response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                //  loader.isDismiss();
                                Toast.makeText(
                                    this@LearnerProfileActivity,
                                    "failed" + response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<PasswordChangeResponse?>, t: Throwable) {

                        // loader.isDismiss();
                        Toast.makeText(this@LearnerProfileActivity, t.message, Toast.LENGTH_SHORT)
                            .show()
                    }
                })
        }
    private val learnerUpdateProfile: Unit
        private get() {
            //loader.isShowProgress();
            val user_id: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                usersharedPrefernce!!.getuserid().toString()
            )
            val goals: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                learningGoals.toString()
            )
            val mobileno: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                changeNumber!!.text.toString()
            )
            val language: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedLanguage!!)
            val name: RequestBody = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                changeName!!.text.toString()
            )
            luggaAPI!!.getlearnerupdateprofile(goals,user_id, mobileno, language, name)
                .enqueue(object : Callback<LearnerUpdateResponse?> {
                    override fun onResponse(
                        call: Call<LearnerUpdateResponse?>,
                        response: Response<LearnerUpdateResponse?>
                    ) {
                        if (response.body() != null) {
                            if (response.body()!!.success == 200) {
                                //    loader.isDismiss();
                                // Toast.makeText(LearnerProfile.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                // loader.isDismiss();
                                // Toast.makeText(LearnerProfile.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    override fun onFailure(call: Call<LearnerUpdateResponse?>, t: Throwable) {
                        // loader.isDismiss();
                        // Toast.makeText(LearnerProfile.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
        }

    private fun setData(language: String) {
        if (language.equals("English", ignoreCase = true)) {
            arraySpinner!![0] = "English"
            arraySpinner!![1] = "Hindi"
            arraySpinner!![2] = "French"
        } else if (language.equals("Hindi", ignoreCase = true)) {
            arraySpinner!![0] = "Hindi"
            arraySpinner!![1] = "English"
            arraySpinner!![2] = "French"
        } else if (language.equals("French", ignoreCase = true)) {
            arraySpinner!![0] = "French"
            arraySpinner!![1] = "English"
            arraySpinner!![2] = "Hindi"
        }
        spinnerLanguage!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun getProfileChange(selectedFile: File) {
        val requestFile: RequestBody =
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedFile)
        val body: MultipartBody.Part =
            MultipartBody.Part.createFormData("profileImage", selectedFile.name, requestFile)
        val user_id: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            usersharedPrefernce!!.getuserid().toString()
        )
        luggaAPI!!.getprofilechange(user_id, body)
            .enqueue(object : Callback<ProfileChangeResponse?> {
                override fun onResponse(
                    call: Call<ProfileChangeResponse?>,
                    response: Response<ProfileChangeResponse?>
                ) {
                    if (response.body() != null) {
                        if (response.body()!!.success == 200) {
                            usersharedPrefernce!!.setprofileimage(response.body()!!.body!![0].profileImage)
                            Glide.with(this@LearnerProfileActivity)
                                .load(ApiClient.IMAGE_URL + usersharedPrefernce!!.getprofileimage())
                                .placeholder(R.drawable.person_girl)
                                .centerCrop().into(personGirlProfile!!)
                            Toast.makeText(
                                this@LearnerProfileActivity,
                                "successfull" + usersharedPrefernce!!.getuserid(),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(
                                this@LearnerProfileActivity,
                                "unsuccessfull" + usersharedPrefernce!!.getuserid(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(call: Call<ProfileChangeResponse?>, t: Throwable) {
                    //loader.isDismiss();
                    Toast.makeText(this@LearnerProfileActivity, t.message, Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    private fun requestStoragePermission() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showPictureDialog()
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Toast.makeText(baseContext, "Error occurred! ", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()
    }

    fun showPictureDialog() {
        val items = arrayOf<CharSequence>(
            "Take Photo", "Choose from Library",
            "Cancel"
        )
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            val userChoosenTask: String
            if (items[item] == "Take Photo") {
                userChoosenTask = "Take Photo"
                cameraIntent()
            } else if (items[item] == "Choose from Library") {
                userChoosenTask = "Choose from Library"
                galleryIntent()
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT //
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY)
    }

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, MediaRecorder.VideoSource.CAMERA)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            return
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                val selectedfile = FilePath.getPath(this, contentURI)
                val bitmap = BitmapFactory.decodeFile(selectedfile)
                saveImage(bitmap)
                personGirlProfile!!.post {
                    val bmp = BitmapFactory.decodeFile(selectedfile)
                    if (selectedfile != null) {
                        getProfileChange(File(selectedfile))
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                    personGirlProfile!!.setImageBitmap(bmp)
                }

            }
        } else if (requestCode == MediaRecorder.VideoSource.CAMERA) {
            val thumbnail = data!!.extras!!["data"] as Bitmap?
            saveImage(thumbnail)
            val tempUri = getImageUri(this, thumbnail)
            val selectedfile = FilePath.getPath(this, tempUri)
            val myfile = File(tempUri.path)
            if (selectedfile != null) {
                getProfileChange(File(selectedfile))
            }
            personGirlProfile!!.setImageBitmap(thumbnail)
        }
    }

    private fun saveImage(bitmap: Bitmap?): String {
        val bytes = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 60, bytes)
        val wallpaperDirectory = File(
            Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY
        )
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            imageFile = File(
                wallpaperDirectory, Calendar.getInstance().timeInMillis.toString() + ".jpg"
            )
            imageFile!!.createNewFile()
            val fo = File(imageFile.toString())
            MediaScannerConnection.scanFile(
                BaseApplication.getContext(), arrayOf(imageFile!!.path), arrayOf("image/jpeg"), null
            )
            return imageFile!!.absolutePath
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return ""
    }

    private fun getImageUri(applicationContext: Context, photo: Bitmap?): Uri {
        val bytes = ByteArrayOutputStream()
        photo!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(this.contentResolver, photo, "Title", null)
        return Uri.parse(path)
    }


    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    companion object {
        private const val GALLERY = 7

        //   Loader loader;
        private const val IMAGE_DIRECTORY = "/Lugga"
    }
}