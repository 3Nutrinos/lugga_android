package com.Lugga.lugga

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_sign_up_sign_in.*

class SignUpSignInActivity : AppCompatActivity(), View.OnClickListener {
    private var from: String?=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_sign_in)

        from=intent.getStringExtra("from")
        tvLearnerSignIn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v){
            tvLearnerSignIn->{
                if(from=="teacher") {
                    startActivity(Intent(this, TeacherNewSignUpActivity::class.java))
                }
                else{
                    startActivity(Intent(this, LearnerNewSignupActivity::class.java))

                }
            }
        }
    }
}