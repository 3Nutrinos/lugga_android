package com.Lugga.lugga

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.adapters.ChatAdapter
import com.Lugga.lugga.adapters.ChatAdapter.OnChat
import com.Lugga.lugga.model.chatList.ChatListBodyItem
import com.Lugga.lugga.model.chatList.ChatListResponse
import com.Lugga.lugga.newchatcall.ChatShowActivity
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.SocketNetworking
import com.google.gson.Gson
import io.socket.client.Ack
import io.socket.client.Socket
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ChatListActivity : AppCompatActivity(), OnChat, View.OnClickListener {
    var rvChatList: RecyclerView? = null
    var socket: Socket? = null
    var usersharedPrefernce: UsersharedPrefernce? = null
    var chatListBodyItemArrayList: ArrayList<ChatListBodyItem>? = null
    var ivBack: ImageView? = null
    var tvNoChat: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)
        rvChatList = findViewById(R.id.rvChatList)
        ivBack = findViewById(R.id.ivBack)
        tvNoChat = findViewById(R.id.tvNoChat)
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket!!.connect()
        chatList
        ivBack!!.setOnClickListener(this)
    }

    private val chatList: Unit
        private get() {
            if (socket!!.connected()) {
                try {
                    list
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                socket!!.on(Socket.EVENT_CONNECT) {
                    if (socket!!.connected()) {
                        try {
                            list
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    } else {
                    }
                }
                socket!!.connect()
            }
        }

    @get:Throws(JSONException::class)
    private val list: Unit
        private get() {
            val obj = JSONObject()
            if (usersharedPrefernce!!.getusertype() == 1) {
                obj.put("user_id", usersharedPrefernce!!.getteacherid())
                obj.put("user_type", "1")
            } else {
                obj.put("user_type", "2")
                obj.put("user_id", usersharedPrefernce!!.getlearnerid())
            }
            try {
                if (socket!!.connected()) {
                    Log.e("sentDta", obj.toString())
                    socket!!.send(obj)
                    socket!!.emit("Chat_Listing", obj, Ack { })
                    chatListingReturn()
                } else {
                    socket!!.connect()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    private fun chatListingReturn() {
        if (socket!!.connected()) {
            if (usersharedPrefernce!!.getusertype() == 1) {
                socket!!.on("returnChat_Listing" + usersharedPrefernce!!.getteacherid()) { args ->
                    val msgresponse = args[0].toString()
                    val gson = Gson()
                    val chatListResponse = gson.fromJson(msgresponse, ChatListResponse::class.java)
                    chatListBodyItemArrayList = chatListResponse.body
                    runOnUiThread { setdata() }
                }
            } else {
                socket!!.on("returnChat_Listing" + usersharedPrefernce!!.getlearnerid()) { args ->
                    val msgresponse = args[0].toString()
                    val gson = Gson()
                    val chatListResponse = gson.fromJson(msgresponse, ChatListResponse::class.java)
                    chatListBodyItemArrayList = chatListResponse.body
                    runOnUiThread { setdata() }
                }
            }
        } else {
            Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
            socket!!.connect()
        }
    }

    private fun setdata() {
        if (chatListBodyItemArrayList!!.size == 0) {
            tvNoChat!!.visibility = View.VISIBLE
        } else {
            tvNoChat!!.visibility = View.GONE
            val chatAdapter = ChatAdapter(this@ChatListActivity, chatListBodyItemArrayList!!, this)
            rvChatList!!.layoutManager = LinearLayoutManager(this)
            rvChatList!!.adapter = chatAdapter
        }
    }


    override fun onClick(v: View) {
        onBackPressed()
    }

    override fun onClickChat(bookingId: Int?, name: String?) {
        val intent = Intent(this@ChatListActivity, ChatShowActivity::class.java)
        intent.putExtra("bookingId", bookingId.toString())
        intent.putExtra("name", name)
        startActivity(intent)
    }
}